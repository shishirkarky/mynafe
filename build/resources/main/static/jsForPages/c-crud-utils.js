import {CAuthentication} from "./c-authentication.js";
import {CToastNotification} from "./c-toastNotification.js";
import {triggerOnError} from "./c-utils.js";

export class CrudUtils {
    sendPostRequest(url, data) {
        this.sendRequest(url, data, 'POST');
    }

    sendPutRequest(url, data) {
        this.sendRequest(url, data, 'PUT');
    }

    sendDeleteRequest(url) {
        this.sendRequest(url, null, 'DELETE');
    }

    sendMultiPartPostRequest(url, data) {
        this.sendMultiPartRequest(url, data, 'POST');
    }

    sendRequest(url, data, methodType) {
        let authentication = new CAuthentication();
        if (confirm('Are you sure?')) {
            let req = new Request(url, {
                method: methodType,
                mode: 'cors',
                headers: authentication.getHeadersWithToken(),
                body: data
            });
            fetch(req)
                .then(resp => resp.json())
                .then(responseData => {
                    const {message: msg, status} = responseData;
                    if (status === 200) {
                        new CToastNotification()
                            .getSuccessToastNotification(msg);
                    } else if (status === 500) {
                        new CToastNotification().getFailureToastNotification("Something went wrong.");
                    } else {
                        new CToastNotification().getFailureToastNotification(msg);
                    }
                })
                .catch(err => {
                    console.error(err.message);
                    triggerOnError(err);
                });
        }
    }

    static async fetchResource(url) {
        let req = new Request(url, {
            method: 'GET',
            mode: 'cors',
            headers: new CAuthentication().getHeadersWithToken()
        });

        return await fetch(req)
            .then(response => {
                if (response.ok) return response.json()
            })
            .then(json => {
                return json;
            })
            .catch(err => {
                console.error(`Error: ${err.message}`);
            });
    }

    static async fetchExternalResource(url) {
        let req = new Request(url, {
            method: 'GET',
            mode: 'cors'
        });

        return await fetch(req)
            .then(response => {
                if (response.ok) return response.json()
            })
            .then(json => {
                return json;
            })
            .catch(err => {
                console.error(`Error: ${err.message}`);
            });
    }

    sendMultiPartRequest(url, data, methodType) {
        let authentication = new CAuthentication();
        if (confirm('Are you sure?')) {
            let req = new Request(url, {
                method: methodType,
                mode: 'cors',
                enctype: "multipart/form-data",
                headers: authentication.getMultiPartHeadersWithToken(),
                body: data,
                contentType: false
            });
            fetch(req)
                .then(resp => resp.json())
                .then(responseData => {
                    const {message: msg, status} = responseData;
                    if (status === 200) {
                        new CToastNotification()
                            .getSuccessToastNotification(msg);
                    } else if (status === 500) {
                        new CToastNotification().getFailureToastNotification("Something went wrong.");
                    } else {
                        new CToastNotification().getFailureToastNotification(msg);
                    }
                })
                .catch(err => {
                    console.error(err.message);
                    triggerOnError(err);
                });
        }
    }
}