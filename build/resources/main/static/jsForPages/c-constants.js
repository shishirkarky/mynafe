export const TOKEN_KEY = 'PEEPAL_SOFT_AUTH_TOKEN';
export const BASE_URL = "http://localhost:8082";
//Resource APIs
export const API_OAUTH_GET = `${BASE_URL}/api/v1/oauth/token`;
export const API_LOGOUT = `${BASE_URL}/api/v1/logout`;
//ACCOUNT
export const API_ACCOUNTS_SAVE = `${BASE_URL}/api/v1/accounts`;
export const API_ACCOUNTS_UPDATE = `${BASE_URL}/api/v1/accounts/`;
export const API_ACCOUNTS_DELETE = `${BASE_URL}/api/v1/accounts/`;
export const API_ACCOUNTS_GET = `${BASE_URL}/api/v1/accounts`;
export const API_ACCOUNTS_GET_ONE = `${BASE_URL}/api/v1/accounts/`;
//COMPANY
export const API_COMPANY_SAVE = `${BASE_URL}/api/v1/companies`;
export const API_COMPANY_UPDATE = `${BASE_URL}/api/v1/companies/`;
export const API_COMPANY_DELETE = `${BASE_URL}/api/v1/companies/`;
export const API_COMPANY_GET = `${BASE_URL}/api/v1/companies`;
export const API_COMPANY_GET_ONE = `${BASE_URL}/api/v1/companies/`;
//DEPO
export const API_DEPO_SAVE = `${BASE_URL}/api/v1/depos`;
export const API_DEPO_UPDATE = `${BASE_URL}/api/v1/depos/`;
export let API_DEPO_DELETE = `${BASE_URL}/api/v1/depos/`;
export const API_DEPO_GET_ALL = `${BASE_URL}/api/v1/depos`;
export const API_DEPO_GET_ONE = `${BASE_URL}/api/v1/depos/`;
//USER
export const API_USER_SAVE = `${BASE_URL}/api/v1/users`;
export const API_USER_UPDATE = `${BASE_URL}/api/v1/users/`;
export const API_USER_GET = `${BASE_URL}/api/v1/users`;
export const API_USER_GET_ONE = `${BASE_URL}/api/v1/users/`;
//ROLES
export const API_ROLES_GET = `${BASE_URL}/api/v1/roles`;

//JSP pages
export const UI_LOGIN_PAGE = `${BASE_URL}/v1/login-page`;
export const UI_DASHBOARD_PAGE = `${BASE_URL}/v1/dashboard-page`;
export const UI_COMPANY_CREATE_PAGE = `${BASE_URL}/v1/company-create-page`;
export const UI_COMPANY_VIEW_PAGE = `${BASE_URL}/v1/company-view-page`;
export const UI_ACCOUNT_CREATE_PAGE = `${BASE_URL}/v1/account-create-page`;
export const UI_ACCOUNT_VIEW_PAGE = `${BASE_URL}/v1/account-view-page`;
export const UI_DEPO_CREATE_PAGE = `${BASE_URL}/v1/depo-create-page`;
export const UI_DEPO_VIEW_PAGE = `${BASE_URL}/v1/depo-view-page`;
export const UI_USER_CREATE_PAGE = `${BASE_URL}/v1/user-create-page`;
export const UI_STATEMENT_SEARCH_PAGE = `${BASE_URL}/v1/statement-search-page`;