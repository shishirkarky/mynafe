import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveUserBtn').addEventListener('click', sendUserSaveReq);
    document.getElementById('updateUserRolesBtn').addEventListener('click', updateUserRoles);
    setStaffsInChoices().then(r => console.log("Staffs loaded successfully."));
    setRolesChoices().then(r => console.log("Roles choices filled."));
    loadUsers().then(r => console.log("Users loaded successfully."));
});

let sendUserSaveReq = (ev) => {
    let crudUtils = new CrudUtils();
    let password = $("[name='password']").val();
    let rePass = $("[name='repass']").val();
    if (password !== rePass) {
        new CToastNotification()
            .getFailureToastNotification("Password Mismatch!");
    } else if ($("[name='staffId']").val() === '0') {
        new CToastNotification().getFailureToastNotification("Please select staff");
    } else {

        crudUtils.sendPostRequest(Constants.API_USER_SAVE, getUserFormData());
    }
};

let updateUserRoles = (ev) => {
    let username = $("[name='editBlockUsername']").val();
    new CrudUtils().sendPutRequest(`${Constants.API_USER_UPDATE}${username}`, getEditRolesFormData());
};

let getUserFormData = () => {
    //get multiple values from roles select option
    let roles_list = $("[name='roles']").val();
    let array = [];

    //convert roles selected values to json array object
    $(roles_list).each(function (index, element) {
        let data = {"id": element};
        array.push(data);
    });

    return JSON.stringify({
        "username": $("[name='username']").val(),
        "email": $("[name='email']").val(),
        "password": $("[name='password']").val(),
        "staffs": {
            "id": $("[name='staffId']").val()
        },
        ['roles']: array
    });
}

let getEditRolesFormData = () => {
    //get multiple values from roles select option
    let roles_list = $("[name='editBlockRoles']").val();
    let array = [];

    //convert roles selected values to json array object
    $(roles_list).each(function (index, element) {
        let data = {"id": element};
        array.push(data);
    });

    return JSON.stringify({
        ['roles']: array
    });
}
let setStaffsInChoices = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_STAFFS_GET_ALL);
    let staffs = resource.data;
    staffs.forEach(function (staff, index) {
        const {firstName, lastName, id} = staff;
        let option = document.createElement("option");

        option.text = firstName + " " + lastName;
        option.value = id;

        let select = document.getElementById("staffId");
        select.appendChild(option);
    });
}
let setRolesChoices = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_ROLES_GET);
    let roles = resource.data;
    $("#roles").empty();
    roles.forEach(function (role, index) {
        const {name, id} = role;
        let option1 = document.createElement("option");
        option1.text = name;
        option1.value = id;

        let option2 = document.createElement("option");
        option2.text = name;
        option2.value = id;

        let editBlockRole = document.getElementById("editBlockRoles");
        let select = document.getElementById("roles");

        editBlockRole.appendChild(option1);
        select.appendChild(option2);
    })
}

let editRoles = async (username) => {
    let url = Constants.API_USER_GET_ONE + username;
    $("[name='editBlockUsername']").val(username);
    const result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {roles} = result.data;
        let roleIds = [];
        roles.forEach(function (role, index) {
            roleIds.push(role.id);
        });
        $("#editBlockRoles").val(roleIds).attr("selected", "selected").change();
    } else {
        new CToastNotification().getFailureToastNotification("Roles not found.");
    }

}

window.editRoles = editRoles;

let loadUsers = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_USER_GET);
    $('#userTable').DataTable({
        "data": resource.data,
        "columns": [
            {data: "username"},
            {data: "staffs.branchCode"},
            {data: "staffs.firstName"},
            {data: "staffs.middleName"},
            {data: "staffs.lastName"},
            {data: "status"},
            {
                "data": "Action",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                    let username = "'" + row.username + "'";
                    let a = '<a type="button" class="btn btn-info btn-sm" onclick="editRoles(' + username + ')"><i class="fa fa-edit" aria-hidden="true"></i></a>'
                    return a;
                },

            }
        ],
        "destroy": true
    });
}


let deactivateUser = (username) => {
    new CrudUtils().sendPutRequest(Constants.API_USER_DEACTIVATE.replace("{username}", username));
    setTimeout(() => {
        loadUsers().then(r => console.log("Users loaded successfully."));
    }, 1000);

}

let activateUser = (username) => {
    new CrudUtils().sendPutRequest(Constants.API_USER_ACTIVATE.replace("{username}", username));
    setTimeout(() => {
        loadUsers().then(r => console.log("Users loaded successfully."));
    }, 1000);
}

window.editRoles = editRoles;
window.activateUser= activateUser;
window.deactivateUser = deactivateUser;


