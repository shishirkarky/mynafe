import * as Constants from "./c-constants.js";
import * as Utils from "./c-utils.js";

export class CAuthentication {
    constructor() {
    }

    getHeaders() {
        let h = new Headers();
        h.append('Content-Type', 'application/json; charset=utf-8');
        h.append('Access-Control-Allow-Origin', '*');
        h.append('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        h.append('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,Cache-Control');
        h.append('Access-Control-Max-Age', '3600');
        h.append('Access-Control-Allow-Credentials', 'true');
        return h;
    }

    getHeadersWithToken() {
        let token = JSON.parse(sessionStorage.getItem(Constants.TOKEN_KEY));
        let headers = this.getHeaders();
        headers.append('Authorization', `Bearer ${token}`);

        return headers;
    }

    getMultiPartHeaders() {
        let h = new Headers();
        h.append('Access-Control-Allow-Origin', '*');
        h.append('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        h.append('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,Cache-Control');
        h.append('Access-Control-Max-Age', '3600');
        h.append('Access-Control-Allow-Credentials', 'true');
        return h;
    }
    getMultiPartHeadersWithToken() {
        let token = JSON.parse(sessionStorage.getItem(Constants.TOKEN_KEY));
        let headers = this.getMultiPartHeaders();
        headers.append('Authorization', `Bearer ${token}`);

        return headers;
    }

    sendAuthenticationRequest(url, authData) {
        let req = new Request(url, {
            method: 'POST',
            mode: 'cors',
            headers: this.getHeaders(),
            body: authData
        });
        fetch(req)
            .then(resp => resp.json())
            .then(data => {
                if (Utils.isEmptyJson(data)) {
                    $("#message").html("Invalid username/password.");
                } else {
                    this.setTokenToSession(data.token);
                    window.location = Constants.UI_DASHBOARD_PAGE;
                }
            })
            .catch(err => {
                console.error(err.message);
            });
    }

    setTokenToSession(token) {
        sessionStorage.setItem(Constants.TOKEN_KEY,
            JSON.stringify(
                token
            ));
    }

    getCurrentToken(){
        return JSON.parse(sessionStorage.getItem(Constants.TOKEN_KEY));
    }

}