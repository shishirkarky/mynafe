import * as Constants from './c-constants.js';
import * as Util from './c-utils.js';
import {CAuthentication} from "./c-authentication.js";

document.addEventListener('DOMContentLoaded', () => {
    checkCurrentAuthToken();
    document.getElementById('dashboardPageBtn').addEventListener('click', loadDashboardPage);
    document.getElementById('userCreatePageBtn').addEventListener('click', loadUserCreatePage);

    document.getElementById('driverCreatePageBtn').addEventListener('click', loadDriverCreatePage);
    document.getElementById('driverViewPageBtn').addEventListener('click', loadDriverViewPage);

    document.getElementById('vehicleCreatePageBtn').addEventListener('click', loadVehicleCreatePage);
    document.getElementById('vehicleViewPageBtn').addEventListener('click', loadVehicleViewPage);

    document.getElementById('customerCreatePageBtn').addEventListener('click', loadCustomerCreatePage);
    document.getElementById('customerViewPageBtn').addEventListener('click', loadCustomerViewPage);

    document.getElementById('gatepassCreatePageBtn').addEventListener('click', loadGatePassCreatePage);

    document.getElementById('vendorCreatePageBtn').addEventListener('click', loadVendorCreatePage);
});

let loadLoginPage = () => Util.loadPage(Constants.UI_LOGIN_PAGE);
let loadDashboardPage = () => Util.loadPage(Constants.UI_DASHBOARD_PAGE);
let loadUserCreatePage = () => Util.loadPage(Constants.UI_USER_CREATE_PAGE);

let loadDriverCreatePage = () => Util.loadPage(Constants.UI_DRIVER_CREATE_PAGE);
let loadDriverViewPage = () => Util.loadPage(Constants.UI_DRIVER_VIEW_PAGE);

let loadVehicleCreatePage = () => Util.loadPage(Constants.UI_VEHICLE_CREATE_PAGE);
let loadVehicleViewPage = () => Util.loadPage(Constants.UI_VEHICLE_VIEW_PAGE);

let loadCustomerCreatePage = () => Util.loadPage(Constants.UI_CUSTOMER_CREATE_PAGE);
let loadCustomerViewPage = () => Util.loadPage(Constants.UI_CUSTOMER_VIEW_PAGE);

let loadGatePassCreatePage = () => Util.loadPage(Constants.UI_GATE_PASS_CREATE_PAGE);

let loadVendorCreatePage = () => Util.loadPage(Constants.UI_VENDOR_CREATE_PAGE);


let checkCurrentAuthToken = () => {
    if (null == new CAuthentication().getCurrentToken()) {
        loadLoginPage();
    }
}
