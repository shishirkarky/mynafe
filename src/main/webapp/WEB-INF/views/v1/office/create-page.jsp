<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Office Details</h2>
            </header>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="officeCode">Office Code</label>
                            <input type="text" class="form-control" id="officeCode" name="officeCode">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="officeRepresentativeName">Office Representative
                                Name</label>
                            <input type="text" class="form-control" id="officeRepresentativeName"
                                   name="officeRepresentativeName">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refLocationId">Location </label>
                            <select data-plugin-selectTwo class="form-control populate" id="refLocationId"
                                    name="refLocationId">
                                <option value="">Select Location</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="facility"> Facility </label>
                            <input type="text" class="form-control" id="facility" name="facility">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="phoneNumber">Phone Number</label>
                            <input type="text" class="form-control" id="phoneNumber" name="phoneNumber">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email">

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="category">Category </label>
                            <input type="text" class="form-control" id="category" name="category">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="type">Type </label>
                            <select class="form-control" id="type" name="type">
                                <option value="HEAD_OFFICE">Head Office</option>
                                <option value="BRANCH_OFFICE">Branch Office</option>
                                <option value="WARE_HOUSE">Ware House</option>
                            </select>
                        </div>
                    </div>

                </div>

                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveOfficeBtn" class="btn btn-primary" onclick="saveOffice()">Save
                                Office
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>
<%---------------------------------------------------OFFICE VIEW AND EDIT--------------------------------------%>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Office Details</h2>
            </header>
            <div class="panel-body">
                <table class="table table-striped nowrap" style="width:100%"
                       id="officeDataTable">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Office Code</th>
                        <th>Type</th>
                        <th>Office Representative Name</th>
                        <th>Location</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Category</th>
                        <th>Facility</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>

<!-- Modal for Update -->
<div id="editOfficeModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE OFFICE</h4>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="eofficeRepresentativeName">Office Representative
                                Name</label>
                            <input type="text" class="form-control" id="eofficeRepresentativeName"
                                   name="eofficeRepresentativeName">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="erefLocationId">Location </label>
                            <select data-plugin-selectTwo class="form-control populate" id="erefLocationId"
                                    name="erefLocationId">
                                <option value="">Select Location</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ephoneNumber">Phone Number</label>
                            <input type="text" class="form-control" id="ephoneNumber" name="ephoneNumber">

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="efacility"> Facility </label>
                            <input type="text" class="form-control" id="efacility" name="efacility">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="eemail">Email</label>
                            <input type="text" class="form-control" id="eemail" name="eemail">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ecategory">Category </label>
                            <input type="text" class="form-control" id="ecategory" name="ecategory">
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="etype">Type </label>
                            <select class="form-control" id="etype" name="etype">
                                <option value="HEAD_OFFICE">Head Office</option>
                                <option value="BRANCH_OFFICE">Branch Office</option>
                                <option value="WARE_HOUSE">Ware House</option>
                            </select>
                        </div>
                    </div>
                </div>


            </div>
            <%--            Form update button--%>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" id="updateOfficeBtn" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>

                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-office-create.js"></script>