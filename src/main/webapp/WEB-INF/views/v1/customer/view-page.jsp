<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#popular" data-toggle="tab"><i
                        class="fa fa-star"></i> Customer</a></li>
            </ul>

            <div class="tab-content">
                <!-- Display account details -->
                <div id="popular" class="tab-pane active">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Customer Details</h2>
                        </header>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped mb-none"
                                   id="customerDataTable">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>F. Name</th>
                                    <th>M. Name</th>
                                    <th>L. Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>PAN No,</th>
                                    <th>Receiver Name</th>
                                    <th>Signature</th>
                                    <th>Email</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal for Update -->
<div id="editCustomerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE CUSTOMER</h4>
            </div>

<%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="firstName"> First Name</label>
                            <input type="text" class="form-control" id="firstName" name="firstName">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="middleName">Middle Name</label>
                            <input type="text" class="form-control" id="middleName" name="middleName">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="lastName">Last Name</label>
                            <input type="text" class="form-control" id="lastName" name="lastName">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="address"> Address </label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="phone">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="panNumber">Pan Number</label>
                            <input type="text" class="form-control" id="panNumber"
                                   name="panNumber">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="receiverName"> Receiver Name </label>
                            <input type="text" class="form-control" id="receiverName" name="receiverName">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="signature">Signature</label>
                            <input type="text" class="form-control" id="signature" name="signature">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email">

                        </div>
                    </div>
                </div>
            </div>
<%--            Form update button--%>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
<%--                            <a type="button" id="deleteCustomerBtn" class="btn btn-danger"><i--%>
<%--                                    class="fa fa-trash-o"></i></a>--%>
                            <button type="button" id="updateCustomerBtn" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>

                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-customer-view.js"></script>