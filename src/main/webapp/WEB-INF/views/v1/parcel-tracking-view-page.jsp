<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html class="fixed sidebar-left-collapsed">
<head>
  <title>${pagetitle }</title>
  <tags:meta/>
  <tags:noAuthStyles/>
  <style>
    span {
      font-weight: bolder;
    }

    h4, caption {
      color: red;
    }
  </style>
  <style>
    .card {
      /* Add shadows to create the "card" effect */
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
      transition: 0.3s;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
    }

    /* Add some padding inside the card container */
    .container {
      padding: 2px 16px;
    }

    span {
      font-weight: bold;
    }
  </style>
</head>
<body>
<section class="body">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
          </div>

          <h2 class="panel-title">PARCEL TRACKING</h2>
        </header>
        <div class="panel-body">

          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="dn">DN Number</label>
                <input type="text" class="form-control" id="dn" name="dn">

              </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="customerBillNumber">Customer Ref. Invoice Number</label>
                <input type="text" class="form-control" id="customerBillNumber"
                       name="customerBillNumber">
              </div>
            </div>

          </div>

          <br/>
          <footer class="panel-footer">
            <div class="row">
              <div class="col-sm-9 ">
                <button type="button" id="saveOfficeBtn" class="btn btn-primary" onclick="searchParcel()">Search
                </button>
              </div>
            </div>
          </footer>
        </div>
      </section>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          <div class="panel-actions">
          </div>

          <h2 class="panel-title">PARCEL DETAILS</h2>
        </header>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-4">
              <div class="card">
                <div class="container">
                  <h4>Booking</h4>
                  <p>Booking Code: <span id="res-bookingCode"></span></p>
                  <p>Dispatched Date: <span id="res-dispatchedDate"></span></p>
                  <p>From: <span id="res-fromLocation"></span></p>
                  <p>To: <span id="res-toLocation"></span></p>
                  <p>Dispatched Branch: <span id="res-dispatchedBranch"></span></p>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="container">
                  <h4>POD</h4>
                  <p>DN : <span id="res-dn"></span></p>
                  <p>Consignor Name: <span id="res-consignerName"></span></p>
                  <p>Cus Ref. Invoice No: <span id="res-customerBillNumber"></span></p>
                  <p>Consignee Name: <span id="res-consigneeName"></span></p>
                  <p>Consignee Contact No: <span id="res-consigneeContact"></span></p>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="container">
                  <h4>Driver</h4>
                  <p>Driver Name: <span id="res-driverName"></span></p>
                  <p>Driver Contact No: <span id="res-driverContactNumber"></span></p>
                </div>
              </div>
            </div>
          </div>
          <br>
          <div class="row">

            <div class="col-md-4">
              <div class="card">
                <div class="container">
                  <h4>Vehicle</h4>
                  <p>Vehicle Number: <span id="res-vehicleNumber"></span></p>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <span class="highlight pull-right" id="res-status">STATUS</span>
                <div class="container">
                  <h4>Receiver</h4>
                  <p>Name: <span id="res-receiverName"></span></p>
                  <p>Contact: <span id="res-receiverContact"></span></p>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
              <table class="table table-bordered table-striped mb-none"
                     id="dataTable">
                <caption>ITEMS IN POD</caption>
                <thead>
                <tr>
                  <th>Description of good</th>
                  <th>Quantity</th>
                  <th>Package</th>
                </tr>
                </thead>
              </table>
            </div>
            <div class="col-md-1"></div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
              <table class="table table-bordered table-striped mb-none"
                     id="transitionsDataTable">
                <caption>TRANSITIONS</caption>
                <thead>
                <tr>
                  <th>Transit branch</th>
                  <th>Transit date time</th>
                  <th>Transit assigned date time</th>
                  <th>Booking Code</th>
                  <th>Driver name</th>
                  <th>Vehicle number</th>
                  <th>Driver contact number</th>
                </tr>
                </thead>
              </table>
            </div>
            <div class="col-md-1"></div>

          </div>
        </div>
      </section>
    </div>
  </div>
</section>
<tags:errorResponse/>
<tags:noAuthJs/>
<ui:loadingModal/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-parcel-tracking.js"></script>
</body>
</html>
