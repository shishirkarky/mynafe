<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>

                <h2 class="panel-title">Available PODs</h2>
            </header>
            <div class="panel-body">
                <table class="table table-striped nowrap" style="width:100%"
                       id="podDataTable">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Dispatched On</th>
                        <th>DN</th>
                        <th>Booking Type</th>
                        <th>Booking Code</th>
                        <th>Consignor Name</th>
                        <th>Driver Name</th>
                        <th>Vehicle No</th>
                        <th>Consignee Name</th>
                        <th>Customer Ref. Invoice No.</th>
                        <th>Contact No</th>
                        <th>From</th>
                        <th>To</th>
                        <th>No. of pkg</th>
                        <th>Item Name</th>
                        <th>Status</th>
                        <th>Status Update Date</th>
                        <th>Transit</th>
                        <th>New Booking Code</th>
                        <th>New Dispatch Date</th>
                        <th>New Vehicle No</th>
                        <th>New Driver Name</th>
                        <th>Transit Location</th>
                        <th>Created By</th>
                        <th>Last Updated By</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>
<!-- Modal for POD Status Change -->
<div id="podStatusChangeModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE STATUS</h4>
                <hr>
                <span id="podStatusModalDN" style="color: red"></span>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Update Details</h2>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="podStatus">Status </label>
                                    <select data-plugin-selectTwo class="form-control populate" id="podStatus"
                                            name="podStatus">
                                        <option value="ACTIVE">ACTIVE</option>
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="UNDELIVERED">UNDELIVERED</option>
                                        <option value="CANCEL">CANCEL</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="podStatusRemarks">Remarks</label>
                                    <input type="text" class="form-control" id="podStatusRemarks"
                                           name="podStatusRemarks">
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="receiverName">Receiver Name</label>
                                    <input type="text" class="form-control" id="receiverName" name="receiverName">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="receiverContact">Receiver Contact</label>
                                    <input type="text" class="form-control" id="receiverContact" name="receiverContact">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="podStatusUpdateDate">Date
                                        <i class="fa  fa-info-circle" data-toggle="tooltip"
                                           data-placement="top"
                                           title="If not entered, sets today's date"></i></label>
                                    <input type="text" class="form-control" id="podStatusUpdateDate" name="podStatusUpdateDate">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                                <button type="button" id="updatePodStatusBtn" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                        </div>
                    </div>
                </section>
                <div class="row">
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                <h2 class="panel-title">Available documents</h2>
                            </header>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped mb-none"
                                       id="podDocumentsDataTable">
                                    <thead>
                                    <tr>
                                        <th>Doc. type</th>
                                        <th>Download</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                <h2 class="panel-title">Documents upload</h2>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="file" id="podFileBytes" name="podFileBytes"
                                               class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" id="uploadPodDocumentBtn"
                                                class="btn btn-primary">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">

                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <%--            Form update button--%>
            <div class="modal-footer">
                <footer class="panel-footer">

                </footer>
            </div>
        </div>

    </div>
</div>

<!-- Modal for Multiple POD Status Change -->
<div id="multiplePodStatusChangeModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Update Details</h2>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="mpodStatus">Status </label>
                                    <select data-plugin-selectTwo class="form-control populate" id="mpodStatus"
                                            name="mpodStatus">
                                        <option value="ACTIVE">ACTIVE</option>
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="UNDELIVERED">UNDELIVERED</option>
                                        <option value="CANCEL">CANCEL</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="mpodStatusRemarks">Remarks</label>
                                    <input type="text" class="form-control" id="mpodStatusRemarks"
                                           name="mpodStatusRemarks">
                                </div>
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="mreceiverName">Receiver Name</label>
                                    <input type="text" class="form-control" id="mreceiverName" name="mreceiverName">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="mreceiverContact">Receiver Contact</label>
                                    <input type="text" class="form-control" id="mreceiverContact" name="mreceiverContact">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="mpodStatusUpdateDate">Date
                                        <i class="fa  fa-info-circle" data-toggle="tooltip"
                                           data-placement="top"
                                           title="If not entered, sets today's date"></i></label>
                                    <input type="text" class="form-control" id="mpodStatusUpdateDate" name="mpodStatusUpdateDate">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <button type="button" id="mupdatePodStatusBtn" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<ui:loadingModal/>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-cashreceipt-view.js"></script>
