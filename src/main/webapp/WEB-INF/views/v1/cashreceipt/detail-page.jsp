<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <input type="hidden" id="id" value="${id}">
                <button type="button" onclick="editPodPage(${id})"
                        class="btn btn-primary"><i class="fa fa-edit"
                                                   aria-hidden="true"></i>
                    Edit
                </button>

                <button type="button" onclick="printReport(${id})"
                        class="btn btn-success "><i class="fa fa-download"
                                                 aria-hidden="true"></i>
                </button>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <iframe src="${pageContext.request.contextPath}/impl/api/v1/reports/pdf/pods/${id}"
                                 height="1000" width="100%"></iframe>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Available documents</h2>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none"
                       id="podDocumentsDataTable">
                    <thead>
                    <tr>
                        <th>Doc. type</th>
                        <th>Download</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
    <div class="col-md-4">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Documents upload</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="file" id="podFileBytes" name="podFileBytes"
                               class="form-control">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" id="uploadPodDocumentBtn" onclick="uploadPodDocument(${id})"
                                class="btn btn-primary">
                            Upload
                        </button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-cashreceipt-detail-view.js"></script>