<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="refVendorRequestId">Select Booking Code</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refVendorRequestId"
                                    name="refVendorRequestId">
                                <option value="">Select Booking Code</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="form-group">

                            <label class="control-label" for="vendorInvoiceNumber">Vendor Invoice No.</label>
                            <input type="text" class="form-control" id="vendorInvoiceNumber" name="vendorInvoiceNumber">

                        </div>

                    </div>

                </div>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="consignorName">Consignor Name</label>
                            <input type="text" class="form-control" id="consignorName" name="consignorName">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="consigneeName">Consignee Name</label>
                            <input type="text" class="form-control" id="consigneeName" name="consigneeName">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="customerBillNumber">Customer Bill Number</label>
                            <input type="text" class="form-control" id="customerBillNumber" name="customerBillNumber">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="date">Date</label>
                            <input type="text" class="form-control date-picker" data-single="true" id="date" name="date"
                                   placeholder="Select Date"
                                   onblur="nepaliToEnglish('#date','#dateEn')">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="dateEn">Date(A.D.)</label>
                            <input type="text" data-plugin-datepicker data-date-format="yyyy-mm-dd"
                                   data-plugin-skin="dark" class="form-control" id="dateEn" name="dateEn"
                                   placeholder="Select Date"
                                   onblur="englishToNepali('#dateEn','#date')">

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refFromLocationId">From</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refFromLocationId"
                                    name="refFromLocationId">
                                <option value="">Select From</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refToLocationId"> To</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refToLocationId"
                                    name="refToLocationId">
                                <option value="">Select To</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="receivedDate">Received Date</label>
                            <input type="text" class="form-control date-picker" data-single="true" id="receivedDate"
                                   name="receivedDate" placeholder="Select Date"
                                   onblur="nepaliToEnglish('#receivedDate','#receivedDateEn')">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="receivedDateEn">Received Date(A.D.)</label>
                            <input type="text" data-plugin-datepicker data-date-format="yyyy-mm-dd"
                                   data-plugin-skin="dark" class="form-control " id="receivedDateEn"
                                   name="receivedDateEn" placeholder="Select Date"
                                   onblur="englishToNepali('#receivedDateEn','#receivedDate')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="receiverName">Receiver Name</label>
                            <input type="text" class="form-control" id="receiverName" name="receiverName">

                        </div>
                    </div>
                </div>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="fa fa-caret-down"></a>
                        </div>

                        <h4 class="panel-title">Items Details</h4>
                    </header>
                    <div class="panel-body cashReceiptDetailsDiv" id="cashReceiptDetailsDiv">
                        <div class="row clonePodDiv">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="cashReceiptDetailsDescription">
                                        Description </label>
                                    <select class="form-control cashReceiptDetailsDescription"
                                            id="cashReceiptDetailsDescription"
                                            name="cashReceiptDetailsDescription">
                                        <option value="">Select Item</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="quantity">Quantity</label>
                                    <input type="text" class="form-control" id="quantity" name="quantity">

                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="taxableAmount">Taxable Amt</label>
                                    <input type="text" class="form-control" id="taxableAmount" name="taxableAmount">

                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="value">Value</label>
                                    <input type="text" class="form-control" id="value" name="value">

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveCashReceiptBtn" class="btn btn-primary">Save
                            </button>
                            <%--                            <button type="button" id="saveAndPrintCashReceiptBtn" class="btn btn-primary">Save + Print--%>
                            <%--                            </button>--%>
                            <button type="button" id="addCashReceiptDetailsBtn" class="btn btn-success">+ Add</button>
                            <button type="button" id="removeCashReceiptDetailsBtn" class="btn btn-danger">+ Remove
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-cashreceipt-create.js"></script>
