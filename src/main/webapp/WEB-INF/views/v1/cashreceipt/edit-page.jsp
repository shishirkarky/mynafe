<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="podForm">
                            <input value="${id}" type="hidden" id="id">
                            <section class="panel" id="createPodSection">
                                <header class="panel-heading">
                                    <div class="panel-actions">
                                    </div>

                                    <h2 class="panel-title">POD Details - <em><span id="dn"></span></em></h2>
                                </header>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label" for="consignorName">Consignor
                                                    Name</label>
                                                <select data-plugin-selectTwo class="form-control populate"
                                                        id="consignorName"
                                                        name="consignorName"
                                                        onchange="consignorNameOnChangeAction()">
                                                    <option value="">Select Consignor</option>
                                                </select>
                                            </div>
                                        </div>
                                        <%--                                                VENDOR VAT NUMBER--%>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" for="consignorVat">Consignor's
                                                    VAT</label>
                                                <input type="text" class="form-control"
                                                       id="consignorVat"
                                                       name="consignorVat">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label"
                                                       for="customerBillNumber">Consignee
                                                    Invoice Number</label>
                                                <input type="text" class="form-control"
                                                       id="customerBillNumber"
                                                       name="customerBillNumber">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label" for="consigneeName">Consignee
                                                    Name</label>
                                                <input type="text" class="form-control"
                                                       id="consigneeName"
                                                       name="consigneeName">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label" for="consigneeVat">Consignee
                                                    VAT</label>
                                                <input type="text" class="form-control"
                                                       id="consigneeVat"
                                                       name="consigneeVat">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label"
                                                       for="refFromLocationId">From</label>
                                                <select data-plugin-selectTwo
                                                        class="form-control populate"
                                                        id="refFromLocationId"
                                                        name="refFromLocationId">
                                                    <option value="">Select From</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="control-label" for="refToLocationId">
                                                To (Delivery Location)</label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="refToLocationId"
                                                    name="refToLocationId">
                                                <option value="">Select Delivery Location</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label" for="consigneeContact">Consignee
                                                    Contact</label>
                                                <input type="text" class="form-control"
                                                       id="consigneeContact"
                                                       name="consigneeContact">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="control-label" for="customerBillDate">Customer
                                                    Bill Date</label>
                                                <input type="text" class="form-control date-picker" data-single="true"
                                                       id="customerBillDate"
                                                       name="customerBillDate">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="panel">
                                <header class="panel-heading">
                                    <div class="panel-actions">
                                        <a href="#" class="fa fa-caret-down"></a>
                                    </div>

                                    <h4 class="panel-title">Items Details</h4>
                                </header>
                                <div class="panel-body cashReceiptDetailsDiv"
                                     id="cashReceiptDetailsDiv">
                                    <div class="clonePodDiv">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="cashReceiptDetailsDescription">
                                                        Description </label>
                                                    <select class="form-control cashReceiptDetailsDescription"
                                                            id="cashReceiptDetailsDescription"
                                                            name="cashReceiptDetailsDescription">
                                                        <option value="">Select Item</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-1">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="quantity">Quantity</label>
                                                    <input type="number" step="any" class="form-control"
                                                           id="quantity"
                                                           name="quantity">

                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="unitOfMeasurement">Measurement unit</label>
                                                    <select class="form-control"
                                                            id="unitOfMeasurement"
                                                            name="unitOfMeasurement">
                                                        <option value="QUANTITY" selected>
                                                            Piece
                                                        </option>
                                                        <option value="PACKAGE">
                                                            Carton
                                                        </option>
                                                    </select>


                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="noOfPackage">No of pkg</label>
                                                    <input type="number" step="any" class="form-control"
                                                           id="noOfPackage"
                                                           name="noOfPackage">

                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="taxableAmount">Taxable
                                                        Amt</label>
                                                    <input type="number" step="any" class="form-control"
                                                           id="taxableAmount" name="taxableAmount">
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label class="control-label"
                                                           for="remarks">Remarks</label>
                                                    <input type="text" class="form-control"
                                                           id="remarks"
                                                           name="remarks">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <footer class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-9 ">

                                            <button type="button" id="addCashReceiptDetailsBtn"
                                                    class="btn btn-success"
                                                    onclick="cloneCashReceiptDetailReq()"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="ADD new item"><i class="fa fa-plus-circle"></i>
                                            </button>
                                            <button type="button" id="removeCashReceiptDetailsBtn"
                                                    class="btn btn-danger"
                                                    onclick="removeCashReceiptDetailReq()"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="DELETE last item"><i
                                                    class="fa fa-minus-circle"></i>
                                            </button>

                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="button" id="saveCashReceiptBtn"
                        class="btn btn-success pull-left"
                        onclick="sendCashReceiptUpdateReq()"> <i class="fa fa-save"></i> Update POD
                </button>
            </div>
        </section>
    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-cashreceipt-edit.js"></script>
