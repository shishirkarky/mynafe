<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
  <div class="col-md-12">
    <section class="panel">
      <header class="panel-heading">
        <div class="panel-actions">
        </div>

        <h2 class="panel-title">PODs with files</h2>
      </header>
      <div class="panel-body">
        <table class="table table-striped nowrap" style="width:100%"
               id="podDataTable">
          <thead>
          <tr>
            <th></th>
            <th>Date</th>
            <th>Office</th>
            <th>Booking Code</th>
            <th>DN</th>
            <th>Consignor Name</th>
            <th>Consignee Name</th>
            <th>Customer Ref. Invoice No.</th>
            <th>Driver Name</th>
            <th>POD Photo</th>
            <th>Remarks</th>
          </tr>
          </thead>
        </table>
      </div>
    </section>
  </div>
</div>
<ui:loadingModal/>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-cashreceipt-uploads-view.js"></script>
