<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#popular" data-toggle="tab"><i
                        class="fa fa-star"></i> File Uploads</a></li>
            </ul>

            <div class="tab-content">
                <!-- Display vendor details -->
                <div id="popular" class="tab-pane active">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">View Files Uploads</h2>
                        </header>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped mb-none"
                                   id="fileUploadDataTable">
                                <thead>
                                <tr>
                                    <th>File Type</th>
                                    <th>File Sub-type</th>
                                    <th>Ref. ID</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-fileuploads-view.js"></script>