<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#driver" data-toggle="tab"><i
                class="fa fa-users"></i> Driver</a></li>
        <li><a href="#documents" data-toggle="tab" onclick="documentTabClickAction()"><i
                class="fa fa-file-excel-o"></i> Documents</a></li>
    </ul>

    <div class="tab-content">
        <div id="driver" class="tab-pane active">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>
                            <h2 class="panel-title">Enter Driver Details</h2>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="refDriverId">Driver (Opt.)
                                            <i class="fa  fa-info-circle" data-toggle="tooltip"
                                               data-placement="top"
                                               title="Optional field. Selected will update, empty will create driver."></i>
                                        </label>
                                        <select data-plugin-selectTwo class="form-control populate" id="refDriverId"
                                                name="refDriverId" onchange="loadDriverById()">
                                            <option value="">Select Driver</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="firstName"> First Name</label>
                                        <input type="text" class="form-control" id="firstName" name="firstName">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="middleName">Middle Name</label>
                                        <input type="text" class="form-control" id="middleName" name="middleName">

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="lastName">Last Name</label>
                                        <input type="text" class="form-control" id="lastName" name="lastName">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="address"> Address </label>
                                        <input type="text" class="form-control" id="address" name="address">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="phone">Phone Number</label>
                                        <input type="text" class="form-control" id="phone" name="phone">

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="licenseNumber">License Number</label>
                                        <input type="text" class="form-control" id="licenseNumber"
                                               name="licenseNumber">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="serviceType"> Service Type </label>
                                        <input type="text" class="form-control" id="serviceType" name="serviceType">
                                    </div>
                                </div>
                            </div>

                            <br/>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-sm-9 ">
                                    <button type="button" id="saveDriverBtn" onclick="saveDriver()"
                                            class="btn btn-success">
                                        <i class="fa fa-save"></i> Save / Update
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </section>
                </div>
            </div>
        </div>
        <div id="documents" class="tab-pane">
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Documents upload</h2>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <select class="form-control" id="driverDocumentSubType"
                                            name="driverDocumentSubType">
                                        <option value="LICENSE">LICENSE</option>
                                        <option value="CITIZENSHIP">CITIZENSHIP</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="file" id="driverFileBytes" name="driverFileBytes"
                                           class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <button type="button" id="uploadDriverDocumentBtn" onclick="uploadDriverDocument()"
                                            class="btn btn-success">
                                        <i class="fa fa-upload"></i> Upload
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Available documents</h2>
                        </header>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped mb-none"
                                   id="driverDocumentsDataTable">
                                <thead>
                                <tr>
                                    <th>Doc. type</th>
                                    <th>Download</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h2 class="panel-title">Driver Details</h2>
            </header>
            <div class="panel-body">
                <table class="table table-striped nowrap" style="width:100%"
                       id="driverDataTable">
                    <thead>
                    <tr>
                        <th>F. Name</th>
                        <th>M. Name</th>
                        <th>L. Name</th>
                        <th>License No.</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Service Type</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
</div>

<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-driver-create.js"></script>
