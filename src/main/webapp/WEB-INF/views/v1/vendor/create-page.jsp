<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Vendor Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="refMainVendorId"> Main Vendor</label>
                            <select data-plugin-selectTwo class="form-control populate"
                                    id="refMainVendorId"
                                    name="refMainVendorId">
                                <option value="0">Select Vendor</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="phone">Phone </label>
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="description"> Description </label>
                            <input type="text" class="form-control" id="description" name="vendorDescription">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="dispatchLocation">Dispatch Location</label>
                            <input type="text" class="form-control" id="dispatchLocation" name="dispatchLocation">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="deliveryMode">Delivery Mode</label>
                            <select data-plugin-selectTwo class="form-control populate" id="deliveryMode"
                                    name="deliveryMode">
                                <option>ONLINE</option>
                                <option>OFFLINE</option>

                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="businessType">Business Type</label>
                            <input type="text" class="form-control" id="businessType"
                                   name="businessType">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="panVat">PAN/VAT</label>
                            <input type="text" class="form-control" id="panVat"
                                   name="panVat">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="rateCalculationType">Rate Calculation Type</label>
                            <select class="form-control" id="rateCalculationType"
                                    name="rateCalculationType">
                                <option value="QUANTITY" selected>
                                    Piece
                                </option>
                                <option value="PACKAGE">
                                    Carton
                                </option>
                            </select>
                        </div>
                    </div>

                </div>

                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveVendorBtn" class="btn btn-success" onclick="saveVendor()">
                                <i class="fa fa-save"></i> Save Vendor
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>
<%---------------------------------------------------------VIEW AND EDIT----------------------------------------------------%>
<div class="row">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
            </div>

            <h2 class="panel-title">Vendor Details</h2>
        </header>
        <div class="panel-body">
            <table class="table table-striped nowrap" style="width:100%"
                   id="vendorDataTable">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Description</th>
                    <th>Dispatch Location</th>
                    <th>Delivery Mode</th>
                    <th>Business Type</th>
                    <th>PAN/VAT</th>
                    <th>Rate Calculation Type</th>
                </tr>
                </thead>
            </table>
        </div>
    </section>
</div>

<!-- Modal for Update -->
<div id="editVendorModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE VENDOR</h4>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="erefMainVendorId"> Main Vendor</label>
                            <select data-plugin-selectTwo class="form-control populate"
                                    id="erefMainVendorId"
                                    name="erefMainVendorId">
                                <option value="0">Select Vendor</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ename">Name</label>
                            <input type="text" class="form-control" id="ename" name="ename">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="eemail">Email</label>
                            <input type="text" class="form-control" id="eemail" name="eemail">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ephone">Phone </label>
                            <input type="text" class="form-control" id="ephone" name="ephone">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="edescription"> Description </label>
                            <input type="text" class="form-control" id="edescription" name="edescription">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="edispatchLocation">Disptach Location</label>
                            <input type="text" class="form-control" id="edispatchLocation" name="edispatchLocation">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="edeliveryMode">Delivery Mode</label>
                            <select data-plugin-selectTwo class="form-control populate" id="edeliveryMode"
                                    name="edeliveryMode">
                                <option>ONLINE</option>
                                <option>OFFLINE</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ebusinessType">Business Type</label>
                            <input type="text" class="form-control" id="ebusinessType"
                                   name="ebusinessType">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="epanVat">PAN/VAT</label>
                            <input type="text" class="form-control" id="epanVat"
                                   name="epanVat">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="erateCalculationType">Rate Calculation Type</label>
                            <select class="form-control" id="erateCalculationType"
                                    name="erateCalculationType">
                                <option value="QUANTITY" selected>
                                    Piece
                                </option>
                                <option value="PACKAGE">
                                    Carton
                                </option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" id="updateVendorBtn" class="btn btn-success"> <i class="fa fa-edit"></i> Update</button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-vendor-create.js"></script>

