<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<ui:header/>
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <div class="panel-actions">
          <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Create User</h2>
        <p>For Staff Or Driver</p>
      </header>
      <div class="panel-body">
        <form class="form-horizontal form-bordered" method="post" name="userform" id="userform">
          <div class="row">
            <div class="col-md-4">
              <label class="control-label" for="staffId">Staff</label>
              <select data-plugin-selectTwo class="form-control populate" id="staffId" name="staffId">
                <option value="0" selected>Select Staff</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">
              <label class="control-label" for="driverId">Driver</label>
              <select data-plugin-selectTwo class="form-control populate" id="driverId"
                      name="driverId">
                <option value="0">Select Driver</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <label class="control-label" for="username">Username</label>
              <input type="text" class="form-control" id="username" name="username">
            </div>
            <div class="col-md-3">
              <label class="control-label" for="roles">Roles</label>
              <select data-plugin-selectTwo class="form-control populate" id="roles" name="roles"
                      multiple>
                <option value="0">NONE</option>
              </select>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
              <label class="control-label" for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="col-md-3">
              <label class="control-label" for="repass">Confirm Password</label>
              <input type="password" class="form-control" id="repass" name="repass">
            </div>
            <div class="col-md-5">
              <label class="control-label" for="accessControl">Access Control</label>
              <select data-plugin-selectTwo class="form-control populate" id="accessControl"
                      name="accessControl" multiple>
                <optgroup label="Booking">
                  <option value="booking-pipeline">Booking Pipeline</option>
                  <option value="booking-view">Booking view</option>
                </optgroup>
                <optgroup label="Transitions">
                  <option value="transition-create">Transition Create</option>
                </optgroup>
                <optgroup label="Vehicle Log Book">
                  <option value="vehicle-log-book-create">Vehicle Log Book Create</option>
                  <option value="vehicle-log-book-view">Vehicle Log Book View</option>
                </optgroup>
                <optgroup label="POD">
                  <option value="pod-view">POD View</option>
                  <option value="pod-uploads-view">POD Uploads View</option>
                </optgroup>
                <optgroup label="Reports">
                  <option value="reports">Reports</option>
                </optgroup>
                <optgroup label="Driver">
                  <option value="driver-settings-page">Driver Settings</option>
                </optgroup>
                <optgroup label="Vehicle">
                  <option value="vehicle-settings-page">Vehicle Settings</option>
                </optgroup>
                <optgroup label="Customer">
                  <option value="customer-settings-page">Customer Settings</option>
                </optgroup>
              </select>
            </div>
          </div>

        </form>
      </div>
      <br/>
      <footer class="panel-footer">
        <div class="row">
          <div class="col-sm-9">
            <button type="button" id="saveUserBtn" onclick="sendUserSaveReq()" class="btn btn-success">
              <i class="fa fa-save"></i> Save
            </button>
          </div>
        </div>
      </footer>
      <hr>
    </section>
    <section class="panel">
      <header class="panel-heading">
        <div class="panel-actions">
        </div>

        <h2 class="panel-title">User Details</h2>
      </header>
      <div class="panel-body">
        <table class="table table-striped nowrap" style="width:100%"
               id="userTable">
          <thead>
          <tr>
            <th>Username</th>
            <th>User Type</th>
            <th>Branch Code</th>
            <th>Name</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
        </table>
      </div>
    </section>
  </div>
</div>
<!-- Modal for Update -->
<div id="editUserModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">UPDATE USER</h4>
      </div>

      <%--            View data form--%>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <label class="control-label" for="editBlockUsername">User Name</label>
            <input type="text" class="form-control" id="editBlockUsername" name="editBlockUsername"
                   disabled>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-8">
            <label class="control-label" for="editBlockRoles">Roles</label>
            <select data-plugin-selectTwo class="form-control populate roles" id="editBlockRoles"
                    name="editBlockRoles" multiple>
            </select>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-8">
            <label class="control-label" for="editAccessControl">Access Control</label>
            <select data-plugin-selectTwo class="form-control populate" id="editAccessControl"
                    name="editAccessControl" multiple>
              <optgroup label="Booking">
                <option value="booking-pipeline">Booking Pipeline</option>
                <option value="booking-view">Booking view</option>
              </optgroup>
              <optgroup label="Transitions">
                <option value="transition-create">Transition Create</option>
              </optgroup>
              <optgroup label="Vehicle Log Book">
                <option value="vehicle-log-book-create">Vehicle Log Book Create</option>
                <option value="vehicle-log-book-view">Vehicle Log Book View</option>
              </optgroup>
              <optgroup label="POD">
                <option value="pod-view">POD View</option>
                <option value="pod-uploads-view">POD Uploads</option>
              </optgroup>
              <optgroup label="Reports">
                <option value="reports">Reports</option>
              </optgroup>
              <optgroup label="Driver">
                <option value="driver-settings-page">Driver Settings</option>
              </optgroup>
              <optgroup label="Vehicle">
                <option value="vehicle-settings-page">Vehicle Settings</option>
              </optgroup>
              <optgroup label="Customer">
                <option value="customer-settings-page">Customer Settings</option>
              </optgroup>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="updateUserBtn" class="btn btn-success">
          <i class="fa fa-edit"></i> Update
        </button>
      </div>
    </div>
  </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-user-create.js"></script>
