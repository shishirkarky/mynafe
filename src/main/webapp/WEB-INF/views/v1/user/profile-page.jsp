<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-md-4 col-lg-3">

        <section class="panel">
            <div class="panel-body">
                        <img class="img-circle" src="" id="logoImage"
                             alt="logo" height="100" width="100">
                <hr class="dotted short">

                <h6 class="text-muted">TLS</h6>
                <p>Total Logistic Service Pvt. Ltd.</p>
                <hr class="dotted short">


            </div>
        </section>


    </div>
    <div class="col-md-8 col-lg-6">

        <div class="tabs">
            <ul class="nav nav-tabs tabs-primary">
                <li class="active"><a href="#overview" data-toggle="tab">Overview</a>
                </li>
                <li><a href="#edit" data-toggle="tab">Edit</a></li>
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane active">
                    <h3 class="mb-md">Basic Information</h3>

                    <section>
                        <table class="table table-hover">
                            <tr>
                                <th>Username</th>
                                <td id="username">
                                </td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td id="firstName"></td>
                            </tr>
                            <tr>
                                <th>Branch Code</th>
                                <td id="branchCode">
                                </td>
                            </tr>
                            <tr>
                                <th>Phone Number</th>
                                <td id="phoneNumber">
                                </td>
                            </tr>
                            <tr>
                                <th>Role</th>
                                <td id="roles">
                                </td>
                            </tr>

                        </table>

                    </section>

                </div>
                <div id="edit" class="tab-pane">

                    <!-- allow changing passwords here -->
                    <hr class="dotted tall">
                    <form class="form-horizontal" method="post" id="changePasswordForm">
                        <h4 class="mb-xlg">Change Password</h4>
                        <fieldset class="mb-xl">
                            <div class="form-group">
                                <label class="col-md-3 control-label"
                                       for="oldPassword">Old Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="oldPassword" class="form-control"
                                           id="oldPassword">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="newPassword">New
                                    Password</label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" name="newPassword"
                                           id="newPassword">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"
                                       for="newPasswordRepeat">Repeat New Password</label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" name="newPasswordRepeat"
                                           id="newPasswordRepeat">
                                </div>
                            </div>
                        </fieldset>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary" id="changePasswordSubmitBtn" >Submit</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-user-profile-view.js"></script>
