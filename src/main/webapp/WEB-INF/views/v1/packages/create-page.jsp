<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>

                <h2 class="panel-title">Enter Package Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refVendorId"> Vendor</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refVendorId"
                                    name="refVendorId">
                                <option value="0">Select Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="code"> Code</label>
                            <input type="text" class="form-control" id="code" name="code">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="brand"> Brand</label>
                            <input type="text" class="form-control" id="brand" name="brand">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="type"> Type</label>
                            <input type="text" class="form-control" id="type" name="type">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="name"> Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="description"> Description</label>
                            <input type="text" class="form-control" id="description" name="packageDescription">
                        </div>
                    </div>
                </div>
                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveConfigPackageBtn" class="btn btn-success"
                                    onclick="savePackage()"><i class="fa fa-save"></i> Save
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>
<%--------------------------------------------------VIEW AND EDIT---------------------------------------%>
<div class="row">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
            </div>

            <h2 class="panel-title">Packages Details</h2>
        </header>
        <div class="panel-body">
            <table class="table table-striped nowrap" style="width:100%"
                   id="packagesDataTable">
                <thead>
                <tr>
                    <th></th>
                    <th>Vendor</th>
                    <th>Code</th>
                    <th>Brand</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
            </table>
        </div>
    </section>
</div>

<!-- Modal for Update -->
<div id="editConfigPackagesModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE Packages</h4>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="erefVendorId"> Vendor</label>
                            <select data-plugin-selectTwo class="form-control populate" id="erefVendorId"
                                    name="erefVendorId">
                                <option value="0">Select Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ecode"> Code</label>
                            <input type="text" class="form-control" id="ecode" name="ecode">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ebrand"> Brand</label>
                            <input type="text" class="form-control" id="ebrand" name="ebrand">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="etype"> Type</label>
                            <input type="text" class="form-control" id="etype" name="etype">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ename"> Name</label>
                            <input type="text" class="form-control" id="ename" name="ename">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="edescription"> Description</label>
                            <input type="text" class="form-control" id="edescription" name="edescription">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" id="updateConfigPackagesBtn" class="btn btn-success"><i
                                    class="fa fa-edit"></i> Update
                            </button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-package-create.js"></script>
