<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#vehicle" data-toggle="tab"><i
                class="fa fa-users"></i> Vehicle </a></li>
        <li><a href="#documents" data-toggle="tab" onclick="documentTabClickAction()"><i
                class="fa fa-file-excel-o"></i> Documents</a></li>
    </ul>

    <div class="tab-content">
        <div id="vehicle" class="tab-pane active">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Enter Vehicle Details</h2>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="refVehicleId">Vehicle(Opt.)
                                            <i class="fa  fa-info-circle" data-toggle="tooltip"
                                               data-placement="top"
                                               title="Optional field. Selected will update, empty will create vehicle."></i>
                                        </label>
                                        <select data-plugin-selectTwo class="form-control populate"
                                                onchange="loadVehicleById()"
                                                id="refVehicleId"
                                                name="refVehicleId">
                                            <option value="">Select Vehicle</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="description"> Description</label>
                                        <input type="text" class="form-control" id="description"
                                               name="vehicleDescription">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="type">Type</label>
                                        <select type="text" class="form-control" id="type" name="type">
                                            <option value="PRIVATE">
                                                PRIVATE
                                            </option>
                                            <option value="COMPANY_OWN" selected>
                                                COMPANY OWN
                                            </option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="registrationNumber">Registration
                                            Number</label>
                                        <input type="text" class="form-control" id="registrationNumber"
                                               name="registrationNumber">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="ownerFirstName"> Owner F. Name </label>
                                        <input type="text" class="form-control" id="ownerFirstName"
                                               name="ownerFirstName">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="ownerMiddleName">Owner M. Name</label>
                                        <input type="text" class="form-control" id="ownerMiddleName"
                                               name="ownerMiddleName">

                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="ownerLastName">Owner L. Name</label>
                                        <input type="text" class="form-control" id="ownerLastName"
                                               name="ownerLastName">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="chasisNumber">Chasis Number</label>
                                        <input type="text" class="form-control" id="chasisNumber"
                                               name="chasisNumber">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="modelNumber"> Model Number </label>
                                        <input type="text" class="form-control" id="modelNumber" name="modelNumber">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="buildYear"> Build Year </label>
                                        <input type="text" class="form-control" id="buildYear" name="buildYear">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="color">Color</label>
                                        <input type="text" class="form-control" id="color"
                                               name="color">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="currentMileage"> Current Mileage </label>
                                        <input type="text" class="form-control" id="currentMileage"
                                               name="currentMileage">
                                    </div>
                                </div>
                            </div>

                            <br/>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-sm-9 ">
                                    <button type="button" id="saveVehicleBtn" class="btn btn-success"
                                            onclick="saveVehicle()"><i class="fa fa-save"></i> Save / Update
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </section>
                </div>
            </div>
        </div>
        <div id="documents" class="tab-pane">
            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Documents upload</h2>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <select class="form-control" id="vehicleDocumentSubType"
                                            name="vehicleDocumentSubType">
                                        <option value="">Select document type</option>
                                        <option value="BLUE_BOOK">BLUE BOOK/ROUTE PERMIT</option>
                                        <option value="OTHER">OTHER</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="file" id="vehicleFileBytes" name="vehicleFileBytes"
                                           class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <button type="button" id="uploadVehicleDocumentBtn" class="btn btn-success"
                                            onclick="uploadVehicleDocument()">
                                        <i class="fa fa-upload"></i> Upload
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Available documents</h2>
                        </header>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped mb-none"
                                   id="vehicleDocumentsDataTable">
                                <thead>
                                <tr>
                                    <th>Doc. type</th>
                                    <th>Download</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<%-------------------------------------VIEW--------------------------------------------------------------%>
<div class="row">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
            </div>
            <h2 class="panel-title">Vehicles Details</h2>
        </header>
        <div class="panel-body">
            <table class="table table-striped nowrap" style="width:100%"
                   id="vehicleDataTable">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Registration No</th>
                    <th>Owner F. Name</th>
                    <th>Owner M. Name</th>
                    <th>Owner L. Name</th>
                    <th>Chasis No</th>
                    <th>Model No</th>
                    <th>Cur. Mileage</th>
                    <th>Build Year</th>
                    <th>Color</th>
                    <th>Description</th>
                </tr>
                </thead>
            </table>
        </div>
    </section>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-vehicle-create.js"></script>
