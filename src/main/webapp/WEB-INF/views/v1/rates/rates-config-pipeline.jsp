<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
  <div class="col-xs-12">
    <section class="panel form-wizard" id="w4">
      <header class="panel-heading">
        <div class="panel-actions">
        </div>

        <h2 class="panel-title">Rate-config Pipe-line</h2>
        <hr>
        <span><a class="btn btn-danger"
                 onclick="clearRateConfigurationBySuperAdmin()"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> DELETE ALL</a></span>
      </header>
      <div class="panel-body">
        <div class="wizard-progress wizard-progress-lg">
          <div class="steps-progress">
            <div class="progress-indicator"></div>
          </div>
          <ul class="wizard-steps">
            <li id="bookingLi" class="active">
              <a href="#w4-filter" data-toggle="tab" id="filterTab"><span>1</span>Filter</a>
            </li>
            <li id="podLi">
              <a href="#w4-add-rate" data-toggle="tab" id="addRateCardTab"><span>2</span>Add Rate Card</a>
            </li>
            <li>
              <a href="#w4-rate-cards" data-toggle="tab" id="availableRateCardsTab" onclick="loadRates()"><span>3</span>Available
                Rate <br>Cards</a>
            </li>

          </ul>
        </div>
        <div class="tab-content">
          <div id="w4-filter" class="tab-pane active">
            <section class="panel">
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label" for="bookingType"> Booking
                        Type</label>
                      <select class="form-control" name="bookingType" id="bookingType"
                              required>
                        <option value="PARTIAL_BOOKING" selected>PARTIAL BOOKING</option>
                        <option value="FULL_BOOKING">FULL BOOKING</option>
                        <option value="HIRING_VEHICLE">HIRING VEHICLE</option>
                        <option value="LOCAL_DELIVERY">LOCAL DELIVERY</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label" for="refVendorId"> Vendor</label>
                      <select data-plugin-selectTwo class="form-control populate"
                              id="refVendorId"
                              name="refVendorId">
                        <option value="0">Select Vendor</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="control-label" for="refConfFromLocationId">Dispatch
                        Location</label>
                      <select data-plugin-selectTwo class="form-control populate"
                              id="refConfFromLocationId"
                              name="refConfFromLocationId">
                        <option value="">Select Dispatch Location</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-footer">
                <div class="pager pull-right">
                  <a class="btn btn-success"
                     onclick="filterNextBtnAction()" data-toggle="tooltip" data-placement="top"
                     title="GOTO NEXT TAB"> <i class="fa fa-forward"></i> <i
                    class="fa fa-forward"></i></a>
                </div>
              </div>
            </section>
          </div>
          <div id="w4-add-rate" class="tab-pane">
            <div class="row">
              <div class="col-md-8">
                <section class="panel-featured panel-featured-success">
                  <div class="panel-heading">
                    <h5>ADD RATE</h5>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-lg-12">
                        <form id="rateCardForm">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label class="control-label" for="refConfPackageId">
                                  Item</label>
                                <select data-plugin-selectTwo
                                        class="form-control populate"
                                        id="refConfPackageId"
                                        name="refConfPackageId">
                                  <option value="">Select Item</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label class="control-label" for="refConfToLocationId">
                                  To</label>
                                <select data-plugin-selectTwo
                                        class="form-control populate"
                                        id="refConfToLocationId"
                                        name="refConfToLocationId">
                                  <option value="">Select Location</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-sm-4">
                              <div class="form-group">
                                <label class="control-label" for="rate"> Rate</label>
                                <input type="text" class="form-control" id="rate"
                                       name="rate">
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="panel-footer">
                    <button type="button" id="saveRateConfigBtn"
                            class="btn btn-success"
                            onclick="saveRateConfigAction()"><i class="fa fa-save"></i> Add Rate
                      Config
                    </button>
                  </div>
                </section>
              </div>
              <div class="col-md-4">
                <section class="panel-featured panel-featured-info">
                  <div class="panel-heading">
                    <h5>IMPORT RATE</h5>
                  </div>
                  <div class="panel-body">
                    <div class="mb-3">
                      <label for="file" class="form-label">Select file (Excel)</label>
                      <input class="form-control" type="file" id="file" name="file"
                             accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                    </div>
                  </div>
                  <div class="panel-footer">
                    <button type="button" id="validateImportRateBtn"
                            class="btn btn-info"
                            onclick="validateImportRateAction()"><i class="fa fa-check"></i>
                      Validate
                    </button>
                    <button type="button" id="importRateBtn"
                            class="btn btn-success"
                            onclick="importRateBtnAction()"><i class="fa fa-upload"></i> Import
                      Rate
                    </button>
                  </div>
                </section>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-12">
                <section class="panel-featured panel-featured-success">
                  <div class="panel-heading">
                    <h5>IMPORT FILE VALIDATION</h5>
                  </div>
                  <div class="panel-body">
                    <table class="table table-bordered table-striped mb-none"
                           id="importRateConfigDataTable">
                      <thead>
                      <tr>
                        <th>Item Name</th>
                        <th>To Location</th>
                        <th>Rate</th>
                      </tr>
                      </thead>
                    </table>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <div id="w4-rate-cards" class="tab-pane">
            <section class="panel">
              <div class="panel-body">
                <table class="table table-striped nowrap" style="width:100%"
                       id="ratesDataTable">
                  <thead>
                  <tr>
                    <th></th>
                    <th>Item</th>
                    <th>To</th>
                    <th>Rate</th>
                  </tr>
                  </thead>
                </table>
              </div>
              <div class="panel-footer">
              </div>
            </section>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<!-- Modal for Update -->
<div id="editConfigRatesModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">UPDATE ITEM</h4>
      </div>

      <%--            View data form--%>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="editRefConfPackageId"> Item</label>
              <select data-plugin-selectTwo class="form-control populate" id="editRefConfPackageId"
                      name="refConfPackageId">
                <option value="">Select Item</option>
              </select>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="editRefConfToLocationId"> To</label>
              <select data-plugin-selectTwo class="form-control populate" id="editRefConfToLocationId"
                      name="refConfToLocationId">
                <option value="">Select Location</option>
              </select>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label class="control-label" for="editRate"> Rate</label>
              <input type="text" class="form-control" id="editRate" name="rate">
            </div>
          </div>

        </div>

      </div>
      <%--            Form update button--%>
      <div class="modal-footer">
        <footer class="panel-footer">
          <div class="row">
            <div class="col-sm-9 col-sm-offset-3">
              <button type="button" id="updateConfigRatesBtn" class="btn btn-primary">Update</button>
              <button type="button" class="btn btn-info"
                      data-dismiss="modal">Close
              </button>
            </div>
          </div>
        </footer>
      </div>
    </div>

  </div>
</div>
<ui:loadingModal/>
<ui:footer/>

<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-rate-config-pipeline.js"></script>
