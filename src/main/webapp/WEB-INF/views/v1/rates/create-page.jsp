<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refConfPackageId"> Item</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refConfPackageId"
                                    name="refConfPackageId">
                                <option value="">Select Item</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refConfFromLocationId"> From</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refConfFromLocationId"
                                    name="refConfFromLocationId">
                                <option value="">Select Location</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refConfToLocationId"> To</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refConfToLocationId"
                                    name="refConfToLocationId">
                                <option value="">Select Location</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="rate"> Rate</label>
                            <input type="text" class="form-control" id="rate" name="rate">
                        </div>
                    </div>

                </div>
                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveConfigRateBtn" class="btn btn-primary">Save
                                Rate
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-rate-create.js"></script>
