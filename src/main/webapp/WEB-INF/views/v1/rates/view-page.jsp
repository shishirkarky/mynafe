
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#popular" data-toggle="tab"><i
                        class="fa fa-star"></i>Rate</a></li>
            </ul>

            <div class="tab-content">
                <!-- Display account details -->
                <div id="popular" class="tab-pane active">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Item Details</h2>
                        </header>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped mb-none"
                                   id="ratesDataTable">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Item</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Rate</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal for Update -->
<div id="editConfigRatesModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE ITEM</h4>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refConfPackageId"> Item</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refConfPackageId"
                                    name="refConfPackageId">
                                <option value="">Select Item</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refConfFromLocationId"> From</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refConfFromLocationId"
                                    name="refConfFromLocationId">
                                <option value="">Select Location</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refConfToLocationId"> To</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refConfToLocationId"
                                    name="refConfToLocationId">
                                <option value="">Select Location</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="rate"> Rate</label>
                            <input type="text" class="form-control" id="rate" name="rate">
                        </div>
                    </div>

                </div>

            </div>
            <%--            Form update button--%>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
<%--                            <a type="button" id="deleteConfigRatesBtn" class="btn btn-danger"><i--%>
<%--                                    class="fa fa-trash-o"></i></a>--%>
                            <button type="button" id="updateConfigRatesBtn" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-rate-view.js"></script>