<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refVendorId"> Vendor</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refVendorId"
                                    name="refVendorId">
                                <option value="0">Select Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="vehicleType"> Vehicle Type</label>
                            <select data-plugin-selectTwo class="form-control populate" id="vehicleType"
                                    name="vehicleType" onchange="vehicleTypeOnChangeAction()">
                                <option value="">Select Vehicle Type</option>
                                <option value="PRIVATE">PRIVATE</option>
                                <option value="COMPANY_OWN">COMPANY OWN</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="refVehicleId"> Vehicle</label>
                            <select data-plugin-selectTwo class="form-control populate" onchange="vehicleOnChangeAction()" id="refVehicleId"
                                    name="refVehicleId">
                                <option value="0">Select Vehicle</option>
                            </select>
                        </div>
                    </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <label class="control-label" for="refVehicleOwnerId"> Vehicle Owner</label>
                      <input type="text" class="form-control" id="refVehicleOwnerId"
                              name="refVehicleOwnerId" readonly>
                    </div>
                  </div>

                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="totalWorkingHours"> Total working hours</label>
                            <input type="number" class="form-control" id="totalWorkingHours" name="totalWorkingHours">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="totalAmount"> Total amount</label>
                            <input type="number" class="form-control" id="totalAmount" name="totalAmount">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="mileage"> Mileage</label>
                            <input type="number" class="form-control" id="mileage" name="mileage">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="fuelRate"> Fuel Rate</label>
                            <input type="number" class="form-control" id="fuelRate" name="fuelRate">
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="otRate"> OT Rate</label>
                            <input type="number" class="form-control" id="otRate" name="otRate">
                        </div>
                    </div>
                </div>
                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveConfigRateBtn" class="btn btn-primary">Save
                                Rate
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
            </div>

            <h2 class="panel-title">Available Rates</h2>
        </header>
        <div class="panel-body">
            <table class="table table-striped nowrap" style="width:100%"
                   id="dataTable">
                <thead>
                <tr>
                    <th>Vendor</th>
                    <th>Vehicle Owner</th>
                    <th>Vehicle Type</th>
                    <th>Vehicle</th>
                    <th>Total working hours</th>
                    <th>Total amount</th>
                    <th>Mileage</th>
                    <th>Fuel rate</th>
                    <th>OT rate</th>
                </tr>
                </thead>
            </table>
        </div>
    </section>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-hiring-vehicle-rate-create.js"></script>
