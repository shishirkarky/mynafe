<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<style>
  .card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
  }

  /* On mouse-over, add a deeper shadow */
  .card:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
  }

  /* Add some padding inside the card container */
  .container {
    padding: 2px 16px;
  }

  span {
    font-weight: bold;
  }
</style>
<section class="panel">
  <header class="panel-heading">
    <div class="panel-actions">
    </div>

    <h2 class="panel-title">Add POD as Transit</h2>
  </header>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label class="control-label" for="dn"> DN </label>
          <input type="text" class="form-control" id="dn" name="dn">
        </div>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <a class="btn btn-primary" id="verifyTransitionBtn" onclick="verifyTransitionAction()"><i class="fa fa-check"></i>
      Verify </a>
    <a class="btn btn-success" id="createTransitBtn" onclick="createTransitAction()"><i class="fa fa-save"></i>
      Add </a>
  </div>
</section>
<section class="panel">
  <header class="panel-heading">
    <div class="panel-actions">
    </div>

    <center><h2 class="panel-title">Transits</h2></center>
  </header>
  <div class="panel-body">
    <table class="table table-bordered table-striped mb-none"
           id="transitionDataTable">
      <thead>
      <tr>
        <th>Transition Date</th>
        <th>Transit To</th>
        <th>Transit From</th>
        <th>Transaction Id</th>
        <th>Booking Type</th>
        <th>Booking Code</th>
        <th>Vendor</th>
        <th>DN</th>
        <th>Destination</th>
        <th>POD Status</th>
      </tr>
      </thead>
    </table>
  </div>
</section>

<%--POD VERIFICATION MODAL--%>
<div class="modal fade right" id="verifyTransitionModal" tabindex="-1"
     role="dialog" aria-labelledby="verifyTransitionModal" aria-hidden="true">

  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="verifyTransitionModalLabel">POD Details</h4>
      </div>
      <div class="modal-body">
        <h4 id="v-dn"></h4>
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="container">
                <h4>Consignee</h4>
                <p id="v-consigneeName"></p>
                <p id="v-consigneeContact"></p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card">
              <div class="container">
                <h4>Consignor</h4>
                <p>Name: <span id="v-consignorName"></span></p>
                <p>VAT: <span id="v-consignorVat"></span></p>
                <p>Bill Date: <span id="v-customerBillDate"></span></p>
                <p>Bill Number: <span id="v-customerBillNumber"></span></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <table class="table table-bordered table-striped mb-none"
                 id="transitionVerificationDataTable" style="width:100%">
            <thead>
            <tr>
              <th>Item</th>
              <th>Quantity</th>
              <th>No of package</th>
            </tr>
            </thead>
          </table>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>

  <ui:footer/>

  <script type="module" src="${pageContext.request.contextPath }/jsForPages/page-transitions-create.js"></script>
