<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Staff Details</h2>
            </header>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="firstName">First Name</label>
                            <input type="text" class="form-control" id="firstName" name="firstName">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="middleName">Middle Name</label>
                            <input type="text" class="form-control" id="middleName" name="middleName">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="lastName">Last Name </label>
                            <input type="text" class="form-control" id="lastName" name="lastName">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="phoneNumber"> Phone Number </label>
                            <input type="text" class="form-control" id="phoneNumber" name="phoneNumber">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="branchCode">Branch</label>
                            <select data-plugin-selectTwo class="form-control populate" id="branchCode"
                                    name="branchCode">
                                <option value="">Select Branch</option>
                            </select>
                        </div>
                    </div>

                </div>

                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveStaffBtn" onclick="sendStaffSaveReq()"
                                    class="btn btn-primary">Save
                                Staff
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>
<%--------------------------------------VIEW AND EDIT------------------------------------------------------------%>
<div class="row">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
            </div>

            <h2 class="panel-title">Staff Details</h2>
        </header>
        <div class="panel-body">
            <table class="table table-striped nowrap" style="width:100%"
                   id="staffDataTable">
                <thead>
                <tr>
                    <th></th>
                    <th>F. Name</th>
                    <th>M. Name</th>
                    <th>L. Name</th>
                    <th>Phone No.</th>
                    <th>Email</th>
                    <th>Branch Code</th>
                </tr>
                </thead>
            </table>
        </div>
    </section>
</div>

<!-- Modal for Update -->
<div id="editStaffModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE STAFF</h4>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <header class="panel-heading">
                                <h2 class="panel-title">Details</h2>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="efirstName">First Name</label>
                                            <input type="text" class="form-control" id="efirstName" name="efirstName">

                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="emiddleName">Middle Name</label>
                                            <input type="text" class="form-control" id="emiddleName" name="emiddleName">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="elastName">Last Name </label>
                                            <input type="text" class="form-control" id="elastName" name="elastName">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="ephoneNumber"> Phone Number </label>
                                            <input type="text" class="form-control" id="ephoneNumber"
                                                   name="ephoneNumber">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="eemail">Email</label>
                                            <input type="text" class="form-control" id="eemail" name="eemail">

                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="ebranchCode">Branch</label>
                                            <select data-plugin-selectTwo class="form-control populate" id="ebranchCode"
                                                    name="ebranchCode">
                                                <option value="">Select Branch</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-12 col-sm-offset-10">
                                        <button type="button" id="updateStaffBtn" class="btn btn-primary">Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <header class="panel-heading">
                                <h2 class="panel-title">Documents upload</h2>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select class="form-control" id="staffDocumentSubType"
                                                name="staffDocumentSubType">
                                            <option value="">Select document type</option>
                                            <option value="CITIZENSHIP">CITIZENSHIP</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="file" id="staffFileBytes" name="staffFileBytes"
                                               class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" id="uploadStaffDocumentBtn" class="btn btn-primary">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <header class="panel-heading">
                                <h2 class="panel-title">Available documents</h2>
                            </header>
                            <div class="panel-body">
                                <table class="table table-striped nowrap" style="width:100%"
                                       id="staffDocumentsDataTable">
                                    <thead>
                                    <tr>
                                        <th>Doc. type</th>
                                        <th>Download</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-staff-create.js"></script>