<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Location Details</h2>
            </header>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="district"> District </label>
                            <input type="text" class="form-control" id="district" name="district">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="city"> City </label>
                            <input type="text" class="form-control" id="city" name="city">
                        </div>
                    </div>
                </div>
                <br/>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 ">
                            <button type="button" id="saveLocationBtn" class="btn btn-success" onclick="saveLocation()"><i class="fa fa-save"></i> Save
                            </button>
                        </div>
                    </div>
                </footer>
            </div>
        </section>
    </div>
</div>
<%-------------------------------------------------VIEW AND EDIT-------------------------------------------------------%>
<div class="row">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
            </div>
            <h2 class="panel-title">Location Details</h2>
        </header>
        <div class="panel-body">
            <table class="table table-striped nowrap" style="width:100%"
                   id="locationsDataTable">
                <thead>
                <tr>
                    <th></th>
                    <th>District</th>
                    <th>City</th>
                </tr>
                </thead>
            </table>
        </div>
    </section>
</div>

<!-- Modal for Update -->
<div id="editConfigLocationsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE LOCATION</h4>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="edistrict"> District </label>
                            <input type="text" class="form-control" id="edistrict" name="edistrict">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ecity"> City </label>
                            <input type="text" class="form-control" id="ecity" name="ecity">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" id="updateConfigLocationsBtn" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>

                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-location-create.js"></script>
