<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                            </div>

                            <h2 class="panel-title">Vehicle Log</h2>
                        </header>
                        <div class="panel-body">
                            <table class="table table-striped nowrap" style="width:100%"
                                   id="vehicleLogBookDatatable">
                                <thead>
                                <tr>
                                    <th>Dispatch Date</th>
                                    <th>Booking Type</th>
                                    <th>Booking Code</th>
                                    <th>Consignor Name</th>
                                    <th>Dispatch Location</th>
                                    <th>Driver</th>
                                    <th>Vehicle</th>
                                    <th>Owner Name</th>
                                    <th>IN Time</th>
                                    <th>Out Time</th>
                                    <th>In KM</th>
                                    <th>Out KM</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </section>
    </div>
</div>

<!-- Modal for Update -->
<div id="editVehicleLogBookModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE LOG BOOK</h4>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="refVendorRequestId">Select Booking Code</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refVendorRequestId"
                                    name="refVendorRequestId">
                                <option value="">Select Booking Code</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkInDate">In Date</label>
                            <input type="text" class="form-control date-picker" data-single="true" id="checkInDate"
                                   name="checkInDate" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkInTime">In Time</label>
                            <input type="time" class="form-control" id="checkInTime"
                                   name="checkInTime">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkOutDate">Out Date </label>
                            <input type="text" class="form-control date-picker" data-single="true" id="checkOutDate"
                                   name="checkOutDate" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkOutTime">Out Time</label>
                            <input type="time" class="form-control" id="checkOutTime"
                                   name="checkOutTime">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="inKm">In Km</label>
                            <input type="text" class="form-control" id="inKm" name="inKm">

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="outKm">Out Km</label>
                            <input type="text" class="form-control" id="outKm"
                                   name="outKm">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="laborName">Labor Name</label>
                            <input type="text" class="form-control" id="laborName"
                                   name="laborName">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="numberOfTrip">Number of trip</label>
                            <input type="text" class="form-control" id="numberOfTrip"
                                   name="numberOfTrip">
                        </div>
                    </div>
                </div>
            </div>
            <%--            Form update button--%>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" id="updateVehicleLogBookBtn" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>

                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-vehiclelogbook-view.js"></script>