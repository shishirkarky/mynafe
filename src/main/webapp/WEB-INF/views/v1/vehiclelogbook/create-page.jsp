<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Vehicle Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="assignedDriver">Assigned Driver</label>
                            <input type="text" class="form-control" id="assignedDriver"
                                   name="assignedDriver" disabled>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="assignedVehicle">Assigned Vehicle</label>
                            <input type="text" class="form-control" id="assignedVehicle"
                                   name="assignedVehicle" disabled>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="ownerFullName">Owner Full Name</label>
                            <input type="text" class="form-control" id="ownerFullName"
                                   name="ownerFullName" disabled>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="refVendorRequestId">Select Booking Code</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refVendorRequestId"
                                    name="refVendorRequestId" onchange="bookingCodeOnChangeAction()">
                                <option value="">Select Booking Code</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkInDate">In Date</label>
                            <input type="text" class="form-control date-picker" data-single="true" id="checkInDate"
                                   name="checkInDate" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkInTime">In Time</label>
                            <input type="time" class="form-control" id="checkInTime"
                                   name="checkInTime">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkOutDate"> Out Date </label>
                            <input type="text" class="form-control date-picker" data-single="true" id="checkOutDate"
                                   name="checkOutDate" placeholder="Select Date">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="checkOutTime">Out Time</label>
                            <input type="time" class="form-control" id="checkOutTime"
                                   name="checkOutTime">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="inKm">Check In Km</label>
                            <input type="text" class="form-control" id="inKm" name="inKm">

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="outKm">Check Out Km</label>
                            <input type="text" class="form-control" id="outKm"
                                   name="outKm">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="laborName">Labor Name</label>
                            <input type="text" class="form-control" id="laborName"
                                   name="laborName">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="numberOfTrip">Number of trip</label>
                            <input type="text" class="form-control" id="numberOfTrip"
                                   name="numberOfTrip">
                        </div>
                    </div>
                </div>
                <br/>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 ">
                        <button type="button" id="saveVehicleLogBookBtn" class="btn btn-primary">Save
                            Log
                        </button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-vehiclelogbook-create.js"></script>
