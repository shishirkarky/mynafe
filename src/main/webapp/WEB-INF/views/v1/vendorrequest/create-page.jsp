<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Enter Details</h2>
            </header>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refVendorId"> Vendor</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refVendorId"
                                    name="refVendorId">
                                <option value="">Select Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refDriverId">Assigned Driver</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refDriverId"
                                    name="refDriverId">
                                <option value="">Select Driver</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refVehicleId">Assigned Vehicle</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refVehicleId"
                                    name="refVehicleId">
                                <option value="">Select Vehicle</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refFromLocationId">From</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refFromLocationId"
                                    name="refFromLocationId">
                                <option value="">Select From</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="refToLocationId">To</label>
                            <select data-plugin-selectTwo class="form-control populate" id="refToLocationId"
                                    name="refToLocationId">
                                <option value="">Select To</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="dateOfRequest"> Request Date </label>
                            <input type="text" class="form-control date-picker" data-single="true" id="dateOfRequest"
                                   name="dateOfRequest" placeholder="Select Date" onblur="nepaliToEnglish('#dateOfRequest','#dateOfRequestEn')"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="dateOfRequestEn"> Request Date(A.D.)</label>
                            <input type="text" data-plugin-datepicker data-date-format="yyyy-mm-dd" data-plugin-skin="dark"  placeholder="Select date" class="form-control" id="dateOfRequestEn" name="dateOfRequestEn"
                                   onblur="englishToNepali('#dateOfRequestEn','#dateOfRequest')"/>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="dispatchDate"> Dispatch Date </label>
                            <input type="text" class="form-control date-picker" data-single="true" id="dispatchDate"
                                   name="dispatchDate" placeholder="Select Date" onblur="nepaliToEnglish('#dispatchDate','#dispatchDateEn') "/>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="dispatchDateEn"> Dispatch Date(A.D.)</label>
                            <input type="text" data-plugin-datepicker data-date-format="yyyy-mm-dd" data-plugin-skin="dark" class="form-control " id="dispatchDateEn" name="dispatchDate"
                                   placeholder="Select Date" onblur="englishToNepali('#dispatchDateEn','#dispatchDate')" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="totalAmount"> Total Amount </label>
                            <input class="form-control" id="totalAmount" name="totalAmount"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="advancePaymentAmount"> Advance Pay Amount </label>
                            <input class="form-control" id="advancePaymentAmount" name="advancePaymentAmount"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="description"> Description </label>
                            <input class="form-control" id="description" name="vendorRequestDescription"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="remarks"> Remarks </label>
                            <input class="form-control" id="remarks" name="remarks"/>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Expenses Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="fuelAmount"> Fuel Amount </label>
                            <input class="form-control" id="fuelAmount" name="fuelAmount"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="dhalaAndPaltiExpenses"> Dhala and Palti </label>
                            <input class="form-control" id="dhalaAndPaltiExpenses" name="dhalaAndPaltiExpenses"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="labourExpenses"> Labour Expenses </label>
                            <input class="form-control" id="labourExpenses" name="labourExpenses"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="maintainanceAmount"> Maintainance Amount </label>
                            <input class="form-control" id="maintainanceAmount" name="maintainanceAmount"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="otherExpenses"> Other Expenses </label>
                            <input class="form-control" id="otherExpenses" name="otherExpenses"/>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 ">
                        <button type="button" id="saveVendorRequestBtn" class="btn btn-primary">Save
                        </button>
                    </div>
                </div>
            </footer>
        </section>

    </div>
</div>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-vendorrequest-create.js"></script>
