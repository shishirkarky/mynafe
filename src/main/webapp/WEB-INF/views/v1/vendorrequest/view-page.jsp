<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
        </div>
        <h2 class="panel-title">Booking Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-striped nowrap" style="width:100%"
               id="vendorRequestDataTable">
            <thead>
            <tr>
                <th>Booking Type</th>
                <th>Booking Code</th>
                <th>Consignor Name</th>
                <th>Driver Name</th>
                <th>Vehicle Number</th>
                <th>Vehicle Type</th>
                <th>Request Date</th>
                <th>Dispatched Office</th>
                <th>Dispatch Date</th>
                <th>Dispatch Location</th>
                <th>Total Expenses</th>
                <th>Advance Payment</th>
                <th>Due Amount</th>
                <th>Dhala and Pali Expenses</th>
                <th>Labour Expenses</th>
                <th>Fuel Expenses</th>
                <th>Maintenance Expenses</th>
                <th>Other Expenses</th>
                <th>Current Km</th>
            </tr>
            </thead>
        </table>
    </div>
</section>

<!-- Modal for Update -->
<%--<div id="editVendorRequestModal" class="modal fade" role="dialog">--%>
<%--    <div class="modal-dialog modal-lg">--%>

<%--        <!-- Modal content-->--%>
<%--        <div class="modal-content">--%>
<%--            <div class="modal-header">--%>
<%--                <button type="button" class="close" data-dismiss="modal">&times;</button>--%>
<%--                <h4 class="modal-title">UPDATE BOOKING</h4>--%>
<%--            </div>--%>

<%--            &lt;%&ndash;            View data form&ndash;%&gt;--%>
<%--            <div class="modal-body">--%>
<%--                <section class="panel">--%>
<%--                    <header class="panel-heading">--%>
<%--                        <div class="panel-actions">--%>
<%--                        </div>--%>

<%--                        <h2 class="panel-title">Booking Details</h2>--%>
<%--                    </header>--%>
<%--                    <div class="panel-body">--%>
<%--                        <div class="row">--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="refVendorId"> Vendor</label>--%>
<%--                                    <select data-plugin-selectTwo class="form-control populate" id="refVendorId"--%>
<%--                                            name="refVendorId">--%>
<%--                                        <option value="">Select Vendor</option>--%>
<%--                                    </select>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="refDriverId">Assigned Driver</label>--%>
<%--                                    <select data-plugin-selectTwo class="form-control populate" id="refDriverId"--%>
<%--                                            name="refDriverId">--%>
<%--                                        <option value="">Select Driver</option>--%>
<%--                                    </select>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="refVehicleId">Assigned Vehicle</label>--%>
<%--                                    <select data-plugin-selectTwo class="form-control populate" id="refVehicleId"--%>
<%--                                            name="refVehicleId">--%>
<%--                                        <option value="">Select Vehicle</option>--%>
<%--                                    </select>--%>
<%--                                </div>--%>
<%--                            </div>--%>

<%--                        </div>--%>
<%--                        <div class="row">--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="refFromLocationId">From</label>--%>
<%--                                    <select data-plugin-selectTwo class="form-control populate" id="refFromLocationId"--%>
<%--                                            name="refFromLocationId">--%>
<%--                                        <option value="">Select From</option>--%>
<%--                                    </select>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="refToLocationId">To</label>--%>
<%--                                    <select data-plugin-selectTwo class="form-control populate" id="refToLocationId"--%>
<%--                                            name="refToLocationId">--%>
<%--                                        <option value="">Select To</option>--%>
<%--                                    </select>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="dateOfRequest"> Request Date </label>--%>
<%--                                    <input type="text" class="form-control date-picker" data-single="true"--%>
<%--                                           id="dateOfRequest" name="dateOfRequest" placeholder="Select Date"--%>
<%--                                           onblur="nepaliToEnglish('#dateOfRequest','#dateOfRequestEn')"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="row">--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="dateOfRequestEn"> Request Date(A.D.)</label>--%>
<%--                                    <input type="text" data-plugin-datepicker data-date-format="yyyy-mm-dd"--%>
<%--                                           data-plugin-skin="dark" class="form-control" id="dateOfRequestEn"--%>
<%--                                           name="dateOfRequestEn" placeholder="Select Date"--%>
<%--                                           onblur="englishToNepali('#dateOfRequestEn','#dateOfRequest')"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="dispatchDate"> Dispatch Date </label>--%>
<%--                                    <input type="text" class="form-control date-picker" data-single="true"--%>
<%--                                           id="dispatchDate" name="dispatchDate" placeholder="Select Date"--%>
<%--                                           onblur="nepaliToEnglish('#dispatchDate','#dispatchDateEn')"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="dispatchDateEn"> Dispatch Date(A.D.)</label>--%>
<%--                                    <input type="text" data-plugin-datepicker data-date-format="yyyy-mm-dd"--%>
<%--                                           data-plugin-skin="dark" class="form-control" id="dispatchDateEn"--%>
<%--                                           name="dispatchDate" placeholder="Select Date"--%>
<%--                                           onblur="englishToNepali('#dispatchDateEn','#dispatchDate')"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="row">--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="totalAmount"> Total Amount </label>--%>
<%--                                    <input class="form-control" id="totalAmount" name="totalAmount"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="advancePaymentAmount"> Advance Pay Amount </label>--%>
<%--                                    <input class="form-control" id="advancePaymentAmount" name="advancePaymentAmount"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="description"> Description </label>--%>
<%--                                    <input class="form-control" id="description" name="vendorRequestDescription"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </section>--%>
<%--                <section class="panel">--%>
<%--                    <header class="panel-heading">--%>
<%--                        <div class="panel-actions">--%>
<%--                        </div>--%>

<%--                        <h2 class="panel-title">Expenses Details</h2>--%>
<%--                    </header>--%>
<%--                    <div class="panel-body">--%>
<%--                        <div class="row">--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="fuelAmount"> Fuel Amount </label>--%>
<%--                                    <input class="form-control" id="fuelAmount" name="fuelAmount"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="dhalaAndPaltiExpenses"> Dhala and Palti </label>--%>
<%--                                    <input class="form-control" id="dhalaAndPaltiExpenses"--%>
<%--                                           name="dhalaAndPaltiExpenses"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="labourExpenses"> Labour Expenses </label>--%>
<%--                                    <input class="form-control" id="labourExpenses" name="labourExpenses"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="row">--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="maintainanceAmount"> Maintainance Amount </label>--%>
<%--                                    <input class="form-control" id="maintainanceAmount" name="maintainanceAmount"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="col-sm-4">--%>
<%--                                <div class="form-group">--%>
<%--                                    <label class="control-label" for="otherExpenses"> Other Expenses </label>--%>
<%--                                    <input class="form-control" id="otherExpenses" name="otherExpenses"/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </section>--%>
<%--            </div>--%>
<%--            &lt;%&ndash;            Form update button&ndash;%&gt;--%>
<%--            <div class="modal-footer">--%>
<%--                <footer class="panel-footer">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-sm-9 col-sm-offset-3">--%>
<%--                            &lt;%&ndash;                            <a type="button" id="deleteVendorRequestBtn" class="btn btn-danger"><i&ndash;%&gt;--%>
<%--                            &lt;%&ndash;                                    class="fa fa-trash-o"></i></a>&ndash;%&gt;--%>
<%--                            <button type="button" id="updateVendorRequestBtn" class="btn btn-primary">Update</button>--%>
<%--                            <button type="button" class="btn btn-info"--%>
<%--                                    data-dismiss="modal">Close--%>
<%--                            </button>--%>

<%--                        </div>--%>
<%--                    </div>--%>
<%--                </footer>--%>
<%--            </div>--%>
<%--        </div>--%>

<%--    </div>--%>
<%--</div>--%>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-vendorrequest-view.js"></script>
