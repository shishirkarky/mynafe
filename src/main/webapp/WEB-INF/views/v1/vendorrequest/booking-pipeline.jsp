<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-xs-10">
        <section class="panel form-wizard" id="w4">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>

                <h2 class="panel-title">Booking Pipe-line</h2>
            </header>
            <div class="panel-body">
                <div class="wizard-progress wizard-progress-lg">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul class="wizard-steps">
                        <li id="bookingLi" class="active">
                            <a href="#w4-booking" data-toggle="tab" id="bookingInfoTab"><span>1</span>Booking</a>
                        </li>
                        <li id="podLi">
                            <a href="#w4-pod" data-toggle="tab" id="podInfoTab"
                               onclick="triggerPodCreationTabClickAction()"><span>2</span>POD Creation</a>
                        </li>
                        <li>
                            <a href="#w4-assign" data-toggle="tab" id="assignmentInfoTab"><span>3</span>Assignment &
                                <br>
                                Expenses</a>
                        </li>
                        <li>
                            <a href="#w4-availablePods" data-toggle="tab" id="availablePodsInfoTab"
                               onclick="clickAvailablePodsSection()"><span>4</span>Available PODs</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div id="w4-booking" class="tab-pane active">
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="refVendorRequestId">Booking
                                                Code (Opt.) <i class="fa  fa-info-circle" data-toggle="tooltip"
                                                               data-placement="top"
                                                               title="Optional field. If not selected, will create new booking."></i></label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="refVendorRequestId"
                                                    name="refVendorRequestId" onchange="bookingCodeChangeAction()">
                                                <option value="">Select Booking Code</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="dateOfRequest">Date
                                            </label>
                                            <input type="text" class="form-control date-picker" data-single="true"
                                                   id="dateOfRequest"
                                                   name="dateOfRequest">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="refTransitionIds">
                                                Transit Items (DN if any)
                                                <i class="fa  fa-info-circle" data-toggle="tooltip" data-placement="top"
                                                   title="Optional field. If booking relates to transit items."></i>
                                            </label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="refTransitionIds"
                                                    name="refTransitionIds" multiple>
                                                <option value="">Select Transitions</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="bookingType"> Booking
                                                Type</label>
                                            <select class="form-control" name="bookingType" id="bookingType"
                                                    required>
                                                <option value="PARTIAL_BOOKING" selected>PARTIAL BOOKING</option>
                                                <option value="FULL_BOOKING">FULL BOOKING</option>
                                                <option value="HIRING_VEHICLE">HIRING VEHICLE</option>
                                                <option value="LOCAL_DELIVERY">LOCAL DELIVERY</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="refVendorId"> Vendor</label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="refVendorId"
                                                    name="refVendorId">
                                                <option value="0">Select Vendor</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="dispatchFrom">Dispatch
                                                Location</label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="dispatchFrom"
                                                    name="dispatchFrom" onchange="changeDispatchLocation()">
                                                <option value="">Select Dispatch Location</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="finalDestination">Final
                                                Destination</label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="finalDestination"
                                                    name="finalDestination">
                                                <option value="">Select Final Destination</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="pager pull-right">
                                    <a class="btn btn-success" id="saveBookingBtn"
                                       onclick="saveBookingAction()" data-toggle="tooltip" data-placement="top"
                                       title="SAVE & GOTO NEXT TAB"><i class="fa fa-save"></i> <i
                                            class="fa fa-forward"></i></a>
                                </div>
                            </div>
                        </section>


                    </div>
                    <div id="w4-pod" class="tab-pane">
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form id="podForm">
                                            <section class="panel" id="createPodSection">
                                                <header class="panel-heading">
                                                    <div class="panel-actions">
                                                    </div>

                                                    <h2 class="panel-title">POD Details</h2>
                                                </header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label" for="consignorName">Consignor
                                                                    Name</label>
                                                                <select data-plugin-selectTwo
                                                                        class="form-control populate"
                                                                        id="consignorName"
                                                                        name="consignorName"
                                                                        onchange="consignorNameOnChangeAction()">
                                                                    <option value="0">Select Consignor</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <%--                                                VENDOR VAT NUMBER--%>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label" for="consignorVat">Consignor's
                                                                    VAT</label>
                                                                <input type="text" class="form-control"
                                                                       id="consignorVat"
                                                                       name="consignorVat">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label"
                                                                       for="customerBillNumber">Customer Ref. Invoice
                                                                    Number</label>
                                                                <input type="text" class="form-control"
                                                                       id="customerBillNumber"
                                                                       name="customerBillNumber">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label" for="consigneeName">Consignee
                                                                    Name</label>
                                                                <input type="text" class="form-control" value="" id="consigneeName">
<%--                                                                <select data-plugin-selectTwo--%>
<%--                                                                        class="form-control populate"--%>
<%--                                                                        id="consigneeId"--%>
<%--                                                                        name="consigneeId"--%>
<%--                                                                        onchange="consigneeNameOnChangeAction()">--%>
<%--                                                                    <option value="0">Select Customer</option>--%>
<%--                                                                </select>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label" for="consigneeVat">Consignee
                                                                    VAT</label>
                                                                <input type="text" class="form-control"
                                                                       id="consigneeVat"
                                                                       name="consigneeVat">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label"
                                                                       for="refFromLocationId">From</label>
                                                                <select data-plugin-selectTwo
                                                                        class="form-control populate"
                                                                        id="refFromLocationId"
                                                                        name="refFromLocationId">
                                                                    <option value="">Select From</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label" for="refToLocationId">
                                                                To (Delivery Location)</label>
                                                            <select data-plugin-selectTwo class="form-control populate"
                                                                    id="refToLocationId"
                                                                    name="refToLocationId">
                                                                <option value="">Select Delivery Location</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label" for="consigneeContact">Consignee
                                                                    Contact</label>
                                                                <input type="text" class="form-control"
                                                                       id="consigneeContact"
                                                                       name="consigneeContact">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label" for="customerBillDate">Customer
                                                                    Bill Date</label>
                                                                <input type="text" class="form-control date-picker"
                                                                       data-single="true"
                                                                       id="customerBillDate"
                                                                       name="customerBillDate">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="panel">
                                                <header class="panel-heading">
                                                    <div class="panel-actions">
                                                        <a href="#" class="fa fa-caret-down"></a>
                                                    </div>

                                                    <h4 class="panel-title">Items Details</h4>
                                                </header>
                                                <div class="panel-body cashReceiptDetailsDiv"
                                                     id="cashReceiptDetailsDiv">
                                                    <div class="clonePodDiv">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="cashReceiptDetailsDescription">
                                                                        Description </label>
                                                                    <select class="form-control cashReceiptDetailsDescription"
                                                                            id="cashReceiptDetailsDescription"
                                                                            name="cashReceiptDetailsDescription">
                                                                        <option value="0">Select Item</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="quantity">Quantity</label>
                                                                    <input type="number" step="any" class="form-control"
                                                                           id="quantity"
                                                                           name="quantity">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="unitOfMeasurement">Measurement
                                                                        unit</label>
                                                                    <select class="form-control"
                                                                            id="unitOfMeasurement"
                                                                            name="unitOfMeasurement">
                                                                        <option value="QUANTITY" selected>
                                                                            Piece
                                                                        </option>
                                                                        <option value="PACKAGE">
                                                                            Carton
                                                                        </option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="noOfPackage">No of pkg</label>
                                                                    <input type="number" step="any" class="form-control"
                                                                           id="noOfPackage"
                                                                           name="noOfPackage">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="taxableAmount">Taxable
                                                                        Amt</label>
                                                                    <input type="number" step="any" class="form-control"
                                                                           id="taxableAmount" name="taxableAmount">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="remarks">Remarks</label>
                                                                    <input type="text" class="form-control"
                                                                           id="remarks"
                                                                           name="remarks">

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>
                                                <footer class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-sm-9 ">

                                                            <button type="button" id="addCashReceiptDetailsBtn"
                                                                    class="btn btn-success"
                                                                    onclick="cloneCashReceiptDetailReq()"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="ADD new item"><i
                                                                    class="fa fa-plus-circle"></i>
                                                            </button>
                                                            <button type="button" id="removeCashReceiptDetailsBtn"
                                                                    class="btn btn-danger"
                                                                    onclick="removeCashReceiptDetailReq()"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="DELETE last item"><i
                                                                    class="fa fa-minus-circle"></i>
                                                            </button>

                                                        </div>
                                                    </div>
                                                </footer>
                                            </section>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="button" id="saveCashReceiptBtn"
                                        class="btn btn-success pull-left"
                                        onclick="savePodAction()"><i class="fa fa-save"></i> Add POD
                                </button>
                                <div class="pager pull-right">
                                    <a type="button" onclick="assignBtnAction()" class="btn btn-success pull-right"
                                       data-toggle="tooltip" data-placement="top" title="GOTO NEXT TAB"><i
                                            class="fa fa-forward"></i></a>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div id="w4-assign" class="tab-pane">
                        <section class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="vehicleType">Vehicle Type</label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="vehicleType"
                                                    name="vehicleType" onchange="vehicleTypeOnChangeAction()">
                                                <option value="PRIVATE">
                                                    PRIVATE
                                                </option>
                                                <option value="COMPANY_OWN">
                                                    COMPANY OWN
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="refVehicleId">Assigned Vehicle</label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="refVehicleId"
                                                    name="refVehicleId">
                                                <option value="">Select Vehicle</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="refDriverId">Assigned Driver</label>
                                            <select data-plugin-selectTwo class="form-control populate"
                                                    id="refDriverId"
                                                    name="refDriverId">
                                                <option value="">Select Driver</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="vcts">VCTS</label>
                                            <select id="vcts" class="form-control"
                                                    name="vcts">
                                                <option selected value="YES">YES</option>
                                                <option value="NO">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="dhalaAndPaltiExpenses"> Dhala and
                                                Palti </label>
                                            <input type="number" step="any" class="form-control"
                                                   id="dhalaAndPaltiExpenses"
                                                   name="dhalaAndPaltiExpenses"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="labourExpenses"> Labour
                                                Expenses </label>
                                            <input type="number" step="any" class="form-control" id="labourExpenses"
                                                   name="labourExpenses"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="fuelAmount"> Fuel Expenses </label>
                                            <input type="number" step="any" class="form-control" id="fuelAmount"
                                                   name="fuelAmount"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="totalAmount"> Total Amount </label>
                                            <input type="number" step="any" class="form-control" id="totalAmount"
                                                   name="totalAmount" onkeyup="updateDueAmount()"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="advancePaymentAmount"> Advance Pay
                                                Amount </label>
                                            <input type="number" step="any" class="form-control"
                                                   id="advancePaymentAmount"
                                                   name="advancePaymentAmount" onkeyup="updateDueAmount()"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="dueAmount"> Due Amount </label>
                                            <input type="number" step="any" class="form-control" id="dueAmount"
                                                   name="dueAmount" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="maintainanceAmount"> Maintainance
                                                Expenses </label>
                                            <input type="number" step="any" class="form-control" id="maintainanceAmount"
                                                   name="maintainanceAmount"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="otherExpenses"> Other
                                                Expenses </label>
                                            <input type="number" step="any" class="form-control" id="otherExpenses"
                                                   name="otherExpenses"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="currentKm"> Current Km
                                            </label>
                                            <input type="number" step="any" class="form-control" id="currentKm"
                                                   name="currentKm"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="pager pull-right">
                                    <a type="button" onclick="saveExpensesAndAssignmentAction()"
                                       class="btn btn-success pull-right" data-toggle="tooltip" data-placement="top"
                                       title="SAVE & GOTO NEXT TAB"><i class="fa fa-save"></i> <i
                                            class="fa fa-forward"></i></a>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div id="w4-availablePods" class="tab-pane">
                        <section class="panel">
                            <div class="panel-body">
                                <a class="btn btn-primary" onclick="printVctsReport()"><i
                                        class="fa fa-file-excel-o"></i>
                                    VCTS Report</a>
                                <a class="btn btn-primary" onclick="printAllPodReports()"><i class="fa fa-print"></i>
                                    Print All</a>
                                <a class="btn btn-primary" onclick="printGatepassReport()"><i class="fa fa-print"></i>
                                    Print Gate-pass</a>
                                <hr>
                                <table class="table table-striped nowrap" style="width:100%"
                                       id="podDataTable">
                                    <thead>
                                    <tr>
                                        <th>DN</th>
                                        <th>Booking Code</th>
                                        <th>Consignor Name</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Status</th>
                                        <th>Transit</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </section>

                        <div id="modalFull" class="modal-block modal-block-full mfp-hide">
                            <section class="panel">
                                <header class="panel-heading">
                                    <h2 class="panel-title">Are you sure?</h2>
                                </header>
                                <div class="panel-body">
                                    <div class="modal-wrapper">
                                        <div class="modal-text">
                                            <p>Are you sure that you want to delete this image?</p>
                                        </div>
                                    </div>
                                </div>
                                <footer class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button class="btn btn-primary modal-confirm">Confirm</button>
                                            <button class="btn btn-default modal-dismiss">Cancel</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </div>

                        <!-- Modal for Update -->
                        <div id="editPodModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="row">
                                                <section class="panel" id="updatePodSection">
                                                    <header class="panel-heading">
                                                        <div class="panel-actions">
                                                        </div>

                                                        <h2 class="panel-title">POD Details - <em><span
                                                                id="edn"></span></em></h2>
                                                    </header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="econsignorName">Consignor
                                                                        Name</label>
                                                                    <select data-plugin-selectTwo
                                                                            class="form-control populate"
                                                                            id="econsignorName"
                                                                            name="econsignorName"
                                                                            onchange="econsignorNameOnChangeAction()">
                                                                        <option value="">Select Consignor</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <%--                                                VENDOR VAT NUMBER--%>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="econsignorVat">Consignor's
                                                                        VAT</label>
                                                                    <input type="text" class="form-control"
                                                                           id="econsignorVat"
                                                                           name="econsignorVat">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="ecustomerBillNumber">Consignee
                                                                        Invoice Number</label>
                                                                    <input type="text" class="form-control"
                                                                           id="ecustomerBillNumber"
                                                                           name="ecustomerBillNumber">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="econsigneeName">Consignee
                                                                        Name</label>
                                                                    <input type="text" class="form-control"
                                                                           id="econsigneeName"
                                                                           name="econsigneeName">
<%--                                                                    <select data-plugin-selectTwo--%>
<%--                                                                            class="form-control populate"--%>
<%--                                                                            id="econsigneeId"--%>
<%--                                                                            name="econsigneeId"--%>
<%--                                                                            onchange="econsigneeNameOnChangeAction()">--%>
<%--                                                                        <option value="0">Select Customer</option>--%>
<%--                                                                    </select>--%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="econsigneeVat">Consignee
                                                                        VAT</label>
                                                                    <input type="text" class="form-control"
                                                                           id="econsigneeVat"
                                                                           name="econsigneeVat">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="erefFromLocationId">From</label>
                                                                    <select data-plugin-selectTwo
                                                                            class="form-control populate"
                                                                            id="erefFromLocationId"
                                                                            name="erefFromLocationId">
                                                                        <option value="">Select From</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                <label class="control-label" for="erefToLocationId">
                                                                    To (Delivery Location)</label>
                                                                <select data-plugin-selectTwo
                                                                        class="form-control populate"
                                                                        id="erefToLocationId"
                                                                        name="erefToLocationId">
                                                                    <option value="">Select Delivery Location
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="econsigneeContact">Consignee
                                                                        Contact</label>
                                                                    <input type="text" class="form-control"
                                                                           id="econsigneeContact"
                                                                           name="econsigneeContact">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"
                                                                           for="ecustomerBillDate">Customer
                                                                        Bill Date</label>
                                                                    <input type="text"
                                                                           class="form-control date-picker"
                                                                           data-single="true"
                                                                           id="ecustomerBillDate"
                                                                           name="ecustomerBillDate">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section class="panel">
                                                    <header class="panel-heading">
                                                        <div class="panel-actions">
                                                        </div>
                                                        <h4 class="panel-title">Items Details</h4>
                                                    </header>
                                                    <div class="panel-body ecashReceiptDetailsDiv"
                                                         id="ecashReceiptDetailsDiv">
                                                        <div class="eclonePodDiv">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label"
                                                                               for="ecashReceiptDetailsDescription">
                                                                            Description </label>
                                                                        <select class="form-control ecashReceiptDetailsDescription"
                                                                                id="ecashReceiptDetailsDescription"
                                                                                name="ecashReceiptDetailsDescription">
                                                                            <option value="">Select Item</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label"
                                                                               for="equantity">Quantity</label>
                                                                        <input type="number" step="any"
                                                                               class="form-control"
                                                                               id="equantity"
                                                                               name="equantity">

                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label"
                                                                               for="eunitOfMeasurement">Measurement
                                                                            unit</label>
                                                                        <select class="form-control"
                                                                                id="eunitOfMeasurement"
                                                                                name="eunitOfMeasurement">
                                                                            <option value="QUANTITY" selected>
                                                                                Piece
                                                                            </option>
                                                                            <option value="PACKAGE">
                                                                                Carton
                                                                            </option>
                                                                        </select>


                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label"
                                                                               for="enoOfPackage">No of pkg</label>
                                                                        <input type="number" step="any"
                                                                               class="form-control"
                                                                               id="enoOfPackage"
                                                                               name="enoOfPackage">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label"
                                                                               for="etaxableAmount">Taxable
                                                                            Amt</label>
                                                                        <input type="number" step="any"
                                                                               class="form-control"
                                                                               id="etaxableAmount"
                                                                               name="etaxableAmount">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label"
                                                                               for="eremarks">Remarks</label>
                                                                        <input type="text" class="form-control"
                                                                               id="eremarks"
                                                                               name="eremarks">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <button type="button" id="eaddCashReceiptDetailsBtn"
                                                                    class="btn btn-success"
                                                                    onclick="ecloneCashReceiptDetailReq()"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="ADD new item"><i
                                                                    class="fa fa-plus-circle"></i>
                                                            </button>
                                                            <button type="button"
                                                                    id="eremoveCashReceiptDetailsBtn"
                                                                    class="btn btn-danger"
                                                                    onclick="eremoveCashReceiptDetailReq()"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="DELETE last item"><i
                                                                    class="fa fa-minus-circle"></i>
                                                            </button>

                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </section>
                                    <div class="modal-footer">
                                        <footer class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <button type="button" id="updatePodBtn"
                                                            class="btn btn-success pull-left"><i
                                                            class="fa fa-save"></i> Update POD
                                                    </button>
                                                    <button class="btn btn-default modal-dismiss"
                                                            onclick="hidePodEditModal()">Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-xs-2">
        <section class="panel panel-transparent">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>
                <h2 class="panel-title">Booking Summary</h2>
            </header>
            <div class="panel-body">
                <section class="panel">
                    <div class="panel-body">
                        <div class="h4 text-bold mb-none" id="summaryBookingCode"></div>
                        <p class="text-xs text-muted mb-none">Booking Code</p>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        <div class="h4 text-bold mb-none" id="summaryBookingType"></div>
                        <p class="text-xs text-muted mb-none">Booking Type</p>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        <div class="h4 text-bold mb-none" id="summaryVendor"></div>
                        <p class="text-xs text-muted mb-none">Vendor</p>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        <div class="h4 text-bold mb-none" id="summaryDate"></div>
                        <p class="text-xs text-muted mb-none">Date</p>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        <div class="col-md-3">
                            <div class="h4 text-bold mb-none" id="summaryPodCount"></div>
                            <p class="text-xs text-muted mb-none">POD Count</p>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </div>
</div>
<!-- Modal for POD Status Change -->
<div id="podStatusChangeModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UPDATE STATUS</h4>
                <hr>
                <span id="podStatusModalDN" style="color: red"></span>
            </div>

            <%--            View data form--%>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="podStatus">Status </label>
                            <select data-plugin-selectTwo class="form-control populate" id="podStatus"
                                    name="podStatus">
                                <option value="ACTIVE">ACTIVE</option>
                                <option value="DELIVERED">DELIVERED</option>
                                <option value="UNDELIVERED">UNDELIVERED</option>
                                <option value="CANCEL">CANCEL</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="podStatusRemarks">Remarks</label>
                            <input type="text" class="form-control" id="podStatusRemarks" name="podStatusRemarks">
                        </div>
                    </div>

                </div>

            </div>
            <%--            Form update button--%>
            <div class="modal-footer">
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" id="updatePodStatusBtn" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-info"
                                    data-dismiss="modal">Close
                            </button>

                        </div>
                    </div>
                </footer>
            </div>
        </div>

    </div>
</div>
<ui:footer/>

<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-booking-pipeline.js"></script>
