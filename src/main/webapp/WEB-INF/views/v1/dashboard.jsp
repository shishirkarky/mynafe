<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <section class="panel-group mb-xlg">
                <div class="widget-twitter-profile">
                    <div class="top-image">
                        <%--                        <img src="" alt="">--%>
                    </div>
                    <div class="profile-info">
                        <div class="profile-picture">
                            <%--                            <img src="" alt="">--%>
                        </div>
                        <div class="profile-account">
                            <h3 class="name text-semibold">Data Summary</h3>
                            <%--                            <a href="#" class="account">@</a>--%>
                            <br>
                            <br>
                        </div>
                        <ul class="profile-stats">
                            <li>
                                <h5 class="stat text-uppercase">Vendors</h5>
                                <h4 class="count" id="vendorsCount"></h4>
                            </li>
                            <li>
                                <h5 class="stat text-uppercase">Vehicles</h5>
                                <h4 class="count" id="vehiclesCount"></h4>
                            </li>
                            <li>
                                <h5 class="stat text-uppercase">Drivers</h5>
                                <h4 class="count" id="driversCount"></h4>
                            </li>
                            <li>
                                <h5 class="stat text-uppercase">Active Drivers</h5>
                                <h4 class="count" id="activeDriversCount"></h4>
                            </li>
                            <li>
                                <h5 class="stat text-uppercase">Active Vehicles</h5>
                                <h4 class="count" id="activeVehiclesCount"></h4>
                            </li>
                            <li>
                                <h5 class="stat text-uppercase">Pending Tasks</h5>
                                <h4 class="count" id="pendingTasksCount"></h4>
                            </li>
                            <li>
                                <h5 class="stat text-uppercase">Completed Tasks</h5>
                                <h4 class="count" id="completedTasksCount"></h4>
                            </li>
                        </ul>
                    </div>
                    <div class="profile-quote">
                        <%--                        <blockquote>--%>
                        <%--                            <p>--%>
                        <%--                                --%>
                        <%--                            </p>--%>
                        <%--                        </blockquote>--%>
                        <div class="quote-footer">
                            <span class="datetime" id="currentDate"></span>
                            <%--                            ---%>
                            <%--                            <a href="#">Details</a>--%>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                </div>

                <h2 class="panel-title">Vendor Booking Summary</h2>
                <p class="panel-subtitle"></p>
            </header>
            <div class="panel-body">
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="bookingBranchCode">Branch Code</label>
                                    <select data-plugin-selectTwo class="form-control populate"
                                            onchange="reloadVendorBookingSummary()" id="bookingBranchCode"
                                            name="bookingBranchCode">
                                        <option value="">Select Branch Code</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Bars -->
                <div class="chart chart-md" id="vendorBookingBarGraph"></div>
            </div>
        </section>
    </div>
<%--    <div class="col-md-4">--%>
<%--        <section class="panel">--%>
<%--            <header class="panel-heading">--%>
<%--                <div class="panel-actions">--%>

<%--                </div>--%>
<%--                <h2 class="panel-title">Reports</h2>--%>
<%--            </header>--%>
<%--            <div class="panel-body">--%>
<%--                <div class="row">--%>
<%--                    <div class="col-md-12">--%>
<%--                        <div class="form-group">--%>
<%--                            <label class="control-label" for="reportType">Report Type</label>--%>
<%--                            <select data-plugin-selectTwo class="form-control populate" id="reportType"--%>
<%--                                    name="reportType">--%>
<%--                                <option value="SALES_REPORT">--%>
<%--                                    Total Sales--%>
<%--                                </option>--%>
<%--                                <option value="TOTAL_PARCEL_REPORT">--%>
<%--                                    Total Parcel--%>
<%--                                </option>--%>
<%--                                <option value="HIRING_VEHICLE_REPORT">--%>
<%--                                    Hiring Vehicle--%>
<%--                                </option>--%>
<%--                                <option value="PARTIAL_BOOKING_REPORT">--%>
<%--                                    Partial Booking--%>
<%--                                </option>--%>
<%--                                <option value="COMPANY_COST_REPORT">--%>
<%--                                    Company Cost--%>
<%--                                </option>--%>
<%--                                <option value="ACTIVE_VEHICLE_REPORT">--%>
<%--                                    Active Vehicle--%>
<%--                                </option>--%>
<%--                            </select>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <footer class="panel-footer">--%>
<%--                <div class="row">--%>
<%--                    <div class="col-sm-9 ">--%>
<%--                        <button type="button" id="exportExcelReportBtn" onclick="exportExcelReport()"--%>
<%--                                class="btn btn-primary">Export--%>
<%--                            Excel--%>
<%--                        </button>--%>
<%--                        <button class="text-uppercase btn btn-success" id="advanceReportSearchBtn" onclick="loadReportsPage()">Advance Search</button>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </footer>--%>
<%--        </section>--%>
<%--    </div>--%>
</div>

<tags:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-dashboard.js"></script>
