<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#daily" data-toggle="tab"><i
                        class="fa fa-star"></i> Daily Report </a></li>
                <li><a href="#custom" data-toggle="tab"><i
                        class="fa fa-star"></i> Custom Report </a></li>
            </ul>

            <div class="tab-content">
                <%--                DAILY--%>
                <div id="daily" class="tab-pane active">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">DAILY REPORT</h2>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="dailyReportType">Select Report Type</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="dailyReportType"
                                                name="reportType">
                                            <option value="PARTIAL_BOOKING_REPORT" selected>
                                                Partial Booking
                                            </option>
                                            <option value="FULL_BOOKING_REPORT">
                                                Full Booking
                                            </option>
                                            <option value="LOCAL_BOOKING_REPORT">
                                                Local Booking
                                            </option>
                                            <option value="HIRING_VEHICLE_REPORT">
                                                Hiring Vehicle
                                            </option>
                                            <option value="SALES_REPORT">
                                                Total Sales
                                            </option>
                                            <option value="AGEING_REPORT">
                                                Ageing
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="vendor"> Select Vendor</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="vendor"
                                                name="vendor">
                                            <option value="0">Select Vendor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="office">Select Office</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="office"
                                                name="office">
                                            <option value="">Select Office</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="vehicleType">Select Vehicle Type</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="vehicleType"
                                                name="vehicleType">
                                            <option value="" selected>Select Vehicle Type</option>
                                            <option value="PRIVATE">
                                                PRIVATE
                                            </option>
                                            <option value="COMPANY_OWN">
                                                COMPANY OWN
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="driver">Select Driver Name</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="driver"
                                                name="driver">
                                            <option value="0">Select Driver Name</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <footer class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-9 ">
                                        <button type="button" id="dailySearchReportBtn" onclick="downloadDailyReport()" class="btn btn-primary">Download
                                            Daily Report (Excel)
                                        </button>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </section>
                </div>
                <%--                CUSTOM--%>
                <div id="custom" class="tab-pane">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                            </div>

                            <h2 class="panel-title">Custom Report Search</h2>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="cReportType">Select Report Type</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="cReportType"
                                                name="cReportType">
                                            <option value="PARTIAL_BOOKING_REPORT" selected>
                                                Partial Booking
                                            </option>
                                            <option value="FULL_BOOKING_REPORT">
                                                Full Booking
                                            </option>
                                            <option value="LOCAL_BOOKING_REPORT">
                                                Local Booking
                                            </option>
                                            <option value="HIRING_VEHICLE_REPORT">
                                                Hiring Vehicle
                                            </option>
                                            <option value="SALES_REPORT">
                                                Total Sales
                                            </option>
                                            <option value="AGEING_REPORT">
                                                Ageing
                                            </option>
<%--                                            <option value="TOTAL_PARCEL_REPORT">--%>
<%--                                                Total Parcel--%>
<%--                                            </option>--%>
<%--                                            <option value="COMPANY_COST_REPORT">--%>
<%--                                                Company Cost--%>
<%--                                            </option>--%>
<%--                                            <option value="ACTIVE_VEHICLE_REPORT">--%>
<%--                                                Active Vehicle--%>
<%--                                            </option>--%>


                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"> From Dispatch Date </label>
                                        <input type="text" class="form-control date-picker" data-single="true" id="fromDate"
                                               name="fromDate" placeholder="From Dispatch Date"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"> To Dispatch Date </label>
                                        <input type="text" class="form-control date-picker" data-single="true" id="toDate"
                                               name="toDate" placeholder="To Dispatch Date"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="cVendor"> Select Vendor</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="cVendor"
                                                name="cVendor">
                                            <option value="0">Select Vendor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="cOffice">Select Office</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="cOffice"
                                                name="cOffice">
                                            <option value="">Select Office</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="cVehicleType">Select Vehicle Type</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="cVehicleType"
                                                name="cVehicleType">
                                            <option value="" selected>Select Vehicle Type</option>
                                            <option value="PRIVATE">
                                                PRIVATE
                                            </option>
                                            <option value="COMPANY_OWN" >
                                                COMPANY OWN
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="cDriver">Select Driver Name</label>
                                        <select data-plugin-selectTwo class="form-control populate" id="cDriver"
                                                name="cDriver">
                                            <option value="0" selected>Select Driver Name</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <footer class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-9 ">
                                        <button type="button" id="customSearchReportBtn" onclick="downloadCustomReport()" class="btn btn-primary">Download
                                            Custom Report (Excel)
                                        </button>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<ui:loadingModal/>
<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-reports-search.js"></script>