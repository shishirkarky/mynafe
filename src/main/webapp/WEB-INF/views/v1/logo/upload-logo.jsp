<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#popular" data-toggle="tab"><i
                        class="fa fa-star"></i> Logo</a></li>
            </ul>
            <div class="tab-content">
                <!-- Display details -->
                <div id="popular" class="tab-pane active">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Logo upload</h2>
                        </header>
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <input type="file" id="logoFileBytes" name="logoFileBytes"
                                           class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <button type="button" id="uploadLogoBtn" onclick="uploadLogo(${id})" class="btn btn-primary">
                                        Upload
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <img src="" id="mynaLogo"
                                         alt="logo" style="width:100%;height:100%;object-fit:cover">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
</div>

<ui:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-upload-logo.js"></script>
