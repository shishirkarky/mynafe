<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:jsFiles/>
<script src="${pageContext.request.contextPath }/javascripts/download.js"></script>

<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-navigation.js"></script>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-logout.js"></script>
