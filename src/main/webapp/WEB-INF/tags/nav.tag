<%--SUPER ADMIN MENU--%>
<div id="superAdminNavigation">
    <nav id="menu" class="nav-main" role="navigation">
        <ul class="nav nav-main">
            <%--        DASHBOARD--%>

            <%--            ADMIN SETTINGS--%>
                <%--                    USER--%>
                <li><a id="userCreatePageBtn"><i
                        class="fa fa-users" aria-hidden="true"></i> <span>User Settings</span></a></li>
                <%--                    OFFICE--%>
                <li><a id="officeCreatePageBtn"><i
                        class="fa fa-briefcase" aria-hidden="true"></i> <span>Office Settings</span></a></li>
                <%--                    DRIVER--%>
                <li><a id="driverCreatePageBtn"><i
                        class="fa fa-user" aria-hidden="true"></i> <span>Driver Settings</span></a></li>
                <%--                    STAFFS--%>
                <li><a id="staffsCreatePageBtn"><i
                        class="fa fa-users" aria-hidden="true"></i> <span>Staff Settings</span></a></li>
                <%--                    VEHICLE--%>
                <li><a id="vehicleCreatePageBtn"><i
                        class="fa fa-automobile" aria-hidden="true"></i> <span>Vehicle Settings</span></a></li>
                <%--                    VENDOR--%>
                <li><a id="vendorCreatePageBtn"><i
                        class="fa fa-building" aria-hidden="true"></i> <span>Vendor Settings</span></a></li>
                <%--                        LOCATION--%>
                <li>
                    <a id="configLocationsCreatePageBtn"><i class="fa fa-location-arrow"
                                                            aria-hidden="true"></i>
                        <span>Location Settings</span></a>
                    <%--                            RATE --%>
                <li>
                    <a id="configRatesCreatePageBtn"><i class="fa fa-money" aria-hidden="true"></i>
                        <span>Rates Settings</span></a>
                </li>
                <%--                            LOGO --%>
                <li>
                    <a id="logoUploadPageBtn"><i class="fa fa-photo" aria-hidden="true"></i>
                        <span>Logo Settings</span></a>
                </li>
                <%--                        PACKAGE--%>
                <li>
                    <a id="configPackagesCreatePageBtn"><i class="fa fa-shopping-cart"
                                                           aria-hidden="true"></i>
                        <span> Packages Settings</span></a>
                </li>
        </ul>
    </nav>
</div>