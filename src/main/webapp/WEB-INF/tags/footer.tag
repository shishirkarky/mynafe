<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
</section>
</div>

<aside id="sidebar-right" class="sidebar-right">
    <div class="nano">
        <div class="nano-content">
            <a href="#" class="mobile-close visible-xs">
                Collapse <i class="fa fa-chevron-right"></i>
            </a>

            <div class="sidebar-right-wrapper">

                <div class="sidebar-widget widget-calendar">
                    <h6>English Calendar</h6>
                    <div data-plugin-datepicker data-plugin-skin="dark"></div>
                </div>
            </div>
        </div>
    </div>
</aside>
</section>
<tags:errorResponse/>
<tags:js/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-footer.js"></script>
</body>
</html>