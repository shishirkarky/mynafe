<!-- Vendor -->
<script src="${pageContext.request.contextPath }/vendor/jquery/jquery.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="${pageContext.request.contextPath }/vendor/bootstrap/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath }/vendor/nanoscroller/nanoscroller.js"></script>
<script src="${pageContext.request.contextPath }/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="${pageContext.request.contextPath }/vendor/magnific-popup/magnific-popup.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Specific Page Vendor -->
<script src="${pageContext.request.contextPath }/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath }/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
<script src="${pageContext.request.contextPath }/vendor/flot/jquery.flot.js"></script>
<script src="${pageContext.request.contextPath }/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
<script src="${pageContext.request.contextPath }/vendor/flot/jquery.flot.pie.js"></script>
<script src="${pageContext.request.contextPath }/vendor/flot/jquery.flot.categories.js"></script>
<script src="${pageContext.request.contextPath }/vendor/flot/jquery.flot.resize.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-sparkline/jquery.sparkline.js"></script>
<script src="${pageContext.request.contextPath }/vendor/raphael/raphael.js"></script>
<script src="${pageContext.request.contextPath }/vendor/morris/morris.js"></script>
<script src="${pageContext.request.contextPath }/vendor/gauge/gauge.js"></script>
<script src="${pageContext.request.contextPath }/vendor/snap-svg/snap.svg.js"></script>
<!-- Specific Page Vendor -->
<script src="${pageContext.request.contextPath }/vendor/select2/select2.js"></script>

<%--DataTable--%>
<script src="${pageContext.request.contextPath }/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/dataTables.select.min.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/dataTables.buttons.min.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/buttons.html5.min.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/dataTables.searchBuilder.min.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/dataTables.dateTime.min.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/dataTables.bootstrap5.min.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/searchBuilder.bootstrap5.min.js"></script>
<script src="${pageContext.request.contextPath }/dataTable/js/jszip.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="${pageContext.request.contextPath }/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="${pageContext.request.contextPath }/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="${pageContext.request.contextPath }/javascripts/theme.init.js"></script>

<!-- Nepali Date Picker -->
<script src="${pageContext.request.contextPath }/javascripts/nepali-date-picker.min.js"></script>
<script>
  $(function(){
    $('.date-picker').nepaliDatePicker();
  });
</script>


<script src="${pageContext.request.contextPath }/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
<script src="${pageContext.request.contextPath }/toast/jquery.toast.min.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-nestable/jquery.nestable.js"></script>
<script src="${pageContext.request.contextPath }/jquery-ui/jquery-ui.min.js"></script>

<script src="${pageContext.request.contextPath }/vendor/jquery-autosize/jquery.autosize.js"></script>
<script src="${pageContext.request.contextPath }/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
