<!-- Modal -->
<div class="modal fade" id="errorResponse" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Error</h5>
            </div>
            <div class="modal-body">
                <p>
                    Status Code: <span id="statusCode"></span>
                </p>
                <p>
                    Message: <span id="msg"></span>
                </p>
                <h5>Errors: <span id="errorsCount"></span></h5>
                <div id="errors" style="color: red"></div>
            </div>
        </div>
    </div>
</div>