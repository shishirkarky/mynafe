<style>
  a {
    cursor: pointer;
  }

  .nav-main a:hover {
    color: yellow;
  }
</style>
<!-- Vendor CSS -->
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/vendor/bootstrap/css/bootstrap.css"/>
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/vendor/font-awesome/css/font-awesome.css"/>
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/vendor/magnific-popup/magnific-popup.css"/>
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/vendor/bootstrap-datepicker/css/datepicker3.css"/>

<!-- Specific Page Vendor CSS -->
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"/>
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/vendor/morris/morris.css"/>

<!-- Specific Page Vendor CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath }/vendor/select2/select2.css"/>

<%--Datatable CSS--%>
<link rel="stylesheet" href="${pageContext.request.contextPath }/dataTable/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath }/dataTable/css/select.dataTables.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath }/dataTable/css/buttons.dataTables.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath }/dataTable/css/searchBuilder.dataTables.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath }/dataTable/css/dataTables.dateTime.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath }/dataTable/css/dataTables.bootstrap5.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath }/dataTable/css/searchBuilder.bootstrap5.min.css"/>

<!-- Theme CSS -->
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/stylesheets/theme.css"/>

<!-- Skin CSS -->
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/stylesheets/skins/default.css"/>

<!-- Theme Custom CSS -->
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/stylesheets/theme-custom.css">

<!-- Nepali Date Picker CSS -->
<link rel="stylesheet"
      href="${pageContext.request.contextPath }/stylesheets/nepali-date-picker.min.css"/>


<link rel="stylesheet" href="${pageContext.request.contextPath }/toast/jquery.toast.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath }/jquery-ui/jquery-ui.css">

<script
  src="${pageContext.request.contextPath }/vendor/modernizr/modernizr.js"></script>
