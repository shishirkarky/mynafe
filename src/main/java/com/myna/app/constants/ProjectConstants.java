package com.myna.app.constants;

public class ProjectConstants {
  private ProjectConstants() {
  }

  public static final String UPLOAD_FILE_PATH = "/opt/myna/uploads";
}
