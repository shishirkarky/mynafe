/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.constants;

public class JspPageAPIConstants {
    private JspPageAPIConstants() {

    }

    public static final String SPRING_SECURITY_CONTEXT = "SPRING_SECURITY_CONTEXT";
    public static final String V1_LOGIN = "/v1/login-page";
    public static final String V1_DASHBOARD = "/v1/dashboard-page";
    public static final String V1_USER_CREATE = "/v1/user-create-page";
    public static final String V1_USER_VIEW = "/v1/user-view-page";
    public static final String V1_USER_PROFILE_VIEW = "/v1/user-profile-page";

    public static final String V1_DRIVER_CREATE = "/v1/driver-create-page";

    public static final String V1_VEHICLE_CREATE = "/v1/vehicle-create-page";

    public static final String V1_VEHICLE_LOG_BOOK_CREATE = "/v1/vehiclelogbook-create-page";
    public static final String V1_VEHICLE_LOG_BOOK_VIEW = "/v1/vehiclelogbook-view-page";

    public static final String V1_CUSTOMER_CREATE = "/v1/customer-create-page";
    public static final String V1_CUSTOMER_VIEW = "/v1/customer-view-page";

    public static final String V1_VENDOR_CREATE = "/v1/vendor-create-page";

    public static final String V1_OFFICE_CREATE = "/v1/office-create-page";

    public static final String V1_STAFF_CREATE = "/v1/staff-create-page";

    public static final String V1_VENDOR_REQUEST_CREATE = "/v1/vendor-request-create-page";
    public static final String V1_VENDOR_REQUEST_VIEW = "/v1/vendor-request-view-page";
    public static final String V1_BOOKING_PIPELINE_CREATE = "/v1/booking-pipeline-page";

    public static final String V1_CASH_RECEIPT_CREATE = "/v1/cash-receipt-create-page";
    public static final String V1_CASH_RECEIPT_VIEW = "/v1/cash-receipt-view-page";
    public static final String V1_CASH_RECEIPT_EDIT = "/v1/cash-receipt-edit-page";
    public static final String V1_CASH_RECEIPT_DETAIL_VIEW = "/v1/cash-receipt-detail-view-page";

    public static final String V1_FILE_UPLOADS_VIEW = "/v1/file-uploads-view-page";

    public static final String V1_CONFIG_LOCATION_CREATE = "/v1/config-locations-create-page";

    public static final String V1_CONFIG_PACKAGES_CREATE = "/v1/config-packages-create-page";

    public static final String V1_CONFIG_RATE_CONFIGURATION = "/v1/config-rates-create-page";

    public static final String V1_UPLOAD_LOGO = "/v1/upload-logo";

    public static final String V1_REPORTS_SEARCH = "/v1/reports-search-page";

    public static final String V1_TRANSITION_CREATE = "/v1/transitions-create-page";

    public static final String V1_HIRING_VEHICLE_RATE_CREATE = "/v1/hiring-vehicle-rate-create-page";

    public static final String V1_POD_UPLOADS_VIEW = "/v1/pod-uploads-view-page";

    public static final String V1_PARCEL_TRACKING = "/public/v1/parcel-tracking";

}
