/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.utils;

import org.springframework.util.StringUtils;

public class NameUtility {
    public static String getName(String firstName, String middleName, String lastName){
        if(!StringUtils.isEmpty(firstName)
                && !StringUtils.isEmpty(middleName)
                && !StringUtils.isEmpty(lastName)){
            return firstName + " " + middleName + " " + lastName;
        }else if(!StringUtils.isEmpty(firstName)
                && !StringUtils.isEmpty(lastName)){
            return firstName + " " + lastName;
        }else{
            return firstName;
        }
    }
}
