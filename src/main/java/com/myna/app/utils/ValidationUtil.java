package com.myna.app.utils;

import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.service.UserService;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.service.VendorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ValidationUtil {
  private UserService userService;
  private VendorRequestService vendorRequestService;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setVendorRequestService(VendorRequestService vendorRequestService) {
    this.vendorRequestService = vendorRequestService;
  }

  public boolean validateBookingUpdate(int bookingId) {
    Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getOneVendorRequest(bookingId);
    return validateBooking(vendorRequestOptional);
  }

  public boolean validatePodUpdate(int podId) {
    Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getByPodId(podId);
    return validateBooking(vendorRequestOptional);
  }

  private boolean validateBooking(Optional<VendorRequest> vendorRequestOptional) {
    if (vendorRequestOptional.isPresent() && vendorRequestOptional.get().getDriver() == null) {
      return true;
    } else {
      return vendorRequestOptional.isPresent() && userService.getCurrentUserRoles().contains(String.valueOf(ERole.ROLE_SUPER_ADMIN));
    }
  }
}
