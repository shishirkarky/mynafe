/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.utils.dates;

import com.myna.app.apiresponse.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("impl/api/v1")
public class DatesController {
    private DatesRepository datesRepository;
    private DateUtility dateUtility;

    @Autowired
    public void setDatesRepository(DatesRepository datesRepository) {
        this.datesRepository = datesRepository;
    }

    @Autowired
    public void setDateUtility(DateUtility dateUtility) {
        this.dateUtility = dateUtility;
    }

    @GetMapping(value = "conversions/eton/{englishDate}")
    public ResponseEntity<?> convertEnglishToNepaliDate(@PathVariable String englishDate) {
        Dates dates = datesRepository.findByEnglishdate(englishDate);
        if (dates != null) {
            return new ApiResponse().getSuccessResponse(null, dates.getNepalidate());
        }
        return new ApiResponse().getSuccessResponse(null, "");
    }

    @GetMapping(value = "conversions/ntoe/{nepaliDate}")
    public ResponseEntity<?> convertNepaliToEnglishDate(@PathVariable String nepaliDate) {
        Dates dates = datesRepository.findByNepalidate(nepaliDate);
        if (dates != null) {
            return new ApiResponse().getSuccessResponse(null, dates.getEnglishdate());
        }
        return new ApiResponse().getSuccessResponse(null, "");
    }

    @GetMapping(value = "/dates/todays/nepali")
    public ResponseEntity<?> getCurrentNepaliDate(){
        return new ApiResponse().getSuccessResponse(null, dateUtility.todayNepaliDate());
    }

    @GetMapping(value = "/dates/todays/english")
    public ResponseEntity<?> getCurrentEnglishDate(){
        return new ApiResponse().getSuccessResponse(null, dateUtility.todayEnglishDate());
    }

}
