/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.utils.jasper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sf.jasperreports.engine.JRDataSource;

import java.sql.Connection;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JasperRequestDto {
    private String jrxmlFileLocation;
    private Map<String, Object> parameters;
    private JRDataSource jrDataSource;
    private Connection connection;
    private String downloadFileLocation;
    private String outputFileName;
    private EFileType outputFileType = EFileType.PDF;

    public String getDownloadFilePathName() {
        if (!downloadFileLocation.endsWith("/")) {
            downloadFileLocation = downloadFileLocation + "/";
        }
        return downloadFileLocation.concat(outputFileName).concat(outputFileType.getExtension());
    }

    public String getOutputFileNameWithExtension() {
        return outputFileName + outputFileType.getExtension();
    }
}
