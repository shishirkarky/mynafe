/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.utils.jasper;

import lombok.Getter;

@Getter
public enum EFileType {
    PDF(".pdf"),
    EXCEL(".xls"),
    CSV("csv"),
    HTML(".html");
    public final String extension;

    EFileType(String extension) {
        this.extension = extension;
    }
}
