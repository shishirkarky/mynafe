package com.myna.app.utils;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.util.List;


@Slf4j
@Component
public class PdfUtility {
  public ByteArrayOutputStream downloadImagesToPdf(List<String> imageUrls) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try (PdfWriter writer = new PdfWriter(baos);
         PdfDocument pdf = new PdfDocument(writer);
         Document document = new Document(pdf)) {

      for (String url : imageUrls) {
        this.addImage(url, document);
      }
      return baos;
    } catch (Exception e) {
      log.error("images {} to pdf error", imageUrls, e);
    }
    return null;
  }

  private void addImage(String url, Document document) {
    try {
      ImageData data = ImageDataFactory.create(url);
      Image image = new Image(data);
      image.scaleAbsolute(400, 400);
      document.add(image);
      document.add(new Paragraph("\n"));
    } catch (MalformedURLException e) {
      log.error("adding image {} to pdf error", url, e);
    }
  }
}
