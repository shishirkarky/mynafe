package com.myna.app.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public class CodeGenerationUtils {
    public static String generateCode(String existingCode, String currentDate) {
        if (null != existingCode && !StringUtils.isEmpty(existingCode)) {
            String[] dnSplit = existingCode.split(currentDate);
            try {
                int count = Integer.parseInt(dnSplit[1]);
                return currentDate + String.format("%05d", count + 1);
            } catch (NumberFormatException ex) {
                log.warn("generateCode() : Unable to parse split [1] by current date {}", currentDate);
            }
        }

        return currentDate + String.format("%05d", 1);
    }
}
