/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendor.dto;

import com.myna.app.vendor.enums.DeliveryMode;
import com.myna.app.vendor.enums.ERateCalculationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class VendorDto {

    private int id;

    private String description;

    private String dispatchLocation;

    private DeliveryMode deliveryMode;

    private String businessType;

    private String name;

    private String panVat;

    private String phone;

    private String email;

    private int refMainVendorId;
    private VendorDto mainVendor;

    private ERateCalculationType rateCalculationType;

}
