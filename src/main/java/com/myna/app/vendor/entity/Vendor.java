/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vendor.entity;

import com.myna.app.commons.Auditable;
import com.myna.app.enums.Status;
import com.myna.app.vendor.enums.DeliveryMode;
import com.myna.app.vendor.enums.ERateCalculationType;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity(name = "vendor")
public class Vendor extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String description;

	private String dispatchLocation;

	@Enumerated(EnumType.STRING)
	private DeliveryMode deliveryMode;

	private String businessType;

	private String name;

	private String panVat;

	private String phone;

	private String email;

	@Enumerated(EnumType.STRING)
	private Status status = Status.ACTIVE;

	private int refMainVendorId;

	@Enumerated(EnumType.STRING)
	private ERateCalculationType rateCalculationType;

}
