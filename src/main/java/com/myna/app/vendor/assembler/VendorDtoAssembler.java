/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vendor.assembler;

import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.vendor.dto.VendorDto;
import com.myna.app.vendor.entity.Vendor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VendorDtoAssembler {

	public Vendor toDomain(VendorDto vendorDto) {
		Vendor vendor = new Vendor();
		CustomBeanUtils.copyNonNullProperties(vendorDto, vendor);
		return vendor;
	}

	public VendorDto toModel(Vendor vendor) {
		VendorDto vendorDto = new VendorDto();
		CustomBeanUtils.copyNonNullProperties(vendor, vendorDto);
		return vendorDto;
	}

	public List<VendorDto> toModels(List<Vendor> vendors) {
		List<VendorDto> VendorDtos = new ArrayList<>();
		vendors.forEach(vendor -> VendorDtos.add(this.toModel(vendor)));
		return VendorDtos;
	}

}
