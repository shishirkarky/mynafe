/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vendor.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.enums.Status;
import com.myna.app.vendor.assembler.VendorDtoAssembler;
import com.myna.app.vendor.dto.VendorDto;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendor.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class VendorController {

    private VendorService vendorService;

    private VendorDtoAssembler vendorDtoAssembler;

    @Autowired
    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    @Autowired
    public void setVendorDtoAssembler(VendorDtoAssembler vendorDtoAssembler) {
        this.vendorDtoAssembler = vendorDtoAssembler;
    }

    @PostMapping("/vendors")
    public ResponseEntity<?> createVendor(@RequestBody VendorDto vendorDto) {
        Optional<Vendor> vendorOptional = vendorService.createVendor(vendorDtoAssembler.toDomain(vendorDto));
        if (vendorOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vendorDtoAssembler.toModel(vendorOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/vendors/{id}")
    public ResponseEntity<?> updateVendor(@PathVariable int id, @RequestBody VendorDto vendorDto) {
        Optional<Vendor> vendorOptional = vendorService.updateVendor(id, vendorDtoAssembler.toDomain(vendorDto));
        if (vendorOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vendorDtoAssembler.toModel(vendorOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);

    }

    @GetMapping("/vendors/{id}")
    public ResponseEntity<?> getOneVendor(@PathVariable int id) {
        Optional<Vendor> vendorOptional = vendorService.getOneVendor(id);
        if (vendorOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vendorDtoAssembler.toModel(vendorOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/vendors")
    public ResponseEntity<?> getAllVendors() {
        return new ApiResponse().getSuccessResponse(null, vendorDtoAssembler.toModels(vendorService.getAllVendors()));
    }

    @GetMapping("/vendors/mains")
    public ResponseEntity<?> getAllMainVendors() {
        return new ApiResponse().getSuccessResponse(null, vendorDtoAssembler.toModels(vendorService.getAllMainVendors()));
    }

    @GetMapping("/vendors/subs")
    public ResponseEntity<?> getAllSubVendors() {
        return new ApiResponse().getSuccessResponse(null, vendorDtoAssembler.toModels(vendorService.getAllSubVendors()));
    }

    @GetMapping("/vendors/{id}/{status}")
    public ResponseEntity<?> updateVendorStatus(@PathVariable int id, @PathVariable Status status) {
        Optional<Vendor> vendorOptional = vendorService.updateVendorStatus(id, status);
        if (vendorOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vendorDtoAssembler.toModel(vendorOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

}
