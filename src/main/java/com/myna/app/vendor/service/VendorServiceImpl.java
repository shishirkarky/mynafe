/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendor.service;

import com.myna.app.enums.Status;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendor.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VendorServiceImpl implements VendorService {

    private VendorRepository vendorRepository;

    @Autowired
    public void setVendorRepository(VendorRepository vendorRepository) {
        this.vendorRepository = vendorRepository;
    }

    @Override
    public int countTotal() {
        return (int) vendorRepository.count();
    }

    @Override
    public Optional<Vendor> createVendor(Vendor vendor) {
        return Optional.of(vendorRepository.save(vendor));
    }

    @Override
    public Optional<Vendor> updateVendor(int id, Vendor vendor) {
        Optional<Vendor> vendorOptional = vendorRepository.findById(id);
        if (vendorOptional.isPresent()) {
            vendor.setId(id);

            Vendor existingVendor = vendorOptional.get();
            CustomBeanUtils.copyNonNullProperties(vendor, existingVendor);
            return Optional.of(vendorRepository.save(existingVendor));
        }
        return Optional.empty();
    }

    @Override
    public List<Vendor> getAllVendors() {
        return vendorRepository.findAll();
    }

    @Override
    public List<Vendor> getAllMainVendors() {
        return vendorRepository.findAllByRefMainVendorId(0);
    }

    @Override
    public List<Vendor> getAllSubVendors() {
        return vendorRepository.findAllByRefMainVendorIdNot(0);
    }

    @Override
    public Optional<Vendor> getOneVendor(int id) {
        return vendorRepository.findById(id);
    }

    @Override
    public Optional<Vendor> updateVendorStatus(int id, Status status) {
        Optional<Vendor> vendorOptional = vendorRepository.findById(id);
        if (vendorOptional.isPresent()) {
            Vendor existingVendor = vendorOptional.get();
            existingVendor.setStatus(status);
            return Optional.of(vendorRepository.save(existingVendor));
        }
        return Optional.empty();
    }

}
