/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendor.service;

import com.myna.app.vendor.entity.Vendor;
import com.myna.app.enums.Status;

import java.util.List;
import java.util.Optional;

public interface VendorService {
    int countTotal();

    Optional<Vendor> createVendor(Vendor vendor);

    Optional<Vendor> updateVendor(int id, Vendor vendor);

    List<Vendor> getAllVendors();

    List<Vendor> getAllMainVendors();

    List<Vendor> getAllSubVendors();

    Optional<Vendor> getOneVendor(int id);

    Optional<Vendor> updateVendorStatus(int id, Status status);

}
