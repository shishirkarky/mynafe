/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.reports;

import com.myna.app.reports.excel.ExcelReportService;
import com.myna.app.reports.jasper.ReportsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class ReportsController {

    private ReportsService reportsService;
    private ExcelReportService excelReportService;

    @Autowired
    public void setReportsService(ReportsService reportsService) {
        this.reportsService = reportsService;
    }

    @Autowired
    public void setExcelReportService(ExcelReportService excelReportService) {
        this.excelReportService = excelReportService;
    }

    @GetMapping(value = "reports/pdf/pods/{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] generatePodJasperReport(@PathVariable int id) {
        return reportsService.generatePodJasperReport(id);
    }

    @GetMapping(value = "reports/pdfs/bookings/{bookingId}/gatepasses", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] generateGatePassJasperReport(@PathVariable int bookingId) {
        return reportsService.generateGatePassJasperReport(bookingId);
    }

    @GetMapping(value = "reports/bookings/{bookingId}/vcts", produces = "application/vnd.ms-excel")
    public InputStreamResource generateVctsJasperReport(@PathVariable int bookingId) {
        return reportsService.generateVctsExcelReport(bookingId);
    }

    @GetMapping(value = "reports/bookings/{bookingId}/pdf/pods")
    public void generatePodsForBookingJasperReport(@PathVariable int bookingId, HttpServletResponse response) {
        reportsService.generatePodsForBookingJasperReport(response, bookingId);
    }

    /**
     * <p>
     * This API generates excel report for all the reports in {@link EReportType}.
     * </p>
     * todo add from date and to date which will further add in filter data for each report,
     * todo set file name while downloading
     *
     * @return download excel file
     */
    @GetMapping(value = "/reports/excel/{reportType}", produces = "application/vnd.ms-excel")
    public InputStreamResource getReportInExcel(@PathVariable EReportType reportType, @ModelAttribute ExcelReportRequest request) {
        log.info("Extraction report called for reportType {}, report request {}", reportType, request.toString());
        ReportDetailsDto reportDetailsDto = excelReportService.getExcelReportDataForExport(reportType, request);
        return reportDetailsDto.getFile();
    }
}
