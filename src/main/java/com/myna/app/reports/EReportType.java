/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.reports;

public enum EReportType {
    SALES_REPORT,
    TOTAL_PARCEL_REPORT,
    PARTIAL_BOOKING_REPORT,
    FULL_BOOKING_REPORT,
    HIRING_VEHICLE_REPORT,
    COMPANY_COST_REPORT,
    ACTIVE_VEHICLE_REPORT,
    VCTS_REPORT,
    LOCAL_BOOKING_REPORT,
    AGEING_REPORT
}
