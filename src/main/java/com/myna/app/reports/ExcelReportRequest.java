package com.myna.app.reports;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ExcelReportRequest {
    private EReportPeriod reportPeriod;
    private String fromDate;
    private String toDate;
    private String branchCode;
    private String vehicleType;
    private int driverId;
    private int vendorId;
}
