package com.myna.app.reports.sales.dto;

import com.myna.app.pod.CashReceiptDto;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SalesReportExcelReportDto extends VendorRequestDto {
  List<CashReceiptDto> pods;
}
