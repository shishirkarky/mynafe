/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.reports.excel;

import com.myna.app.utils.NameUtility;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class CompanyCostReportService implements ExcelExportService {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<VendorRequestDto> vendorRequestDtos;

    public CompanyCostReportService(List<VendorRequestDto> vendorRequestDtos) {
        this.vendorRequestDtos = vendorRequestDtos;
        workbook = new XSSFWorkbook();
    }

    @Override
    public void writeHeaderLine() {
        sheet = workbook.createSheet("Company Cost");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Date", style);
        createCell(row, 1, "Vehicle Number", style);
        createCell(row, 2, "Fuel Amount", style);
        createCell(row, 3, "Dhala and Palti expenses(Labour Expenses)", style);
        createCell(row, 4, "Maintainance", style);
        createCell(row, 5, "Other Expenses", style);
        createCell(row, 6, "Driver name", style);
        createCell(row, 7, "Bill confirmed by", style);
        createCell(row, 8, "Kilometer of vehicle  (millage calculation)", style);
        createCell(row, 9, "Driver Advance ( if taken for any Trip)", style);
    }

    @Override
    public void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
        if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    @Override
    public void writeDataLines() {
        AtomicInteger rowCount = new AtomicInteger(1);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        vendorRequestDtos.forEach(vendorRequestDto -> {
            Row row = sheet.createRow(rowCount.getAndIncrement());
            AtomicInteger columnCount = new AtomicInteger();

            createCell(row, columnCount.getAndIncrement(), "", style);

            if (null != vendorRequestDto.getVehicle()) {
                createCell(row, columnCount.getAndIncrement(), vendorRequestDto.getVehicle().getRegistrationNumber(), style);
            } else {
                createCell(row, columnCount.getAndIncrement(), "", style);
            }

            createCell(row, columnCount.getAndIncrement(), vendorRequestDto.getFuelAmount(), style);
            createCell(row, columnCount.getAndIncrement(), vendorRequestDto.getDhalaAndPaltiExpenses() + vendorRequestDto.getLabourExpenses(), style);
            createCell(row, columnCount.getAndIncrement(), vendorRequestDto.getMaintainanceAmount(), style);
            createCell(row, columnCount.getAndIncrement(), vendorRequestDto.getOtherExpenses(), style);

            if (null != vendorRequestDto.getDriver()) {
                createCell(row, columnCount.getAndIncrement(),
                        NameUtility.getName(vendorRequestDto.getDriver().getFirstName(),
                                vendorRequestDto.getDriver().getMiddleName(),
                                vendorRequestDto.getDriver().getLastName())
                        , style);
            } else {
                createCell(row, columnCount.getAndIncrement(), "", style);
            }

            //todo what is bill confirmed by?
            createCell(row, columnCount.getAndIncrement(), "", style);
            //todo how to calculate km of vehicle from booking?
            createCell(row, columnCount.getAndIncrement(), "", style);
            createCell(row, columnCount.getAndIncrement(), vendorRequestDto.getAdvancePaymentAmount(), style);
        });

    }

    @Override
    public ByteArrayInputStream export() throws IOException {
        writeHeaderLine();
        writeDataLines();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);

        return new ByteArrayInputStream(out.toByteArray());
    }
}
