/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.reports.excel;

import com.myna.app.pod.CashReceiptDto;
import com.myna.app.pod.PodService;
import com.myna.app.reports.*;
import com.myna.app.reports.sales.dto.SalesReportExcelReportDto;
import com.myna.app.utils.dates.DateUtility;
import com.myna.app.vehicle.enums.VehicleType;
import com.myna.app.vehicleLogBook.VehicleLogBook;
import com.myna.app.vehicleLogBook.VehicleLogBookService;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.enums.BookingType;
import com.myna.app.vendorrequest.service.VendorRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ExcelReportService {
  private PodService podService;
  private VendorRequestService vendorRequestService;
  private VendorRequestDtoAssembler vendorRequestDtoAssembler;
  private VehicleLogBookService vehicleLogBookService;
  private DateUtility dateUtility;

  @Autowired
  public void setPodService(PodService podService) {
    this.podService = podService;
  }

  @Autowired
  public void setVendorRequestService(VendorRequestService vendorRequestService) {
    this.vendorRequestService = vendorRequestService;
  }

  @Autowired
  public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
    this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
  }

  @Autowired
  public void setVehicleLogBookService(VehicleLogBookService vehicleLogBookService) {
    this.vehicleLogBookService = vehicleLogBookService;
  }

  @Autowired
  public void setDateUtility(DateUtility dateUtility) {
    this.dateUtility = dateUtility;
  }

  public ReportDetailsDto getExcelReportDataForExport(EReportType reportType, ExcelReportRequest request) {
    String filename = null;
    InputStreamResource file = null;

    switch (reportType) {
      case PARTIAL_BOOKING_REPORT:
        file = this.getPartialBookingReport(request);
        break;
      case FULL_BOOKING_REPORT:
        file = this.getFullBookingReport(request);
        break;
      case LOCAL_BOOKING_REPORT:
        file = this.getLocalBookingReport(request);
        break;
      case HIRING_VEHICLE_REPORT:
        file = this.getHiringVehicleReport(request);
        break;
      case SALES_REPORT:
        file = this.getSalesReport(request);
        break;
      case AGEING_REPORT:
        file = this.getAgeingReport(request);
        break;
      default:
    }

    ReportDetailsDto reportDetailsDto = new ReportDetailsDto();
    reportDetailsDto.setFileName(filename);
    reportDetailsDto.setFile(file);

    return reportDetailsDto;
  }

  private InputStreamResource getAgeingReport(ExcelReportRequest request) {
    try {
      List<CashReceiptDto> pods = this.getCashReceipts(BookingType.PARTIAL_BOOKING, request.getReportPeriod(), request.getFromDate(), request.getToDate());

      List<CashReceiptDto> filteredPods = this.filterPods(request, pods);

      AgeingReportService ageingReportService = new AgeingReportService(filteredPods);
      return new InputStreamResource(ageingReportService.export());
    } catch (IOException ex) {
      log.error("getAgeingReport() : IO Exception for Ageing report", ex);
    }
    return null;
  }

  private InputStreamResource getSalesReport(ExcelReportRequest request) {
    try {

      List<VendorRequest> bookings = this.getBookings(null, request.getReportPeriod(), request.getFromDate(), request.getToDate());
      List<VendorRequestDto> filteredBookings = this.filterBooking(request, vendorRequestDtoAssembler.toModels(bookings));

      List<SalesReportExcelReportDto> salesReport = new ArrayList<>();
      filteredBookings.forEach(vendorRequestDto -> {
        SalesReportExcelReportDto report = new SalesReportExcelReportDto();
        List<CashReceiptDto> cashReceipts = podService.getAllCashReceiptsByBookingId(vendorRequestDto.getId());
        BeanUtils.copyProperties(vendorRequestDto, report);
        report.setPods(cashReceipts);

        salesReport.add(report);
      });
      SalesReportService salesReportService = new SalesReportService(salesReport);
      return new InputStreamResource(salesReportService.export());
    } catch (IOException ex) {
      log.error("getSalesReport() : IO Exception for sales report", ex);
    }
    return null;
  }


  private List<CashReceiptDto> getCashReceipts(BookingType bookingType, EReportPeriod reportPeriod, String fromDate, String toDate) {
    switch (reportPeriod) {
      case DAILY:
        String todayNepaliDate = dateUtility.todayNepaliDate();
        return podService.getCashReceiptsByBookingTypeDispatchDateBetween(bookingType, todayNepaliDate, todayNepaliDate);
      case CUSTOM:
        if (StringUtils.isEmpty(fromDate) && StringUtils.isEmpty(toDate)) {
          return podService.getAllCashReceipts().stream().filter(cashReceiptDto ->
          null!=cashReceiptDto.getVendorRequest()
            && null!=cashReceiptDto.getVendorRequest().getBookingType()
            && bookingType.equals(cashReceiptDto.getVendorRequest().getBookingType()))
            .collect(Collectors.toList());
        } else if (null != bookingType) {
          return podService.getCashReceiptsByBookingTypeDispatchDateBetween(bookingType, fromDate, toDate);
        } else {
          return podService.getCashReceiptsByDispatchDateBetween(fromDate, toDate);
        }
      default:
        return new ArrayList<>();
    }
  }

  private InputStreamResource getPartialBookingReport(ExcelReportRequest request) {
    try {
      List<CashReceiptDto> pods = this.getCashReceipts(BookingType.PARTIAL_BOOKING, request.getReportPeriod(), request.getFromDate(), request.getToDate());

      List<CashReceiptDto> filteredPods = this.filterPods(request, pods);

      BookingReportService bookingReportService = new BookingReportService(filteredPods);
      return new InputStreamResource(bookingReportService.export());
    } catch (IOException ex) {
      log.error("getPartialBookingReport() : IO Exception for full time booking report", ex);
    }
    return null;
  }

  private InputStreamResource getFullBookingReport(ExcelReportRequest request) {
    try {
      List<CashReceiptDto> pods = this.getCashReceipts(BookingType.FULL_BOOKING, request.getReportPeriod(), request.getFromDate(), request.getToDate());

      List<CashReceiptDto> filteredPods = this.filterPods(request, pods);

      FullBookingReportService bookingReportService = new FullBookingReportService(filteredPods);
      return new InputStreamResource(bookingReportService.export());
    } catch (IOException ex) {
      log.error("getPartialBookingReport() : IO Exception for full time booking report", ex);
    }
    return null;
  }

  private InputStreamResource getLocalBookingReport(ExcelReportRequest request) {
    try {
      List<CashReceiptDto> pods = this.getCashReceipts(BookingType.LOCAL_DELIVERY, request.getReportPeriod(), request.getFromDate(), request.getToDate());

      List<CashReceiptDto> filteredPods = this.filterPods(request, pods);

      BookingReportService bookingReportService = new BookingReportService(filteredPods);
      return new InputStreamResource(bookingReportService.export());
    } catch (IOException ex) {
      log.error("getLocalBookingReport() : IO Exception for full time booking report", ex);
    }
    return null;
  }


  private List<VendorRequest> getBookings(BookingType bookingType, EReportPeriod reportPeriod, String fromDate, String toDate) {
    switch (reportPeriod) {
      case DAILY:
        String todayNepaliDate = dateUtility.todayNepaliDate();
        return vendorRequestService.getAllVendorRequestByBookingTypeAndDispatchDateBetween(bookingType, todayNepaliDate, todayNepaliDate);
      case CUSTOM:
        if (StringUtils.isEmpty(fromDate) && StringUtils.isEmpty(toDate)) {
          return vendorRequestService.getAllVendorRequestByBookingType(bookingType);
        } else if (null != bookingType) {
          return vendorRequestService.getAllVendorRequestByBookingTypeAndDispatchDateBetween(bookingType, fromDate, toDate);
        } else {
          return vendorRequestService.getAllVendorRequestByDispatchDateBetween(fromDate, toDate);
        }
      default:
        return new ArrayList<>();
    }
  }

  private InputStreamResource getHiringVehicleReport(ExcelReportRequest request) {
    try {
      List<CashReceiptDto> pods = this.getCashReceipts(BookingType.HIRING_VEHICLE, request.getReportPeriod(), request.getFromDate(), request.getToDate());

      List<CashReceiptDto> filteredPods = this.filterPods(request, pods);

      Set<Integer> bookingIds = new HashSet<>();
      filteredPods.forEach(cashReceiptDto -> bookingIds.add(cashReceiptDto.getVendorRequest().getId()));

      List<VehicleLogBook> vehicleLogBooks = vehicleLogBookService.findAllByBookingIdIn(new ArrayList<>(bookingIds));

      List<HiringVehicleReportDto> hiringVehicleReportDtos = new ArrayList<>();
      pods.forEach(cashReceiptDto -> {
        HiringVehicleReportDto hiringVehicleReportDto = new HiringVehicleReportDto();
        hiringVehicleReportDto.setPod(cashReceiptDto);
        List<VehicleLogBook> vl = vehicleLogBooks.stream().filter(vehicleLogBook -> vehicleLogBook.getBooking().getId() == cashReceiptDto.getVendorRequest().getId()).collect(Collectors.toList());
        if (!vl.isEmpty()) {
          hiringVehicleReportDto.setVehicleLogBook(vl.get(0));
        }
        hiringVehicleReportDtos.add(hiringVehicleReportDto);
      });

      HiringVehicleReportService hiringVehicleReportService = new HiringVehicleReportService(hiringVehicleReportDtos);
      return new InputStreamResource(hiringVehicleReportService.export());
    } catch (IOException ex) {
      log.error("getHiringVehicleReport() : IO Exception for partial booking report", ex);
    }
    return null;
  }

  private List<CashReceiptDto> filterPods(ExcelReportRequest request, List<CashReceiptDto> pods) {
    return pods.stream().filter(cashReceiptDto -> {
      if (request.getVendorId() > 0 && null != cashReceiptDto.getVendorRequest() && null != cashReceiptDto.getVendorRequest().getVendor()) {
        return request.getVendorId() == cashReceiptDto.getVendorRequest().getVendor().getId();
      }
      return true;
    }).filter(cashReceiptDto -> {
      if (request.getVehicleType() != null && !StringUtils.isEmpty(request.getVehicleType())
        && null != cashReceiptDto.getVendorRequest() && null != cashReceiptDto.getVendorRequest().getVehicle()) {
        return cashReceiptDto.getVendorRequest().getVehicle().getType().equals(VehicleType.valueOf(request.getVehicleType()));
      }
      return true;
    }).filter(cashReceiptDto -> {
      if (request.getDriverId() > 0 && null != cashReceiptDto.getVendorRequest() && null != cashReceiptDto.getVendorRequest().getDriver()) {
        return cashReceiptDto.getVendorRequest().getDriver().getId() == request.getDriverId();
      }
      return true;
    }).filter(cashReceiptDto -> {
      if (null != request.getBranchCode() && !StringUtils.isEmpty(request.getBranchCode())) {
        return cashReceiptDto.getBranchCode().equals(request.getBranchCode());
      }
      return true;
    })
      .collect(Collectors.toList());
  }

  private List<VendorRequestDto> filterBooking(ExcelReportRequest request, List<VendorRequestDto> bookings) {
    return bookings.stream().filter(vendorRequestDto -> {
      if (request.getVendorId() > 0 && null != vendorRequestDto.getVendor()) {
        return request.getVendorId() == vendorRequestDto.getVendor().getId();
      }
      return true;
    }).filter(vendorRequestDto -> {
      if (null != request.getBranchCode() && !StringUtils.isEmpty(request.getBranchCode())) {
        return vendorRequestDto.getBranchCode().equals(request.getBranchCode());
      }
      return true;
    }).filter(vendorRequestDto -> {
      if (request.getVehicleType() != null && !StringUtils.isEmpty(request.getVehicleType()) && null != vendorRequestDto.getVehicle()) {
        return vendorRequestDto.getVehicle().getType().equals(VehicleType.valueOf(request.getVehicleType()));
      }
      return true;
    }).filter(vendorRequestDto -> {
      if (request.getDriverId() > 0 && null != vendorRequestDto.getDriver()) {
        return vendorRequestDto.getDriver().getId() == request.getDriverId();
      }
      return true;
    })
      .collect(Collectors.toList());
  }
}
