/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.reports.excel;

import com.myna.app.pod.CashReceiptDto;
import com.myna.app.utils.NameUtility;
import com.myna.app.vendorrequest.enums.BookingType;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BookingReportService implements ExcelExportService {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<CashReceiptDto> cashReceipts;

    public BookingReportService(List<CashReceiptDto> cashReceipts) {
        this.cashReceipts = cashReceipts;
        workbook = new XSSFWorkbook();
    }

    @Override
    public void writeHeaderLine() {
        sheet = workbook.createSheet("Booking");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        List<String> headers = new ArrayList<>();
        headers.add("Dispatched Date");
        headers.add("Office Branch");
        headers.add("Booking Type");
        headers.add("Booking Code");
        headers.add("Driver Name");
        headers.add("Vehicle Number");
        headers.add("Vehicle Type");
        headers.add("DN");
        headers.add("Customer Ref. invoice number");
        headers.add("Consignee Name");
        headers.add("Consignor Name");
        headers.add("Dispatched Location(From)");
        headers.add("Final Location(To)");
        headers.add("Product name");
        headers.add("Quantity");
        headers.add("Unit of Measurement");
        headers.add("Package");
        headers.add("Rate");
        headers.add("Total Amount");
        //todo transits
        headers.add("Status");

        AtomicInteger counter = new AtomicInteger();
        headers.forEach(s -> {
            createCell(row, counter.get(), s, style);
            counter.getAndIncrement();
        });
    }

    @Override
    public void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
        if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    @Override
    public void writeDataLines() {
        AtomicInteger rowCount = new AtomicInteger(1);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        cashReceipts.forEach(pod -> {
            if (null != pod.getCashReceiptDetails()) {
                pod.getCashReceiptDetails().forEach(podDetails -> {
                    Row row = sheet.createRow(rowCount.getAndIncrement());
                    AtomicInteger columnCount = new AtomicInteger();

                    createCell(row, columnCount.getAndIncrement(), pod.getVendorRequest().getDateOfRequest(), style);
                    createCell(row, columnCount.getAndIncrement(), pod.getBranchCode(), style);
                    createCell(row, columnCount.getAndIncrement(), String.valueOf(pod.getVendorRequest().getBookingType()), style);
                    createCell(row, columnCount.getAndIncrement(), pod.getVendorRequest().getBookingCode(), style);
                    createCell(row, columnCount.getAndIncrement(), (null != pod.getVendorRequest().getDriver()) ? NameUtility.getName(
                            pod.getVendorRequest().getDriver().getFirstName()
                            , pod.getVendorRequest().getDriver().getMiddleName()
                            , pod.getVendorRequest().getDriver().getLastName()
                    ) : "", style);
                    createCell(row, columnCount.getAndIncrement(), (null != pod.getVendorRequest().getVehicle()) ? pod.getVendorRequest().getVehicle().getRegistrationNumber() : "", style);
                    createCell(row, columnCount.getAndIncrement(), (null != pod.getVendorRequest().getVehicle()) ? String.valueOf(pod.getVendorRequest().getVehicle().getType()) : "", style);
                    createCell(row, columnCount.getAndIncrement(), pod.getDn(), style);
                    createCell(row, columnCount.getAndIncrement(), pod.getCustomerBillNumber(), style);
                    createCell(row, columnCount.getAndIncrement(), pod.getConsigneeName(), style);
                    createCell(row, columnCount.getAndIncrement(), pod.getConsignorName(), style);
                    createCell(row, columnCount.getAndIncrement(), (null != pod.getFromLocation()) ? (pod.getFromLocation().getDistrict() + ", " + pod.getFromLocation().getCity()) : "", style);
                    createCell(row, columnCount.getAndIncrement(), (null != pod.getToLocation()) ? (pod.getToLocation().getDistrict() + ", " + pod.getToLocation().getCity()) : "", style);
                    createCell(row, columnCount.getAndIncrement(), (null != podDetails.getPackageDetails()) ? podDetails.getPackageDetails().getName() : "", style);
                    createCell(row, columnCount.getAndIncrement(), podDetails.getQuantity(), style);
                    createCell(row, columnCount.getAndIncrement(), podDetails.getUnitOfMeasurement(), style);
                    createCell(row, columnCount.getAndIncrement(), podDetails.getNoOfPackage(), style);

                    if(BookingType.FULL_BOOKING.equals(pod.getVendorRequest().getBookingType())){
                      createCell(row, columnCount.getAndIncrement(), pod.getVendorRequest().getRate(), style);
                    }else{
                      createCell(row, columnCount.getAndIncrement(), podDetails.getRate(), style);
                    }

                    if("PACKAGE".equals(podDetails.getUnitOfMeasurement())){
                       createCell(row, columnCount.getAndIncrement(), StringUtils.isNotBlank(podDetails.getNoOfPackage())?Double.parseDouble(podDetails.getNoOfPackage())*podDetails.getRate():0, style);
                    }else if("QUANTITY".equals(podDetails.getUnitOfMeasurement())){
                        createCell(row, columnCount.getAndIncrement(), podDetails.getQuantity()*podDetails.getRate(), style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "0", style);
                    }

                    createCell(row, columnCount.getAndIncrement(), String.valueOf(pod.getPodStatus()), style);
                });
            }
        });
    }

    @Override
    public ByteArrayInputStream export() throws IOException {
        writeHeaderLine();
        writeDataLines();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);

        return new ByteArrayInputStream(out.toByteArray());
    }
}
