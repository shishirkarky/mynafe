/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.reports.excel;

import com.myna.app.vehicleLogBook.VehicleLogBookDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ActiveVehiclesReport implements ExcelExportService {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<VehicleLogBookDto> vehicleLogBooks;

    public ActiveVehiclesReport(List<VehicleLogBookDto> vehicleLogBooks) {
        this.vehicleLogBooks = vehicleLogBooks;
        workbook = new XSSFWorkbook();
    }

    @Override
    public void writeHeaderLine() {
        sheet = workbook.createSheet("Active Vehicle");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Sno", style);
        createCell(row, 1, "Vehicle Number", style);
        createCell(row, 2, "Company vehicle /outsider", style);
        createCell(row, 3, "Dispatch from", style);
        createCell(row, 4, "To", style);
        createCell(row, 5, "Driver Name", style);
        createCell(row, 6, "Driver Contact Number", style);
        createCell(row, 7, "Assigned from ( branch Name)", style);
        createCell(row, 8, "Vendor Name", style);
    }

    @Override
    public void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
        if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    @Override
    public void writeDataLines() {
        AtomicInteger rowCount = new AtomicInteger(1);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        //todo set values to excel

    }

    @Override
    public ByteArrayInputStream export() throws IOException {
        writeHeaderLine();
        writeDataLines();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);

        return new ByteArrayInputStream(out.toByteArray());
    }
}
