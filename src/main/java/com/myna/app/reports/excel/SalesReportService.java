/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.reports.excel;

import com.myna.app.reports.sales.dto.SalesReportExcelReportDto;
import com.myna.app.utils.NameUtility;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * <p>
 * This is the details about booking.
 * </p>
 */
public class SalesReportService implements ExcelExportService {
  private XSSFWorkbook workbook;
  private XSSFSheet sheet;
  private List<SalesReportExcelReportDto> salesReport;

  public SalesReportService(List<SalesReportExcelReportDto> salesReport) {
    this.salesReport = salesReport;
    workbook = new XSSFWorkbook();
  }

  @Override
  public void writeHeaderLine() {
    sheet = workbook.createSheet("Sales Report");

    Row row = sheet.createRow(0);

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setBold(true);
    font.setFontHeight(16);
    style.setFont(font);
    style.setWrapText(true);

    List<String> headers = new ArrayList<>();

    headers.add("Dispatched Date");
    headers.add("Office Branch");
    headers.add("Booking Type");
    headers.add("Booking Code");
    headers.add("Driver Name");
    headers.add("Vehicle Number");
    headers.add("Vehicle Type");
    headers.add("Consigner Name");
    headers.add("Dispatch Location(From)");
    headers.add("Final Location(To)");
    headers.add("Total Booked Amount");
    headers.add("Fuel Expenses");
    headers.add("Dhala Palti Expenses");
    headers.add("Labour Expenses");
    headers.add("Maintenance Expenses");
    headers.add("Other Expenses");
    headers.add("Advance Amount");
    headers.add("Due Amount");
    headers.add("Total Expenses(Booked Amount+ Fuel Expenses + Dhalapalti Expenses+ Labour Expenses + Maintenance Expenses + Other Expenses)");
    headers.add("Total Earned  Amount (Total sales amount of this booking code)");
    headers.add("Total Profit or loss (Total Earned Amount - Total Expenses)");


    AtomicInteger counter = new AtomicInteger();
    headers.forEach(s -> {
      createCell(row, counter.get(), s, style);
      counter.getAndIncrement();
    });
  }

  @Override
  public void createCell(Row row, int columnCount, Object value, CellStyle style) {
    sheet.autoSizeColumn(columnCount);
    Cell cell = row.createCell(columnCount);
    if (value instanceof Integer) {
      cell.setCellValue((Integer) value);
    } else if (value instanceof Boolean) {
      cell.setCellValue((Boolean) value);
    }
    if (value instanceof Double) {
      cell.setCellValue((Double) value);
    } else {
      cell.setCellValue((String) value);
    }
    cell.setCellStyle(style);
  }

  @Override
  public void writeDataLines() {
    AtomicInteger rowCount = new AtomicInteger(1);

    CellStyle style = workbook.createCellStyle();
    XSSFFont font = workbook.createFont();
    font.setFontHeight(14);
    style.setFont(font);

    salesReport.forEach(b -> {
      Row row = sheet.createRow(rowCount.getAndIncrement());
      AtomicInteger columnCount = new AtomicInteger();

      createCell(row, columnCount.getAndIncrement(), b.getDispatchDate(), style);
      createCell(row, columnCount.getAndIncrement(), b.getBranchCode(), style);
      createCell(row, columnCount.getAndIncrement(), String.valueOf(b.getBookingType()), style);
      createCell(row, columnCount.getAndIncrement(), b.getBookingCode(), style);
      createCell(row, columnCount.getAndIncrement(), (null != b.getDriver()) ? NameUtility.getName(
        b.getDriver().getFirstName()
        , b.getDriver().getMiddleName()
        , b.getDriver().getLastName()
      ) : "", style);
      createCell(row, columnCount.getAndIncrement(), (null != b.getVehicle()) ? b.getVehicle().getRegistrationNumber() : "", style);
      createCell(row, columnCount.getAndIncrement(), (null != b.getVehicle()) ? String.valueOf(b.getVehicle().getType()) : "", style);
      createCell(row, columnCount.getAndIncrement(), (null != b.getVendor() && null != b.getVendor().getName()) ? String.valueOf(b.getVendor().getName()) : "", style);
      createCell(row, columnCount.getAndIncrement(), (null != b.getFromLocation()) ?
        b.getFromLocation().getDistrict().concat(", ").concat(b.getFromLocation().getDistrict())
        : "", style);
      createCell(row, columnCount.getAndIncrement(), (null != b.getToLocation()) ?
        b.getToLocation().getDistrict().concat(", ").concat(b.getToLocation().getDistrict())
        : "", style);
      createCell(row, columnCount.getAndIncrement(), b.getTotalAmount(), style);
      createCell(row, columnCount.getAndIncrement(), b.getFuelAmount(), style);
      createCell(row, columnCount.getAndIncrement(), b.getDhalaAndPaltiExpenses(), style);
      createCell(row, columnCount.getAndIncrement(), b.getLabourExpenses(), style);
      createCell(row, columnCount.getAndIncrement(), b.getMaintainanceAmount(), style);
      createCell(row, columnCount.getAndIncrement(), b.getOtherExpenses(), style);
      createCell(row, columnCount.getAndIncrement(), b.getAdvancePaymentAmount(), style);
      createCell(row, columnCount.getAndIncrement(), b.getDueAmount(), style);

      double totalExpenses = b.getTotalAmount() + b.getFuelAmount() + b.getDhalaAndPaltiExpenses() + b.getLabourExpenses() + b.getMaintainanceAmount() + b.getOtherExpenses();
      createCell(row, columnCount.getAndIncrement(),
        totalExpenses
        , style);

      AtomicReference<Double> total = new AtomicReference<>(0.0);
      if (null != b.getPods()) {
        b.getPods().forEach(cashReceiptDto -> {
          if (null != cashReceiptDto.getCashReceiptDetails()) {
            cashReceiptDto.getCashReceiptDetails().forEach(cashReceiptDetailsDto -> total.set(total.get() + cashReceiptDetailsDto.getTotal()));
          }
        });
        createCell(row, columnCount.getAndIncrement(),
          total.get()
          , style);
      } else {
        createCell(row, columnCount.getAndIncrement(),
          0
          , style);
      }

      createCell(row, columnCount.getAndIncrement(),
        total.get() - totalExpenses
        , style);

    });

  }

  @Override
  public ByteArrayInputStream export() throws IOException {
    writeHeaderLine();
    writeDataLines();

    ByteArrayOutputStream out = new ByteArrayOutputStream();
    workbook.write(out);

    return new ByteArrayInputStream(out.toByteArray());
  }
}
