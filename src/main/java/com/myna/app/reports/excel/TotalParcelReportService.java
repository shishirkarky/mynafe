/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.reports.excel;

import com.myna.app.pod.CashReceiptDto;
import com.myna.app.utils.NameUtility;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TotalParcelReportService implements ExcelExportService {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<CashReceiptDto> cashReceipts;

    public TotalParcelReportService(List<CashReceiptDto> cashReceipts) {
        this.cashReceipts = cashReceipts;
        workbook = new XSSFWorkbook();
    }

    @Override
    public void writeHeaderLine() {
        sheet = workbook.createSheet("Total Parcel Delivery");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "Date", style);
        createCell(row, 1, "Booking Date", style);
        createCell(row, 2, "Dispatched Date", style);
        createCell(row, 3, "Vehicle Number", style);
        createCell(row, 4, "Driver Name", style);
        createCell(row, 5, "Driver Contact", style);
        createCell(row, 6, "DN", style);
        createCell(row, 7, "Vendor Name", style);
        createCell(row, 8, "Vendor Invoice Number", style);
        createCell(row, 9, "Customer Name", style);
        createCell(row, 10, "Customer Location", style);
        createCell(row, 11, "Parcel", style);
        createCell(row, 12, "Quantity", style);
        createCell(row, 13, "Rate", style);
        createCell(row, 14, "Delivery Status", style);
    }

    @Override
    public void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
        if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    @Override
    public void writeDataLines() {
        AtomicInteger rowCount = new AtomicInteger(1);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        cashReceipts.forEach(pod -> {
            if (null != pod.getCashReceiptDetails()) {
                pod.getCashReceiptDetails().forEach(podDetails -> {
                    Row row = sheet.createRow(rowCount.getAndIncrement());
                    AtomicInteger columnCount = new AtomicInteger();

                    createCell(row, columnCount.getAndIncrement(), pod.getDate(), style);

                    if (null != pod.getVendorRequest()) {
                        createCell(row, columnCount.getAndIncrement(), pod.getVendorRequest().getDateOfRequest(), style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "", style);
                    }

                    if(null!=pod.getVendorRequest()) {
                        createCell(row, columnCount.getAndIncrement(), pod.getVendorRequest().getDispatchDate(), style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "", style);
                    }

                    if (null != pod.getVendorRequest().getVehicle()) {
                        createCell(row, columnCount.getAndIncrement(),
                                pod.getVendorRequest().getVehicle().getRegistrationNumber()
                                , style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "", style);
                    }

                    if (null != pod.getVendorRequest() && null != pod.getVendorRequest().getDriver()) {
                        String driverName = NameUtility.getName(
                                pod.getVendorRequest().getDriver().getFirstName()
                                , pod.getVendorRequest().getDriver().getMiddleName()
                                , pod.getVendorRequest().getDriver().getLastName()
                        );

                        createCell(row, columnCount.getAndIncrement(),
                                driverName
                                , style);

                        createCell(row, columnCount.getAndIncrement(), pod.getVendorRequest().getDriver().getPhone(), style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "", style);
                        createCell(row, columnCount.getAndIncrement(), "", style);
                    }


                    createCell(row, columnCount.getAndIncrement(), pod.getDn(), style);

                    if(null!=pod.getVendorRequest() && null!=pod.getVendorRequest().getVendor()) {
                        createCell(row, columnCount.getAndIncrement(),
                                pod.getVendorRequest().getVendor().getName()
                                , style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "", style);
                    }

                    createCell(row, columnCount.getAndIncrement(), pod.getVendorInvoiceNumber(), style);
                    createCell(row, columnCount.getAndIncrement(), pod.getConsigneeName(), style);

                    if(null!=pod.getToLocation()) {
                        String location = pod.getToLocation().getDistrict() + ", " + pod.getToLocation().getCity();
                        createCell(row, columnCount.getAndIncrement(), location, style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "", style);
                    }

                    if(null!=podDetails.getPackageDetails()) {
                        createCell(row, columnCount.getAndIncrement(),
                                podDetails.getPackageDetails().getName()
                                , style);
                    }else{
                        createCell(row, columnCount.getAndIncrement(), "", style);
                    }

                    createCell(row, columnCount.getAndIncrement(), podDetails.getQuantity(), style);
                    createCell(row, columnCount.getAndIncrement(), podDetails.getRate(), style);
                    createCell(row, columnCount.getAndIncrement(), String.valueOf(pod.getPodStatus()), style);
                });
            }
        });
    }

    @Override
    public ByteArrayInputStream export() throws IOException {
        writeHeaderLine();
        writeDataLines();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);

        return new ByteArrayInputStream(out.toByteArray());
    }
}
