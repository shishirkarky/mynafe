package com.myna.app.reports.excel;

import com.myna.app.reports.VctsReportDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class VctsReportService implements ExcelExportService {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<VctsReportDto> vcts;

    public VctsReportService(List<VctsReportDto> vcts) {
        this.vcts = vcts;
        workbook = new XSSFWorkbook();
    }

    @Override
    public void writeHeaderLine() {
        sheet = workbook.createSheet("Total Parcel Delivery");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "DOCUMENT_TYPE_ID", style);
        createCell(row, 1, "OTHERS_DOC_TYPE", style);
        createCell(row, 2, "CUSTOM_OFFICE_ID", style);
        createCell(row, 3, "BILTI_NO", style);
        createCell(row, 4, "DOCUMENT_NO", style);
        createCell(row, 5, "DOCUMENT_DATE", style);
        createCell(row, 6, "ITEM_NAME", style);
        createCell(row, 7, "QUANTITY", style);
        createCell(row, 8, "UNIT_TYPE", style);
        createCell(row, 9, "TOTAL_AMOUNT", style);
        createCell(row, 10, "BUYER_PAN", style);
        createCell(row, 11, "SUPPLIER_PAN", style);
        createCell(row, 12, "DESTINATION_LOCATION", style);
        createCell(row, 13, "REMARKS", style);
    }

    @Override
    public void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
        if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    @Override
    public void writeDataLines() {
        AtomicInteger rowCount = new AtomicInteger(1);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        vcts.forEach(vctsReportDto -> {
            Row row = sheet.createRow(rowCount.getAndIncrement());
            AtomicInteger columnCount = new AtomicInteger();

            createCell(row, columnCount.getAndIncrement(), "", style);
            createCell(row, columnCount.getAndIncrement(), "", style);
            createCell(row, columnCount.getAndIncrement(), "", style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getDn(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getCustomerRefInvoice(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getDispatchDate(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getItemName(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getQuantity(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getUnitType(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getTotalAmount(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getConsignee(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getVendor(), style);
            createCell(row, columnCount.getAndIncrement(), vctsReportDto.getDestination(), style);
            createCell(row, columnCount.getAndIncrement(), "", style);
        });
    }

    @Override
    public ByteArrayInputStream export() throws IOException {
        writeHeaderLine();
        writeDataLines();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);

        return new ByteArrayInputStream(out.toByteArray());
    }
}
