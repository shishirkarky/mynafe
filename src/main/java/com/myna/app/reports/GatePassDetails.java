package com.myna.app.reports;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GatePassDetails {
    private String invoiceNumber;
    private String deliveryDestination;
    private int noOfPackages;
}
