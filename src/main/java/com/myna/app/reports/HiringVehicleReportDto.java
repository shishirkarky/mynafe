package com.myna.app.reports;

import com.myna.app.pod.CashReceiptDto;
import com.myna.app.vehicleLogBook.VehicleLogBook;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HiringVehicleReportDto {
  private CashReceiptDto pod;
  private VehicleLogBook vehicleLogBook;
}
