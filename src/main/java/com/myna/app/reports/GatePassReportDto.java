package com.myna.app.reports;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GatePassReportDto {
    private String issuingDepartment;
    private String transportName;
    private String vehicleNumber;
    private String contactNumber;
    private String driverName;
    private String date;
    private String preparedBy;
    private List<GatePassDetails> details;
    private String logoUrl;
}
