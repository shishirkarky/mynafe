/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.reports.jasper;

import com.myna.app.auth.service.UserService;
import com.myna.app.pod.CashReceiptDto;
import com.myna.app.pod.PodService;
import com.myna.app.filestorage.Documents;
import com.myna.app.filestorage.FilesStorageService;
import com.myna.app.office.service.OfficeService;
import com.myna.app.reports.GatePassDetails;
import com.myna.app.reports.GatePassReportDto;
import com.myna.app.reports.VctsReportDto;
import com.myna.app.reports.excel.VctsReportService;
import com.myna.app.utils.NameUtility;
import com.myna.app.utils.jasper.EFileType;
import com.myna.app.utils.jasper.JasperRequestDto;
import com.myna.app.utils.jasper.ReportUtility;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.service.VendorRequestService;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service
public class ReportsServiceImpl implements ReportsService {
    @Value("${myna.reports.pod}")
    private String podJrxmlLocation;
    @Value("${myna.reports.gatepass}")
    private String gatePassJrxmlLocation;

    private ReportUtility reportUtility;
    private PodService podService;
    private FilesStorageService filesStorageService;
    private VendorRequestService vendorRequestService;
    private VendorRequestDtoAssembler vendorRequestDtoAssembler;
    private UserService userService;
    private OfficeService officeService;

    @Autowired
    public void setReportUtility(ReportUtility reportUtility) {
        this.reportUtility = reportUtility;
    }

    @Autowired
    public void setPodService(PodService podService) {
        this.podService = podService;
    }

    @Autowired
    public void setFilesStorageService(FilesStorageService filesStorageService) {
        this.filesStorageService = filesStorageService;
    }

    @Autowired
    public void setVendorRequestService(VendorRequestService vendorRequestService) {
        this.vendorRequestService = vendorRequestService;
    }

    @Autowired
    public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
        this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setOfficeService(OfficeService officeService) {
        this.officeService = officeService;
    }

    @Override
    public byte[] generatePodJasperReport(int id) {
        try {
            Optional<CashReceiptDto> cashReceiptDtoOptional = podService.getCashReceipt(id);
            if (cashReceiptDtoOptional.isPresent()) {
                JasperRequestDto jasperRequest = this.getPodJasperRequest(cashReceiptDtoOptional.get());
                if (null != jasperRequest) {
                    return reportUtility.exportDownloadableReport(jasperRequest);
                }
            }
        } catch (Exception e) {
            log.error("Exception generating POD report for id {}", id, e);
        }
        return null;
    }

    private JasperRequestDto getPodJasperRequest(CashReceiptDto cashReceiptDto) {
        if (null != cashReceiptDto) {
            String logoUrl = this.getLogoUrl();
            if (null != logoUrl) {
                cashReceiptDto.setLogo(logoUrl);
            }

            JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(cashReceiptDto.getCashReceiptDetails());

            Map<String, Object> params = new HashMap<>();
            params.put("request", cashReceiptDto);
            params.put("ds", ds);

            JasperRequestDto jasperRequest = new JasperRequestDto();
            jasperRequest.setOutputFileName("pod_" + cashReceiptDto.getDn());

            jasperRequest.setJrxmlFileLocation(podJrxmlLocation);

            jasperRequest.setParameters(params);
            jasperRequest.setJrDataSource(ds);

            return jasperRequest;
        }
        return null;
    }

    private String getLogoUrl() {
        Optional<Documents> documentsOptional = filesStorageService.findDocuments("LOGO", "LOGO", 0);
        return documentsOptional.map(Documents::getUrl).orElse(null);
    }

    @Override
    public byte[] generateGatePassJasperReport(int bookingId) {
        try {
            JasperRequestDto jasperRequest = this.getGatePassJasperRequest(bookingId);
            if (null != jasperRequest) {
                return reportUtility.exportDownloadableReport(jasperRequest);
            }
        } catch (Exception e) {
            log.error("Exception generating GatePass report for bookingId {}", bookingId, e);
        }
        return null;
    }

    @Override
    public InputStreamResource generateVctsExcelReport(int bookingId) {
        try {
            VctsReportService vctsReportService = new VctsReportService(this.getVctsReport(bookingId));
            return new InputStreamResource(vctsReportService.export());
        } catch (Exception e) {
            log.error("Exception generating VCTS report for bookingId {}", bookingId, e);
        }
        return null;
    }

    private List<VctsReportDto> getVctsReport(int bookingId) {
        Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getOneVendorRequest(bookingId);
        if (vendorRequestOptional.isPresent()) {
            List<CashReceiptDto> cashReceiptDtos = podService.getAllCashReceiptsByBookingId(bookingId);
            if (!cashReceiptDtos.isEmpty()) {

                VendorRequestDto vendorRequestDto = vendorRequestDtoAssembler.toModel(vendorRequestOptional.get());

                List<VctsReportDto> vctsReports = new ArrayList<>();
                cashReceiptDtos.forEach(pod -> {
                    VctsReportDto vctsReportDto = new VctsReportDto();

                    vctsReportDto.setDn(pod.getDn());
                    vctsReportDto.setCustomerRefInvoice(pod.getCustomerBillNumber());
                    vctsReportDto.setDispatchDate(vendorRequestDto.getDispatchDate());
                    if (StringUtils.isEmpty(pod.getConsigneeVat())) {
                        vctsReportDto.setConsignee(pod.getConsigneeName());
                    } else {
                        vctsReportDto.setConsignee(pod.getConsigneeVat());
                    }
                    if (null != pod.getToLocation()) {
                        vctsReportDto.setDestination(
                                pod.getToLocation().getCity() + ", " + pod.getToLocation().getDistrict()
                        );
                    }
                    vctsReportDto.setVendor(pod.getConsignorVat());

                    /*
                     * INITIALIZATION
                     */
                    AtomicReference<Double> quantity = new AtomicReference<>(0.0);
                    AtomicReference<Double> taxableAmount = new AtomicReference<>(0.0);
                    AtomicReference<String> itemName = new AtomicReference<>("");
                    AtomicReference<String> unitOfMeasurement = new AtomicReference<>("");

                    /*
                     * SET VALUES TO VARIABLES
                     */
                    if(null!=pod.getCashReceiptDetails() && !pod.getCashReceiptDetails().isEmpty()) {
                        pod.getCashReceiptDetails().forEach(podDetails -> {
                            try {
                                quantity.set(quantity.get() + Double.parseDouble(podDetails.getNoOfPackage()));
                            } catch (NumberFormatException ex) {
                                log.warn("getVctsReport() : Unable to parse NoOfPackage to Double for pod detail id {}", podDetails.getId());
                            }

                            taxableAmount.set(taxableAmount.get() + podDetails.getTaxableAmount());
                            unitOfMeasurement.set(podDetails.getUnitOfMeasurement());
                            if (null != podDetails.getPackageDetails()) {
                                itemName.set(itemName.get().concat(podDetails.getPackageDetails().getName()).concat(","));
                            }
                        });
                    }

                    /*
                     * SET VALUES TO DTO
                     */
                    vctsReportDto.setQuantity(quantity.get());
                    vctsReportDto.setTotalAmount(taxableAmount.get());

                    if (itemName.get().endsWith(",")) {
                        String finalItemName = itemName.get().substring(0, itemName.get().length() - 1);
                        vctsReportDto.setItemName(finalItemName);
                    } else {
                        vctsReportDto.setItemName(itemName.get());
                    }

                    vctsReportDto.setUnitType(unitOfMeasurement.get());

                    vctsReports.add(vctsReportDto);
                });


                return vctsReports;
            }
        }
        return new ArrayList<>();
    }

    private JasperRequestDto getGatePassJasperRequest(int bookingId) {
        if (bookingId > 0) {

            Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getOneVendorRequest(bookingId);
            if (vendorRequestOptional.isPresent()) {
                List<CashReceiptDto> cashReceipts = podService.getAllCashReceiptsByBookingId(bookingId);

                GatePassReportDto gatePassReportDto = new GatePassReportDto();
                this.mapCashReceiptToGatePass(cashReceipts, gatePassReportDto);
                this.mapBookingToGatePass(vendorRequestDtoAssembler.toModel(vendorRequestOptional.get()), gatePassReportDto);
                gatePassReportDto.setPreparedBy(userService.getTokenUserStaffName());

                String logoUrl = this.getLogoUrl();
                if (null != logoUrl) {
                    gatePassReportDto.setLogoUrl(logoUrl);
                }

                List<GatePassDetails> gatePassDetails = gatePassReportDto.getDetails();

                JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(gatePassDetails);
                gatePassReportDto.setDetails(null);

                Map<String, Object> params = new HashMap<>();
                params.put("request", gatePassReportDto);
                params.put("ds", ds);

                JasperRequestDto jasperRequest = new JasperRequestDto();
                jasperRequest.setOutputFileName("gatepass_booking_" + bookingId);

                jasperRequest.setJrxmlFileLocation(gatePassJrxmlLocation);

                jasperRequest.setParameters(params);
                jasperRequest.setJrDataSource(ds);

                return jasperRequest;
            }
        }
        return null;
    }

    private void mapCashReceiptToGatePass(List<CashReceiptDto> cashReceipt, GatePassReportDto gatePassReportDto) {
        List<GatePassDetails> gatePassDetailsList = new ArrayList<>();
        cashReceipt.forEach(cashReceiptDto -> {
            GatePassDetails gatePassDetails = new GatePassDetails();

            if (null != cashReceiptDto.getToLocation()) {
                gatePassDetails.setDeliveryDestination(cashReceiptDto.getToLocation().getCity() + ", " + cashReceiptDto.getToLocation().getDistrict());
            }
            gatePassDetails.setInvoiceNumber(cashReceiptDto.getCustomerBillNumber());

            if (null != cashReceiptDto.getCashReceiptDetails() && !cashReceiptDto.getCashReceiptDetails().isEmpty()) {
                gatePassDetails.setNoOfPackages(cashReceiptDto.getCashReceiptDetails().size());
            }

            gatePassDetailsList.add(gatePassDetails);
        });
        gatePassReportDto.setDetails(gatePassDetailsList);
    }

    private void mapBookingToGatePass(VendorRequestDto vendorRequestDto, GatePassReportDto gatePass) {
        if (null != vendorRequestDto.getDriver()) {
            gatePass.setDriverName(
                    NameUtility.getName(
                            vendorRequestDto.getDriver().getFirstName(),
                            vendorRequestDto.getDriver().getMiddleName(),
                            vendorRequestDto.getDriver().getLastName()
                    )
            );

            gatePass.setContactNumber(vendorRequestDto.getDriver().getPhone());
        }

        gatePass.setDate(vendorRequestDto.getDateOfRequest());
        gatePass.setIssuingDepartment(vendorRequestDto.getBranchCode() + " - " + officeService.getOfficeLocation(vendorRequestDto.getBranchCode()));
        gatePass.setPreparedBy(vendorRequestDto.getCreatedBy());

        if (null != vendorRequestDto.getVehicle()) {
            gatePass.setVehicleNumber(vendorRequestDto.getVehicle().getRegistrationNumber());
        }
    }

    @Override
    public void generatePodsForBookingJasperReport(HttpServletResponse response, int bookingId) {
        try {
            List<JasperRequestDto> jrList = new ArrayList<>();
            List<CashReceiptDto> cashReceipts = podService.getAllCashReceiptsByBookingId(bookingId);
            cashReceipts.forEach(cashReceiptDto -> {
                JasperRequestDto jasperRequest = this.getPodJasperRequest(cashReceiptDto);
                jrList.add(jasperRequest);
            });
            if (!jrList.isEmpty()) {
                reportUtility.exportBulkDownloadableReport(response, jrList, "pod", EFileType.PDF);
            }
        } catch (Exception e) {
            log.error("Exception generating POD report for id {}", bookingId, e);
        }
    }
}
