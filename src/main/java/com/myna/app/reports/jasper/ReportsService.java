/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.reports.jasper;

import org.springframework.core.io.InputStreamResource;

import javax.servlet.http.HttpServletResponse;

public interface ReportsService {
    byte[] generatePodJasperReport(int id);

    byte[] generateGatePassJasperReport(int bookingId);

    InputStreamResource generateVctsExcelReport(int bookingId);

    void generatePodsForBookingJasperReport(HttpServletResponse response, int bookingId);
}
