package com.myna.app.reports;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VctsReportDto {
    private String dn;
    private String customerRefInvoice;
    private String dispatchDate;
    private String itemName;
    private double quantity;
    private String unitType;
    private double totalAmount; //taxable amount
    private String consignee; // pan or name
    private String vendor; //pan or name
    private String destination;
    private String remarks;
}
