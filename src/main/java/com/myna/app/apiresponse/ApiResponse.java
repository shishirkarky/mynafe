/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.apiresponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ApiResponse {
  private String message;
  private HttpStatus httpStatus;
  private int status;
  private Object data;


  public ResponseEntity<?> getSuccessResponse(String message, Object data) {
    if (null != message) {
      this.message = message;
    } else {
      this.message = HttpStatus.OK.getReasonPhrase();
    }

    return new ResponseEntity<>(
      new ApiResponse(
        this.message,
        HttpStatus.OK,
        HttpStatus.OK.value(),
        data
      )
      , HttpStatus.OK
    );
  }

  public ResponseEntity<?> getDataNotFoundResponse(String message) {
    if (null != message) {
      this.message = message;
    } else {
      this.message = HttpStatus.NOT_FOUND.getReasonPhrase();
    }
    return new ResponseEntity<>(
      new ApiResponse(
        this.message,
        HttpStatus.OK
        , HttpStatus.NOT_FOUND.value()
        , new EmptyJSONResponse()
      )
      , HttpStatus.OK
    );
  }

  public ResponseEntity<?> getFailureResponse(String message) {
    if (null != message) {
      this.message = message;
    } else {
      this.message = HttpStatus.BAD_REQUEST.getReasonPhrase();
    }
    return new ResponseEntity<>(
      new ApiResponse(
        this.message,
        HttpStatus.BAD_REQUEST
        , HttpStatus.BAD_REQUEST.value()
        , new EmptyJSONResponse()
      )
      , HttpStatus.BAD_REQUEST
    );
  }
}
