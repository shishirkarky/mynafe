/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.apiresponse;

import lombok.Getter;

@Getter
public enum EApiResponseMessages {
    POD_STATUS_UPDATED("POD status updated."),
    POD_UPDATE_FAILED("POD update failed.");
    private final String message;

    EApiResponseMessages(String message) {
        this.message = message;
    }
}
