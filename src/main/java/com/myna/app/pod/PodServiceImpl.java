/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.pod;

import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.service.UserService;
import com.myna.app.enums.Status;
import com.myna.app.filestorage.Documents;
import com.myna.app.filestorage.FilesStorageService;
import com.myna.app.rates.RatesComponent;
import com.myna.app.transitions.Transition;
import com.myna.app.transitions.TransitionService;
import com.myna.app.utils.CodeGenerationUtils;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.utils.dates.DateUtility;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.enums.BookingType;
import com.myna.app.vendorrequest.service.VendorRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PodServiceImpl implements PodService {
  private VendorRequestService vendorRequestService;
  private CashReceiptRepository cashReceiptRepository;
  private CashReceiptDetailsRepository cashReceiptDetailsRepository;
  private CashReceiptDtoAssembler cashReceiptDtoAssembler;
  private UserService userService;
  private DateUtility dateUtility;
  private RatesComponent ratesComponent;
  private TransitionService transitionService;
  private VendorRequestDtoAssembler vendorRequestDtoAssembler;
  private FilesStorageService filesStorageService;
  private PodDropLogRepository podDropLogRepository;
  private PodDetailsDropLogRepository podDetailsDropLogRepository;

  @Autowired
  public void setVendorRequestService(VendorRequestService vendorRequestService) {
    this.vendorRequestService = vendorRequestService;
  }

  @Autowired
  public void setCashReceiptRepository(CashReceiptRepository cashReceiptRepository) {
    this.cashReceiptRepository = cashReceiptRepository;
  }

  @Autowired
  public void setCashReceiptDetailsRepository(CashReceiptDetailsRepository cashReceiptDetailsRepository) {
    this.cashReceiptDetailsRepository = cashReceiptDetailsRepository;
  }

  @Autowired
  public void setCashReceiptDtoAssembler(CashReceiptDtoAssembler cashReceiptDtoAssembler) {
    this.cashReceiptDtoAssembler = cashReceiptDtoAssembler;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setDateUtility(DateUtility dateUtility) {
    this.dateUtility = dateUtility;
  }

  @Autowired
  public void setRatesComponent(RatesComponent ratesComponent) {
    this.ratesComponent = ratesComponent;
  }

  @Autowired
  public void setTransitionService(TransitionService transitionService) {
    this.transitionService = transitionService;
  }

  @Autowired
  public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
    this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
  }

  @Autowired
  public void setFilesStorageService(FilesStorageService filesStorageService) {
    this.filesStorageService = filesStorageService;
  }

  @Autowired
  public void setPodDropLogRepository(PodDropLogRepository podDropLogRepository) {
    this.podDropLogRepository = podDropLogRepository;
  }

  @Autowired
  public void setPodDetailsDropLogRepository(PodDetailsDropLogRepository podDetailsDropLogRepository) {
    this.podDetailsDropLogRepository = podDetailsDropLogRepository;
  }

  @Override
  public Optional<CashReceiptDto> saveOrUpdate(CashReceiptDto cashReceiptDto) {
    if (StringUtils.isEmpty(cashReceiptDto.getDn())) {
      cashReceiptDto.setDn(this.generateDNNumber());
    }

    CashReceipt savedCashReceipt = null;
    if (cashReceiptDto.getId() > 0) {
      Optional<CashReceipt> existingCashReceiptOptional = cashReceiptRepository.findById(cashReceiptDto.getId());
      if (existingCashReceiptOptional.isPresent()) {
        CashReceipt cashReceipt = existingCashReceiptOptional.get();
        CustomBeanUtils.copyNonNullProperties(cashReceiptDto, cashReceipt);
        savedCashReceipt = cashReceiptRepository.save(cashReceipt);
      }
    } else {
      savedCashReceipt = cashReceiptRepository.save(
        cashReceiptDtoAssembler.toDomain(cashReceiptDto));
    }

    if (savedCashReceipt != null) {
      int refCashReceiptId = savedCashReceipt.getId();
      String bookingCode = savedCashReceipt.getBranchCode();

      List<CashReceiptDetails> cashReceiptDetails = cashReceiptDtoAssembler.toCashReceiptDetailsDomains(cashReceiptDto.getCashReceiptDetails());
      cashReceiptDetails.forEach(cd -> {
        cd.setRefCashReceiptId(refCashReceiptId);
        cd.setBranchCode(bookingCode);

        try {

          Map<String, Double> rateMap = ratesComponent.getPartialBookingRate(
            cashReceiptDto.getRefVendorRequestId()
            , cashReceiptDto.getRefFromLocationId()
            , cashReceiptDto.getRefToLocationId()
            , Integer.parseInt(cd.getDescription())
            , Double.parseDouble(cd.getNoOfPackage())
            , cd.getQuantity()
          );

          cd.setRate(rateMap.get("rate"));
          cd.setTotal(rateMap.get("total"));

        } catch (Exception e) {
          log.error("saveOrUpdate() : Exception in setting rates from config for pod id {}", refCashReceiptId);
        }
      });

      if (!cashReceiptDetails.isEmpty()) {
        cashReceiptDetailsRepository.saveAll(cashReceiptDetails);
      }

      cashReceiptDto.setId(refCashReceiptId);
      return Optional.of(cashReceiptDto);
    }
    return Optional.empty();
  }

  @Override
  public Optional<CashReceiptDto> updateCashReceipt(int id, CashReceiptDto cashReceiptDto) {
    Optional<CashReceipt> cashReceiptOptional = cashReceiptRepository.findById(id);
    if (cashReceiptOptional.isPresent()) {
      cashReceiptDto.setDn(cashReceiptOptional.get().getDn());
      cashReceiptDto.setId(id);
      cashReceiptDto.setBranchCode(cashReceiptOptional.get().getBranchCode());
      cashReceiptDto.setRefVendorRequestId(cashReceiptOptional.get().getRefVendorRequestId());

      List<CashReceiptDetails> cashReceiptDetails = cashReceiptDetailsRepository.findAllByRefCashReceiptId(id);
      cashReceiptDetailsRepository.deleteAll(cashReceiptDetails);

      return this.saveOrUpdate(cashReceiptDto);
    }
    return Optional.empty();
  }

  @Override
  public Optional<CashReceiptDto> getCashReceipt(int id) {
    Optional<CashReceipt> cashReceiptOptional = cashReceiptRepository.findById(id);
    if (cashReceiptOptional.isPresent()) {
      CashReceiptDto cashReceiptDto = cashReceiptDtoAssembler.toModel(cashReceiptOptional.get());
      this.setCashReceiptDetails(cashReceiptDto);
      return Optional.of(cashReceiptDto);
    }
    return Optional.empty();
  }

  @Transactional
  @Override
  public Optional<CashReceipt> deleteCashReceipt(int id) {
    log.warn("Delete POD {} initiated ", id);

    if(userService.getCurrentUserRoles().contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      Optional<CashReceipt> cashReceiptOptional = cashReceiptRepository.findById(id);
      if (cashReceiptOptional.isPresent()) {


        List<CashReceiptDetails> cashReceiptDetailsList = cashReceiptDetailsRepository.findAllByRefCashReceiptId(id);
        if (!cashReceiptDetailsList.isEmpty()) {

          List<PodDetailsDropLog> podDetailsDropLogs = new ArrayList<>();
          cashReceiptDetailsList.forEach(cashReceiptDetails -> {
            PodDetailsDropLog podDetailsDropLog = new PodDetailsDropLog();
            CustomBeanUtils.copyProperties(cashReceiptDetails, podDetailsDropLog);
            podDetailsDropLogs.add(podDetailsDropLog);
          });
          podDetailsDropLogRepository.saveAll(podDetailsDropLogs);

          cashReceiptDetailsRepository.deleteAll(cashReceiptDetailsList);
        }

        PodDropLog podDropLog = new PodDropLog();
        CustomBeanUtils.copyProperties(cashReceiptOptional.get(), podDropLog);
        podDropLogRepository.save(podDropLog);

        List<Transition> transitions = transitionService.getTransitionsFromDn(cashReceiptOptional.get().getDn());
        if(!transitions.isEmpty()) {
          transitionService.deleteTransitions(transitions);
        }else {
          cashReceiptRepository.deleteById(id);
        }
        return cashReceiptOptional;
      }
    }
    return Optional.empty();
  }

  @Override
  public Optional<CashReceiptDto> getCashReceiptByCustomerBillNumber(String customerBillNumber) {
    Optional<CashReceipt> cashReceiptOptional = cashReceiptRepository.findFirstByCustomerBillNumber(customerBillNumber);
    if (cashReceiptOptional.isPresent()) {
      CashReceiptDto cashReceiptDto = cashReceiptDtoAssembler.toModel(cashReceiptOptional.get());
      this.setCashReceiptDetails(cashReceiptDto);
      return Optional.of(cashReceiptDto);
    }
    return Optional.empty();
  }

  @Override
  public Optional<CashReceiptDto> getCashReceiptByDnNumber(String dnNumber) {
    Optional<CashReceipt> cashReceiptOptional = cashReceiptRepository.findFirstByDn(dnNumber);
    if (cashReceiptOptional.isPresent()) {
      CashReceiptDto cashReceiptDto = cashReceiptDtoAssembler.toModel(cashReceiptOptional.get());
      this.setCashReceiptDetails(cashReceiptDto);
      return Optional.of(cashReceiptDto);
    }
    return Optional.empty();
  }

  @Override
  public List<CashReceiptDto> getAllCashReceipts() {
    List<CashReceipt> cashReceipts = this.getCashReceiptsByCurrentUserRoles(); //0.3
    List<CashReceiptDto> cashReceiptDtos = cashReceiptDtoAssembler.toModels(cashReceipts); //9
    List<Transition> transitions = transitionService.getUserBranchTransitions(); //2
    transitions.forEach(transition -> {
      CashReceiptDto cashReceiptDto = cashReceiptDtoAssembler.toModel(transition.getPod());
      cashReceiptDto.setTransit(true);
      Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getByTransition(transition);
      if (vendorRequestOptional.isPresent()) {
        VendorRequestDto vendorRequestDto = vendorRequestDtoAssembler.toModel(vendorRequestOptional.get());
        cashReceiptDto.setTransitBooking(vendorRequestDto);
      }

      cashReceiptDtos.add(cashReceiptDto);
    });
    setCashReceiptDetails(cashReceiptDtos); //3
    return cashReceiptDtos;
  }

  @Override
  public List<CashReceiptDto> getPodsWithUploads() {
    List<Documents> documents = filesStorageService.findDocuments("POD");
    Set<Integer> podIds = new HashSet<>();
    documents.forEach(
      d -> podIds.add(d.getRefId())
    );

    List<CashReceipt> cashReceipts = this.getCashReceiptsByIdsIn(podIds);
    cashReceipts.forEach(cashReceipt -> {
      List<Documents> podDocuments = documents.stream().filter(d -> {
        return d.getRefId() == cashReceipt.getId();
      }).collect(Collectors.toList());

      List<CashReceipt.PodPhoto> podPhotos = new ArrayList<>();
      podDocuments.forEach(podDocument -> {
        CashReceipt.PodPhoto podPhoto = new CashReceipt.PodPhoto();
        podPhoto.setPhotoUrl(podDocument.getUrl());
        podPhoto.setTitle(podDocument.getSubType());

        podPhotos.add(podPhoto);
      });

      cashReceipt.setPhotos(podPhotos);
    });

    if (!cashReceipts.isEmpty()) {
      return cashReceiptDtoAssembler.toModels(cashReceipts);
    }

    return new ArrayList<>();
  }

  @Override
  public List<CashReceipt> getCashReceiptsByIdsIn(Set<Integer> podIds) {
    return cashReceiptRepository.findAllByIdIn(podIds);
  }

  @Override
  public List<CashReceiptDto> filterCashReceipt(int driver, String dispatchDate) {
    if (StringUtils.isEmpty(dispatchDate) && driver > 0) {
      return this.getCashReceiptsByDriverId(driver);
    } else if (!StringUtils.isEmpty(dispatchDate) && driver == 0) {
      return this.getCashReceiptsByDispatchDate(dispatchDate);
    } else if (!StringUtils.isEmpty(dispatchDate) && driver > 0) {
      return this.getCashReceiptsByAssignedDriverAndDispatchDate(driver, dispatchDate);
    } else {
      return this.getAllCashReceipts();
    }
  }

  @Override
  public List<CashReceiptDto> getCashReceiptsByDriverId(int assignedDriverId) {
    List<CashReceiptDto> cashReceipts = new ArrayList<>();
    List<VendorRequest> vendorRequests = vendorRequestService.getAllVendorRequestByDriverId(assignedDriverId);
    vendorRequests.forEach(vendorRequest -> {
      List<CashReceiptDto> cashReceiptDtos = this.getAllCashReceiptsByBookingId(vendorRequest.getId());
      cashReceipts.addAll(cashReceiptDtos);
    });
    return cashReceipts;
  }

  @Override
  public List<CashReceiptDto> getCashReceiptsByDispatchDateBetween(String fromDispatchDate, String toDispatchDate) {
    List<CashReceiptDto> cashReceipts = new ArrayList<>();
    List<VendorRequest> vendorRequests = vendorRequestService.getAllVendorRequestByDispatchDateBetween(fromDispatchDate, toDispatchDate);
    vendorRequests.forEach(vendorRequest -> {
      List<CashReceiptDto> cashReceiptDtos = this.getAllCashReceiptsByBookingId(vendorRequest.getId());
      cashReceipts.addAll(cashReceiptDtos);
    });
    return cashReceipts;
  }

  @Override
  public List<CashReceiptDto> getCashReceiptsByBookingTypeDispatchDateBetween(BookingType bookingType, String fromDispatchDate, String toDispatchDate) {
    List<CashReceiptDto> cashReceipts = new ArrayList<>();
    List<VendorRequest> vendorRequests = vendorRequestService.getAllVendorRequestByBookingTypeAndDispatchDateBetween(bookingType, fromDispatchDate, toDispatchDate);
    vendorRequests.forEach(vendorRequest -> {
      List<CashReceiptDto> cashReceiptDtos = this.getAllCashReceiptsByBookingId(vendorRequest.getId());
      cashReceipts.addAll(cashReceiptDtos);
    });
    return cashReceipts;
  }

  @Override
  public List<CashReceiptDto> getCashReceiptsByDispatchDate(String dispatchDate) {
    List<CashReceiptDto> cashReceipts = new ArrayList<>();
    List<VendorRequest> vendorRequests = vendorRequestService.getAllVendorRequestByDispatchDate(dispatchDate);
    vendorRequests.forEach(vendorRequest -> {
      List<CashReceiptDto> cashReceiptDtos = this.getAllCashReceiptsByBookingId(vendorRequest.getId());
      cashReceipts.addAll(cashReceiptDtos);
    });
    return cashReceipts;
  }

  @Override
  public List<CashReceiptDto> getCashReceiptsByAssignedDriverAndDispatchDate(int driver, String dispatchDate) {
    List<CashReceiptDto> cashReceipts = new ArrayList<>();
    List<VendorRequest> vendorRequests = vendorRequestService.getAllVendorRequestByDriverAndDispatchDate(driver, dispatchDate);
    vendorRequests.forEach(vendorRequest -> {
      List<CashReceiptDto> cashReceiptDtos = this.getAllCashReceiptsByBookingId(vendorRequest.getId());
      cashReceipts.addAll(cashReceiptDtos);
    });
    return cashReceipts;
  }

  /**
   * fetch pods for booking id and if transitions are also present add pod in the list
   *
   * @param bookingId
   * @return
   */
  @Override
  public List<CashReceiptDto> getAllCashReceiptsByBookingId(int bookingId) {
    Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getOneVendorRequest(bookingId);
    if (vendorRequestOptional.isPresent()) {
      VendorRequest vendorRequest = vendorRequestOptional.get();

      List<CashReceipt> cashReceipts = cashReceiptRepository.findAllByRefVendorRequestId(vendorRequestOptional.get().getId());

      List<CashReceiptDto> cashReceiptDtos = cashReceiptDtoAssembler.toModels(cashReceipts);

      if (null != vendorRequest.getTransitions() && !vendorRequest.getTransitions().isEmpty()) {
        vendorRequest.getTransitions().forEach(transition -> {
          CashReceiptDto cashReceiptDto = cashReceiptDtoAssembler.toModel(transition.getPod());
          cashReceiptDto.setTransit(true);
          cashReceiptDtos.add(cashReceiptDto);
        });
      }

      cashReceiptDtos.forEach(this::setCashReceiptDetails);
      return cashReceiptDtos;
    }
    return new ArrayList<>();
  }

  @Override
  public List<CashReceiptDetailsDto> getAllCashReceiptDetailsForBookingId(int bookingId) {
    List<CashReceiptDto> cashReceiptDtos = this.getAllCashReceiptsByBookingId(bookingId);
    if (!cashReceiptDtos.isEmpty()) {
      List<CashReceiptDetailsDto> cashReceiptDetailsDtos = new ArrayList<>();
      cashReceiptDtos.forEach(cashReceiptDto -> {
        cashReceiptDetailsDtos.addAll(cashReceiptDto.getCashReceiptDetails());
      });
      return cashReceiptDetailsDtos;
    }
    return new ArrayList<>();
  }

  //todo this process is taking more thatn 1 minute to execute need to optimize
  private void setCashReceiptDetails(CashReceiptDto cashReceiptDto) {
    int cashReceiptId = cashReceiptDto.getId();

    List<CashReceiptDetails> cashReceiptDetails = cashReceiptDetailsRepository.findAllByRefCashReceiptId(cashReceiptId);

    List<CashReceiptDetailsDto> cashReceiptDetailsDtos = cashReceiptDtoAssembler.toCashReceiptDetailsModels(cashReceiptDetails);
    if (!cashReceiptDetailsDtos.isEmpty()) {
      cashReceiptDto.setCashReceiptDetails(cashReceiptDetailsDtos);
    }
  }

  private void setCashReceiptDetails(List<CashReceiptDto> cashReceiptDtos) {

    List<Integer> podIds = new ArrayList<>();
    cashReceiptDtos.forEach(cashReceiptDto -> podIds.add(cashReceiptDto.getId()));

    List<CashReceiptDetails> cashReceiptDetails = cashReceiptDetailsRepository.findAllByRefCashReceiptIdIn(podIds);

    cashReceiptDtos.forEach(cashReceiptDto -> {
      List<CashReceiptDetails> cashReceiptDetailsList = cashReceiptDetails.stream().filter(cd -> cd.getRefCashReceiptId() == cashReceiptDto.getId()).collect(Collectors.toList());
      cashReceiptDto.setCashReceiptDetails(cashReceiptDtoAssembler.toCashReceiptDetailsModels(cashReceiptDetailsList));
    });
  }

  /**
   * fetch pods for bookingId, add pods if present in transition for booking
   *
   * @param bookingId
   * @param status
   * @return
   */
  @Override
  public List<CashReceiptDto> getAllCashReceiptsByBookingIdAndStatus(int bookingId, Status status) {
    Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getOneVendorRequest(bookingId);
    if (vendorRequestOptional.isPresent()) {
      VendorRequest vendorRequest = vendorRequestOptional.get();

      List<CashReceipt> cashReceipts = cashReceiptRepository.findAllByRefVendorRequestIdAndPodStatus(vendorRequest.getId(), status);

      List<CashReceiptDto> cashReceiptDtos = cashReceiptDtoAssembler.toModels(cashReceipts);

      if (null != vendorRequest.getTransitions() && !vendorRequest.getTransitions().isEmpty()) {
        vendorRequest.getTransitions().forEach(transition -> {
          CashReceiptDto cashReceiptDto = cashReceiptDtoAssembler.toModel(transition.getPod());
          cashReceiptDto.setTransit(true);
          cashReceiptDtos.add(cashReceiptDto);
        });
      }

      return cashReceiptDtos;

    }
    return new ArrayList<>();
  }

  private List<CashReceipt> getCashReceiptsByCurrentUserRoles() {
    List<String> rolesOptional = userService.getCurrentUserRoles();
    if (!rolesOptional.isEmpty() && rolesOptional.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      return cashReceiptRepository.findAll();
    } else {
      Optional<String> branchCode = userService.getTokenUserBranchCode();
      if (branchCode.isPresent()) {
        return cashReceiptRepository.findAllByBranchCode(branchCode.get());
      }
    }
    return new ArrayList<>();
  }

  /**
   * Get all active PODs
   * if super admin = get for all branches
   * else get all for the user specific branch
   *
   * @return List<CashReceipt>
   */
  @Override
  public List<CashReceipt> getAllActiveCashReceiptsEntity() {
    List<String> rolesOptional = userService.getCurrentUserRoles();
    if (!rolesOptional.isEmpty() && rolesOptional.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      return cashReceiptRepository.findAllByPodStatus(Status.ACTIVE);
    } else {
      Optional<String> branchCode = userService.getTokenUserBranchCode();
      if (branchCode.isPresent()) {
        return cashReceiptRepository.findAllByPodStatusAndBranchCode(Status.ACTIVE, branchCode.get());
      }
    }
    return new ArrayList<>();
  }

  @Override
  public boolean updateCashReceiptStatus(int id, CashReceiptDto cashReceiptDto, String type) {
    if ("pod".equals(type)) {
      if (id == 0 && !cashReceiptDto.getIds().isEmpty()) {
        cashReceiptDto.getIds().forEach(podId -> this.updateCashReceiptStatus(podId, cashReceiptDto));
      } else {
        this.updateCashReceiptStatus(id, cashReceiptDto);
      }
      return true;
    } else if ("booking".equals(type)) {
      List<CashReceipt> cashReceipts = cashReceiptRepository.findAllByRefVendorRequestId(id);
      cashReceipts.forEach(cashReceipt -> this.updateCashReceiptStatus(cashReceipt.getId(), cashReceiptDto));
      return true;
    }
    return false;
  }

  @Override
  public boolean updateCashReceiptStatus(int id, Status status) {
    CashReceiptDto cashReceiptDto = new CashReceiptDto();
    cashReceiptDto.setId(id);
    cashReceiptDto.setPodStatus(status);
    this.updateCashReceiptStatus(id, cashReceiptDto);
    return true;
  }

  public void updateCashReceiptStatus(int id, CashReceiptDto cashReceiptDto) {
    Optional<CashReceipt> cashReceiptOptional = cashReceiptRepository.findById(id);
    if (cashReceiptOptional.isPresent()) {
      CashReceipt existingCashReceipt = cashReceiptOptional.get();
      existingCashReceipt.setPodStatus(cashReceiptDto.getPodStatus());

      if (null != cashReceiptDto.getPodStatusRemarks()) {
        existingCashReceipt.setPodStatusRemarks(cashReceiptDto.getPodStatusRemarks());
      }

      if (null != cashReceiptDto.getReceiverName()) {
        existingCashReceipt.setReceiverName(cashReceiptDto.getReceiverName());
      }

      if (null != cashReceiptDto.getReceiverContact()) {
        existingCashReceipt.setReceiverContact(cashReceiptDto.getReceiverContact());
      }

      if (null == cashReceiptDto.getPodStatusUpdateDate() || StringUtils.isEmpty(cashReceiptDto.getPodStatusUpdateDate())) {
        existingCashReceipt.setPodStatusUpdateDate(dateUtility.todayNepaliDate());
        existingCashReceipt.setPodStatusUpdateDateEn(dateUtility.todayEnglishDate());
      } else {
        existingCashReceipt.setPodStatusUpdateDate(cashReceiptDto.getPodStatusUpdateDate());
        existingCashReceipt.setPodStatusUpdateDateEn(dateUtility.nepaliToEnglish(cashReceiptDto.getPodStatusUpdateDate()));
      }

      cashReceiptRepository.save(existingCashReceipt);
    }
  }

  /**
   * if super admin , count for all branches by status
   * else , count for specific branch of user by status
   *
   * @param podStatus
   * @return
   */
  @Override
  public int countAllByPodStatus(Status podStatus) {
    List<String> rolesOptional = userService.getCurrentUserRoles();
    if (!rolesOptional.isEmpty() && rolesOptional.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      return cashReceiptRepository.countAllByPodStatus(podStatus);
    } else {
      Optional<String> branchCode = userService.getTokenUserBranchCode();
      if (branchCode.isPresent()) {
        return cashReceiptRepository.countAllByPodStatusAndBranchCode(Status.ACTIVE, branchCode.get());
      }
    }
    return 0;
  }

  @Override
  public int countAllByBookingId(int bookingId) {
    return cashReceiptRepository.countAllByRefVendorRequestId(bookingId);
  }

  @Override
  public Optional<CashReceipt> getEntityByDn(String dn) {
    return cashReceiptRepository.findFirstByDn(dn);
  }

  @Override
  public Optional<CashReceipt> getEntityByDnAndStatusNot(String dn, Status status) {
    return cashReceiptRepository.findFirstByDnAndPodStatusNot(dn, status);
  }

  @Override
  public List<CashReceipt> getCurrentUserDriverAssignedPods() {
    List<VendorRequest> vendorRequests = vendorRequestService.getCurrentUserDriverBookings();
    if (!vendorRequests.isEmpty()) {
      Set<Integer> bookingIds = new HashSet<>();
      vendorRequests.forEach(vendorRequest -> bookingIds.add(vendorRequest.getId()));
      return cashReceiptRepository.findAllByRefVendorRequestIdIn(bookingIds);
    }
    return new ArrayList<>();
  }

  private String generateDNNumber() {
    String currentDate = DateUtility.getCurrentDate();
    String existingCode = "";

    Optional<CashReceipt> cashReceiptOptional = cashReceiptRepository.findFirstByDnLikeOrderByIdDesc(currentDate + "%");
    if (cashReceiptOptional.isPresent()) {
      CashReceipt cashReceipt = cashReceiptOptional.get();
      existingCode = cashReceipt.getDn();
    }
    return CodeGenerationUtils.generateCode(existingCode, currentDate);
  }

  @Override
  public Optional<CashReceipt> getOne(int id){
    return cashReceiptRepository.findById(id);
  }
}
