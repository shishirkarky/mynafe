package com.myna.app.pod.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverTasksRatio {
    private long completed;
    private long total;
}
