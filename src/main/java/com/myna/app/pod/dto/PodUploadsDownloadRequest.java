package com.myna.app.pod.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PodUploadsDownloadRequest {
  private List<Integer> pods;
}
