package com.myna.app.pod;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PodDropLogRepository extends JpaRepository<PodDropLog, Integer> {
}
