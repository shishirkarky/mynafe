/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.pod;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CashReceiptDetailsRepository extends JpaRepository<CashReceiptDetails, Integer> {
    List<CashReceiptDetails> findAllByRefCashReceiptId(int refCashReceiptId);
    List<CashReceiptDetails> findAllByRefCashReceiptIdIn(List<Integer> podIds);
}
