/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.pod;

import com.myna.app.dataconfig.EntityConfigService;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDtoAssembler;
import com.myna.app.dataconfig.packages.ConfigPackages;
import com.myna.app.dataconfig.packages.ConfigPackagesDtoAssembler;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.service.VendorRequestService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class CashReceiptDtoAssembler {
  private ConfigLocationsDtoAssembler configLocationsDtoAssembler;
  private ConfigPackagesDtoAssembler configPackagesDtoAssembler;
  private VendorRequestDtoAssembler vendorRequestDtoAssembler;
  private EntityConfigService entityConfigService;
  private VendorRequestService vendorRequestService;
  private CashReceiptDetailsRepository cashReceiptDetailsRepository;

  @Autowired
  public void setConfigLocationsDtoAssembler(ConfigLocationsDtoAssembler configLocationsDtoAssembler) {
    this.configLocationsDtoAssembler = configLocationsDtoAssembler;
  }

  @Autowired
  public void setConfigPackagesDtoAssembler(ConfigPackagesDtoAssembler configPackagesDtoAssembler) {
    this.configPackagesDtoAssembler = configPackagesDtoAssembler;
  }

  @Autowired
  public void setEntityConfigService(EntityConfigService entityConfigService) {
    this.entityConfigService = entityConfigService;
  }

  @Autowired
  public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
    this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
  }

  @Autowired
  public void setVendorRequestService(VendorRequestService vendorRequestService) {
    this.vendorRequestService = vendorRequestService;
  }

  @Autowired
  public void setCashReceiptDetailsRepository(CashReceiptDetailsRepository cashReceiptDetailsRepository) {
    this.cashReceiptDetailsRepository = cashReceiptDetailsRepository;
  }

  public CashReceipt toDomain(CashReceiptDto cashReceiptDto) {
    CashReceipt cashReceipt = new CashReceipt();
    CustomBeanUtils.copyNonNullProperties(cashReceiptDto, cashReceipt);

    if (cashReceiptDto.getRefFromLocationId() > 0) {
      ConfigLocations fromLocation = new ConfigLocations();
      fromLocation.setId(cashReceiptDto.getRefFromLocationId());
      cashReceipt.setFromLocation(fromLocation);
    }

    if (cashReceiptDto.getRefToLocationId() > 0) {
      ConfigLocations toLocation = new ConfigLocations();
      toLocation.setId(cashReceiptDto.getRefToLocationId());
      cashReceipt.setToLocation(toLocation);
    }

    return cashReceipt;
  }

  public CashReceiptDto toModel(CashReceipt cashReceipt) {
    CashReceiptDto cashReceiptDto = new CashReceiptDto();
    CustomBeanUtils.copyNonNullProperties(cashReceipt, cashReceiptDto);

    cashReceiptDto.setCreatedBy(cashReceipt.getCreatedBy());
    cashReceiptDto.setLastModifiedBy(cashReceipt.getLastModifiedBy());

    vendorRequestService.getOneVendorRequest(cashReceipt.getRefVendorRequestId())
      .ifPresent(vendorRequest -> cashReceiptDto.setVendorRequest(vendorRequestDtoAssembler.toModel(vendorRequest)));

    if (null != cashReceipt.getFromLocation()) {
      cashReceiptDto.setFromLocation(configLocationsDtoAssembler.toModel(cashReceipt.getFromLocation()));
    }

    if (null != cashReceipt.getToLocation()) {
      cashReceiptDto.setToLocation(configLocationsDtoAssembler.toModel(cashReceipt.getToLocation()));
    }
    return cashReceiptDto;
  }

  public List<CashReceiptDto> toModels(List<CashReceipt> cashReceipts) {
    if (!cashReceipts.isEmpty()) {
      List<CashReceiptDto> cashReceiptDtos = new ArrayList<>();
      cashReceipts.forEach(cashReceipt -> cashReceiptDtos.add(this.toModel(cashReceipt)));

      return cashReceiptDtos;
    }
    return new ArrayList<>();
  }

  public CashReceiptDetails toCashReceiptDetailsDomain(CashReceiptDetailsDto cashReceiptDetailsDto) {
    CashReceiptDetails cashReceiptDetails = new CashReceiptDetails();
    CustomBeanUtils.copyNonNullProperties(cashReceiptDetailsDto, cashReceiptDetails);
    if (cashReceiptDetailsDto.getDescription() != null && !cashReceiptDetailsDto.getDescription().isEmpty() &&
      !cashReceiptDetailsDto.getDescription().equals("0")) {
      try {
        ConfigPackages configPackages = new ConfigPackages();
        configPackages.setId(Integer.parseInt(cashReceiptDetailsDto.getDescription()));

        cashReceiptDetails.setConfigPackages(configPackages);
      } catch (NumberFormatException ex) {
        log.warn("unable to parse {} to integer", cashReceiptDetailsDto.getDescription());
      }
    }
    return cashReceiptDetails;
  }

  public List<CashReceiptDetails> toCashReceiptDetailsDomains(List<CashReceiptDetailsDto> cashReceiptDetailsDtos) {
    List<CashReceiptDetails> cashReceiptDetails = new ArrayList<>();
    cashReceiptDetailsDtos.forEach(cashReceiptDetailsDto -> cashReceiptDetails.add(this.toCashReceiptDetailsDomain(cashReceiptDetailsDto)));

    return cashReceiptDetails;
  }

  public CashReceiptDetailsDto toCashReceiptDetailsModel(CashReceiptDetails cashReceiptDetails) {
    CashReceiptDetailsDto cashReceiptDetailsDto = new CashReceiptDetailsDto();
    CustomBeanUtils.copyNonNullProperties(cashReceiptDetails, cashReceiptDetailsDto);

    if (cashReceiptDetails.getDescription() != null && !cashReceiptDetails.getDescription().isEmpty() && !cashReceiptDetails.getDescription().equals("0")) {
      cashReceiptDetailsDto.setPackageDetails(configPackagesDtoAssembler.toModel(cashReceiptDetails.getConfigPackages()));
    }

    return cashReceiptDetailsDto;
  }

  public List<CashReceiptDetailsDto> toCashReceiptDetailsModels(List<CashReceiptDetails> cashReceiptDetailsList) {
    if (!cashReceiptDetailsList.isEmpty()) {
      List<CashReceiptDetailsDto> cashReceiptDetailsDtos = new ArrayList<>();
      cashReceiptDetailsList.forEach(cashReceiptDetails -> cashReceiptDetailsDtos.add(this.toCashReceiptDetailsModel(cashReceiptDetails)));

      return cashReceiptDetailsDtos;
    }
    return new ArrayList<>();
  }

  public List<DriverAssignedTasksDto> toDriverAssignedTasks(List<CashReceipt> cashReceipts) {
    List<CashReceiptDto> cashReceiptDtos = this.toModels(cashReceipts);
    List<DriverAssignedTasksDto> tasks = new ArrayList<>();
    cashReceiptDtos.forEach(cashReceipt -> {
      List<CashReceiptDetails> cashReceiptDetails = cashReceiptDetailsRepository.findAllByRefCashReceiptId(cashReceipt.getId());

      DriverAssignedTasksDto driverAssignedTasksDto = new DriverAssignedTasksDto();
      driverAssignedTasksDto.setDate(cashReceipt.getDate());
      driverAssignedTasksDto.setId(cashReceipt.getId());
      driverAssignedTasksDto.setDnNumber(cashReceipt.getDn());
      driverAssignedTasksDto.setBookingCode(cashReceipt.getVendorRequest().getBookingCode());
      driverAssignedTasksDto.setPodStatus(cashReceipt.getPodStatus());
      driverAssignedTasksDto.setConsigneeName(cashReceipt.getConsigneeName());
      driverAssignedTasksDto.setConsigneeNumber(cashReceipt.getConsigneeNumber());

      if (null != cashReceiptDetails) {
        cashReceiptDetails.forEach(cashReceiptDetailsDto -> {
          if (cashReceiptDetailsDto.getQuantity() > 0) {
            driverAssignedTasksDto.setTotalPackage(driverAssignedTasksDto.getTotalPackage() + cashReceiptDetailsDto.getQuantity());
          } else if (StringUtils.isNotBlank(cashReceiptDetailsDto.getNoOfPackage()) && Integer.parseInt(cashReceiptDetailsDto.getNoOfPackage()) > 0) {
            driverAssignedTasksDto.setTotalPackage(
              driverAssignedTasksDto.getTotalPackage() + Integer.parseInt(cashReceiptDetailsDto.getNoOfPackage()));
          }

          if (cashReceiptDetailsDto.getDescription() != null && !cashReceiptDetailsDto.getDescription().isEmpty()) {
            Optional<ConfigPackages> configPackagesOptional = entityConfigService.getOnePackage(Integer.parseInt(cashReceiptDetailsDto.getDescription()));
            if (configPackagesOptional.isPresent()) {
              if (driverAssignedTasksDto.getDescriptionOfItem() != null && !driverAssignedTasksDto.getDescriptionOfItem().isBlank()) {
                driverAssignedTasksDto.setDescriptionOfItem(
                  driverAssignedTasksDto.getDescriptionOfItem().concat(", ").concat(
                    configPackagesOptional.get().getName()
                  )
                );
              } else {
                driverAssignedTasksDto.setDescriptionOfItem(
                  configPackagesOptional.get().getName()
                );
              }
            }
          }
        });
      }

      tasks.add(driverAssignedTasksDto);
    });
    return tasks;
  }
}
