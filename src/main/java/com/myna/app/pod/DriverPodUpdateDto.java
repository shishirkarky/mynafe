package com.myna.app.pod;

import com.myna.app.enums.Status;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class DriverPodUpdateDto {
    private Status status;
    private MultipartFile signature;
    private MultipartFile bill;
}
