/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.pod;

import com.myna.app.commons.Auditable;
import com.myna.app.customer.entity.Customer;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.enums.Status;
import com.myna.app.vendor.entity.Vendor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * <p>
 * This entity is for POD (Print On Demand) provided to Driver once the packages are loaded.
 * This entity is sync with the receipt provided by Vendor.
 * <p>
 * int id = primary key
 * String dn = auto generated e.g. 2078011200001
 * </p>
 */
@Setter
@Getter
@Entity
@Table(name = "pod")
public class CashReceipt extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(unique = true)
  private String dn;

  /**
   * Consignor Details ({@link com.myna.app.vendor.entity.Vendor}
   */
  @OneToOne(optional = true)
  private Vendor vendor;
  private String consignorName;
  private String consignorVat;
  private String vendorInvoiceNumber;

  private int refToLocationId;
  @OneToOne(optional = true)
  private ConfigLocations toLocation;
  private int refFromLocationId;
  @OneToOne(optional = true)
  private ConfigLocations fromLocation;
  private int refVendorRequestId;
  private String date;
  @Deprecated
  private String dateEn;

  private String branchCode;
  /**
   * Consignee Details
   */
  @OneToOne(optional = true)
  private Customer consignee;
  private String consigneeName;
  @Deprecated
  private String consigneeNumber;
  private String consigneeContact;
  private String customerBillNumber;
  private String consigneeVat;
  private String customerBillDate;

  /**
   * Receiver Details
   */
  private String receiverName;
  private String receiverContact;
  private String receivedDate;
  private String receivedDateEn;
  /**
   * POD Status Details
   */
  @Enumerated(EnumType.STRING)
  private Status podStatus = Status.ACTIVE;
  private String podStatusRemarks;
  private String podStatusUpdateDate;
  private String podStatusUpdateDateEn;
  private String deliveredDate;

  @Transient
  private List<PodPhoto> photos;

  @Getter
  @Setter
  public static class PodPhoto{
    private String title;
    private String photoUrl;
  }
}
