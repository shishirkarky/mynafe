package com.myna.app.pod;

import com.myna.app.enums.Status;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverAssignedTasksDto {
    private int id;
    private String date;
    private String bookingCode;
    private String dnNumber;
    private String descriptionOfItem;
    private double totalPackage;
    private String consigneeName;
    private String consigneeNumber;
    private Status podStatus;
}
