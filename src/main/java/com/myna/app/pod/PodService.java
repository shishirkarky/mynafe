/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.pod;

import com.myna.app.enums.Status;
import com.myna.app.vendorrequest.enums.BookingType;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PodService {
  Optional<CashReceiptDto> saveOrUpdate(CashReceiptDto cashReceiptDto);

  Optional<CashReceiptDto> updateCashReceipt(int id, CashReceiptDto cashReceiptDto);

  Optional<CashReceiptDto> getCashReceipt(int id);

  Optional<CashReceipt> deleteCashReceipt(int id);

  Optional<CashReceiptDto> getCashReceiptByCustomerBillNumber(String customerBillNumber);

  Optional<CashReceiptDto> getCashReceiptByDnNumber(String dnNumber);

  List<CashReceiptDto> getAllCashReceipts();

  List<CashReceiptDto> getPodsWithUploads();

  List<CashReceipt> getCashReceiptsByIdsIn(Set<Integer> podIds);

  List<CashReceiptDto> filterCashReceipt(int driver, String dispatchDate);

  List<CashReceiptDto> getCashReceiptsByDriverId(int driver);

  List<CashReceiptDto> getCashReceiptsByDispatchDateBetween(String fromDispatchDate, String toDispatchDate);

  List<CashReceiptDto> getCashReceiptsByBookingTypeDispatchDateBetween(BookingType bookingType, String fromDispatchDate, String toDispatchDate);

  List<CashReceiptDto> getCashReceiptsByDispatchDate(String dispatchDate);

  List<CashReceiptDto> getCashReceiptsByAssignedDriverAndDispatchDate(int driver, String dispatchDate);

  List<CashReceiptDto> getAllCashReceiptsByBookingId(int bookingId);

  List<CashReceiptDetailsDto> getAllCashReceiptDetailsForBookingId(int bookingId);

  List<CashReceiptDto> getAllCashReceiptsByBookingIdAndStatus(int bookingId, Status status);

  List<CashReceipt> getAllActiveCashReceiptsEntity();

  boolean updateCashReceiptStatus(int id, CashReceiptDto cashReceiptDto, String type);

  boolean updateCashReceiptStatus(int id, Status status);

  int countAllByPodStatus(Status status);

  int countAllByBookingId(int bookingId);

  Optional<CashReceipt> getEntityByDn(String dn);

  Optional<CashReceipt> getEntityByDnAndStatusNot(String dn, Status status);

  List<CashReceipt> getCurrentUserDriverAssignedPods();

  Optional<CashReceipt> getOne(int id);
}
