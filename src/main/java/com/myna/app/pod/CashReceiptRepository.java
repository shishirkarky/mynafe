/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.pod;

import com.myna.app.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface CashReceiptRepository extends JpaRepository<CashReceipt, Integer> {
  List<CashReceipt> findAllByIdIn(Set<Integer> podIds);

  Optional<CashReceipt> findFirstByCustomerBillNumber(String customerBillNumber);

  Optional<CashReceipt> findFirstByDn(String dnNumber);

  Optional<CashReceipt> findFirstByDnAndPodStatusNot(String dnNumber, Status status);

  int countAllByPodStatus(Status status);

  int countAllByPodStatusAndBranchCode(Status status, String branchCode);

  List<CashReceipt> findAllByPodStatus(Status status);

  List<CashReceipt> findAllByPodStatusAndBranchCode(Status status, String branchCode);

  List<CashReceipt> findAllByBranchCode(String branchCode);

  List<CashReceipt> findAllByRefVendorRequestId(int vendorRequestId);

  List<CashReceipt> findAllByRefVendorRequestIdIn(Set<Integer> vendorRequestIds);

  List<CashReceipt> findAllByRefVendorRequestIdAndPodStatus(int vendorRequestId, Status status);

  int countAllByRefVendorRequestId(int bookingId);

  Optional<CashReceipt> findFirstByDnLikeOrderByIdDesc(String currentDate);
}
