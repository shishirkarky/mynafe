/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.pod;

import com.myna.app.commons.Auditable;
import com.myna.app.dataconfig.packages.ConfigPackages;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "pod_details_drop_log")
public class PodDetailsDropLog extends Auditable<String> {
    @Id
    private int id;
    private int refCashReceiptId;
    private String description;
    @OneToOne(optional = true)
    private ConfigPackages configPackages;
    private double quantity;
    private String unitOfMeasurement;
    private String noOfPackage;
    private double rate;
    private double total;
    private double value;
    private double taxableAmount;
    private String branchCode;
    private String remarks;
}
