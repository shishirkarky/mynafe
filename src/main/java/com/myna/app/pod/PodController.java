/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.pod;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.apiresponse.EApiResponseMessages;
import com.myna.app.auth.service.UserService;
import com.myna.app.enums.Status;
import com.myna.app.filestorage.Documents;
import com.myna.app.filestorage.FileInfo;
import com.myna.app.filestorage.FilesStorageService;
import com.myna.app.pod.dto.DriverTasksRatio;
import com.myna.app.pod.dto.PodUploadsDownloadRequest;
import com.myna.app.utils.PdfUtility;
import com.myna.app.utils.ValidationUtil;
import com.myna.app.vendorrequest.service.VendorRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class PodController {
  private PodService podService;
  private CashReceiptDtoAssembler cashReceiptDtoAssembler;
  private UserService userService;
  private VendorRequestService vendorRequestService;
  private FilesStorageService filesStorageService;
  private PdfUtility pdfUtility;
  private ValidationUtil validationUtil;

  @Autowired
  public void setPodService(PodService podService) {
    this.podService = podService;
  }

  @Autowired
  public void setCashReceiptDtoAssembler(CashReceiptDtoAssembler cashReceiptDtoAssembler) {
    this.cashReceiptDtoAssembler = cashReceiptDtoAssembler;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setVendorRequestService(VendorRequestService vendorRequestService) {
    this.vendorRequestService = vendorRequestService;
  }

  @Autowired
  public void setFilesStorageService(FilesStorageService filesStorageService) {
    this.filesStorageService = filesStorageService;
  }

  @Autowired
  public void setPdfUtility(PdfUtility pdfUtility) {
    this.pdfUtility = pdfUtility;
  }

  @Autowired
  public void setValidationUtil(ValidationUtil validationUtil) {
    this.validationUtil = validationUtil;
  }

  @PostMapping("/cashreceipts")
  public ResponseEntity<?> saveCashReceipt(@RequestBody CashReceiptDto cashReceiptDto) {
    Optional<String> branchCode = userService.getTokenUserBranchCode();
    branchCode.ifPresent(cashReceiptDto::setBranchCode);

    /*
     * setting rates
     */
    Optional<CashReceiptDto> cashReceiptDtoOptional = podService.saveOrUpdate(cashReceiptDto);
    if (cashReceiptDtoOptional.isPresent()) {

      vendorRequestService.updateDispatchDate(
        cashReceiptDtoOptional.get().getRefVendorRequestId(),
        cashReceiptDtoOptional.get().getDate());

      return new ApiResponse().getSuccessResponse(null, cashReceiptDtoOptional.get());
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @PutMapping("/cashreceipts/{id}")
  public ResponseEntity<?> updateCashReceipt(@PathVariable int id, @RequestBody CashReceiptDto cashReceiptDto) {
    if(!validationUtil.validatePodUpdate(id)){
      return new ApiResponse().getFailureResponse("Unauthorized!");
    }

    Optional<CashReceiptDto> cashReceiptDtoOptional = podService.updateCashReceipt(id, cashReceiptDto);
    if (cashReceiptDtoOptional.isPresent()) {

      return new ApiResponse().getSuccessResponse(null, cashReceiptDtoOptional.get());
    }
    return new ApiResponse().getFailureResponse(EApiResponseMessages.POD_UPDATE_FAILED.getMessage());
  }

  @GetMapping("/cashreceipts/bills/{customerBillNumber}")
  public ResponseEntity<?> getCashReceiptByCustomerBillNumber(@PathVariable String customerBillNumber) {
    Optional<CashReceiptDto> cashReceiptDtoOptional = podService.getCashReceiptByCustomerBillNumber(customerBillNumber);
    if (cashReceiptDtoOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null, cashReceiptDtoOptional.get());
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @GetMapping("/cashreceipts/dn/{dnNumber}")
  public ResponseEntity<?> getCashReceiptByDnNumber(@PathVariable String dnNumber) {
    Optional<CashReceiptDto> cashReceiptDtoOptional = podService.getCashReceiptByDnNumber(dnNumber);
    if (cashReceiptDtoOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null, cashReceiptDtoOptional.get());
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @GetMapping("/cashreceipts/{id}")
  public ResponseEntity<?> getCashOneReceipt(@PathVariable int id) {
    Optional<CashReceiptDto> cashReceiptDtoOptional = podService.getCashReceipt(id);
    if (cashReceiptDtoOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null, cashReceiptDtoOptional.get());
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @DeleteMapping("/cashreceipts/{id}")
  public ResponseEntity<?> deleteOneCashReceipt(@PathVariable int id) {
    Optional<CashReceipt> cashReceiptDtoOptional = podService.deleteCashReceipt(id);
    if (cashReceiptDtoOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse("Pod deleted successfully", cashReceiptDtoOptional.get());
    }
    return new ApiResponse().getFailureResponse("Pod delete failed");
  }

  /**
   * This API fetches all pods
   * 1. if super admin role, returns all existing pods
   * 2. if other than this, returns all existing pods by branch code of user
   *
   * @return Pod list
   */
  @GetMapping("/cashreceipts")
  public ResponseEntity<?> getAllCashReceipts() {
    return new ApiResponse().getSuccessResponse(null, podService.getAllCashReceipts());
  }

  @GetMapping("/cashreceipts/filters")
  public ResponseEntity<?> filterPods(int driver, String dispatchDate) {
    return new ApiResponse().getSuccessResponse(null, podService.filterCashReceipt(driver, dispatchDate));
  }

  @GetMapping("/cashreceipts/bookings/{bookingId}")
  public ResponseEntity<?> getAllCashReceiptsByBookingCode(@PathVariable int bookingId, Status status) {
    if (null != status) {
      return new ApiResponse().getSuccessResponse(null, podService.getAllCashReceiptsByBookingIdAndStatus(bookingId, status));
    } else {
      return new ApiResponse().getSuccessResponse(null, podService.getAllCashReceiptsByBookingId(bookingId));
    }

  }


  @PatchMapping("/cashreceipts/{id}/status")
  public ResponseEntity<?> updateCashReceiptStatus(@PathVariable int id, @RequestBody CashReceiptDto cashReceiptDto, String type) {
    boolean updateStatus = podService.updateCashReceiptStatus(id, cashReceiptDto, type);
    if (updateStatus) {
      return new ApiResponse().getSuccessResponse(EApiResponseMessages.POD_STATUS_UPDATED.getMessage(), true);
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @GetMapping("/drivers/tasks")
  public ResponseEntity<?> getDriverAssignedTasks() {
    List<CashReceipt> cashReceipts = podService.getCurrentUserDriverAssignedPods();
    return new ApiResponse().getSuccessResponse(null, cashReceiptDtoAssembler.toDriverAssignedTasks(cashReceipts));
  }

  /**
   * {
   * "message": "OK",
   * "httpStatus": "OK",
   * "status": 200,
   * "data": {
   * "completed": 3,
   * "total": 51
   * }
   * }
   *
   * @return
   */
  @GetMapping("/drivers/tasks/ratios")
  public ResponseEntity<?> getDriverTasksRatio() {
    List<CashReceipt> cashReceipts = podService.getCurrentUserDriverAssignedPods();

    long completed = cashReceipts.stream().filter(cashReceipt -> cashReceipt.getPodStatus().equals(Status.DELIVERED)).count();
    long total = cashReceipts.size();

    DriverTasksRatio driverTasksRatio = new DriverTasksRatio();
    driverTasksRatio.setCompleted(completed);
    driverTasksRatio.setTotal(total);

    return new ApiResponse().getSuccessResponse(null, driverTasksRatio);
  }

  @PostMapping("/drivers/pods/{id}")
  public ResponseEntity<?> updateCashReceiptStatusByDriver(@PathVariable int id, @ModelAttribute DriverPodUpdateDto driverPodUpdateDto) {
    boolean updateStatus = podService.updateCashReceiptStatus(id, driverPodUpdateDto.getStatus());
    if (updateStatus) {

      if (null != driverPodUpdateDto.getSignature()) {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setType("POD");
        fileInfo.setSubType("SIGNATURE");
        fileInfo.setRefId(id);
        filesStorageService.createDocuments(driverPodUpdateDto.getSignature(), fileInfo);
      }

      if (null != driverPodUpdateDto.getBill()) {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setType("POD");
        fileInfo.setSubType("BILL");
        fileInfo.setRefId(id);
        filesStorageService.createDocuments(driverPodUpdateDto.getBill(), fileInfo);
      }

      return new ApiResponse().getSuccessResponse(EApiResponseMessages.POD_STATUS_UPDATED.getMessage(), true);
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @GetMapping("/cashreceipts/uploads")
  public ResponseEntity<?> getPodsWithUploads() {
    return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), podService.getPodsWithUploads());
  }

  @PostMapping(value = "/cashreceipts/images/download")
  public ResponseEntity<?> downloadAccumulatedPdfOfPodUploads(@RequestBody PodUploadsDownloadRequest request, HttpServletResponse response) {

    if (!request.getPods().isEmpty()) {
      List<Documents> documents = filesStorageService.findDocuments("POD", request.getPods());
      if (!documents.isEmpty()) {
        List<String> imageUrls = new ArrayList<>();
        documents.forEach(d -> imageUrls.add(d.getUrl()));

        ByteArrayOutputStream byteArrayOutputStream = pdfUtility.downloadImagesToPdf(imageUrls);
        byte[] encoded = Base64.getEncoder().encode(byteArrayOutputStream.toByteArray());

        return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), new String(encoded));
      }
    }
    return new ApiResponse().getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
  }
}
