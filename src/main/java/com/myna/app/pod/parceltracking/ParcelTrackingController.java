package com.myna.app.pod.parceltracking;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.pod.CashReceipt;
import com.myna.app.pod.CashReceiptDto;
import com.myna.app.pod.CashReceiptDtoAssembler;
import com.myna.app.pod.PodService;
import com.myna.app.transitions.ParcelTrackingTransitionResponse;
import com.myna.app.transitions.Transition;
import com.myna.app.transitions.TransitionService;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.service.VendorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/public/api/v1")
public class ParcelTrackingController {
  private PodService podService;
  private TransitionService transitionService;
  private VendorRequestService vendorRequestService;
  private VendorRequestDtoAssembler vendorRequestDtoAssembler;

  @Autowired
  public void setPodService(PodService podService) {
    this.podService = podService;
  }

  @Autowired
  public void setTransitionService(TransitionService transitionService) {
    this.transitionService = transitionService;
  }

  @Autowired
  public void setVendorRequestService(VendorRequestService vendorRequestService) {
    this.vendorRequestService = vendorRequestService;
  }

  @Autowired
  public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
    this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
  }

  @GetMapping("/parcels")
  public ResponseEntity<?> getParcelDetails(@ModelAttribute ParcelTrackingRequest request) {
    if (!request.getDn().isEmpty()) {
      Optional<CashReceiptDto> cashReceiptOptional = podService.getCashReceiptByDnNumber(request.getDn());
      if (cashReceiptOptional.isPresent()) {
        return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), cashReceiptOptional.get());
      }
    } else if (!request.getCustomerBillNumber().isEmpty()) {
      Optional<CashReceiptDto> cashReceiptOptional = podService.getCashReceiptByCustomerBillNumber(request.getCustomerBillNumber());
      if (cashReceiptOptional.isPresent()) {
        return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), cashReceiptOptional.get());
      }
    }
    return new ApiResponse().getDataNotFoundResponse(HttpStatus.NOT_FOUND.getReasonPhrase());
  }

  @GetMapping("/transitions/pods/{dn}")
  public ResponseEntity<?> getTransitionsForPod(@PathVariable String dn) {
    Optional<CashReceipt> cashReceiptOptional = podService.getEntityByDn(dn);
    if (cashReceiptOptional.isPresent()) {
      List<Transition> transitionList = transitionService.getTransitionsFromDn(dn);

      List<ParcelTrackingTransitionResponse> response = new ArrayList<>();
      if (!transitionList.isEmpty()) {
        transitionList.forEach(transition -> {
          List<VendorRequest> vendorRequests = vendorRequestService.findAllByTransitionsIn(transitionList);
          if (!vendorRequests.isEmpty()) {
            vendorRequests.forEach(vendorRequest -> {
              ParcelTrackingTransitionResponse parcelTrackingTransitionResponse = new ParcelTrackingTransitionResponse();
              parcelTrackingTransitionResponse.setBooking(vendorRequestDtoAssembler.toModel(vendorRequest));
              parcelTrackingTransitionResponse.setTransition(transition);

              response.add(parcelTrackingTransitionResponse);
            });
          } else {
            ParcelTrackingTransitionResponse parcelTrackingTransitionResponse = new ParcelTrackingTransitionResponse();
            parcelTrackingTransitionResponse.setTransition(transition);

            response.add(parcelTrackingTransitionResponse);
          }
        });
        return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), response);
      }
    }
    return new ApiResponse().getSuccessResponse(HttpStatus.NOT_FOUND.getReasonPhrase(), new ArrayList<>());
  }
}
