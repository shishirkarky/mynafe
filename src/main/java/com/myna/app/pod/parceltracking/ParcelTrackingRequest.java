package com.myna.app.pod.parceltracking;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParcelTrackingRequest {
  private String dn;
  private String customerBillNumber;
}
