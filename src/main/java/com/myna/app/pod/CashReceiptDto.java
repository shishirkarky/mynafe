/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.pod;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.myna.app.customer.entity.Customer;
import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.enums.Status;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class CashReceiptDto {
  private List<Integer> ids;
  private int id;
  private String dn;

  /**
   * Consignor Details {@link com.myna.app.vendor.entity.Vendor}
   */
  private Vendor vendor;
  private String consignorName;
  private String consignorVat;
  private String vendorInvoiceNumber;

  private int refToLocationId;
  private int refFromLocationId;
  private int refVendorRequestId;
  private String date;
  @Deprecated
  private String dateEn;

  private String branchCode;
  /**
   * Consignee Details
   */
  private Customer consignee;
  private String consigneeName;
  private String consigneeNumber;
  private String consigneeContact;
  private String customerBillNumber;
  private String consigneeVat;
  private String customerBillDate;

  /**
   * Receiver Details
   */
  private String receiverName;
  private String receiverContact;
  private String receivedDate;
  private String receivedDateEn;
  /**
   * POD Status Details
   */
  private Status podStatus = Status.ACTIVE;
  private String podStatusRemarks;
  private String podStatusUpdateDate;
  private String podStatusUpdateDateEn;
  private String deliveredDate;

  private ConfigLocationsDto toLocation;

  private ConfigLocationsDto fromLocation;
  private VendorRequestDto vendorRequest;
  private VendorRequestDto transitBooking;

  private List<CashReceiptDetailsDto> cashReceiptDetails;

  //todo set default logo
  private String logo;

  private boolean transit;

  private List<CashReceipt.PodPhoto> photos;

  private String createdBy;
  private String lastModifiedBy;
}
