package com.myna.app.pod;

import com.myna.app.commons.Auditable;
import com.myna.app.customer.entity.Customer;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.enums.Status;
import com.myna.app.vendor.entity.Vendor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "pod_drop_log")
public class PodDropLog extends Auditable<String> {
  @Id
  private int id;

  @Column(unique = true)
  private String dn;
  @OneToOne(optional = true)
  private Vendor vendor;
  private String consignorName;
  private String consignorVat;
  private String vendorInvoiceNumber;

  private int refToLocationId;
  @OneToOne(optional = true)
  private ConfigLocations toLocation;
  private int refFromLocationId;
  @OneToOne(optional = true)
  private ConfigLocations fromLocation;
  private int refVendorRequestId;
  private String date;

  private String branchCode;
  /**
   * Consignee Details
   */
  @OneToOne(optional = true)
  private Customer consignee;
  private String consigneeName;
  private String consigneeContact;
  private String customerBillNumber;
  private String consigneeVat;
  private String customerBillDate;

  /**
   * Receiver Details
   */
  private String receiverName;
  private String receiverContact;
  private String receivedDate;
  private String receivedDateEn;
  /**
   * POD Status Details
   */
  @Enumerated(EnumType.STRING)
  private Status podStatus = Status.ACTIVE;
  private String podStatusRemarks;
  private String podStatusUpdateDate;
  private String podStatusUpdateDateEn;
  private String deliveredDate;
}
