/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.pod;

import com.myna.app.dataconfig.packages.ConfigPackagesDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CashReceiptDetailsDto {
    private int id;
    private int refCashReceiptId;
    private String description;
    private ConfigPackagesDto packageDetails;
    private double quantity;
    private String unitOfMeasurement;
    private String noOfPackage;
    private double rate;
    private double total;
    private double value;
    private double taxableAmount;
    private String branchCode;
    private String remarks;
}
