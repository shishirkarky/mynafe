/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vehicle.entity;

import com.myna.app.commons.Auditable;
import com.myna.app.vehicle.enums.VehicleType;
import com.myna.app.enums.Status;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity(name = "vehicle")
public class Vehicle extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String description;

	@Enumerated(EnumType.STRING)
	private VehicleType type;

	private String registrationNumber;

	private String ownerFirstName;

	private String ownerMiddleName;

	private String ownerLastName;

	private String chasisNumber;

	private String modelNumber;

	private int buildYear;

	private String color;

	private double currentMileage;

	@Enumerated(EnumType.STRING)
	private Status status;

}
