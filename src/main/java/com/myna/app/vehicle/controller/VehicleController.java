/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vehicle.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.enums.Status;
import com.myna.app.vehicle.assembler.VehicleDtoAssembler;
import com.myna.app.vehicle.dto.VehicleDto;
import com.myna.app.vehicle.entity.Vehicle;
import com.myna.app.vehicle.enums.VehicleType;
import com.myna.app.vehicle.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class VehicleController {

    private VehicleService vehicleService;

    private VehicleDtoAssembler vehicleDtoAssembler;

    @Autowired
    public void setVehicleService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @Autowired
    public void setVehicleDtoAssembler(VehicleDtoAssembler vehicleDtoAssembler) {
        this.vehicleDtoAssembler = vehicleDtoAssembler;
    }

    @PostMapping("/vehicles")
    public ResponseEntity<?> createVehicle(@RequestBody VehicleDto vehicleDto) {
        Optional<Vehicle> vehicleOptional = vehicleService.createVehicle(vehicleDtoAssembler.toDomain(vehicleDto));
        if (vehicleOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vehicleDtoAssembler.toModel(vehicleOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/vehicles/{id}")
    public ResponseEntity<?> updateVehicle(@PathVariable int id, @RequestBody VehicleDto vehicleDto) {
        Optional<Vehicle> vehicleOptional = vehicleService.updateVehicle(id, vehicleDtoAssembler.toDomain(vehicleDto));
        if (vehicleOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vehicleDtoAssembler.toModel(vehicleOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/vehicles/{id}")
    public ResponseEntity<?> getOneVehicle(@PathVariable int id) {
        Optional<Vehicle> vehicleOptional = vehicleService.getOneVehicle(id);
        if (vehicleOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vehicleDtoAssembler.toModel(vehicleOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/vehicles")
    public ResponseEntity<?> getAllVehicles() {
        return new ApiResponse().getSuccessResponse(null, vehicleDtoAssembler.toModels(vehicleService.getAllVehicles()));
    }

    @GetMapping("/vehicles/vehicletypes/{vehicleType}")
    public ResponseEntity<?> getAllVehiclesByVehicleTypes(@PathVariable VehicleType vehicleType) {
        return new ApiResponse().getSuccessResponse(null, vehicleDtoAssembler.toModels(vehicleService.getAllVehiclesByVehicleType(vehicleType)));
    }

    @GetMapping("/vehicles/{id}/{status}")
    public ResponseEntity<?> updateVehicleStatus(@PathVariable int id, @PathVariable Status status) {
        Optional<Vehicle> vehicleOptional = vehicleService.updateVehicleStatus(id, status);
        if (vehicleOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vehicleDtoAssembler.toModel(vehicleOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

}
