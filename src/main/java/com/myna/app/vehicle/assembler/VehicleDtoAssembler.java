/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vehicle.assembler;

import com.myna.app.vehicle.dto.VehicleDto;
import com.myna.app.vehicle.entity.Vehicle;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VehicleDtoAssembler {

	public Vehicle toDomain(VehicleDto vehicleDto) {
		Vehicle vehicle = new Vehicle();
		CustomBeanUtils.copyNonNullProperties(vehicleDto, vehicle);
		return vehicle;
	}

	public VehicleDto toModel(Vehicle vehicle) {
		VehicleDto vehicleDto = new VehicleDto();
		CustomBeanUtils.copyNonNullProperties(vehicle, vehicleDto);
		return vehicleDto;
	}

	public List<VehicleDto> toModels(List<Vehicle> vehicleList) {
		List<VehicleDto> vehicleDtos = new ArrayList<>();
		vehicleList.forEach(vehicle -> vehicleDtos.add(this.toModel(vehicle)));
		return vehicleDtos;
	}

}
