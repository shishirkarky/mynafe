/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vehicle.service;

import com.myna.app.vehicle.entity.Vehicle;
import com.myna.app.enums.Status;
import com.myna.app.vehicle.enums.VehicleType;

import java.util.List;
import java.util.Optional;

public interface VehicleService {
    int countTotal();

    Optional<Vehicle> createVehicle(Vehicle vehicle);

    Optional<Vehicle> updateVehicle(int id, Vehicle vehicle);

    List<Vehicle> getAllVehicles();

    List<Vehicle> getAllVehiclesByVehicleType(VehicleType vehicleType);

    Optional<Vehicle> getOneVehicle(int id);

    Optional<Vehicle> updateVehicleStatus(int id, Status status);

}
