/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vehicle.service;

import com.myna.app.vehicle.entity.Vehicle;
import com.myna.app.vehicle.enums.VehicleType;
import com.myna.app.vehicle.repository.VehicleRepository;
import com.myna.app.enums.Status;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService {

	private VehicleRepository vehicleRepository;

	@Autowired
	public void setVehicleRepository(VehicleRepository vehicleRepository) {
		this.vehicleRepository = vehicleRepository;
	}

	@Override
	public int countTotal() {
		return (int) vehicleRepository.count();
	}

	@Override
	public Optional<Vehicle> createVehicle(Vehicle vehicle) {
		return Optional.of(vehicleRepository.save(vehicle));
	}

	@Override
	public Optional<Vehicle> updateVehicle(int id, Vehicle vehicle) {
		Optional<Vehicle> vehicleOptional = vehicleRepository.findById(id);
		if (vehicleOptional.isPresent()) {
			vehicle.setId(id);

			Vehicle existingVehicle = vehicleOptional.get();
			CustomBeanUtils.copyNonNullProperties(vehicle, existingVehicle);
			return Optional.of(vehicleRepository.save(existingVehicle));
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public List<Vehicle> getAllVehicles() {
		return vehicleRepository.findAll();
	}

	@Override
	public List<Vehicle> getAllVehiclesByVehicleType(VehicleType vehicleType) {
		return vehicleRepository.findAllByType(vehicleType);
	}

	@Override
	public Optional<Vehicle> getOneVehicle(int id) {
		return vehicleRepository.findById(id);
	}

	@Override
	public Optional<Vehicle> updateVehicleStatus(int id, Status status) {
		Optional<Vehicle> vehicleOptional = vehicleRepository.findById(id);
		if (vehicleOptional.isPresent()) {
			Vehicle existingVehicle = vehicleOptional.get();
			existingVehicle.setStatus(status);
			return Optional.of(vehicleRepository.save(existingVehicle));
		}
		else {
			return Optional.empty();
		}
	}

}
