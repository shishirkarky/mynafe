/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.office.service;

import com.myna.app.office.assembler.OfficeDtoAssembler;
import com.myna.app.office.dto.OfficeDto;
import com.myna.app.office.entity.Office;
import com.myna.app.office.repository.OfficeRepository;
import com.myna.app.enums.Status;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeServiceImpl implements OfficeService {

	private OfficeRepository officeRepository;
	private OfficeDtoAssembler officeDtoAssembler;

	@Autowired
	public void setOfficeRepository(OfficeRepository officeRepository) {
		this.officeRepository = officeRepository;
	}

	@Autowired
	public void setOfficeDtoAssembler(OfficeDtoAssembler officeDtoAssembler) {
		this.officeDtoAssembler = officeDtoAssembler;
	}

	@Override
	public Optional<Office> createOffice(Office office) {
		return Optional.of(officeRepository.save(office));
	}

	@Override
	public Optional<Office> updateOffice(String officeCode, Office office) {
		Optional<Office> officeOptional = officeRepository.findByOfficeCode(officeCode);
		if (officeOptional.isPresent()) {
			office.setOfficeCode(officeCode);
			Office existingOffice = officeOptional.get();
			CustomBeanUtils.copyNonNullProperties(office, existingOffice);
			return Optional.of(officeRepository.save(existingOffice));
		}
		return Optional.empty();
	}

	@Override
	public List<Office> getAllOffices() {
		return officeRepository.findAll();
	}

	@Override
	public Optional<Office> getOneOffice(String officeCode) {
		return officeRepository.findByOfficeCode(officeCode);
	}

	@Override
	public Optional<Office> updateOfficeStatus(String officeCode, Status status) {
		Optional<Office> officeOptional = officeRepository.findByOfficeCode(officeCode);
		if (officeOptional.isPresent()) {
			Office existingOffice = officeOptional.get();
			existingOffice.setStatus(status);
			return Optional.of(officeRepository.save(existingOffice));
		}
		return Optional.empty();
	}

	@Override
	public String getOfficeLocation(String officeCode) {
		Optional<Office> officeOptional = this.getOneOffice(officeCode);
		if(officeOptional.isPresent()){
			OfficeDto officeDto = officeDtoAssembler.toModel(officeOptional.get());
			if(null!=officeDto.getLocation()){
				return officeDto.getLocation().getCity() + ", " + officeDto.getLocation().getDistrict();
			}
		}
		return "";
	}

}
