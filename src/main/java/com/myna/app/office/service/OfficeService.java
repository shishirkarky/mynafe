/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.office.service;

import com.myna.app.office.entity.Office;
import com.myna.app.enums.Status;

import java.util.List;
import java.util.Optional;

public interface OfficeService {

	Optional<Office> createOffice(Office office);

	Optional<Office> updateOffice(String officeCode, Office office);

	List<Office> getAllOffices();

	Optional<Office> getOneOffice(String officeCode);

	Optional<Office> updateOfficeStatus(String officeCode, Status status);

	String getOfficeLocation(String officeCode);

}
