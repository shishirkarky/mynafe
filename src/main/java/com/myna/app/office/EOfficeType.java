package com.myna.app.office;

public enum EOfficeType {
    HEAD_OFFICE,
    BRANCH_OFFICE,
    WARE_HOUSE
}
