/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.office.assembler;

import com.myna.app.dataconfig.EntityConfigService;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDtoAssembler;
import com.myna.app.office.dto.OfficeDto;
import com.myna.app.office.entity.Office;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OfficeDtoAssembler {
	private EntityConfigService entityConfigService;
	private ConfigLocationsDtoAssembler configLocationsDtoAssembler;

	@Autowired
	public void setEntityConfigService(EntityConfigService entityConfigService) {
		this.entityConfigService = entityConfigService;
	}

	@Autowired
	public void setConfigLocationsDtoAssembler(ConfigLocationsDtoAssembler configLocationsDtoAssembler) {
		this.configLocationsDtoAssembler = configLocationsDtoAssembler;
	}

	public Office toDomain(OfficeDto officeDto) {
		var office = new Office();
		CustomBeanUtils.copyNonNullProperties(officeDto, office);
		return office;
	}

	public OfficeDto toModel(Office office) {
		var officeDto = new OfficeDto();
		CustomBeanUtils.copyNonNullProperties(office, officeDto);

		Optional<ConfigLocations> configLocationsOptional = entityConfigService.getOneLocations(office.getRefLocationId());
		configLocationsOptional.ifPresent(configLocations -> officeDto.setLocation(configLocationsDtoAssembler.toModel(configLocations)));

		return officeDto;
	}

	public List<OfficeDto> toModels(List<Office> officeList) {
		List<OfficeDto> officeDtos = new ArrayList<>();
		officeList.forEach(office -> officeDtos.add(toModel(office)));
		return officeDtos;
	}

}
