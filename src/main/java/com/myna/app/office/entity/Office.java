/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.office.entity;

import com.myna.app.commons.Auditable;
import com.myna.app.enums.Status;
import com.myna.app.office.EOfficeType;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity(name = "office")
public class Office extends Auditable<String> {

	@Id
	private String officeCode;

	private int refLocationId;

	private String category;

	private String facility;

	private String officeRepresentativeName;

	private String phoneNumber;

	private String email;

	@Enumerated(EnumType.STRING)
	private EOfficeType type;

	@Enumerated(EnumType.STRING)
	private Status status = Status.ACTIVE;

}
