/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.office.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.enums.Status;
import com.myna.app.office.assembler.OfficeDtoAssembler;
import com.myna.app.office.dto.OfficeDto;
import com.myna.app.office.entity.Office;
import com.myna.app.office.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class OfficeController {

    private OfficeService officeService;

    private OfficeDtoAssembler officeDtoAssembler;

    @Autowired
    public void setOfficeService(OfficeService officeService) {
        this.officeService = officeService;
    }

    @Autowired
    public void setOfficeDtoAssembler(OfficeDtoAssembler officeDtoAssembler) {
        this.officeDtoAssembler = officeDtoAssembler;
    }

    @PostMapping("/offices")
    public ResponseEntity<?> createOffice(@RequestBody OfficeDto officeDto) {
        var office = officeDtoAssembler.toDomain(officeDto);
        Optional<Office> officeOptional = officeService.createOffice(office);
        if (officeOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, officeDtoAssembler.toModel(officeOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/offices/{officeCode}")
    public ResponseEntity<?> updateOffice(@PathVariable String officeCode,
                                          @RequestBody OfficeDto officeDto) {
        var office = officeDtoAssembler.toDomain(officeDto);
        Optional<Office> officeOptional = officeService.updateOffice(officeCode, office);
        if (officeOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, officeDtoAssembler.toModel(officeOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/offices")
    public ResponseEntity<?> getAllOffices() {
        return new ApiResponse().getSuccessResponse(null, officeDtoAssembler.toModels(officeService.getAllOffices()));
    }

    @GetMapping("/offices/{officeCode}")
    public ResponseEntity<?> getOneOffice(@PathVariable String officeCode) {
        Optional<Office> officeOptional = officeService.getOneOffice(officeCode);
        if (officeOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, officeDtoAssembler.toModel(officeOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PatchMapping("/offices/{officeCode}/{status}")
    public ResponseEntity<?> updateOfficeStatus(@PathVariable(required = true) String officeCode,
                                                @PathVariable(required = true) Status status) {
        Optional<Office> officeOptional = officeService.updateOfficeStatus(officeCode, status);
        if(officeOptional.isPresent()){
            return new ApiResponse().getSuccessResponse(null, officeDtoAssembler.toModel(officeOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

}
