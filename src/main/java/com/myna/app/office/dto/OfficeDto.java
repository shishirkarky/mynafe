/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.office.dto;

import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.office.EOfficeType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OfficeDto {

    private String officeCode;

    private int refLocationId;

    private ConfigLocationsDto location;

    private String category;

    private String facility;

    private String officeRepresentativeName;

    private String phoneNumber;

    private String email;

    private EOfficeType type;

    private String status;

}
