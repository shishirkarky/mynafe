/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.driver.assembler;

import com.myna.app.driver.dto.DriverDto;
import com.myna.app.driver.entity.Driver;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DriverDtoAssembler {

	public Driver toDomain(DriverDto driverDto) {
		Driver driver = new Driver();
		CustomBeanUtils.copyNonNullProperties(driverDto, driver);
		return driver;
	}

	public DriverDto toModel(Driver driver) {
		DriverDto driverDto = new DriverDto();
		CustomBeanUtils.copyNonNullProperties(driver, driverDto);
		return driverDto;
	}

	public List<DriverDto> toModels(List<Driver> drivers) {
		List<DriverDto> driverDtos = new ArrayList<>();
		drivers.forEach(driver -> driverDtos.add(this.toModel(driver)));
		return driverDtos;
	}

}
