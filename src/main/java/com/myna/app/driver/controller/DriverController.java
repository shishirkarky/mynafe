/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.driver.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.driver.assembler.DriverDtoAssembler;
import com.myna.app.driver.dto.DriverDto;
import com.myna.app.driver.entity.Driver;
import com.myna.app.driver.service.DriverService;
import com.myna.app.enums.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class DriverController {

    private DriverService driverService;

    private DriverDtoAssembler driverDtoAssembler;

    @Autowired
    public void setDriverService(DriverService driverService) {
        this.driverService = driverService;
    }

    @Autowired
    public void setDriverDtoAssembler(DriverDtoAssembler driverDtoAssembler) {
        this.driverDtoAssembler = driverDtoAssembler;
    }

    @PostMapping("/drivers")
    public ResponseEntity<?> createDriver(@RequestBody DriverDto driverDto) {
        Optional<Driver> driverOptional = driverService.createDriver(driverDtoAssembler.toDomain(driverDto));
        if (driverOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, driverDtoAssembler.toModel(driverOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/drivers/{id}")
    public ResponseEntity<?> updateDriver(@PathVariable int id, @RequestBody DriverDto driverDto) {
        Optional<Driver> driverOptional = driverService.updateDriver(id, driverDtoAssembler.toDomain(driverDto));
        if (driverOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, driverDtoAssembler.toModel(driverOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/drivers/{id}")
    public ResponseEntity<?> getOneDriver(@PathVariable int id) {
        Optional<Driver> driverOptional = driverService.getOneDriver(id);
        if (driverOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, driverDtoAssembler.toModel(driverOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/drivers")
    public ResponseEntity<?> getAllDrivers() {
        return new ApiResponse().getSuccessResponse(null, driverDtoAssembler.toModels(driverService.getAllDrivers()));
    }

    @GetMapping("/drivers/{id}/{status}")
    public ResponseEntity<?> updateDriverStatus(@PathVariable int id, @PathVariable Status status) {
        Optional<Driver> driverOptional = driverService.updateDriverStatus(id, status);
        if (driverOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, driverDtoAssembler.toModel(driverOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

}
