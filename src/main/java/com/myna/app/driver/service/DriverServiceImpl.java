/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.driver.service;

import com.myna.app.driver.entity.Driver;
import com.myna.app.driver.repository.DriverRepository;
import com.myna.app.enums.Status;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DriverServiceImpl implements DriverService {

	private DriverRepository driverRepository;

	@Autowired
	public void setDriverRepository(DriverRepository driverRepository) {
		this.driverRepository = driverRepository;
	}

	@Override
	public int countTotal() {
		return (int) driverRepository.count();
	}

	@Override
	public Optional<Driver> createDriver(Driver driver) {
		return Optional.of(driverRepository.save(driver));
	}

	@Override
	public Optional<Driver> updateDriver(int id, Driver driver) {
		Optional<Driver> driverOptional = driverRepository.findById(id);
		if (driverOptional.isPresent()) {
			driver.setId(id);

			Driver existingDriver = driverOptional.get();
			CustomBeanUtils.copyNonNullProperties(driver, existingDriver);
			return Optional.of(driverRepository.save(existingDriver));
		}
		return Optional.empty();
	}

	@Override
	public List<Driver> getAllDrivers() {
		return driverRepository.findAll();
	}

	@Override
	public Optional<Driver> getOneDriver(int id) {
		return driverRepository.findById(id);
	}

	@Override
	public Optional<Driver> updateDriverStatus(int id, Status status) {
		Optional<Driver> driverOptional = driverRepository.findById(id);
		if (driverOptional.isPresent()) {
			Driver existingDriver = driverOptional.get();
			existingDriver.setStatus(status);
			return Optional.of(driverRepository.save(existingDriver));
		}
		return Optional.empty();
	}

}
