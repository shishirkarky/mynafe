/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.driver.service;

import com.myna.app.driver.entity.Driver;
import com.myna.app.enums.Status;

import java.util.List;
import java.util.Optional;

public interface DriverService {

    int countTotal();

    Optional<Driver> createDriver(Driver driver);

    Optional<Driver> updateDriver(int id, Driver driver);

    List<Driver> getAllDrivers();

    Optional<Driver> getOneDriver(int id);

    Optional<Driver> updateDriverStatus(int id, Status status);

}
