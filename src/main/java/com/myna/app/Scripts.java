package com.myna.app;

import com.myna.app.enums.Status;
import com.myna.app.pod.CashReceipt;
import com.myna.app.pod.CashReceiptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class Scripts {
  @Autowired private CashReceiptRepository cashReceiptRepository;
  @PostConstruct
  public void script(){
//    this.updatePodDeliveredDate();
  }

  /**
   * new element deliveredDate is added, podStatusUpdate date is added when status is changed to DELIVERED
   */
  protected void updatePodDeliveredDate(){
   List<CashReceipt> cashReceipts = cashReceiptRepository.findAll();
   cashReceipts.forEach(cashReceipt -> {
     if(Status.DELIVERED.equals(cashReceipt.getPodStatus())){
       cashReceipt.setDeliveredDate(cashReceipt.getPodStatusUpdateDate());
     }
   });
   cashReceiptRepository.saveAll(cashReceipts);
  }

  protected void updatePodDeliveredDateRollBack(){

  }

}
