/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.customer.assembler;

import com.myna.app.customer.dto.CustomerDto;
import com.myna.app.customer.entity.Customer;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerDtoAssembler {

	public Customer toDomain(CustomerDto customerDto) {
		Customer customer = new Customer();
		CustomBeanUtils.copyNonNullProperties(customerDto, customer);
		return customer;
	}

	public CustomerDto toModel(Customer customer) {
		CustomerDto customerDto = new CustomerDto();
		CustomBeanUtils.copyNonNullProperties(customer, customerDto);
		return customerDto;
	}

	public List<CustomerDto> toModels(List<Customer> customerList) {
		List<CustomerDto> customerDtos = new ArrayList<>();
		customerList.forEach(customer -> customerDtos.add(this.toModel(customer)));
		return customerDtos;
	}

}
