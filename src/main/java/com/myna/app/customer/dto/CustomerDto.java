/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.customer.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDto {

	private int id;

	private String firstName;

	private String middleName;

	private String lastName;

	private String address;

	private String phone;

	private String panNumber;

	private String receiverName;

	private String signature;

	private String email;

}
