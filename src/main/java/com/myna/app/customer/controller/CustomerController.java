/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.customer.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.customer.assembler.CustomerDtoAssembler;
import com.myna.app.customer.dto.CustomerDto;
import com.myna.app.customer.entity.Customer;
import com.myna.app.customer.service.CustomerService;
import com.myna.app.enums.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class CustomerController {

    private CustomerService customerService;

    private CustomerDtoAssembler customerDtoAssembler;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setCustomerDtoAssembler(CustomerDtoAssembler customerDtoAssembler) {
        this.customerDtoAssembler = customerDtoAssembler;
    }

    @PostMapping("/customers")
    public ResponseEntity<?> createCustomer(@RequestBody CustomerDto customerDto) {
        Optional<Customer> customerOptional = customerService
                .createCustomer(customerDtoAssembler.toDomain(customerDto));
        if(customerOptional.isPresent()){
            return new ApiResponse().getSuccessResponse(null, customerDtoAssembler.toModel(customerOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable int id, @RequestBody CustomerDto customerDto) {
        Optional<Customer> customerOptional = customerService.updateCustomer(id,
                customerDtoAssembler.toDomain(customerDto));
        if(customerOptional.isPresent()){
            return new ApiResponse().getSuccessResponse(null, customerDtoAssembler.toModel(customerOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<?> getOneCustomer(@PathVariable int id) {
        Optional<Customer> customerOptional = customerService.getOneCustomer(id);
        if(customerOptional.isPresent()){
            return new ApiResponse().getSuccessResponse(null, customerDtoAssembler.toModel(customerOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/customers")
    public ResponseEntity<?> getAllCustomers() {
        return new ApiResponse().getSuccessResponse(null, customerDtoAssembler.toModels(customerService.getAllCustomers()));
    }

    @GetMapping("/customers/{id}/{status}")
    public ResponseEntity<?> updateCustomerStatus(@PathVariable int id, @PathVariable Status status) {
        Optional<Customer> customerOptional = customerService.updateCustomerStatus(id, status);
        if(customerOptional.isPresent()){
            return new ApiResponse().getSuccessResponse(null, customerDtoAssembler.toModel(customerOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

}
