/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.customer.entity;

import com.myna.app.commons.PersonalInformation;
import com.myna.app.enums.Status;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity(name = "customer")
public class Customer extends PersonalInformation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String panNumber;

	private String receiverName;

	private String signature;
}
