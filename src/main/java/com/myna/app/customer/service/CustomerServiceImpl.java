/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.customer.service;

import com.myna.app.customer.entity.Customer;
import com.myna.app.customer.repository.CustomerRepository;
import com.myna.app.enums.Status;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Optional<Customer> createCustomer(Customer customer) {
        return Optional.of(customerRepository.save(customer));
    }

    @Override
    public Optional<Customer> updateCustomer(int id, Customer customer) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isPresent()) {
            customer.setId(id);
            Customer existingCustomer = customerOptional.get();
            CustomBeanUtils.copyNonNullProperties(customer, existingCustomer);
            return Optional.of(customerRepository.save(existingCustomer));
        }
        return Optional.empty();
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Optional<Customer> getOneCustomer(int id) {
        return customerRepository.findById(id);
    }

    @Override
    public Optional<Customer> updateCustomerStatus(int id, Status status) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isPresent()) {
            Customer existingCustomer = customerOptional.get();
            existingCustomer.setStatus(status);
            return Optional.of(customerRepository.save(existingCustomer));
        }
        return Optional.empty();
    }

}
