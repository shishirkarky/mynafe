/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.customer.service;

import com.myna.app.customer.entity.Customer;
import com.myna.app.enums.Status;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

	Optional<Customer> createCustomer(Customer customer);

	Optional<Customer> updateCustomer(int id, Customer customer);

	List<Customer> getAllCustomers();

	Optional<Customer> getOneCustomer(int id);

	Optional<Customer> updateCustomerStatus(int id, Status status);

}
