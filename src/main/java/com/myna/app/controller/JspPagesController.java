/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.controller;

import com.myna.app.constants.JspPageAPIConstants;
import com.myna.app.enums.EJspDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public class JspPagesController {

  @GetMapping(value = "/")
  public ModelAndView index(HttpServletRequest request) {
    return dashboard(request);
  }

  @GetMapping(value = JspPageAPIConstants.V1_LOGIN)
  public ModelAndView login(String error, String logout) {
    ModelAndView model = new ModelAndView(EJspDetails.LOGIN.getPage());
    if (!StringUtils.isEmpty(error)) {
      model.addObject("msg", error);
    }
    if (!StringUtils.isEmpty(logout)) {
      model.addObject("msg", logout);
    }
    return model;
  }

  @GetMapping(JspPageAPIConstants.V1_DASHBOARD)
  public ModelAndView dashboard(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.DASHBOARD.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.DASHBOARD.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_USER_CREATE)
  public ModelAndView createUser(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.USER_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.USER_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_USER_VIEW)
  public ModelAndView viewUser(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.USER_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.USER_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_USER_PROFILE_VIEW)
  public ModelAndView viewMyProfile(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.USER_PROFILE_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.USER_PROFILE_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
   * DRIVER PAGES
   */
  @GetMapping(JspPageAPIConstants.V1_DRIVER_CREATE)
  public ModelAndView createDriver(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.DRIVER_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.DRIVER_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
   * VEHICLE PAGES
   */
  @GetMapping(JspPageAPIConstants.V1_VEHICLE_CREATE)
  public ModelAndView createVehicle(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.VEHICLE_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.VEHICLE_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  VEHICLE LOG BOOK
   */
  @GetMapping(JspPageAPIConstants.V1_VEHICLE_LOG_BOOK_CREATE)
  public ModelAndView createVehicleLogBook(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.VEHICLE_LOG_BOOK_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.VEHICLE_LOG_BOOK_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_VEHICLE_LOG_BOOK_VIEW)
  public ModelAndView viewVehicleLogBook(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.VEHICLE_LOG_BOOK_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.VEHICLE_LOG_BOOK_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
   * CUSTOMER PAGES
   */
  @GetMapping(JspPageAPIConstants.V1_CUSTOMER_CREATE)
  public ModelAndView createCustomer(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CUSTOMER_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CUSTOMER_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_CUSTOMER_VIEW)
  public ModelAndView viewCustomer(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CUSTOMER_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CUSTOMER_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
   * VENDOR PAGES
   */
  @GetMapping(JspPageAPIConstants.V1_VENDOR_CREATE)
  public ModelAndView createVendor(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.VENDOR_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.VENDOR_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  OFFICE PAGES
   */
  @GetMapping(JspPageAPIConstants.V1_OFFICE_CREATE)
  public ModelAndView createOffice(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.OFFICE_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.OFFICE_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }


  /*
   * VENDOR REQUEST PAGE
   */
  @GetMapping(JspPageAPIConstants.V1_VENDOR_REQUEST_CREATE)
  public ModelAndView createVendorRequest(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.VENDOR_REQUEST_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.VENDOR_REQUEST_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_VENDOR_REQUEST_VIEW)
  public ModelAndView viewVendorRequest(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.VENDOR_REQUEST_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.VENDOR_REQUEST_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_BOOKING_PIPELINE_CREATE)
  public ModelAndView getBookingPipelinePage(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.BOOKING_PIPELINE_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.BOOKING_PIPELINE_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  CASH RECEIPT PAGE
   */
  @GetMapping(JspPageAPIConstants.V1_CASH_RECEIPT_CREATE)
  public ModelAndView createCashReceipt(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CASH_RECEIPT_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CASH_RECEIPT_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_CASH_RECEIPT_VIEW)
  public ModelAndView viewCashReceipt(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CASH_RECEIPT_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CASH_RECEIPT_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_CASH_RECEIPT_EDIT)
  public ModelAndView editCashReceipt(int id, HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CASH_RECEIPT_EDIT.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CASH_RECEIPT_EDIT.getTitle());

    model.addObject("id", id);
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_CASH_RECEIPT_DETAIL_VIEW)
  public ModelAndView viewCashReceiptDetail(int id, HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CASH_RECEIPT_DETAIL_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CASH_RECEIPT_DETAIL_VIEW.getTitle());

    model.addObject("id", id);
    return checkSessionSecurityContext(model);
  }

  /*
  FILE UPLOADS
   */
  @GetMapping(JspPageAPIConstants.V1_FILE_UPLOADS_VIEW)
  public ModelAndView viewFileUploads(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.FILES_UPLOAD_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.FILES_UPLOAD_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  CONFIG DATA
   */
    /*
    LOCATIONS
     */
  @GetMapping(JspPageAPIConstants.V1_CONFIG_LOCATION_CREATE)
  public ModelAndView createConfigLocations(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CONFIG_LOCATION_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CONFIG_LOCATION_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  PACKAGES
   */
  @GetMapping(JspPageAPIConstants.V1_CONFIG_PACKAGES_CREATE)
  public ModelAndView createConfigPackages(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CONFIG_PACKAGES_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CONFIG_PACKAGES_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  RATES
   */
  @GetMapping(JspPageAPIConstants.V1_CONFIG_RATE_CONFIGURATION)
  public ModelAndView createConfigRates(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.CONFIG_RATES_CONFIGURATION.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.CONFIG_RATES_CONFIGURATION.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  LOGO UPLOAD
   */
  @GetMapping(JspPageAPIConstants.V1_UPLOAD_LOGO)
  public ModelAndView uploadLogo(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.UPLOAD_LOGO.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.UPLOAD_LOGO.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
  STAFF DETAILS
   */
  @GetMapping(JspPageAPIConstants.V1_STAFF_CREATE)
  public ModelAndView createPage(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.STAFF_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.STAFF_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
   * REPORTS
   */
  @GetMapping(JspPageAPIConstants.V1_REPORTS_SEARCH)
  public ModelAndView reportsSearchPage(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.REPORTS_SEARCH.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.REPORTS_SEARCH.getTitle());
    return checkSessionSecurityContext(model);
  }

  /*
   * POD TRANSITION
   */
  @GetMapping(JspPageAPIConstants.V1_TRANSITION_CREATE)
  public ModelAndView createTransitionPage(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.POD_TRANSITION_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.POD_TRANSITION_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }


  /*
   * HIRING VEHICLE RATE CONFIGURATION
   */
  @GetMapping(JspPageAPIConstants.V1_HIRING_VEHICLE_RATE_CREATE)
  public ModelAndView createHiringVehicleRateConfig(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.HIRING_VEHICLE_RATE_CREATE.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.HIRING_VEHICLE_RATE_CREATE.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_POD_UPLOADS_VIEW)
  public ModelAndView getPodUploadsViewPage(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.POD_UPLOADS_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.POD_UPLOADS_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  @GetMapping(JspPageAPIConstants.V1_PARCEL_TRACKING)
  public ModelAndView getParcelTrackingPage(HttpServletRequest request) {
    ModelAndView model = new ModelAndView(EJspDetails.PARCEL_TRACKING_VIEW.getPage());
    model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.PARCEL_TRACKING_VIEW.getTitle());
    return checkSessionSecurityContext(model);
  }

  private ModelAndView checkSessionSecurityContext(ModelAndView modelAndView) {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    if (securityContext == null) {
      log.error("checkSessionSecurityContext() : Security context not found");
      //todo: this line should be uncommented. fix security context being null after some time
//            return login("Please Login", "");
    } else {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      String currentPrincipalName = authentication.getName();
      log.info("checkSessionSecurityContext() : Security context found as {}", currentPrincipalName);
    }
    return modelAndView;
  }
}
