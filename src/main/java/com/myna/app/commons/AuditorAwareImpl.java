/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.commons;

import com.myna.app.auth.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class AuditorAwareImpl implements AuditorAware<String> {
    private JwtUtil jwtUtil;

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Optional<String> getCurrentAuditor() {
        try {
            Optional<String> userOptional = jwtUtil.getCurrentUserNameFromToken();
            if (userOptional.isPresent()) {
                return userOptional;
            } else {
                return Optional.of("unknown");
            }
        }catch (Exception e){
            return Optional.of("unknown");
        }
    }


}
