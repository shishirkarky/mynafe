/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.commons;

import com.myna.app.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public class PersonalInformation extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    private String firstName;

    private String middleName;

    private String lastName;

    private String address;

    private String phone;

    private String email;

    @Enumerated(EnumType.STRING)
    private Status status =  Status.ACTIVE;
}
