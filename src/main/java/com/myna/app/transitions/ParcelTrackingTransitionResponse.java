package com.myna.app.transitions;

import com.myna.app.vendorrequest.dto.VendorRequestDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParcelTrackingTransitionResponse {
  private VendorRequestDto booking;
  private Transition transition;
}
