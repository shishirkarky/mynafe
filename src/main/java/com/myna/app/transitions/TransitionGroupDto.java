/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.transitions;

import com.myna.app.enums.Status;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransitionGroupDto {
    private String transactionId;
    private String createdBy;
    private String fromBranchCode;
    private Status status;
}
