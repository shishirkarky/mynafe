package com.myna.app.transitions;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DeleteTransitionsDto {
    List<Integer> transitionIds;
}
