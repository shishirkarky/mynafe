/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.transitions;

import com.myna.app.enums.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransitionRepository extends JpaRepository<Transition, Integer> {
  List<Transition> findAllByBranchCode(String branchCode);

  List<Transition> findAllByFromBranchCode(String fromBranchCode);

  List<Transition> findAllByFromBranchCodeAndBranchCode(String fromBranchCode, String branchCode);

  Optional<Transition> findFirstByTransactionIdLikeOrderByIdDesc(String currentDate);

  List<Transition> findAllByPodDn(String dn);

  List<Transition> findAllByStatusNot(Status status);

  List<Transition> findAllByBranchCodeAndStatusNot(String branchCode, Status status);

  List<Transition> findAllByFromBranchCodeAndStatusNot(String fromBranchCode, Status status);

  List<Transition> findAllByFromBranchCodeAndBranchCodeAndStatusNot(String fromBranchCode, String branchCode, Status status);

  Optional<Transition> findFirstByStatusNotAndTransactionIdLikeOrderByIdDesc(Status status, String currentDate);

  List<Transition> findAllByPodDnAndStatusNot(String dn, Status status);
}
