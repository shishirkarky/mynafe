/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.transitions;

import com.myna.app.pod.CashReceiptDtoAssembler;
import com.myna.app.pod.PodService;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.utils.dates.DateUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TransitionDtoAssembler {
    private PodService podService;
    private CashReceiptDtoAssembler cashReceiptDtoAssembler;

    @Autowired
    public void setPodService(PodService podService) {
        this.podService = podService;
    }

    @Autowired
    public void setCashReceiptDtoAssembler(CashReceiptDtoAssembler cashReceiptDtoAssembler) {
        this.cashReceiptDtoAssembler = cashReceiptDtoAssembler;
    }

    public Transition toDomain(TransitionDto transitionDto) {
        Transition transition = new Transition();
        CustomBeanUtils.copyNonNullProperties(transitionDto, transition);

        if (null != transitionDto.getPod())
            transition.setPod(cashReceiptDtoAssembler.toDomain(transitionDto.getPod()));

        return transition;
    }

    public TransitionDto toModel(Transition transition) {
        TransitionDto transitionDto = new TransitionDto();

        CustomBeanUtils.copyNonNullProperties(transition, transitionDto);

        transitionDto.setCreatedBy(transition.getCreatedBy());
        transitionDto.setCreatedDate(DateUtility.dateToString(transition.getCreatedDate()));

        if (null != transition.getPod())
            transitionDto.setPod(cashReceiptDtoAssembler.toModel(transition.getPod()));

        return transitionDto;
    }

    public List<Transition> toDomains(List<TransitionDto> transitionDtos) {
        List<Transition> transitionList = new ArrayList<>();
        transitionDtos.forEach(transitionDto ->
                transitionList.add(this.toDomain(transitionDto))
        );
        return transitionList;
    }

    public List<TransitionDto> toModels(List<Transition> transitions) {
        List<TransitionDto> transitionDtos = new ArrayList<>();
        transitions.forEach(transition -> transitionDtos.add(this.toModel(transition)));
        return transitionDtos;
    }
}

