/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.transitions;

import com.myna.app.enums.Status;
import com.myna.app.pod.CashReceipt;
import com.myna.app.commons.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "transitions")
public class Transition extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String fromBranchCode;
    private String branchCode;
    private String transactionId;
    @OneToOne(optional = true)
    private CashReceipt pod;
    @Enumerated(EnumType.STRING)
    private Status status;
}
