/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.transitions;

import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.service.UserService;
import com.myna.app.enums.Status;
import com.myna.app.pod.CashReceipt;
import com.myna.app.pod.PodService;
import com.myna.app.utils.CodeGenerationUtils;
import com.myna.app.utils.dates.DateUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TransitionServiceImpl implements TransitionService {
  private UserService userService;
  private TransitionRepository transitionRepository;
  private PodService podService;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setTransitionRepository(TransitionRepository transitionRepository) {
    this.transitionRepository = transitionRepository;
  }

  @Autowired
  public void setPodService(PodService podService) {
    this.podService = podService;
  }

  @Override
  public Optional<Transition> createTransition(String dn, Transition transition) {
    transition.setStatus(Status.ACTIVE);

    Optional<String> branchCodeOptional = userService.getTokenUserBranchCode();
    if (branchCodeOptional.isPresent()) {
      Optional<CashReceipt> cashReceiptOptional = podService.getEntityByDnAndStatusNot(dn, Status.DELIVERED);
      if (cashReceiptOptional.isPresent()) {
        transition.setTransactionId(this.generateTransitionId());

        transition.setBranchCode(branchCodeOptional.get());

        CashReceipt pod = new CashReceipt();
        pod.setId(cashReceiptOptional.get().getId());
        transition.setPod(pod);

        transition.setFromBranchCode(cashReceiptOptional.get().getBranchCode());

        return Optional.of(transitionRepository.save(transition));
      }
    }
    return Optional.empty();
  }

  private String generateTransitionId() {
    String currentDate = DateUtility.getCurrentDate();

    String existingTransitionId = "";
    Optional<Transition> transitionOptional = transitionRepository.findFirstByStatusNotAndTransactionIdLikeOrderByIdDesc(Status.DELETE, currentDate + "%");
    if (transitionOptional.isPresent()) {
      Transition transition = transitionOptional.get();
      existingTransitionId = transition.getTransactionId();
    }
    return CodeGenerationUtils.generateCode(existingTransitionId, currentDate);
  }

  /**
   * If fromBranchCode is null, then transition fetch will be based on branch code of current user
   *
   * @param fromBranchCode
   * @return
   */
  @Override
  public List<Transition> getTransitionsFromBranchCode(String fromBranchCode) {
    List<String> userRoles = userService.getCurrentUserRoles();
    if (userRoles.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      if (null == fromBranchCode || StringUtils.isEmpty(fromBranchCode)) {
        return transitionRepository.findAllByStatusNot(Status.DELETE);
      } else {
        return transitionRepository.findAllByFromBranchCodeAndStatusNot(fromBranchCode, Status.DELETE);
      }
    } else {
      Optional<String> branchCode = userService.getTokenUserBranchCode();
      if (branchCode.isPresent()) {
        if (null == fromBranchCode || StringUtils.isEmpty(fromBranchCode)) {
          return transitionRepository.findAllByBranchCodeAndStatusNot(branchCode.get(), Status.DELETE);
        } else {
          return transitionRepository.findAllByFromBranchCodeAndBranchCodeAndStatusNot(fromBranchCode, branchCode.get(), Status.DELETE);
        }
      }
    }
    return new ArrayList<>();
  }

  @Override
  public List<Transition> getTransitionsFromDn(String dn) {
    return transitionRepository.findAllByPodDnAndStatusNot(dn, Status.DELETE);
  }

  @Override
  public List<Transition> getUserBranchTransitions() {
    List<String> userRoles = userService.getCurrentUserRoles();
    if (userRoles.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      return transitionRepository.findAllByStatusNot(Status.DELETE);
    } else {
      Optional<String> branchCode = userService.getTokenUserBranchCode();
      if (branchCode.isPresent()) {
        return transitionRepository.findAllByBranchCodeAndStatusNot(branchCode.get(), Status.DELETE);
      }
    }
    return new ArrayList<>();
  }

  @Override
  public void deleteTransition(int id) {
    transitionRepository.deleteById(id);
  }

  @Override
  public void deleteTransitions(List<Transition> transitions) {
      transitions.forEach(transition -> {
        transition.setPod(null);
        transition.setStatus(Status.DELETE);
      });
      transitionRepository.saveAll(transitions);
  }
}
