/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.transitions;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.pod.CashReceipt;
import com.myna.app.pod.PodService;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.service.VendorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class TransitionController {
  private TransitionDtoAssembler transitionDtoAssembler;
  private TransitionService transitionService;

  @Autowired
  public void setTransitionDtoAssembler(TransitionDtoAssembler transitionDtoAssembler) {
    this.transitionDtoAssembler = transitionDtoAssembler;
  }

  @Autowired
  public void setTransitionService(TransitionService transitionService) {
    this.transitionService = transitionService;
  }

  @PostMapping("transitions")
  public ResponseEntity<?> createTransitions(@RequestBody TransitionDto transitionDto) {
    if (null != transitionDto && null != transitionDto.getPod() && !StringUtils.isEmpty(transitionDto.getPod().getDn())) {
      Optional<Transition> transitionOptional = transitionService.createTransition(transitionDto.getPod().getDn(), transitionDtoAssembler.toDomain(transitionDto));
      if (transitionOptional.isPresent()) {
        return new ApiResponse().getSuccessResponse("Transition added",
          transitionDtoAssembler.toModel(transitionOptional.get()));
      } else {
        return new ApiResponse().getFailureResponse("Unable to add");
      }
    } else {
      return new ApiResponse().getFailureResponse("DN not found");
    }
  }

  @GetMapping("transitions")
  public ResponseEntity<?> getUserBranchTransitions() {
    return new ApiResponse().getSuccessResponse(null,
      transitionDtoAssembler.toModels(transitionService.getTransitionsFromBranchCode(null)));
  }

  @GetMapping("transitions/branches/{fromBranchCode}")
  public ResponseEntity<?> getUserBranchTransitions(@PathVariable String fromBranchCode) {
    return new ApiResponse().getSuccessResponse(null,
      transitionDtoAssembler.toModels(transitionService.getTransitionsFromBranchCode(fromBranchCode)));
  }

  @DeleteMapping("transitions/{id}")
  public ResponseEntity<?> deleteTransition(@PathVariable int id) {
    transitionService.deleteTransition(id);
    return new ApiResponse().getSuccessResponse("Transition deleted", true);
  }

  @DeleteMapping("transitions")
  public ResponseEntity<?> deleteTransition(@RequestBody DeleteTransitionsDto deleteTransitionsDto) {
    if (!deleteTransitionsDto.transitionIds.isEmpty()) {
      deleteTransitionsDto.transitionIds.forEach(id -> transitionService.deleteTransition(id));
    }
    return new ApiResponse().getSuccessResponse("Transition deleted", true);
  }


}
