/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.transitions;

import java.util.List;
import java.util.Optional;

public interface TransitionService {
  Optional<Transition> createTransition(String dn, Transition transition);

  List<Transition> getTransitionsFromBranchCode(String fromBranchCode);

  List<Transition> getTransitionsFromDn(String dn);

  List<Transition> getUserBranchTransitions();

  void deleteTransition(int id);

  void deleteTransitions(List<Transition> transitions);
}
