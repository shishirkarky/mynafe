/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.entity;

import com.myna.app.commons.Auditable;
import com.myna.app.auth.enums.MatcherStatus;
import lombok.*;

import javax.persistence.*;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "security_mapping")
public class SecurityMapping extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String apiPath;
    /**
     * {@link org.springframework.http.HttpMethod}
     */
    private String method;
    /**
     * {@link MatcherStatus}
     */
    private String authorizationStatus;
    /**
     * {@link Roles}
     */
    private String roleValue;
}
