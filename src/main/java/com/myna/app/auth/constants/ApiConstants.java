/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.constants;

import lombok.Getter;

@Getter
public class ApiConstants {
    private ApiConstants() {
    }

    public static final String VERSION_1 = "/api/v1";
    public static final String OAUTH = "/oauth/token";
    public static final String LOGOUT = "/logout";


    public static final String USERS = "/users/**";
    public static final String USERS_CREATE = "/users";
    public static final String USERS_UPDATE = "/users/{username}";
    public static final String USERS_DELETE = "/users/{username}";
    public static final String USERS_GET_ALL = "/users";
    public static final String USERS_GET_ONE = "/users/{username}";
    public static final String CURRENT_USERS_PROFILE_GET = "/profiles";
    public static final String USERS_ACTIVATE = "/users/{username}/activate";
    public static final String USERS_DEACTIVATE = "/users/{username}/deactivate";
    public static final String USERS_ROLES_GET_ALL = "/users/roles";


}
