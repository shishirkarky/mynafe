/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.enums;

import lombok.Getter;

@Getter
public enum EUserStatus {
    ACTIVE,
    PENDING,
    DISABLED,
    VALIDATED,
    RESTRICTED,
    VALIDATED_AS_RESTRICTED,
    SUSPENDED,
    DELETED;
}
