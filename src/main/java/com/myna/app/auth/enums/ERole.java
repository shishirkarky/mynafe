/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.enums;

import lombok.Getter;

@Getter
public enum ERole {
    ROLE_SUPER_ADMIN("SUPER_ADMIN"),
    ROLE_BRANCH_ADMIN("BRANCH_ADMIN"),
    ROLE_DRIVER("DRIVER");
    private final String value;

    ERole(String value) {
        this.value = value;
    }
}
