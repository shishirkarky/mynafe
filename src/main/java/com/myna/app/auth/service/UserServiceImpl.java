/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.auth.service;

import com.google.common.net.HttpHeaders;
import com.myna.app.auth.entity.Roles;
import com.myna.app.auth.entity.User;
import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.enums.EUserStatus;
import com.myna.app.auth.repository.RolesRepository;
import com.myna.app.auth.repository.UserRepository;
import com.myna.app.auth.util.JwtUtil;
import com.myna.app.staffs.Staffs;
import com.myna.app.staffs.StaffsService;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.utils.NameUtility;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private RolesRepository rolesRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private JwtUtil jwtUtil;
    private StaffsService staffsService;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRolesRepository(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    @Autowired
    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Autowired
    public void setStaffsService(StaffsService staffsService) {
        this.staffsService = staffsService;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findByStatusNot(EUserStatus.DELETED);
    }

    @Override
    public Optional<User> getUser(String username) {
        return userRepository.findByUsernameAndStatusNot(username, EUserStatus.DELETED);
    }

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Optional<User> saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return Optional.of(userRepository.save(user));
    }


    @Override
    public Optional<User> updateUser(String username, User user) {
        Optional<User> existingUserOptional = getUser(username);
        if (existingUserOptional.isPresent()) {
            User existingUser = existingUserOptional.get();
            EUserStatus existingStatus = existingUser.getStatus();
            CustomBeanUtils.copyNonNullProperties(user, existingUser);

            if (null == user.getStatus()) {
                existingUser.setStatus(existingStatus);
            }

            if (null != user.getPassword()) {
                user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            }

            return Optional.of(userRepository.save(existingUser));
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> updateUserStatus(String username, EUserStatus status) {
        User user = new User();
        user.setStatus(status);
        return updateUser(username, user);
    }

    @Override
    public Optional<User> activateUser(String username) {
        return updateUserStatus(username, EUserStatus.ACTIVE);
    }

    @Override
    public Optional<User> deActivateUser(String username) {
        return updateUserStatus(username, EUserStatus.DISABLED);
    }

    @Override
    public Optional<User> deleteUser(String username) {
        Optional<User> existingUserOptional = getUser(username);
        if (existingUserOptional.isPresent()) {
            User existingUser = existingUserOptional.get();
            existingUser.setStatus(EUserStatus.DELETED);
            return Optional.of(userRepository.save(existingUser));
        }
        return Optional.empty();
    }

    @Override
    public List<Roles> getRoles() {
        return rolesRepository.findAll();
    }

    @Override
    public List<String> getCurrentUserRoles() {
        Optional<User> userOptional = this.getCurrentUserFromToken();
        if (userOptional.isPresent()) {
            Set<Roles> roles = userOptional.get().getRoles();
            if (!roles.isEmpty()) {
                List<String> rolesList = new ArrayList<>();
                roles.forEach(r -> {
                    rolesList.add(String.valueOf(r.getName()));
                });

                return rolesList;
            }
        }
        return new ArrayList<>();
    }

    @Override
    public Optional<User> getCurrentUserFromToken() {
        try {
            HttpServletRequest request =
                    ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                            .getRequest();
            String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
            if (!StringUtils.isEmpty(authorizationHeader) && authorizationHeader.startsWith("Bearer ")) {
                String token = authorizationHeader.substring(7);
                if (!StringUtils.isEmpty(token)) {
                    String userName = jwtUtil.extractUsername(token);
                    if (null != userName) {
                        return this.getUser(userName);
                    }
                }
            }
        } catch (Exception e) {
            log.warn("getUserNameFromToken() : ", e);
        }
        return Optional.empty();
    }

    @Override
    public String getTokenUserStaffName() {
        Optional<User> userOptional = this.getCurrentUserFromToken();
        if (userOptional.isPresent()) {
            Staffs staffs = userOptional.get().getStaffs();
            if (null != staffs) {
                return NameUtility.getName(
                        staffs.getFirstName(),
                        staffs.getMiddleName(),
                        staffs.getLastName()
                );
            }
        }
        return "";
    }

    @Override
    public List<User> getCurrentUserRelatedBranchUsers() {
        Optional<User> userOptional = this.getCurrentUserFromToken();
        if (userOptional.isPresent()) {
            User currentUser = userOptional.get();
            if (null != currentUser.getStaffs()) {
                Staffs staffs = currentUser.getStaffs();
                String branchCode = staffs.getBranchCode();
                if (!StringUtils.isEmpty(branchCode)) {
                    List<Staffs> staffsList = staffsService.getByBranchCode(branchCode);
                    return userRepository.findAllByStaffsIn(staffsList);
                }
            }
        }
        return new ArrayList<>();
    }

    @Override
    public Optional<String> getTokenUserBranchCode() {
        Optional<User> userOptional = this.getCurrentUserFromToken();
        if (userOptional.isPresent()) {
            User currentUser = userOptional.get();
            if (null != currentUser.getStaffs()) {
                Staffs staffs = currentUser.getStaffs();
                String branchCode = staffs.getBranchCode();
                return Optional.of(branchCode);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean validateUserPassword(String username, String password) {
        Optional<User> userOptional = this.getUser(username);
        return userOptional.filter(user -> bCryptPasswordEncoder.matches(password, user.getPassword())).isPresent();
    }

    @Override
    public boolean validateCurrentUserPassword(String password) {
        Optional<User> userOptional = this.getCurrentUserFromToken();
        return userOptional.filter(user -> bCryptPasswordEncoder.matches(password, user.getPassword())).isPresent();
    }

    @Override
    public boolean updateCurrentUserPassword(String password) {
        Optional<User> userOptional = this.getCurrentUserFromToken();
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setPassword(bCryptPasswordEncoder.encode(password));

            userRepository.save(user);
            return true;
        }
        return false;
    }

}
