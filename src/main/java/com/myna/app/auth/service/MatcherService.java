/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.service;

import com.myna.app.auth.entity.SecurityMapping;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import java.util.List;

public interface MatcherService {
    List<SecurityMapping> getAll();
    void setMatcherToHttpSecurity(HttpSecurity http);
    List<SecurityMapping> saveAll(List<SecurityMapping> securityMappings);
}
