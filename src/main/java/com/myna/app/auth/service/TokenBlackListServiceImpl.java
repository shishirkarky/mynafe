/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.service;

import com.myna.app.auth.entity.TokenBlackList;
import com.myna.app.auth.repository.TokenBlackListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenBlackListServiceImpl implements TokenBlackListService {
    private TokenBlackListRepository tokenBlackListRepository;

    @Autowired
    public void setTokenBlackListRepository(TokenBlackListRepository tokenBlackListRepository) {
        this.tokenBlackListRepository = tokenBlackListRepository;
    }

    @Override
    public Optional<TokenBlackList> saveTokenAsBlackListed(String token) {
        TokenBlackList tokenBlackList = new TokenBlackList(token);
        return Optional.of(tokenBlackListRepository.save(tokenBlackList));
    }

    @Override
    public Optional<TokenBlackList> getBlackListToken(String token) {
        return tokenBlackListRepository.findByToken(token);
    }

    @Override
    public boolean isTokenBlackListed(String token) {
        return getBlackListToken(token).isPresent();
    }

}
