/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.service;

import com.myna.app.auth.entity.TokenBlackList;

import java.util.Optional;

public interface TokenBlackListService {
    Optional<TokenBlackList> saveTokenAsBlackListed(String token);

    Optional<TokenBlackList> getBlackListToken(String token);

    boolean isTokenBlackListed(String token);

}
