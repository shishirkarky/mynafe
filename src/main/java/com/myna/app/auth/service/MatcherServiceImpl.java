/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.service;

import com.myna.app.auth.entity.SecurityMapping;
import com.myna.app.auth.repository.MatcherRepository;
import com.myna.app.auth.enums.MatcherStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class MatcherServiceImpl implements MatcherService {
    private MatcherRepository matcherRepository;

    @Autowired
    public void setMatcherRepository(MatcherRepository matcherRepository) {
        this.matcherRepository = matcherRepository;
    }

    @Override
    public List<SecurityMapping> getAll() {
        return matcherRepository.findAll();
    }

    @Override
    public void setMatcherToHttpSecurity(HttpSecurity http) {
        List<SecurityMapping> securityMappingList = getAll();

        securityMappingList.stream().filter(securityMapping -> securityMapping.getAuthorizationStatus().equals(MatcherStatus.PERMIT_ALL.getValue())).forEach(securityMapping -> {
            try {
                http.authorizeRequests().antMatchers(HttpMethod.valueOf(securityMapping.getMethod()), securityMapping.getApiPath()).permitAll().and();
            } catch (Exception e) {
                log.warn("mapping permitAll antMatchers exception occurred with error cause {}", e.getMessage());
            }
        });

        securityMappingList.stream().filter(securityMapping -> securityMapping.getAuthorizationStatus().equals(MatcherStatus.AUTHORIZED.getValue())).forEach(securityMapping -> {
            try {
                http.authorizeRequests().antMatchers(HttpMethod.valueOf(securityMapping.getMethod()), securityMapping.getApiPath()).hasRole(securityMapping.getRoleValue()).and();
            } catch (Exception e) {
                log.warn("antMatchers exception occurred with error cause {}", e.getMessage());
            }
        });
    }

    @Override
    public List<SecurityMapping> saveAll(List<SecurityMapping> securityMappings) {
        return matcherRepository.saveAll(securityMappings);
    }
}
