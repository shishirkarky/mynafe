/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.service;

import com.myna.app.auth.entity.Roles;
import com.myna.app.auth.entity.User;
import com.myna.app.auth.enums.EUserStatus;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getUsers();

    Optional<User> getUser(String username);

    Optional<User> saveUser(User user);

    Optional<User> updateUser(String username, User user);

    Optional<User> updateUserStatus(String username, EUserStatus status);

    Optional<User> activateUser(String username);

    Optional<User> deActivateUser(String username);

    Optional<User> deleteUser(String username);

    List<Roles> getRoles();

    List<String> getCurrentUserRoles();

    Optional<User> getCurrentUserFromToken();

    String getTokenUserStaffName();

    List<User> getCurrentUserRelatedBranchUsers();

    Optional<String> getTokenUserBranchCode();

    boolean validateUserPassword(String username, String password);

    boolean validateCurrentUserPassword(String password);

    boolean updateCurrentUserPassword(String password);
}
