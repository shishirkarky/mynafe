/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.auth.controller;


import com.myna.app.auth.constants.ApiConstants;
import com.myna.app.auth.dto.AuthenticationRequest;
import com.myna.app.auth.dto.JwtTokenDetails;
import com.myna.app.auth.entity.TokenBlackList;
import com.myna.app.auth.util.JwtUtil;
import com.myna.app.constants.JspPageAPIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@RestController
@RequestMapping(value = ApiConstants.VERSION_1)
public class AuthController {
    private JwtUtil jwtUtil;
    private AuthenticationManager authenticationManager;

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @PostMapping(ApiConstants.OAUTH)
    public ResponseEntity<?> generateToken(HttpServletRequest request, @ModelAttribute AuthenticationRequest authenticationRequest) {
        JwtTokenDetails jwtTokenDetails = new JwtTokenDetails();
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword()
                    )
            );
            setSecurityContextToSession(request, authentication);

            jwtTokenDetails = jwtUtil.generateToken(authenticationRequest.getUserName());
            jwtTokenDetails.setStatus(1);
            jwtTokenDetails.setMessage("Successfully logged in");
        } catch (Exception ex) {
            jwtTokenDetails.setStatus(0);
            jwtTokenDetails.setMessage("Invalid username/password");
            return new ResponseEntity<>(jwtTokenDetails, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(jwtTokenDetails, HttpStatus.OK);
    }

    private void setSecurityContextToSession(HttpServletRequest request, Authentication authentication) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        HttpSession session = request.getSession(true);
        session.setAttribute(JspPageAPIConstants.SPRING_SECURITY_CONTEXT, securityContext);
    }

    @GetMapping(ApiConstants.LOGOUT)
    public ResponseEntity<?> logout() {
        Optional<String> tokenOptional = jwtUtil.getToken();
        if (tokenOptional.isPresent()) {
            Optional<TokenBlackList> tokenBlackListOptional = jwtUtil.blackListToken(tokenOptional.get());
            if (tokenBlackListOptional.isPresent()) {

                HttpServletRequest request =
                        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                                .getRequest();
                request.getSession().invalidate();

                return new ResponseEntity<>("Logout successful.", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("Logout failed.", HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
