/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.apiresponse.EmptyJSONResponse;
import com.myna.app.auth.apiresponse.EApiResponseMessages;
import com.myna.app.auth.constants.ApiConstants;
import com.myna.app.auth.dto.UserDto;
import com.myna.app.auth.entity.User;
import com.myna.app.auth.mapper.UserMapper;
import com.myna.app.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(ApiConstants.VERSION_1)
public class UserController {
    private UserMapper userMapper;
    private UserService userService;

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(ApiConstants.USERS_GET_ALL)
    public ResponseEntity<?> getUsers() {
        List<UserDto> userDtos = userMapper.modelToDto(userService.getUsers());

        Optional<User> userOptional = userService.getCurrentUserFromToken();
        userOptional.ifPresent(user ->
                userDtos.removeAll(
                        userDtos.stream().filter(userDto -> userDto.getUsername().equals(user.getUsername())).collect(Collectors.toList()))
        );

        return new ApiResponse().getSuccessResponse(
                EApiResponseMessages.USERS_FOUND.getValue(),
                userDtos
        );
    }

    @GetMapping(ApiConstants.USERS_GET_ONE)
    public ResponseEntity<?> getUser(@PathVariable String username) {
        Optional<User> userOptional = userService.getUser(username);
        if (userOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(
                    EApiResponseMessages.USER_FOUND.getValue(),
                    userMapper.modelToDto(userOptional.get())
            );
        }
        return new ApiResponse().getFailureResponse(
                EApiResponseMessages.USER_NOT_FOUND.getValue()
        );
    }

    @GetMapping(ApiConstants.CURRENT_USERS_PROFILE_GET)
    public ResponseEntity<?> getCurrentUserProfile() {
        Optional<User> userOptional = userService.getCurrentUserFromToken();
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setPassword(null);
            return new ApiResponse().getSuccessResponse(null, userMapper.modelToDto(user));
        } else {
            return new ApiResponse().getFailureResponse(
                    EApiResponseMessages.USER_NOT_FOUND.getValue()
            );
        }
    }

    @PostMapping(ApiConstants.USERS_CREATE)
    public ResponseEntity<?> saveUser(@RequestBody UserDto userDto) {
        Optional<User> existingUserOptional = userService.getUser(userDto.getUsername());
        if (existingUserOptional.isPresent()) {
            return new ApiResponse().getFailureResponse(
                    EApiResponseMessages.USER_CREATING_ALREADY_EXISTS.getValue()
            );
        }

        Optional<User> userOptional = userMapper.dtoToModel(userDto);
        if (userOptional.isPresent()) {
            Optional<User> savedUserOptional = userService.saveUser(userOptional.get());
            if (savedUserOptional.isPresent()) {
                return new ApiResponse().getSuccessResponse(
                        EApiResponseMessages.USER_CREATED.getValue(),
                        userDto
                );
            }
        }
        return new ApiResponse().getFailureResponse(
                EApiResponseMessages.USER_CREATING_FAILED.getValue()
        );
    }

    @PutMapping(ApiConstants.USERS_UPDATE)
    public ResponseEntity<?> updateUser(@PathVariable String username, @RequestBody UserDto userDto) {
        Optional<User> userOptional = userMapper.dtoToModel(userDto);
        if (userOptional.isPresent()) {
            Optional<User> updatedUserOptional = userService.updateUser(username, userOptional.get());
            if (updatedUserOptional.isPresent()) {
                return new ApiResponse().getSuccessResponse(
                        EApiResponseMessages.USER_UPDATED.getValue(),
                        userMapper.modelToDto(updatedUserOptional.get())
                );
            }
        }
        return new ApiResponse().getFailureResponse(
                EApiResponseMessages.USER_UPDATING_FAILED.getValue()
        );
    }

    @DeleteMapping(ApiConstants.USERS_DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable String username) {
        Optional<User> userOptional = userService.deleteUser(username);
        if (userOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(
                    EApiResponseMessages.USER_DELETED.getValue(),
                    new EmptyJSONResponse()
            );
        }
        return new ApiResponse().getFailureResponse(
                EApiResponseMessages.USER_DELETING_FAILED.getValue()
        );
    }

    @PutMapping(ApiConstants.USERS_ACTIVATE)
    public ResponseEntity<?> activateUserStatus(@PathVariable String username) {
        Optional<User> userOptional = userService.activateUser(username);
        if (userOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(
                    EApiResponseMessages.USER_ACTIVATED.getValue(),
                    new EmptyJSONResponse()
            );
        }
        return new ApiResponse().getFailureResponse(
                EApiResponseMessages.USER_STATUS_UPDATING_FAILED.getValue()
        );
    }

    @PutMapping(ApiConstants.USERS_DEACTIVATE)
    public ResponseEntity<?> deActivateUserStatus(@PathVariable String username) {
        Optional<User> userOptional = userService.deActivateUser(username);
        if (userOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(
                    EApiResponseMessages.USER_DEACTIVATED.getValue(),
                    new EmptyJSONResponse()
            );
        }
        return new ApiResponse().getFailureResponse(
                EApiResponseMessages.USER_STATUS_UPDATING_FAILED.getValue()
        );
    }

    @GetMapping(ApiConstants.USERS_ROLES_GET_ALL)
    public ResponseEntity<?> getRoles() {
        return new ApiResponse().getSuccessResponse(
                EApiResponseMessages.ROLES_FOUND.getValue(),
                userService.getRoles()
        );
    }
}
