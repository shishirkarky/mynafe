/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.apiresponse;

import lombok.Getter;

@Getter
public enum EApiResponseMessages {
    USER_CREATED("User created."),
    USER_CREATING_FAILED("User creating failed."),
    USER_CREATING_ALREADY_EXISTS("User already exists."),
    USER_UPDATED("User updated"),
    USER_UPDATING_FAILED("User updating failed."),
    USER_DELETED("User deleted."),
    USER_DELETING_FAILED("User deleting failed."),
    USER_FOUND("User found."),
    USER_NOT_FOUND("User not found."),
    USERS_FOUND("Users found."),
    USER_ACTIVATED("User activated."),
    USER_DEACTIVATED("User deactivated."),
    USER_STATUS_UPDATED("User status updated."),
    USER_STATUS_UPDATING_FAILED("User status updating failed."),
    ROLES_FOUND("Roles found");
    private final String value;

    EApiResponseMessages(String value) {
        this.value = value;
    }
}
