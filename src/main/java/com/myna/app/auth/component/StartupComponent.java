/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.auth.component;

import com.google.common.collect.Lists;
import com.myna.app.auth.constants.ApiConstants;
import com.myna.app.auth.entity.Roles;
import com.myna.app.auth.entity.SecurityMapping;
import com.myna.app.auth.entity.User;
import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.enums.EUserStatus;
import com.myna.app.auth.enums.MatcherStatus;
import com.myna.app.auth.repository.RolesRepository;
import com.myna.app.auth.repository.UserRepository;
import com.myna.app.auth.service.MatcherService;
import com.myna.app.constants.JspPageAPIConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class StartupComponent {
  @Value("${initial.user.name}")
  private String username;
  @Value("${initial.user.password}")
  private String password;

  private UserRepository usersRepo;
  private RolesRepository rolesRepo;
  private MatcherService matcherService;


  @Autowired
  public void setUsersRepo(UserRepository usersRepo) {
    this.usersRepo = usersRepo;
  }


  @Autowired
  public void setRolesRepo(RolesRepository rolesRepo) {
    this.rolesRepo = rolesRepo;
  }

  @Autowired
  public void setMatcherService(MatcherService matcherService) {
    this.matcherService = matcherService;
  }


  private void saveRoles() {
    if (rolesRepo.findAll().isEmpty()) {
      List<Roles> rolesToSave = Lists.newArrayList();
      ERole[] roleEnums = ERole.values();
      for (ERole roleEnum : roleEnums) {
        Roles role = new Roles();
        role.setName(roleEnum);
        rolesToSave.add(role);
      }
      rolesRepo.saveAll(rolesToSave);
    }
  }

  @EventListener(ApplicationReadyEvent.class)
  public void doSomethingAfterStartup() {
    saveInitialMatcher();
    log.info("Initial security_mapping setup completed.");
    saveRoles();
    log.info("Initial roles setup completed.");
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    Optional<User> usersOptional = usersRepo.findByUsernameAndStatusNot(username, EUserStatus.DELETED);
    if (!usersOptional.isPresent()) {
      User user = new User();
      Optional<Roles> roleOptional = rolesRepo.findByName(ERole.ROLE_SUPER_ADMIN);
      roleOptional.ifPresent(role -> user.setRoles(Collections.singleton(role)));
      user.setUsername(username);
      user.setPassword(encoder.encode(password));
      user.setStatus(EUserStatus.ACTIVE);
      usersRepo.save(user);
    }
    log.info("Initial user setup completed.");

  }

  public void saveInitialMatcher() {
    if (matcherService.getAll().isEmpty()) {
      List<SecurityMapping> securityMappings = new ManagedList<>();
      //OTHERS AUTHENTICATION
      securityMappings.add(new SecurityMapping(1, "/swagger-resources/**", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(2, "/swagger-ui.html", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(3, "/v2/api-docs", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(4, "/webjars/**", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(5, "/octopus/**", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      //API AUTHENTICATION
      securityMappings.add(new SecurityMapping(6, ApiConstants.VERSION_1.concat(ApiConstants.OAUTH), HttpMethod.POST.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(7, JspPageAPIConstants.V1_LOGIN, HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(8, ApiConstants.VERSION_1.concat(ApiConstants.USERS), HttpMethod.GET.name(), MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_SUPER_ADMIN.getValue()));
      securityMappings.add(new SecurityMapping(9, ApiConstants.VERSION_1.concat(ApiConstants.USERS), HttpMethod.POST.name(), MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_SUPER_ADMIN.getValue()));
      securityMappings.add(new SecurityMapping(10, JspPageAPIConstants.V1_PARCEL_TRACKING, HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      //JSP PAGES API AUTHENTICATION
//            securityMappings.add(new SecurityMapping(9, JspPageAPIConstants.V1_USER_CREATE, HttpMethod.GET.name(), MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_SUPER_ADMIN.getValue()));

      final String[] WHITE_LIST_EXTENSIONS = {".css", ".js", ".png", ".woff", ".gif"};
      int index = 10;
      for (String white_list_extension : WHITE_LIST_EXTENSIONS) {
        securityMappings.add(new SecurityMapping(index, "**" + white_list_extension, HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
        index++;
      }

      matcherService.saveAll(securityMappings);
    }
  }
}
