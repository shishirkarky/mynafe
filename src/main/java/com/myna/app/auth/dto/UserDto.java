/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.auth.dto;

import com.myna.app.auth.entity.Roles;
import com.myna.app.auth.enums.EUserStatus;
import com.myna.app.driver.entity.Driver;
import com.myna.app.staffs.Staffs;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class UserDto {
    private String username;
    private String password;
    private EUserStatus status = EUserStatus.ACTIVE;
    private Set<Roles> roles;
    private Staffs staffs;
    private Driver driver;
    private String navigationJson;
}
