/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.mapper;

import com.myna.app.auth.entity.User;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.auth.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserMapper {
    public Optional<User> dtoToModel(UserDto userDto) {
        User user = new User();
        CustomBeanUtils.copyNonNullProperties(userDto, user);
        return Optional.of(user);
    }

    public List<User> dtoToModel(List<UserDto> userDtos) {
        List<User> users = new ArrayList<>();
        userDtos.forEach(userDto -> {
            Optional<User> userOptional = dtoToModel(userDto);
            userOptional.ifPresent(users::add);
        });
        return users;
    }

    public Optional<UserDto> modelToDto(User user) {
        UserDto userDto = new UserDto();
        CustomBeanUtils.copyNonNullProperties(user, userDto);
        return Optional.of(userDto);
    }

    public List<UserDto> modelToDto(List<User> users) {
        List<UserDto> userDtos = new ArrayList<>();
        users.forEach(user -> {
            user.setPassword(null);
            Optional<UserDto> userDtoOptional = modelToDto(user);
            userDtoOptional.ifPresent(userDtos::add);
        });
        return userDtos;
    }


}
