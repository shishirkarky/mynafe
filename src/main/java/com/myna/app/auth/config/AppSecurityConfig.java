/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.auth.config;

import com.myna.app.auth.filter.JwtFilter;
import com.myna.app.auth.service.CustomUserDetailsService;
import com.myna.app.auth.service.MatcherService;
import com.myna.app.constants.JspPageAPIConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.LinkedList;

@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {
  private static final RequestMatcher SECURITY_EXCLUSION_MATCHER;

  static {
    String[] urls = new String[]{
      "**/public/**",
      "**/ajax/**",
      "**/dataTable/**",
      "**/images/**",
      "**/javascripts/**",
      "**/jquery-ui/**",
      "**/jsForPages/**",
      "**/modals/**",
      "**/stylesheets/**",
      "**/toast/**",
      "**/vendor/**",
    };

    //Build Matcher List
    LinkedList<RequestMatcher> matcherList = new LinkedList<>();
    for (String url : urls) {
      matcherList.add(new AntPathRequestMatcher(url));
    }

    //Link Matchers in "OR" config.
    SECURITY_EXCLUSION_MATCHER = new OrRequestMatcher(matcherList);
  }

  private CustomUserDetailsService userDetailsService;
  private JwtFilter jwtFilter;
  private MatcherService matcherService;

  @Autowired
  public void setUserDetailsService(CustomUserDetailsService userDetailsService) {
    this.userDetailsService = userDetailsService;
  }

  @Autowired
  public void setJwtFilter(JwtFilter jwtFilter) {
    this.jwtFilter = jwtFilter;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService);
  }

  @Autowired
  public void setMatcherService(MatcherService matcherService) {
    this.matcherService = matcherService;
  }

  @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public AuthenticationProvider authProvider() {
    DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
    provider.setUserDetailsService(userDetailsService);
    provider.setPasswordEncoder(new BCryptPasswordEncoder());
    return provider;
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring().requestMatchers(SECURITY_EXCLUSION_MATCHER);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors()
      .and().csrf().disable();

    http.exceptionHandling().and().sessionManagement()
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    http.headers().frameOptions().sameOrigin();

    matcherService.setMatcherToHttpSecurity(http);

    //todo: this should be authenticated, fix later
    http.authorizeRequests()
      .requestMatchers(SECURITY_EXCLUSION_MATCHER).permitAll();

    http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    http
      .formLogin()
      .loginPage(JspPageAPIConstants.V1_LOGIN)
      .loginProcessingUrl("/login")
      .defaultSuccessUrl(JspPageAPIConstants.V1_DASHBOARD, true)
      .failureUrl(JspPageAPIConstants.V1_LOGIN.concat("?error=Invalid credentials"))
      .and()
      .logout()
      .logoutUrl("/logout")
      .deleteCookies("JSESSIONID")
      .and();


  }

  //defining a bean to encrypt passwords
  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public CorsFilter corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = new CorsConfiguration();
    //config.setAllowCredentials(true); // you USUALLY want this
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.addAllowedMethod("OPTIONS");
    config.addAllowedMethod("HEAD");
    config.addAllowedMethod("GET");
    config.addAllowedMethod("PUT");
    config.addAllowedMethod("POST");
    config.addAllowedMethod("DELETE");
    config.addAllowedMethod("PATCH");
    source.registerCorsConfiguration("/**", config);
    return new CorsFilter(source);
  }

  @Bean
  GrantedAuthorityDefaults grantedAuthorityDefaults() {
    return new GrantedAuthorityDefaults("");
  }
}
