/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.auth.repository;

import com.myna.app.auth.entity.User;
import com.myna.app.auth.enums.EUserStatus;
import com.myna.app.staffs.Staffs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameAndStatusNot(String username, EUserStatus status);

    List<User> findByStatusNot(EUserStatus status);

    Optional<User> findByUsernameAndStatusIn(String username, List<EUserStatus> validUserStatus);

    List<User> findAllByStaffsIn(List<Staffs> staffsList);
}
