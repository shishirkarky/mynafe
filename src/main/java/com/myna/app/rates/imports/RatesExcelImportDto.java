package com.myna.app.rates.imports;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatesExcelImportDto {
    private String itemCode;
    private int toLocationId;
    private double rate;
}
