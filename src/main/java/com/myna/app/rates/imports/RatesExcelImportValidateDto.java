package com.myna.app.rates.imports;

import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.dataconfig.packages.ConfigPackagesDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RatesExcelImportValidateDto {
    private int itemId;
    private int toLocationId;
    private double rate;

    private String itemName;
    private String locationCity;

    private ConfigPackagesDto item;
    private ConfigLocationsDto toLocation;
}
