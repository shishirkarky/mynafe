package com.myna.app.rates.imports;

import com.myna.app.dataconfig.EntityConfigService;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDtoAssembler;
import com.myna.app.dataconfig.packages.ConfigPackages;
import com.myna.app.dataconfig.packages.ConfigPackagesDtoAssembler;
import com.myna.app.dataconfig.rates.ConfigRates;
import com.myna.app.dataconfig.rates.ConfigRatesDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RatesExcelImportComponent {
    private EntityConfigService entityConfigService;
    private ConfigLocationsDtoAssembler configLocationsDtoAssembler;
    private ConfigPackagesDtoAssembler configPackagesDtoAssembler;

    @Autowired
    public void setEntityConfigService(EntityConfigService entityConfigService) {
        this.entityConfigService = entityConfigService;
    }

    @Autowired
    public void setConfigLocationsDtoAssembler(ConfigLocationsDtoAssembler configLocationsDtoAssembler) {
        this.configLocationsDtoAssembler = configLocationsDtoAssembler;
    }

    @Autowired
    public void setConfigPackagesDtoAssembler(ConfigPackagesDtoAssembler configPackagesDtoAssembler) {
        this.configPackagesDtoAssembler = configPackagesDtoAssembler;
    }

    public RateImportValidationWrapper validate(MultipartFile file, int refVendorId) {
        RateImportValidationWrapper rateImportValidationWrapper = new RateImportValidationWrapper();

        List<RatesExcelImportValidateDto> rates = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        if (null != file) {
            try {
                XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
                XSSFSheet worksheet = workbook.getSheetAt(0);
                int rowsCount = worksheet.getPhysicalNumberOfRows();


                for (int i = 1; i < rowsCount; i++) {
                    XSSFRow row = worksheet.getRow(i);

                    RatesExcelImportValidateDto rate = new RatesExcelImportValidateDto();

                    String itemName = "";
                    try {
                       itemName = row.getCell(0).getStringCellValue();
                    } catch (Exception e) {
                        log.warn("rowCount {} itemName not found", i);
                    }

                    String city = "";
                    try {
                        city = row.getCell(1).getStringCellValue();
                    } catch (Exception e) {
                        log.warn("rowCount {} city not found", i);
                    }

                    double rateValue = 0.0;
                    try {
                        rateValue = row.getCell(2).getNumericCellValue();
                    } catch (Exception e) {
                        log.warn("rowCount {} rate not found", i);
                    }

                    if (!StringUtils.isEmpty(itemName)) {
                        Optional<ConfigPackages> configPackagesOptional = entityConfigService.getOnePackageByVendorAndName(refVendorId, itemName);
                        if (configPackagesOptional.isPresent()) {
                            ConfigPackages configPackages = configPackagesOptional.get();
                            rate.setItemId(configPackages.getId());
                            rate.setItem(configPackagesDtoAssembler.toModel(configPackages));
                        } else {
                            errors.add("rowCount " + i + " : Package not found for vendor " + refVendorId + " name " + itemName);
                            log.warn("rowCount {} : Package not found for vendor {} name {}", i, refVendorId, itemName);
                        }
                    } else {
                        errors.add("rowCount " + i + " : ItemName found empty");
                        log.warn("rowCount {} : ItemName found empty", i);
                    }

                    if (null != city && !StringUtils.isEmpty(city)) {
                        Optional<ConfigLocations> configLocationsOptional = entityConfigService.getOneLocationByCity(city);
                        if (configLocationsOptional.isPresent()) {
                            ConfigLocations configLocations = configLocationsOptional.get();
                            rate.setToLocationId(configLocations.getId());
                            rate.setToLocation(configLocationsDtoAssembler.toModel(configLocations));
                        } else {
                            errors.add("rowCount " + i + " : Location not found for city " + city);
                            log.warn("rowCount {} : Location not found for city {}", i, city);
                        }
                    } else {
                        errors.add("rowCount " + i + " : city found empty");
                        log.warn("rowCount {} : city found empty", i);
                    }

                    rate.setRate(rateValue);

                    rates.add(rate);
                }
            } catch (IOException e) {
                log.error("validate() : IOException occurred", e);
            }
        }

        rateImportValidationWrapper.setRates(rates);
        rateImportValidationWrapper.setErrors(errors);

        return rateImportValidationWrapper;
    }

    public boolean importRates(MultipartFile file, ConfigRatesDto configRatesDto) {
        if (null != file) {
            RateImportValidationWrapper wrapper = this.validate(file, configRatesDto.getRefVendorId());
            if (null != wrapper && (wrapper.getErrors() == null || wrapper.getErrors().isEmpty())) {
                List<RatesExcelImportValidateDto> rates = wrapper.getRates();
                if (null != rates && !rates.isEmpty()) {
                    rates = rates.stream().filter(validatedRate -> {
                        return null != validatedRate.getToLocation() && null != validatedRate.getItem();
                    }).collect(Collectors.toList());

                    List<ConfigRates> ratesToSave = new ArrayList<>();
                    rates.forEach(ratesExcelImportValidateDto -> {
                        ConfigRates configRates = new ConfigRates();

                        configRates.setRefConfPackageId(ratesExcelImportValidateDto.getItemId());
                        configRates.setRefConfToLocationId(ratesExcelImportValidateDto.getToLocationId());
                        configRates.setRate(ratesExcelImportValidateDto.getRate());

                        configRates.setBookingType(configRatesDto.getBookingType());
                        configRates.setRefConfFromLocationId(configRatesDto.getRefConfFromLocationId());
                        configRates.setRefVendorId(configRatesDto.getRefVendorId());

                        ratesToSave.add(configRates);
                    });

                    return entityConfigService.saveRates(ratesToSave).isEmpty();
                }
            }
        }
        return false;
    }
}
