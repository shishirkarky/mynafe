package com.myna.app.rates.imports;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RateImportValidationWrapper {
    private List<String> errors;
    private List<RatesExcelImportValidateDto> rates;
}
