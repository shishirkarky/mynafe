package com.myna.app.rates.imports;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.dataconfig.rates.ConfigRatesDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class RatesExcelImportController {
    private RatesExcelImportComponent ratesExcelImportComponent;

    @Autowired
    public void setRatesExcelImportComponent(RatesExcelImportComponent ratesExcelImportComponent) {
        this.ratesExcelImportComponent = ratesExcelImportComponent;
    }

    @PostMapping("imports/rates/configs")
    public ResponseEntity<?> importRatesConfigExcel(@RequestParam("file") MultipartFile file, String payload) {
        try {
            ConfigRatesDto configRatesDto = new ObjectMapper().readValue(payload, ConfigRatesDto.class);
            return new ApiResponse().getSuccessResponse("Import Successful.", ratesExcelImportComponent.importRates(file, configRatesDto));
        } catch (Exception e) {
            return new ApiResponse().getFailureResponse("Import Failure. Invalid Request.");
        }
    }

    @PostMapping("imports/rates/configs/validate")
    public ResponseEntity<?> validateImportRatesConfigExcel(@RequestParam("file") MultipartFile file, String payload) {
        try {
            ConfigRatesDto configRatesDto = new ObjectMapper().readValue(payload, ConfigRatesDto.class);
            return new ApiResponse().getSuccessResponse("Validation Successful.", ratesExcelImportComponent.validate(file, configRatesDto.getRefVendorId()));
        } catch (Exception e) {
            log.error("Validate rate config error",e);
            return new ApiResponse().getFailureResponse("Import Failure. Invalid Request.");
        }
    }

}
