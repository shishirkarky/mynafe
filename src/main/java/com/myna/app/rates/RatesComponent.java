package com.myna.app.rates;

import com.myna.app.dataconfig.EntityConfigService;
import com.myna.app.dataconfig.rates.ConfigRates;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendor.enums.ERateCalculationType;
import com.myna.app.vendor.service.VendorService;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.enums.BookingType;
import com.myna.app.vendorrequest.service.VendorRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Component
public class RatesComponent {
    private VendorRequestService vendorRequestService;
    private VendorService vendorService;
    private EntityConfigService entityConfigService;

    @Autowired
    public void setVendorRequestService(VendorRequestService vendorRequestService) {
        this.vendorRequestService = vendorRequestService;
    }

    @Autowired
    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    @Autowired
    public void setEntityConfigService(EntityConfigService entityConfigService) {
        this.entityConfigService = entityConfigService;
    }

    /**
     * <p>
     * This method gets rate from rate config. Uses (itemCode, fromLocation, toLocation, vendor, bookingType)
     * This rate is calculated for pod items only for PARTIAL_BOOKING booking type.
     * Based on the rate configuration > calculation type in Vendor, the calculation logic is determined
     * 1. rate * package
     * 2. rate * quantity
     * If booking type is FULL_BOOKING
     * </p>
     *
     * @param bookingId
     * @param fromLocationId
     * @param toLocationId
     * @param itemId
     * @param noOfPackage
     * @param quantity
     * @return Map : total and rate
     */
    public Map<String, Double> getPartialBookingRate(int bookingId, int fromLocationId, int toLocationId, int itemId, double noOfPackage, double quantity) {
        Map<String, Double> map = new HashMap<>();

        Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getOneVendorRequest(bookingId);
        vendorRequestOptional.ifPresent(vendorRequest -> {

            Optional<Vendor> vendorOptional = vendorService.getOneVendor(vendorRequest.getRefVendorId());
            vendorOptional.ifPresent(vendor -> {
                if (BookingType.PARTIAL_BOOKING.equals(vendorRequest.getBookingType())) {
                    Optional<ConfigRates> configRatesOptional = this.getConfigRates(
                            vendorRequest.getBookingType()
                            , vendor.getId()
                            , itemId
                            , fromLocationId
                            , toLocationId);

                    configRatesOptional.ifPresent(configRates -> {
                        map.put("rate", configRates.getRate());
                        if (ERateCalculationType.PACKAGE.equals(vendor.getRateCalculationType())) {
                            try {
                                map.put("total", noOfPackage * configRates.getRate());
                            } catch (NumberFormatException e) {
                                log.warn("setRateFromRateConfiguration() : Unable to parse No of package to double");
                            }
                        } else if (ERateCalculationType.QUANTITY.equals(vendor.getRateCalculationType())) {
                            map.put("total", quantity * configRates.getRate());
                        }
                    });
                }
            });
        });

        return map;
    }

    public double getFullBookingRate(int vendorId, int dispatchLocationId, int finalDestinationId){
        Optional<ConfigRates> configRatesOptional = this.getConfigRates(
                BookingType.FULL_BOOKING
                , vendorId
                , 0
                , dispatchLocationId
                , finalDestinationId);
        return configRatesOptional.map(ConfigRates::getRate).orElse(0.0);
    }

    private Optional<ConfigRates> getConfigRates(BookingType bookingType, int vendorId, int itemId, int fromLocationId, int toLocationId) {
        if (BookingType.PARTIAL_BOOKING.equals(bookingType)) {
            return entityConfigService.getOneRates(
                    itemId,
                    fromLocationId,
                    toLocationId, vendorId, bookingType);
        } else if (BookingType.FULL_BOOKING.equals(bookingType)) {
            return entityConfigService.getOneRates(
                    itemId,
                    fromLocationId,
                    toLocationId, vendorId, bookingType);
        }
        return Optional.empty();
    }

}
