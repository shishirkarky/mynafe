/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.staffs;

import com.myna.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StaffsServiceImpl implements StaffsService {
    private StaffsRepository staffsRepository;

    @Autowired
    public void setStaffsRepository(StaffsRepository staffsRepository) {
        this.staffsRepository = staffsRepository;
    }

    @Override
    public List<Staffs> getByBranchCode(String branchCode) {
        return staffsRepository.findAllByBranchCode(branchCode);
    }

    @Override
    public Optional<Staffs> save(Staffs staffs) {
        return Optional.of(staffsRepository.save(staffs));
    }

    @Override
    public Optional<Staffs> update(int id, Staffs staffs) {
        Optional<Staffs> staffsOptional = this.getOne(id);
        if(staffsOptional.isPresent()){
            staffs.setId(id);

            Staffs existingStaffs = staffsOptional.get();
            CustomBeanUtils.copyNonNullProperties(staffs, existingStaffs);

            return this.save(existingStaffs);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Staffs> getOne(int id) {
        return staffsRepository.findById(id);
    }

    @Override
    public List<Staffs> getAll() {
        return staffsRepository.findAll();
    }

    @Override
    public void deleteById(int id) {
        staffsRepository.deleteById(id);
    }
}
