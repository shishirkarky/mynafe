/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.staffs;

import com.myna.app.utils.CustomBeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StaffsDtoAssembler {

    public Staffs toDomain(StaffsDto staffsDto) {
        Staffs staffs = new Staffs();
        CustomBeanUtils.copyNonNullProperties(staffsDto, staffs);
        return staffs;
    }

    public StaffsDto toModel(Staffs staffs) {
        StaffsDto staffsDto = new StaffsDto();
        CustomBeanUtils.copyNonNullProperties(staffs, staffsDto);
        return staffsDto;
    }

    public List<StaffsDto> toModels(List<Staffs> staffsList) {
        List<StaffsDto> staffsDtos = new ArrayList<>();
        staffsList.forEach(staffs -> staffsDtos.add(this.toModel(staffs)));
        return staffsDtos;
    }
}
