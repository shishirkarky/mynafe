/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.staffs;

import java.util.List;
import java.util.Optional;

public interface StaffsService {
    List<Staffs> getByBranchCode(String branchCode);

    Optional<Staffs> save(Staffs staffs);

    Optional<Staffs> update(int id, Staffs staffs);

    Optional<Staffs> getOne(int id);

    List<Staffs> getAll();

    void deleteById(int id);
}
