package com.myna.app.confpackages;

import com.myna.app.dataconfig.packages.ConfigPackages;
import com.myna.app.dataconfig.packages.ConfigPackagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfPackagesServiceImpl implements ConfPackagesService {
    private ConfigPackagesRepository configPackagesRepository;

    @Autowired
    public void setConfigPackagesRepository(ConfigPackagesRepository configPackagesRepository) {
        this.configPackagesRepository = configPackagesRepository;
    }

    @Override
    public List<ConfigPackages> getByVendor(int vendorId) {
        return configPackagesRepository.findAllByRefVendorId(vendorId);
    }
}
