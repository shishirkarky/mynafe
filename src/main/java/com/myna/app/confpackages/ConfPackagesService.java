package com.myna.app.confpackages;

import com.myna.app.dataconfig.packages.ConfigPackages;

import java.util.List;

public interface ConfPackagesService {
    List<ConfigPackages> getByVendor(int vendorId);
}
