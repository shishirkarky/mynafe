package com.myna.app.confpackages;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.dataconfig.packages.ConfigPackagesDtoAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("impl/api/v1")
public class ConfPackagesController {
    private ConfPackagesService confPackagesService;
    private ConfigPackagesDtoAssembler configPackagesDtoAssembler;

    @Autowired
    public void setConfPackagesService(ConfPackagesService confPackagesService) {
        this.confPackagesService = confPackagesService;
    }

    @Autowired
    public void setConfigPackagesDtoAssembler(ConfigPackagesDtoAssembler configPackagesDtoAssembler) {
        this.configPackagesDtoAssembler = configPackagesDtoAssembler;
    }

    @GetMapping("packages/vendors/{vendorId}")
    public ResponseEntity<?> getPackagesByVendor(@PathVariable int vendorId) {
        return new ResponseEntity<>(
                new ApiResponse().getSuccessResponse(null,
                        configPackagesDtoAssembler.toModels(confPackagesService.getByVendor(vendorId)))
                , HttpStatus.OK);
    }
}
