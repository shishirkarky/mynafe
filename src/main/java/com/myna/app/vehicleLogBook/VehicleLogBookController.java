/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vehicleLogBook;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.auth.service.UserService;
import com.myna.app.office.assembler.OfficeDtoAssembler;
import com.myna.app.office.entity.Office;
import com.myna.app.office.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/impl/api/v1")
public class VehicleLogBookController {
    private VehicleLogBookService vehicleLogBookService;
    private VehicleLogBookDtoAssembler vehicleLogBookDtoAssembler;
    private UserService userService;
    private OfficeService officeService;
    private OfficeDtoAssembler officeDtoAssembler;

    @Autowired
    public void setVehicleLogBookService(VehicleLogBookService vehicleLogBookService) {
        this.vehicleLogBookService = vehicleLogBookService;
    }

    @Autowired
    public void setVehicleLogBookDtoAssembler(VehicleLogBookDtoAssembler vehicleLogBookDtoAssembler) {
        this.vehicleLogBookDtoAssembler = vehicleLogBookDtoAssembler;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setOfficeService(OfficeService officeService) {
        this.officeService = officeService;
    }

    @Autowired
    public void setOfficeDtoAssembler(OfficeDtoAssembler officeDtoAssembler) {
        this.officeDtoAssembler = officeDtoAssembler;
    }

    @PostMapping("/vehicles/logbooks")
    public ResponseEntity<?> create(@RequestBody VehicleLogBookDto vehicleLogBookDto) {
        Optional<String> branchCode = userService.getTokenUserBranchCode();
        if (branchCode.isPresent()) {
            Optional<Office> officeOptional = officeService.getOneOffice(branchCode.get());
            officeOptional.ifPresent(office -> vehicleLogBookDto.setOffice(officeDtoAssembler.toModel(office)));

            Optional<VehicleLogBook> vehicleLogBookOptional = vehicleLogBookService.create(
                    vehicleLogBookDtoAssembler.toEntity(vehicleLogBookDto)
            );
            if (vehicleLogBookOptional.isPresent()) {
                return new ApiResponse().getSuccessResponse(null, vehicleLogBookDtoAssembler.toModel(vehicleLogBookOptional.get()));
            }
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/vehicles/logbooks/{id}")
    public ResponseEntity<?> update(@PathVariable int id, @RequestBody VehicleLogBookDto vehicleLogBookDto) {
        Optional<VehicleLogBook> vehicleLogBookOptional = vehicleLogBookService.update(
                id,
                vehicleLogBookDtoAssembler.toEntity(vehicleLogBookDto)
        );
        if (vehicleLogBookOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vehicleLogBookDtoAssembler.toModel(vehicleLogBookOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/vehicles/logbooks")
    public ResponseEntity<?> getAll() {
        return new ApiResponse().getSuccessResponse(null,
                vehicleLogBookDtoAssembler.toModels(
                        vehicleLogBookService.getAll()
                ));
    }

    @GetMapping("/vehicles/logbooks/{id}")
    public ResponseEntity<?> getOne(@PathVariable int id) {
        Optional<VehicleLogBook> vehicleLogBookOptional = vehicleLogBookService.getOne(
                id
        );
        if (vehicleLogBookOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, vehicleLogBookDtoAssembler.toModel(vehicleLogBookOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @DeleteMapping("/vehicles/logbooks/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable int id) {
        boolean vehicleLogBookOptional = vehicleLogBookService.delete(
                id
        );
        if (vehicleLogBookOptional) {
            return new ApiResponse().getSuccessResponse(null, null);
        }
        return new ApiResponse().getFailureResponse(null);
    }
}
