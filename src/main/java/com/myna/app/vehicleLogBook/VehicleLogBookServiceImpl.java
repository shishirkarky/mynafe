/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vehicleLogBook;

import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.service.UserService;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.vendorrequest.entity.VendorRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VehicleLogBookServiceImpl implements VehicleLogBookService {
    private VehicleLogBookRepository vehicleLogBookRepository;
    private UserService userService;

    @Autowired
    public void setVehicleLogBookRepository(VehicleLogBookRepository vehicleLogBookRepository) {
        this.vehicleLogBookRepository = vehicleLogBookRepository;
    }

    @Override
    public Optional<VehicleLogBook> create(VehicleLogBook vehicleLogBook) {
        return Optional.of(vehicleLogBookRepository.save(vehicleLogBook));
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Optional<VehicleLogBook> update(int id, VehicleLogBook vehicleLogBook) {
        Optional<VehicleLogBook> vehicleLogBookOptional = this.getOne(id);
        if (vehicleLogBookOptional.isPresent()) {
            vehicleLogBook.setId(id);

            VehicleLogBook existingVehicleLogBook = vehicleLogBookOptional.get();
            CustomBeanUtils.copyNonNullProperties(vehicleLogBook, existingVehicleLogBook);

            return Optional.of(vehicleLogBookRepository.save(existingVehicleLogBook));
        }
        return Optional.empty();
    }

    @Override
    public Optional<VehicleLogBook> getOne(int id) {
        return vehicleLogBookRepository.findById(id);
    }

    @Override
    public List<VehicleLogBook> getAll() {
        return this.getAllByCurrentUserRoles();
    }

    private List<VehicleLogBook> getAllByCurrentUserRoles() {
        List<String> rolesOptional = userService.getCurrentUserRoles();
        if(!rolesOptional.isEmpty() && rolesOptional.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))){
            return vehicleLogBookRepository.findAll();
        }else{
            Optional<String> branchCode = userService.getTokenUserBranchCode();
            if(branchCode.isPresent()) {
                return vehicleLogBookRepository.findAllByOfficeOfficeCode(branchCode.get());
            }
        }
        return new ArrayList<>();
    }

    @Override
    public boolean delete(int id) {
        if (this.getOne(id).isPresent()) {
            vehicleLogBookRepository.deleteById(id);
            return true;
        }
        return false;
    }

  @Override
  public List<VehicleLogBook> findAllByBookingIdIn(List<Integer> bookingIds) {
    return vehicleLogBookRepository.findAllByBookingIdIn(bookingIds);
  }
}
