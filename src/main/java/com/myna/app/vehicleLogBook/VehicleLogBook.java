/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vehicleLogBook;

import com.myna.app.commons.Auditable;
import com.myna.app.office.entity.Office;
import com.myna.app.vendorrequest.entity.VendorRequest;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "vehicle_log_book")
public class VehicleLogBook extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne(optional = true)
    private VendorRequest booking;

    private String checkInDate;
    private String checkInTime;
    private String checkOutDate;
    private String checkOutTime;
    private double inKm;
    private double outKm;
    private String laborName;
    private int numberOfTrip;

    @OneToOne(optional = true)
    private Office office;

    private double rate;
}
