/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vehicleLogBook;

import java.util.List;
import java.util.Optional;

public interface VehicleLogBookService {
    Optional<VehicleLogBook> create(VehicleLogBook vehicleLogBook);

    Optional<VehicleLogBook> update(int id, VehicleLogBook vehicleLogBook);

    Optional<VehicleLogBook> getOne(int id);

    List<VehicleLogBook> getAll();

    boolean delete(int id);

  List<VehicleLogBook> findAllByBookingIdIn(List<Integer> bookingIds);
}
