/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vehicleLogBook;

import com.myna.app.office.assembler.OfficeDtoAssembler;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VehicleLogBookDtoAssembler extends RepresentationModelAssemblerSupport<VehicleLogBook, VehicleLogBookDto> {
    private VendorRequestDtoAssembler vendorRequestDtoAssembler;
    private OfficeDtoAssembler officeDtoAssembler;

    @Autowired
    public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
        this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
    }

    public VehicleLogBookDtoAssembler() {
        super(VehicleLogBook.class, VehicleLogBookDto.class);
    }

    @Autowired
    public void setOfficeDtoAssembler(OfficeDtoAssembler officeDtoAssembler) {
        this.officeDtoAssembler = officeDtoAssembler;
    }

    @Override
    public VehicleLogBookDto toModel(VehicleLogBook entity) {
        VehicleLogBookDto model = new VehicleLogBookDto();
        CustomBeanUtils.copyNonNullProperties(entity, model);

        model.setBooking(vendorRequestDtoAssembler.toModel(entity.getBooking()));
        if (null != entity.getOffice())
            model.setOffice(officeDtoAssembler.toModel(entity.getOffice()));

        return model;
    }

    public List<VehicleLogBookDto> toModels(List<VehicleLogBook> entities) {
        List<VehicleLogBookDto> models = new ArrayList<>();
        entities.forEach(VehicleLogBook -> models.add(this.toModel(VehicleLogBook)));
        return models;
    }

    public VehicleLogBook toEntity(VehicleLogBookDto model) {
        VehicleLogBook entity = new VehicleLogBook();
        CustomBeanUtils.copyNonNullProperties(model, entity);

        entity.setBooking(vendorRequestDtoAssembler.toDomain(model.getBooking()));
        if (null != model.getOffice())
            entity.setOffice(officeDtoAssembler.toDomain(model.getOffice()));

        return entity;
    }

    public List<VehicleLogBook> toEntities(List<VehicleLogBookDto> models) {
        List<VehicleLogBook> entities = new ArrayList<>();
        models.forEach(VehicleLogBookDto -> entities.add(this.toEntity(VehicleLogBookDto)));
        return entities;
    }
}
