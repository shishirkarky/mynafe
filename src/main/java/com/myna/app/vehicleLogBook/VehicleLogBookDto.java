/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vehicleLogBook;

import com.myna.app.office.dto.OfficeDto;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
public class VehicleLogBookDto extends RepresentationModel<VehicleLogBookDto> {
    private int id;
    private VendorRequestDto booking;

    private String checkInDate;
    private String checkInTime;
    private String checkOutDate;
    private String checkOutTime;
    private double inKm;
    private double outKm;
    private String laborName;
    private int numberOfTrip;

    private OfficeDto office;

    private double rate;
}
