/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vehicleLogBook;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleLogBookRepository extends JpaRepository<VehicleLogBook, Integer> {
  List<VehicleLogBook> findAllByOfficeOfficeCode(String branchCode);

  List<VehicleLogBook> findAllByBookingIdIn(List<Integer> bookingIds);
}
