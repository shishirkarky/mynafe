/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dashboard.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashboardBarGraph {
    private int vendors;
    private int drivers;
    private int vehicles;
    private int activeVehicles;
    private int activeDrivers;
    private int completedTasks;
    private int pendingTasks;
}
