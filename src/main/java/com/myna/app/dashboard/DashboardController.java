/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dashboard;

import com.myna.app.apiresponse.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("impl/api/v1")
public class DashboardController {
    private DashboardComponent dashboardComponent;

    @Autowired
    public void setDashboardComponent(DashboardComponent dashboardComponent) {
        this.dashboardComponent = dashboardComponent;
    }

    @GetMapping("dashboard")
    public ResponseEntity<?> getDashboardDetails(){
        return new ApiResponse().getSuccessResponse(null, dashboardComponent.getDashboardDetails());
    }

    @GetMapping("dashboard/bookings")
    public ResponseEntity<?> getDashboardBookingDetails(String branchCode){
        return new ApiResponse().getSuccessResponse(null, dashboardComponent.setVendorBookingBarGraphDetails(branchCode));
    }
}
