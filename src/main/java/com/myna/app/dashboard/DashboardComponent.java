/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dashboard;

import com.myna.app.pod.CashReceipt;
import com.myna.app.pod.PodService;
import com.myna.app.dashboard.dto.DashboardBarGraph;
import com.myna.app.dashboard.dto.DashboardDto;
import com.myna.app.driver.service.DriverService;
import com.myna.app.enums.Status;
import com.myna.app.vehicle.service.VehicleService;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendor.service.VendorService;
import com.myna.app.vendorrequest.dto.VendorRequestCountDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.service.VendorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class DashboardComponent {
    private DriverService driverService;
    private VehicleService vehicleService;
    private VendorService vendorService;
    private VendorRequestService vendorRequestService;
    private PodService podService;

    @Autowired
    public void setDriverService(DriverService driverService) {
        this.driverService = driverService;
    }

    @Autowired
    public void setVehicleService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @Autowired
    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    @Autowired
    public void setVendorRequestService(VendorRequestService vendorRequestService) {
        this.vendorRequestService = vendorRequestService;
    }

    @Autowired
    public void setPodService(PodService podService) {
        this.podService = podService;
    }

    public DashboardDto getDashboardDetails() {
        DashboardDto dashboard = new DashboardDto();
        dashboard.setBarGraph(this.getBarGraphDetails());
        return dashboard;
    }

    /**
     *
     * @return
     */
    private DashboardBarGraph getBarGraphDetails() {
        DashboardBarGraph barGraph = new DashboardBarGraph();
        barGraph.setDrivers(driverService.countTotal());
        barGraph.setVehicles(vehicleService.countTotal());
        barGraph.setVendors(vendorService.countTotal());

        this.setActiveDriversVehiclesCount(barGraph);

        barGraph.setCompletedTasks(podService.countAllByPodStatus(Status.DELIVERED));
        barGraph.setPendingTasks(podService.countAllByPodStatus(Status.ACTIVE));
        return barGraph;
    }

    private void setActiveDriversVehiclesCount(DashboardBarGraph barGraph) {
        List<CashReceipt> cashReceipts = podService.getAllActiveCashReceiptsEntity();
        Set<Integer> uniqueVendorRequests = new HashSet<>();
        cashReceipts.forEach(cashReceipt -> uniqueVendorRequests.add(cashReceipt.getRefVendorRequestId()));

        List<VendorRequest> vendorRequests = vendorRequestService.getVendorRequestsByIdIn(new ArrayList<>(uniqueVendorRequests));
        Set<Integer> uniqueDrivers = new HashSet<>();
        Set<Integer> uniqueVehicles = new HashSet<>();

        vendorRequests.forEach(vendorRequest -> {
            uniqueDrivers.add(vendorRequest.getRefDriverId());
            uniqueVehicles.add(vendorRequest.getRefVehicleId());
        });

        barGraph.setActiveDrivers(uniqueDrivers.size());
        barGraph.setActiveVehicles(uniqueVehicles.size());
    }

    public Map<String, Integer> setVendorBookingBarGraphDetails(String branchCode) {
        Map<String, Integer> vendorBooking = new HashMap<>();
        List<VendorRequestCountDto> groupCount = vendorRequestService.getCountGroupByVendor(branchCode);
        AtomicInteger total = new AtomicInteger(0);

        groupCount.forEach(vendorRequestCountDto -> {
            total.set(total.get() + (int) vendorRequestCountDto.getCount());
            Optional<Vendor> vendorOptional = vendorService.getOneVendor(vendorRequestCountDto.getRefVendorId());
            vendorOptional.ifPresent(vendor ->
                    vendorBooking.put(vendor.getName(), (int) vendorRequestCountDto.getCount()));
        });
        vendorBooking.put("Total", total.get());
        return vendorBooking;
    }

}
