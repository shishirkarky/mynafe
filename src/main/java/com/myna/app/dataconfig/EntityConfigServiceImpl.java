/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.dataconfig;

import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.dataconfig.locations.ConfigLocationsRepository;
import com.myna.app.dataconfig.packages.ConfigPackages;
import com.myna.app.dataconfig.packages.ConfigPackagesRepository;
import com.myna.app.dataconfig.rates.ConfigRates;
import com.myna.app.dataconfig.rates.ConfigRatesRepository;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.vendorrequest.enums.BookingType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EntityConfigServiceImpl implements EntityConfigService {
    /*
     * Repositories
     */
    private ConfigLocationsRepository configLocationsRepository;
    private ConfigPackagesRepository configPackagesRepository;
    private ConfigRatesRepository configRatesRepository;

    /*
     * Autowiring
     */
    @Autowired
    public void setConfigLocationsRepository(ConfigLocationsRepository configLocationsRepository) {
        this.configLocationsRepository = configLocationsRepository;
    }

    @Autowired
    public void setConfigPackagesRepository(ConfigPackagesRepository configPackagesRepository) {
        this.configPackagesRepository = configPackagesRepository;
    }

    @Autowired
    public void setConfigRatesRepository(ConfigRatesRepository configRatesRepository) {
        this.configRatesRepository = configRatesRepository;
    }

    /*
     * ConfigLocations
     */
    @Override
    public Optional<ConfigLocations> saveLocations(ConfigLocations configLocations) {
        return Optional.of(configLocationsRepository.save(configLocations));
    }

    @Override
    public boolean locationExists(ConfigLocationsDto configLocations) {
        return configLocationsRepository.findFirstByCityAndDistrict(configLocations.getCity(), configLocations.getDistrict()).isPresent();
    }

    @Override
    public Optional<ConfigLocations> updateLocation(int id, ConfigLocations configLocations) {
        Optional<ConfigLocations> locationsOptional = this.getOneLocations(id);
        if (locationsOptional.isPresent()) {
            configLocations.setId(id);

            ConfigLocations existingConfigLocations = locationsOptional.get();
            CustomBeanUtils.copyNonNullProperties(configLocations, existingConfigLocations);

            return this.saveLocations(existingConfigLocations);
        }
        return Optional.empty();
    }

    @Override
    public Optional<ConfigLocations> getOneLocations(int id) {
        return configLocationsRepository.findById(id);
    }

    @Override
    public Optional<ConfigLocations> getOneLocationByCity(String city) {
        city = city.trim();
        return configLocationsRepository.findFirstByCity(city);
    }

    @Override
    public List<ConfigLocations> getAllLocations() {
        return configLocationsRepository.findAll();
    }

    /*
     * ConfigPackages
     */
    @Override
    public Optional<ConfigPackages> savePackages(ConfigPackages configPackages) {
        return Optional.of(configPackagesRepository.save(configPackages));
    }

    @Override
    public Optional<ConfigPackages> updatePackage(int id, ConfigPackages configPackages) {
        Optional<ConfigPackages> configPackagesOptional = this.getOnePackage(id);
        if (configPackagesOptional.isPresent()) {
            configPackages.setId(id);

            ConfigPackages existingConfigPackages = configPackagesOptional.get();
            CustomBeanUtils.copyNonNullProperties(configPackages, existingConfigPackages);

            return this.savePackages(existingConfigPackages);
        }
        return Optional.empty();
    }

    @Override
    public Optional<ConfigPackages> getOnePackage(int id) {
        return configPackagesRepository.findById(id);
    }

    @Override
    public Optional<ConfigPackages> getOnePackageByCode(String code) {
        return configPackagesRepository.findFirstByCode(code);
    }

    @Override
    public Optional<ConfigPackages> getOnePackageByVendorAndName(int refVendorId, String name) {
        name = name.trim();
        return configPackagesRepository.findFirstByRefVendorIdAndName(refVendorId, name);
    }

    @Override
    public List<ConfigPackages> getAllPackages() {
        return configPackagesRepository.findAll();
    }

    @Override
    public Optional<ConfigRates> saveRates(ConfigRates configRates) {
        return Optional.of(configRatesRepository.save(configRates));
    }

    @Override
    public List<ConfigRates> saveRates(List<ConfigRates> configRates) {
        return configRatesRepository.saveAll(configRates);
    }

    @Override
    public Optional<ConfigRates> updateRates(int id, ConfigRates configRates) {
        Optional<ConfigRates> configRatesOptional = this.getOneRates(id);
        if (configRatesOptional.isPresent()) {
            configRates.setId(id);

            ConfigRates existingConfigRates = configRatesOptional.get();
            CustomBeanUtils.copyNonNullProperties(configRates, existingConfigRates);

            return this.saveRates(existingConfigRates);
        }
        return Optional.empty();
    }

    @Override
    public Optional<ConfigRates> getOneRates(int id) {
        return configRatesRepository.findById(id);
    }

    @Override
    public List<ConfigRates> getAllRates() {
        return configRatesRepository.findAll();
    }

    @Override
    public List<ConfigRates> getAllRates(BookingType bookingType, int vendorId, int fromLocationId) {
        return configRatesRepository.findAllByBookingTypeAndRefVendorIdAndRefConfFromLocationId(bookingType, vendorId, fromLocationId);
    }

    @Override
    public void deleteAllRates(){
      configRatesRepository.deleteAll();
    }

    @Override
    public Optional<ConfigRates> getOneRates(int refPackageId, int refFromLocationId, int refToLocationId, int refVendorId, BookingType bookingType) {
        return configRatesRepository.findFirstByRefConfPackageIdAndRefConfFromLocationIdAndRefConfToLocationIdAndRefVendorIdAndBookingType(refPackageId, refFromLocationId, refToLocationId, refVendorId, bookingType);
    }
}
