/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dataconfig.packages;

import com.myna.app.vendor.dto.VendorDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
public class ConfigPackagesDto extends RepresentationModel<ConfigPackagesDto> {
    private int id;
    private String code;
    private String type;
    private String name;
    private String description;
    private String brand;
    private int refVendorId;
    private VendorDto vendor;
}
