/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.dataconfig.packages;

import com.myna.app.commons.Auditable;
import com.myna.app.vendor.entity.Vendor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "conf_packages")
public class ConfigPackages extends Auditable<String> {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  private String code;
  private String type;
  private String name;
  private String description;
  private String brand;
  private int refVendorId;
  @OneToOne(optional = true)
  private Vendor vendor;
}
