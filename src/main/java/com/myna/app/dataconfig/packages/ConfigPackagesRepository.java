/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.dataconfig.packages;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConfigPackagesRepository extends JpaRepository<ConfigPackages, Integer> {
    List<ConfigPackages> findAllByRefVendorId(int vendorId);

    Optional<ConfigPackages> findFirstByCode(String code);

    Optional<ConfigPackages> findFirstByRefVendorIdAndName(int refVendorId, String name);
}
