/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.dataconfig.packages;

import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.vendor.assembler.VendorDtoAssembler;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendor.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ConfigPackagesDtoAssembler extends RepresentationModelAssemblerSupport<ConfigPackages, ConfigPackagesDto> {
    private VendorDtoAssembler vendorDtoAssembler;

    @Autowired
    public void setVendorDtoAssembler(VendorDtoAssembler vendorDtoAssembler) {
        this.vendorDtoAssembler = vendorDtoAssembler;
    }

    public ConfigPackagesDtoAssembler() {
        super(ConfigPackages.class, ConfigPackagesDto.class);
    }

    @Override
    public ConfigPackagesDto toModel(ConfigPackages entity) {
        ConfigPackagesDto model = new ConfigPackagesDto();
        CustomBeanUtils.copyNonNullProperties(entity, model);

        if(null!=entity.getVendor()){
            model.setVendor(vendorDtoAssembler.toModel(entity.getVendor()));
        }

        return model;
    }

    public List<ConfigPackagesDto> toModels(List<ConfigPackages> entities) {
        List<ConfigPackagesDto> models = new ArrayList<>();
        entities.forEach(configPackages -> models.add(this.toModel(configPackages)));
        return models;
    }

    public ConfigPackages toEntity(ConfigPackagesDto model) {
        ConfigPackages entity = new ConfigPackages();
        CustomBeanUtils.copyNonNullProperties(model, entity);
        if(model.getRefVendorId()>0){
          Vendor vendor = new Vendor();
          vendor.setId(model.getRefVendorId());
          entity.setVendor(vendor);
        }
        return entity;
    }

    public List<ConfigPackages> toEntities(List<ConfigPackagesDto> models) {
        List<ConfigPackages> entities = new ArrayList<>();
        models.forEach(configPackagesDto -> entities.add(this.toEntity(configPackagesDto)));
        return entities;
    }
}
