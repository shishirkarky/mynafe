package com.myna.app.dataconfig;

import com.myna.app.dataconfig.rates.HiringVehicleConfigRates;

import java.util.List;
import java.util.Optional;

public interface HiringVehicleRateService {
  Optional<HiringVehicleConfigRates> save(HiringVehicleConfigRates hiringVehicleConfigRates);
  List<HiringVehicleConfigRates> getAll();

}
