/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.dataconfig;

import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.dataconfig.packages.ConfigPackages;
import com.myna.app.dataconfig.rates.ConfigRates;
import com.myna.app.vendorrequest.enums.BookingType;

import java.util.List;
import java.util.Optional;

public interface EntityConfigService {
    /*
     * ConfigLocations
     */
    Optional<ConfigLocations> saveLocations(ConfigLocations configLocations);

    boolean locationExists(ConfigLocationsDto configLocations);

    Optional<ConfigLocations> updateLocation(int id, ConfigLocations configLocations);

    Optional<ConfigLocations> getOneLocations(int id);

    Optional<ConfigLocations> getOneLocationByCity(String city);

    List<ConfigLocations> getAllLocations();

    /*
     * ConfigPackages
     */
    Optional<ConfigPackages> savePackages(ConfigPackages configPackages);

    Optional<ConfigPackages> updatePackage(int id, ConfigPackages configPackages);

    Optional<ConfigPackages> getOnePackage(int id);

    Optional<ConfigPackages> getOnePackageByCode(String code);

    Optional<ConfigPackages> getOnePackageByVendorAndName(int refVendorId, String name);

    List<ConfigPackages> getAllPackages();

    /*
     * ConfigRates
     */
    Optional<ConfigRates> saveRates(ConfigRates configRates);

    List<ConfigRates> saveRates(List<ConfigRates> configRates);

    Optional<ConfigRates> updateRates(int id, ConfigRates configRates);

    Optional<ConfigRates> getOneRates(int id);

    List<ConfigRates> getAllRates();

    List<ConfigRates> getAllRates(BookingType bookingType, int vendorId, int fromLocationId);

    void deleteAllRates();

    Optional<ConfigRates> getOneRates(int refPackageId, int refFromLocationId, int refToLocationId, int refVendorId, BookingType bookingType);
}
