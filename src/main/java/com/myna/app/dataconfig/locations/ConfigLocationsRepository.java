/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dataconfig.locations;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfigLocationsRepository extends JpaRepository<ConfigLocations, Integer> {
    Optional<ConfigLocations> findFirstByCity(String city);
    Optional<ConfigLocations> findFirstByCityAndDistrict(String city, String district);
}
