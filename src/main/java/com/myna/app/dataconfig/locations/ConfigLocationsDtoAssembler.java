/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dataconfig.locations;

import com.myna.app.utils.CustomBeanUtils;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConfigLocationsDtoAssembler extends RepresentationModelAssemblerSupport<ConfigLocations, ConfigLocationsDto> {

    public ConfigLocationsDtoAssembler() {
        super(ConfigLocations.class, ConfigLocationsDto.class);
    }

    @Override
    public ConfigLocationsDto toModel(ConfigLocations entity) {
        ConfigLocationsDto model = new ConfigLocationsDto();
        CustomBeanUtils.copyNonNullProperties(entity, model);
        return model;
    }

    public List<ConfigLocationsDto> toModels(List<ConfigLocations> entities) {
        List<ConfigLocationsDto> models = new ArrayList<>();
        entities.forEach(configLocations -> models.add(this.toModel(configLocations)));
        return models;
    }

    public ConfigLocations toEntity(ConfigLocationsDto model) {
        ConfigLocations entity = new ConfigLocations();
        CustomBeanUtils.copyNonNullProperties(model, entity);
        return entity;
    }

    public List<ConfigLocations> toEntities(List<ConfigLocationsDto> models) {
        List<ConfigLocations> entities = new ArrayList<>();
        models.forEach(configLocationsDto -> entities.add(this.toEntity(configLocationsDto)));
        return entities;
    }
}
