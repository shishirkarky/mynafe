package com.myna.app.dataconfig.rates;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HiringVehicleConfigRatesRepository extends JpaRepository<HiringVehicleConfigRates, Integer> {
}
