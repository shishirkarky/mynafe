/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dataconfig.rates;

import com.myna.app.dataconfig.EntityConfigService;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDtoAssembler;
import com.myna.app.dataconfig.packages.ConfigPackages;
import com.myna.app.dataconfig.packages.ConfigPackagesDtoAssembler;
import com.myna.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ConfigRatesDtoAssembler extends RepresentationModelAssemblerSupport<ConfigRates, ConfigRatesDto> {
    private EntityConfigService entityConfigService;
    private ConfigPackagesDtoAssembler configPackagesDtoAssembler;
    private ConfigLocationsDtoAssembler configLocationsDtoAssembler;

    public ConfigRatesDtoAssembler() {
        super(ConfigRates.class, ConfigRatesDto.class);
    }

    @Autowired
    public void setEntityConfigService(EntityConfigService entityConfigService) {
        this.entityConfigService = entityConfigService;
    }

    @Autowired
    public void setConfigPackagesDtoAssembler(ConfigPackagesDtoAssembler configPackagesDtoAssembler) {
        this.configPackagesDtoAssembler = configPackagesDtoAssembler;
    }

    @Autowired
    public void setConfigLocationsDtoAssembler(ConfigLocationsDtoAssembler configLocationsDtoAssembler) {
        this.configLocationsDtoAssembler = configLocationsDtoAssembler;
    }

    @Override
    public ConfigRatesDto toModel(ConfigRates entity) {
        ConfigRatesDto model = new ConfigRatesDto();
        CustomBeanUtils.copyNonNullProperties(entity, model);

        Optional<ConfigPackages> configPackagesOptional = entityConfigService.getOnePackage(entity.getRefConfPackageId());
        configPackagesOptional.ifPresent(configPackages -> model.setItem(configPackagesDtoAssembler.toModel(configPackages)));

        Optional<ConfigLocations> configFromLocationsOptional = entityConfigService.getOneLocations(entity.getRefConfFromLocationId());
        configFromLocationsOptional.ifPresent(configLocations -> model.setFromLocation(configLocationsDtoAssembler.toModel(configLocations)));

        Optional<ConfigLocations> configToLocationsOptional = entityConfigService.getOneLocations(entity.getRefConfToLocationId());
        configToLocationsOptional.ifPresent(configLocations -> model.setToLocation(configLocationsDtoAssembler.toModel(configLocations)));

        return model;
    }

    public List<ConfigRatesDto> toModels(List<ConfigRates> entities) {
        List<ConfigRatesDto> models = new ArrayList<>();
        entities.forEach(ConfigRates -> models.add(this.toModel(ConfigRates)));
        return models;
    }

    public ConfigRates toEntity(ConfigRatesDto model) {
        ConfigRates entity = new ConfigRates();
        CustomBeanUtils.copyNonNullProperties(model, entity);
        return entity;
    }

    public List<ConfigRates> toEntities(List<ConfigRatesDto> models) {
        List<ConfigRates> entities = new ArrayList<>();
        models.forEach(ConfigRatesDto -> entities.add(this.toEntity(ConfigRatesDto)));
        return entities;
    }
}
