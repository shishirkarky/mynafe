/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dataconfig.rates;

import com.myna.app.vendorrequest.enums.BookingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConfigRatesRepository extends JpaRepository<ConfigRates, Integer> {
    Optional<ConfigRates> findFirstByRefConfPackageIdAndRefConfFromLocationIdAndRefConfToLocationIdAndRefVendorIdAndBookingType(int refPackageId, int refFromLocationId, int refToLocationId, int refVendorId, BookingType bookingType);
    List<ConfigRates> findAllByBookingTypeAndRefVendorIdAndRefConfFromLocationId(BookingType bookingType, int vendorId, int fromLocationId);
}
