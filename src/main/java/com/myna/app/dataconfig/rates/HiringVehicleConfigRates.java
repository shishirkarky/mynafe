package com.myna.app.dataconfig.rates;

import com.myna.app.commons.Auditable;
import com.myna.app.vehicle.entity.Vehicle;
import com.myna.app.vehicle.enums.VehicleType;
import com.myna.app.vendor.entity.Vendor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table
@Entity(name = "conf_hiring_vehicle_rates")
@Getter
@Setter
public class HiringVehicleConfigRates extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    private Vendor vendor;

    @OneToOne
    private Vehicle vehicle;

    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;

    private double totalWorkingHours;
    private double totalAmount;
    private double mileage;
    private double fuelRate;
    private double otRate;
}
