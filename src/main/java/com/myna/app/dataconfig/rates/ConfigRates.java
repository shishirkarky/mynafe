/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dataconfig.rates;

import com.myna.app.commons.Auditable;
import com.myna.app.vendorrequest.enums.BookingType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "conf_rates")
public class ConfigRates extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Enumerated(EnumType.STRING)
    private BookingType bookingType;
    private int refVendorId;
    private int refConfPackageId; //conf_packages
    private int refConfFromLocationId; //conf_location
    private int refConfToLocationId; //conf_location
    private double rate;
}
