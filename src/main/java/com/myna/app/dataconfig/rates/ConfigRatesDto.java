/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.dataconfig.rates;

import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.dataconfig.packages.ConfigPackagesDto;
import com.myna.app.vendor.dto.VendorDto;
import com.myna.app.vendorrequest.enums.BookingType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class ConfigRatesDto extends RepresentationModel<ConfigRatesDto> {
    private int id;
    private BookingType bookingType;

    private int refVendorId;
    private VendorDto vendor;

    private int refConfPackageId; //conf_packages
    private ConfigPackagesDto item;

    private int refConfFromLocationId; //conf_location
    private ConfigLocationsDto fromLocation;

    private int refConfToLocationId; //conf_location
    private ConfigLocationsDto toLocation;
    private double rate;
}
