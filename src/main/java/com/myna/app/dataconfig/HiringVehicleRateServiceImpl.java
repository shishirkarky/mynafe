package com.myna.app.dataconfig;

import com.myna.app.dataconfig.rates.HiringVehicleConfigRates;
import com.myna.app.dataconfig.rates.HiringVehicleConfigRatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HiringVehicleRateServiceImpl implements HiringVehicleRateService {
  private HiringVehicleConfigRatesRepository hiringVehicleConfigRatesRepository;

  @Autowired
  public void setHiringVehicleConfigRatesRepository(HiringVehicleConfigRatesRepository hiringVehicleConfigRatesRepository) {
    this.hiringVehicleConfigRatesRepository = hiringVehicleConfigRatesRepository;
  }

  @Override
  public Optional<HiringVehicleConfigRates> save(HiringVehicleConfigRates hiringVehicleConfigRates) {
    return Optional.of(hiringVehicleConfigRatesRepository.save(hiringVehicleConfigRates));
  }

  @Override
  public List<HiringVehicleConfigRates> getAll() {
    return hiringVehicleConfigRatesRepository.findAll();
  }
}
