/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.dataconfig;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.service.UserService;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.dataconfig.locations.ConfigLocationsDtoAssembler;
import com.myna.app.dataconfig.packages.ConfigPackages;
import com.myna.app.dataconfig.packages.ConfigPackagesDto;
import com.myna.app.dataconfig.packages.ConfigPackagesDtoAssembler;
import com.myna.app.dataconfig.rates.ConfigRates;
import com.myna.app.dataconfig.rates.ConfigRatesDto;
import com.myna.app.dataconfig.rates.ConfigRatesDtoAssembler;
import com.myna.app.vendorrequest.enums.BookingType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class EntityConfigController {
  private EntityConfigService entityConfigService;
  private ConfigLocationsDtoAssembler configLocationsDtoAssembler;
  private ConfigPackagesDtoAssembler configPackagesDtoAssembler;
  private ConfigRatesDtoAssembler configRatesDtoAssembler;
  private HiringVehicleRateService hiringVehicleRateService;
  private UserService userService;

  @Autowired
  public void setEntityConfigService(EntityConfigService entityConfigService) {
    this.entityConfigService = entityConfigService;
  }

  @Autowired
  public void setConfigLocationsDtoAssembler(ConfigLocationsDtoAssembler configLocationsDtoAssembler) {
    this.configLocationsDtoAssembler = configLocationsDtoAssembler;
  }

  @Autowired
  public void setConfigPackagesDtoAssembler(ConfigPackagesDtoAssembler configPackagesDtoAssembler) {
    this.configPackagesDtoAssembler = configPackagesDtoAssembler;
  }

  @Autowired
  public void setConfigRatesDtoAssembler(ConfigRatesDtoAssembler configRatesDtoAssembler) {
    this.configRatesDtoAssembler = configRatesDtoAssembler;
  }

  @Autowired
  public void setHiringVehicleRateService(HiringVehicleRateService hiringVehicleRateService) {
    this.hiringVehicleRateService = hiringVehicleRateService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  /*
   * ConfigLocations CRUD
   */
  @PostMapping("configs/locations")
  public ResponseEntity<?> createLocations(@RequestBody ConfigLocationsDto configLocationsDto) {
    if (!entityConfigService.locationExists(configLocationsDto)) {

      Optional<ConfigLocations> configLocationsOptional = entityConfigService.saveLocations(
        configLocationsDtoAssembler.toEntity(configLocationsDto));
      if (configLocationsOptional.isPresent()) {
        return new ApiResponse().getSuccessResponse(null,
          configLocationsDtoAssembler.toModel(configLocationsOptional.get()));
      } else {
        return new ApiResponse().getFailureResponse(null);
      }
    } else {
      return new ApiResponse().getFailureResponse("Location already exists.");
    }
  }

  @PutMapping("configs/locations/{id}")
  public ResponseEntity<?> updateLocations(@PathVariable int id, @RequestBody ConfigLocationsDto configLocationsDto) {
    Optional<ConfigLocations> configLocationsOptional = entityConfigService.updateLocation(
      id,
      configLocationsDtoAssembler.toEntity(configLocationsDto));
    if (configLocationsOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configLocationsDtoAssembler.toModel(configLocationsOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @GetMapping("configs/locations")
  public ResponseEntity<?> getAllLocations() {
    return new ApiResponse().getSuccessResponse(null,
      configLocationsDtoAssembler.toModels(entityConfigService.getAllLocations())
    );
  }

  @GetMapping("configs/locations/{id}")
  public ResponseEntity<?> getOneLocations(@PathVariable int id) {
    Optional<ConfigLocations> configLocationsOptional = entityConfigService.getOneLocations(
      id);
    if (configLocationsOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configLocationsDtoAssembler.toModel(configLocationsOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  /*
   * ConfigPackages CRUD
   */
  @PostMapping("configs/packages")
  public ResponseEntity<?> createPackages(@RequestBody ConfigPackagesDto configPackagesDto) {
    Optional<ConfigPackages> configPackagesOptional = entityConfigService.savePackages(
      configPackagesDtoAssembler.toEntity(configPackagesDto));
    if (configPackagesOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configPackagesDtoAssembler.toModel(configPackagesOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @PutMapping("configs/packages/{id}")
  public ResponseEntity<?> updatePackages(@PathVariable int id, @RequestBody ConfigPackagesDto configPackagesDto) {
    Optional<ConfigPackages> configPackagesOptional = entityConfigService.updatePackage(
      id,
      configPackagesDtoAssembler.toEntity(configPackagesDto));
    if (configPackagesOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configPackagesDtoAssembler.toModel(configPackagesOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @GetMapping("configs/packages")
  public ResponseEntity<?> getAllPackages() {
    return new ApiResponse().getSuccessResponse(null,
      configPackagesDtoAssembler.toModels(entityConfigService.getAllPackages()));
  }

  @GetMapping("configs/packages/{id}")
  public ResponseEntity<?> getOnePackages(@PathVariable int id) {
    Optional<ConfigPackages> configPackagesOptional = entityConfigService.getOnePackage(id);
    if (configPackagesOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configPackagesDtoAssembler.toModel(configPackagesOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  /*
   * ConfigRates CRUD
   */
  @PostMapping("configs/rates")
  public ResponseEntity<?> createRates(@RequestBody ConfigRatesDto configRatesDto) {
    Optional<ConfigRates> configRatesOptional = entityConfigService.saveRates(
      configRatesDtoAssembler.toEntity(configRatesDto));
    if (configRatesOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configRatesDtoAssembler.toModel(configRatesOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @PutMapping("configs/rates/{id}")
  public ResponseEntity<?> updateRates(@PathVariable int id, @RequestBody ConfigRatesDto configRatesDto) {
    Optional<ConfigRates> configRatesOptional = entityConfigService.updateRates(
      id,
      configRatesDtoAssembler.toEntity(configRatesDto));
    if (configRatesOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configRatesDtoAssembler.toModel(configRatesOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  @GetMapping("configs/rates")
  public ResponseEntity<?> getAllRates() {
    return new ApiResponse().getSuccessResponse(null,
      configRatesDtoAssembler.toModels(
        entityConfigService.getAllRates()
      ));
  }

  @DeleteMapping("/configs/rates")
  public ResponseEntity<?> deleteRateConfigs() {
    List<String> roles= userService.getCurrentUserRoles();
    if (roles.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      entityConfigService.deleteAllRates();
      return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), null);
    } else {
      return new ApiResponse().getFailureResponse(HttpStatus.UNAUTHORIZED.getReasonPhrase());
    }
  }

  @GetMapping("configs/rates/vendors/{vendor}/locations/{from}")
  public ResponseEntity<?> getAllRatesByBookingTypeAndVendorAndFromLocation(@PathVariable("vendor") int vendorId, @PathVariable("from") int fromLocationId, @RequestParam("bookingType") BookingType bookingType) {
    return new ApiResponse().getSuccessResponse(null,
      configRatesDtoAssembler.toModels(
        entityConfigService.getAllRates(bookingType, vendorId, fromLocationId)
      ));
  }

  @GetMapping("configs/rates/{id}")
  public ResponseEntity<?> getOneRates(@PathVariable int id) {
    Optional<ConfigRates> configRatesOptional = entityConfigService.getOneRates(id);
    if (configRatesOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(null,
        configRatesDtoAssembler.toModel(configRatesOptional.get()));
    }
    return new ApiResponse().getFailureResponse(null);
  }

  /*
  HIRING VEHICLE RATE CONFIG
   */
  @GetMapping("configs/rates/hiring/vehicles")
  public ResponseEntity<?> getOneRates() {
    return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(),
      hiringVehicleRateService.getAll());
  }
}
