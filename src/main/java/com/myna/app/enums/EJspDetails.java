/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EJspDetails {
    PAGE_TITLE("pagetitle", ""),
    LOGIN("v1/login", ""),
    DASHBOARD("v1/dashboard", "DASHBOARD"),
    USER_CREATE("v1/user/create-page", "CREATE USER"),
    USER_VIEW("v1/user/view-page", "VIEW USER"),
    USER_PROFILE_VIEW("v1/user/profile-page", "PROFILE"),
    DRIVER_CREATE("v1/driver/create-page", "CREATE DRIVER"),
    VEHICLE_CREATE("v1/vehicle/create-page", "CREATE VEHICLE"),
    CUSTOMER_CREATE("v1/customer/create-page", "CREATE CUSTOMER"),
    CUSTOMER_VIEW("v1/customer/view-page", "VIEW CUSTOMER"),
    VENDOR_CREATE("v1/vendor/create-page", "CREATE VENDOR"),
    OFFICE_CREATE("v1/office/create-page", "CREATE OFFICE"),
    VENDOR_REQUEST_CREATE("v1/vendorrequest/create-page", "CREATE BOOKING"),
    VENDOR_REQUEST_VIEW("v1/vendorrequest/view-page", "VIEW BOOKINGS"),
    BOOKING_PIPELINE_CREATE("v1/vendorrequest/booking-pipeline", "BOOKING PIPE-LINE"),
    CASH_RECEIPT_CREATE("v1/cashreceipt/create-page", "CREATE POD"),
    CASH_RECEIPT_VIEW("v1/cashreceipt/view-page", "VIEW POD"),
    CASH_RECEIPT_EDIT("v1/cashreceipt/edit-page", "EDIT POD"),
    CASH_RECEIPT_DETAIL_VIEW("v1/cashreceipt/detail-page", "POD DETAIL"),
    FILES_UPLOAD_VIEW("v1/fileuploads/view-page", "VIEW FILES"),
    CONFIG_LOCATION_CREATE("v1/locations/create-page", "CREATE LOCATION"),
    CONFIG_PACKAGES_CREATE("v1/packages/create-page", "CREATE PACKAGE"),
    CONFIG_RATES_CONFIGURATION("v1/rates/rates-config-pipeline", "RATES CONFIGURATION"),
    VEHICLE_LOG_BOOK_CREATE("v1/vehiclelogbook/create-page", "CREATE VEHICLE LOG BOOK"),
    VEHICLE_LOG_BOOK_VIEW("v1/vehiclelogbook/view-page", "VIEW VEHICLE LOG BOOK"),
    UPLOAD_LOGO("v1/logo/upload-logo", "UPLOAD LOGO"),
    STAFF_CREATE("v1/staffs/create-page", "CREATE STAFF"),
    POD_TRANSITION_CREATE("v1/transitions/create-page", "CREATE POD TRANSITION"),
    HIRING_VEHICLE_RATE_CREATE("v1/rates/hiringvehicle/create-page", "CREATE HIRING VEHICLE RATE"),
    POD_UPLOADS_VIEW("v1/cashreceipt/uploads-view-page", "POD UPLOADS"),
    PARCEL_TRACKING_VIEW("v1/parcel-tracking-view-page", "TRACK PARCEL"),
    REPORTS_SEARCH("v1/reports/search-page", "SEARCH REPORT");


    private final String page;
    private final String title;
}
