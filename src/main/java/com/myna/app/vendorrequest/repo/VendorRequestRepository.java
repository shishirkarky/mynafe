/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendorrequest.repo;

import com.myna.app.transitions.Transition;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.enums.BookingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VendorRequestRepository extends JpaRepository<VendorRequest, Integer> {
    @Query("SELECT b.refVendorId, count(b) as count FROM VendorRequest b GROUP BY b.refVendorId")
    List<Object[]> countAndGroupByVendor();

    @Query("SELECT b.refVendorId, count(b) as count FROM VendorRequest b WHERE b.branchCode=:branchCode GROUP BY b.refVendorId")
    List<Object[]> countAndGroupByVendor(String branchCode);

    List<VendorRequest> findAllByIdIn(List<Integer> ids);

    List<VendorRequest> findAllByBranchCode(String branchCode);

    List<VendorRequest> findAllByBookingType(BookingType bookingType);

    Optional<VendorRequest> findFirstByBookingCode(String bookingCode);

    Optional<VendorRequest> findFirstByCreatedByOrderByLastModifiedDateDesc(String createdBy);

    List<VendorRequest> findAllByRefDriverId(int driverId);

    List<VendorRequest> findAllByDispatchDate(String dispatchDate);

    List<VendorRequest> findAllByRefDriverIdAndDispatchDate(int driverId, String dispatchDate);

    List<VendorRequest> findAllByDispatchDateBetween(String from, String to);

    List<VendorRequest> findAllByBookingTypeAndDispatchDateBetween(BookingType bookingType, String from, String to);

    Optional<VendorRequest> findFirstByBookingCodeLikeOrderByIdDesc(String currentDate);

    Optional<VendorRequest> findFirstByTransitionsIn(List<Transition> transitions);

    List<VendorRequest> findAllByTransitionsIn(List<Transition> transitions);

    List<VendorRequest> findByRefDriverIdOrderByCreatedDateDesc(int refDriverId);

}
