package com.myna.app.vendorrequest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverTripDto {
    private String bookingCode;
    private double totalAmount;
    private double advancePayment;
    private double remainingAmount;
}
