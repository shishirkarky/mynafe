/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vendorrequest.dto;

import com.myna.app.dataconfig.locations.ConfigLocationsDto;
import com.myna.app.driver.dto.DriverDto;
import com.myna.app.enums.YesNoEnum;
import com.myna.app.transitions.Transition;
import com.myna.app.vehicle.dto.VehicleDto;
import com.myna.app.vehicle.enums.VehicleType;
import com.myna.app.vendor.dto.VendorDto;
import com.myna.app.vendorrequest.enums.BookingType;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class VendorRequestDto {
    private int id;
    private String bookingCode;
    private int refVendorId;
    private VendorDto vendor;
    private int refFromLocationId;
    private ConfigLocationsDto fromLocation;
    private int refToLocationId;
    private ConfigLocationsDto toLocation;
    private String dateOfRequest;
    private String dateOfRequestEn;
    private YesNoEnum vcts;
    private double rate;

    private String description;
    private String remarks;
    private int refDriverId;
    private DriverDto driver;
    private VehicleType vehicleType;
    private int refVehicleId;
    private VehicleDto vehicle;
    private double advancePaymentAmount;
    private double totalAmount;
    private double dueAmount;
    private String dispatchDate;
    private String dispatchDateEn;

    private double fuelAmount;
    private double dhalaAndPaltiExpenses;
    private double labourExpenses;
    private double maintainanceAmount;
    private double otherExpenses;
    private double currentKm;

    private String branchCode;
    private BookingType bookingType;
    private String createdBy;

    private Set<Transition> transitions;

    private boolean withTransition;
}
