/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendorrequest.assembler;

import com.myna.app.dataconfig.EntityConfigService;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.dataconfig.locations.ConfigLocationsDtoAssembler;
import com.myna.app.driver.assembler.DriverDtoAssembler;
import com.myna.app.driver.entity.Driver;
import com.myna.app.driver.service.DriverService;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.utils.dates.DateUtility;
import com.myna.app.vehicle.assembler.VehicleDtoAssembler;
import com.myna.app.vehicle.entity.Vehicle;
import com.myna.app.vehicle.service.VehicleService;
import com.myna.app.vendor.assembler.VendorDtoAssembler;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendor.service.VendorService;
import com.myna.app.vendorrequest.dto.DriverTripDto;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VendorRequestDtoAssembler {
  private VendorService vendorService;
  private DriverService driverService;
  private VehicleService vehicleService;
  private EntityConfigService entityConfigService;
  private VendorDtoAssembler vendorDtoAssembler;
  private DriverDtoAssembler driverDtoAssembler;
  private VehicleDtoAssembler vehicleDtoAssembler;
  private ConfigLocationsDtoAssembler configLocationsDtoAssembler;
  private DateUtility dateUtility;


  @Autowired
  public void setVendorService(VendorService vendorService) {
    this.vendorService = vendorService;
  }

  @Autowired
  public void setDriverService(DriverService driverService) {
    this.driverService = driverService;
  }

  @Autowired
  public void setVendorDtoAssembler(VendorDtoAssembler vendorDtoAssembler) {
    this.vendorDtoAssembler = vendorDtoAssembler;
  }

  @Autowired
  public void setDriverDtoAssembler(DriverDtoAssembler driverDtoAssembler) {
    this.driverDtoAssembler = driverDtoAssembler;
  }

  @Autowired
  public void setVehicleService(VehicleService vehicleService) {
    this.vehicleService = vehicleService;
  }

  @Autowired
  public void setVehicleDtoAssembler(VehicleDtoAssembler vehicleDtoAssembler) {
    this.vehicleDtoAssembler = vehicleDtoAssembler;
  }

  @Autowired
  public void setEntityConfigService(EntityConfigService entityConfigService) {
    this.entityConfigService = entityConfigService;
  }

  @Autowired
  public void setConfigLocationsDtoAssembler(ConfigLocationsDtoAssembler configLocationsDtoAssembler) {
    this.configLocationsDtoAssembler = configLocationsDtoAssembler;
  }

  @Autowired
  public void setDateUtility(DateUtility dateUtility) {
    this.dateUtility = dateUtility;
  }

  public VendorRequest toDomain(VendorRequestDto vendorRequestDto) {
    VendorRequest vendorRequest = new VendorRequest();
    CustomBeanUtils.copyNonNullProperties(vendorRequestDto, vendorRequest);
    vendorRequest.setDueAmount(vendorRequest.getTotalAmount() - vendorRequest.getAdvancePaymentAmount());

    vendorRequest.setDateOfRequestEn(dateUtility.nepaliToEnglish(vendorRequestDto.getDateOfRequest()));
    vendorRequest.setDispatchDate(vendorRequestDto.getDateOfRequest());
    vendorRequest.setDispatchDateEn(vendorRequest.getDateOfRequestEn());

    if (vendorRequestDto.getRefDriverId() > 0) {
      Driver driver = new Driver();
      driver.setId(vendorRequestDto.getRefDriverId());
      vendorRequest.setDriver(driver);
    }

    if (vendorRequestDto.getRefVehicleId() > 0) {
      Vehicle vehicle = new Vehicle();
      vehicle.setId(vendorRequestDto.getRefVehicleId());
      vendorRequest.setVehicle(vehicle);
    }

    if (vendorRequestDto.getRefVendorId() > 0) {
      Vendor vendor = new Vendor();
      vendor.setId(vendorRequestDto.getRefVendorId());
      vendorRequest.setVendor(vendor);
    }

    if (vendorRequestDto.getRefToLocationId() > 0) {
      ConfigLocations toLocation = new ConfigLocations();
      toLocation.setId(vendorRequestDto.getRefToLocationId());
      vendorRequest.setToLocation(toLocation);
    }

    if (vendorRequestDto.getRefFromLocationId() > 0) {
      ConfigLocations fromLocation = new ConfigLocations();
      fromLocation.setId(vendorRequestDto.getRefFromLocationId());
      vendorRequest.setFromLocation(fromLocation);
    }

    return vendorRequest;
  }

  public VendorRequestDto toModel(VendorRequest vendorRequest) {
    VendorRequestDto vendorRequestDto = new VendorRequestDto();
    CustomBeanUtils.copyNonNullProperties(vendorRequest, vendorRequestDto);

    if (null != vendorRequest.getVendor()) {
      vendorRequestDto.setVendor(vendorDtoAssembler.toModel(vendorRequest.getVendor()));
    }

    if (null != vendorRequest.getDriver()) {
      vendorRequestDto.setDriver(driverDtoAssembler.toModel(vendorRequest.getDriver()));
    }

    if (null != vendorRequest.getVehicle()) {
      vendorRequestDto.setVehicle(vehicleDtoAssembler.toModel(vendorRequest.getVehicle()));
    }

    if (null != vendorRequest.getFromLocation()) {
      vendorRequestDto.setFromLocation(configLocationsDtoAssembler.toModel(vendorRequest.getFromLocation()));
    }

    if (null != vendorRequest.getToLocation()) {
      vendorRequestDto.setToLocation(configLocationsDtoAssembler.toModel(vendorRequest.getToLocation()));
    }

    return vendorRequestDto;
  }

  public List<VendorRequestDto> toModels(List<VendorRequest> vendorRequestList) {
    List<VendorRequestDto> vendorRequestDtos = new ArrayList<>();
    vendorRequestList.forEach(vendorRequest -> vendorRequestDtos.add(this.toModel(vendorRequest)));
    return vendorRequestDtos;
  }

  public List<DriverTripDto> toDriverTripDtos(List<VendorRequest> vendorRequestList) {
    List<DriverTripDto> driverTripDtos = new ArrayList<>();

    vendorRequestList.forEach(vendorRequest -> {
      DriverTripDto driverTripDto = new DriverTripDto();
      driverTripDto.setBookingCode(vendorRequest.getBookingCode());
      driverTripDto.setAdvancePayment(vendorRequest.getAdvancePaymentAmount());
      driverTripDto.setRemainingAmount(vendorRequest.getDueAmount());
      driverTripDto.setTotalAmount(vendorRequest.getTotalAmount());

      driverTripDtos.add(driverTripDto);
    });

    return driverTripDtos;
  }
}
