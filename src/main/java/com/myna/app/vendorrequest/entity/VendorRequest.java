/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.vendorrequest.entity;

import com.myna.app.commons.Auditable;
import com.myna.app.dataconfig.locations.ConfigLocations;
import com.myna.app.driver.entity.Driver;
import com.myna.app.enums.YesNoEnum;
import com.myna.app.transitions.Transition;
import com.myna.app.vehicle.entity.Vehicle;
import com.myna.app.vehicle.enums.VehicleType;
import com.myna.app.vendor.entity.Vendor;
import com.myna.app.vendorrequest.enums.BookingType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * This Entity represents booking from vendor.
 */
@Getter
@Setter
@Entity
@Table(name = "bookings")
public class VendorRequest extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String bookingCode;
    private int refVendorId;
    @OneToOne(optional = true)
    private Vendor vendor;
    @Deprecated
    private int refFromLocationId;
    @OneToOne(optional = true)
    private ConfigLocations fromLocation;
    @Deprecated
    private int refToLocationId;
    @OneToOne(optional = true)
    private ConfigLocations toLocation;
    private String dateOfRequest;
    private String dateOfRequestEn;
    private String description;
    private String remarks;
    @Deprecated
    private int refDriverId;
    @OneToOne(optional = true)
    private Driver driver;
    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;
    @Deprecated
    private int refVehicleId;
    @OneToOne(optional = true)
    private Vehicle vehicle;
    private double advancePaymentAmount;
    private double totalAmount;
    private double dueAmount;
    private String dispatchDate;
    private String dispatchDateEn;
    @Enumerated(EnumType.STRING)
    private YesNoEnum vcts;
    private double rate;

    //EXPENSES
    private double fuelAmount;
    private double dhalaAndPaltiExpenses;
    private double labourExpenses;
    private double maintainanceAmount;
    private double otherExpenses;
    private double currentKm;

    private String branchCode;
    @Enumerated(EnumType.STRING)
    private BookingType bookingType;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "booking_transitions",
            joinColumns = @JoinColumn(name = "booking_id"),
            inverseJoinColumns = @JoinColumn(name = "transition_id"))
    private Set<Transition> transitions = new HashSet<>();
}
