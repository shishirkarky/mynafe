/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendorrequest.service;

import com.myna.app.transitions.Transition;
import com.myna.app.vehicleLogBook.VehicleLogBook;
import com.myna.app.vendorrequest.dto.VendorRequestCountDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.enums.BookingType;

import java.util.List;
import java.util.Optional;

public interface VendorRequestService {
    Optional<VendorRequest> saveVendorRequest(VendorRequest vendorRequest);

    Optional<VendorRequest> updateVendorRequest(int id, VendorRequest vendorRequest);

    List<VendorRequest> getAllVendorRequest();

    List<VendorRequest> getAllVendorRequestByBookingType(BookingType bookingType);

    List<VendorRequest> getAllVendorRequestByDriverId(int driverId);

    List<VendorRequest> getAllVendorRequestByDispatchDate(String dispatchDate);

    List<VendorRequest> getAllVendorRequestByDispatchDateBetween(String fromDispatchDate, String toDispatchDate);

    List<VendorRequest> getAllVendorRequestByBookingTypeAndDispatchDateBetween(BookingType bookingType, String fromDispatchDate, String toDispatchDate);

    List<VendorRequest> getAllVendorRequestByDriverAndDispatchDate(int driver, String dispatchDate);

    Optional<VendorRequest> getOneVendorRequest(int id);

    Optional<VendorRequest> getOneVendorRequestByBookingCode(String bookingCode);

    List<VendorRequestCountDto> getCountGroupByVendor(String branchCode);

    List<VendorRequest> getVendorRequestsByIdIn(List<Integer> ids);

    void updateDispatchDate(int id, String dispatchDate);

    Optional<VendorRequest> getLastUpdatedVendorRequestForCurrentUser();

    Optional<VendorRequest> getByTransition(Transition transition);

    List<VendorRequest> getByAssignedDriver(int refDriverId);

    List<VendorRequest> getCurrentUserDriverBookings();

  List<VendorRequest> findAllByTransitionsIn(List<Transition> transitions);

  Optional<VendorRequest> getByPodId(int podId);
}
