/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendorrequest.service;

import com.myna.app.auth.entity.User;
import com.myna.app.auth.enums.ERole;
import com.myna.app.auth.service.UserService;
import com.myna.app.pod.CashReceipt;
import com.myna.app.pod.PodService;
import com.myna.app.transitions.Transition;
import com.myna.app.utils.CodeGenerationUtils;
import com.myna.app.utils.CustomBeanUtils;
import com.myna.app.utils.dates.DateUtility;
import com.myna.app.vendorrequest.dto.VendorRequestCountDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.enums.BookingType;
import com.myna.app.vendorrequest.repo.VendorRequestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class VendorRequestServiceImpl implements VendorRequestService {
  private VendorRequestRepository vendorRequestRepository;
  private UserService userService;
  private PodService podService;

  @Autowired
  public void setVendorRequestRepository(VendorRequestRepository vendorRequestRepository) {
    this.vendorRequestRepository = vendorRequestRepository;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setPodService(PodService podService) {
    this.podService = podService;
  }

  @Override
  public Optional<VendorRequest> saveVendorRequest(VendorRequest vendorRequest) {
    vendorRequest.setBookingCode(this.generateBookingCode());
    return Optional.of(vendorRequestRepository.save(vendorRequest));
  }

  private String generateBookingCode() {
    String currentDate = DateUtility.getCurrentDate();
    String existingCode = "";

    Optional<VendorRequest> vendorRequestOptional = vendorRequestRepository.findFirstByBookingCodeLikeOrderByIdDesc(currentDate + "%");
    if (vendorRequestOptional.isPresent()) {
      VendorRequest vendorRequest = vendorRequestOptional.get();
      existingCode = vendorRequest.getBookingCode();
    }
    return CodeGenerationUtils.generateCode(existingCode, currentDate);
  }


  @Override
  public Optional<VendorRequest> updateVendorRequest(int id, VendorRequest vendorRequest) {
    Optional<VendorRequest> vendorRequestOptional = vendorRequestRepository.findById(id);
    if (vendorRequestOptional.isPresent()) {
      vendorRequest.setId(id);
      VendorRequest existingVendorRequest = vendorRequestOptional.get();
      CustomBeanUtils.copyNonNullAndNonZeroProperties(vendorRequest, existingVendorRequest);
      return Optional.of(vendorRequestRepository.save(existingVendorRequest));
    }
    return Optional.empty();
  }

  @Override
  public List<VendorRequest> getAllVendorRequest() {
    return this.getAllByCurrentUserRoles();
  }

  @Override
  public List<VendorRequest> getAllVendorRequestByBookingType(BookingType bookingType) {
    return vendorRequestRepository.findAllByBookingType(bookingType);
  }

  @Override
  public List<VendorRequest> getAllVendorRequestByDriverId(int driverId) {
    return vendorRequestRepository.findAllByRefDriverId(driverId);
  }

  @Override
  public List<VendorRequest> getAllVendorRequestByDispatchDate(String dispatchDate) {
    return vendorRequestRepository.findAllByDispatchDate(dispatchDate);
  }

  @Override
  public List<VendorRequest> getAllVendorRequestByDispatchDateBetween(String fromDispatchDate, String toDispatchDate) {
    return vendorRequestRepository.findAllByDispatchDateBetween(fromDispatchDate, toDispatchDate);
  }

  @Override
  public List<VendorRequest> getAllVendorRequestByBookingTypeAndDispatchDateBetween(BookingType bookingType, String fromDispatchDate, String toDispatchDate) {
    return vendorRequestRepository.findAllByBookingTypeAndDispatchDateBetween(bookingType, fromDispatchDate, toDispatchDate);
  }

  @Override
  public List<VendorRequest> getAllVendorRequestByDriverAndDispatchDate(int driver, String dispatchDate) {
    return vendorRequestRepository.findAllByRefDriverIdAndDispatchDate(driver, dispatchDate);
  }

  private List<VendorRequest> getAllByCurrentUserRoles() {
    List<String> rolesOptional = userService.getCurrentUserRoles();
    if (!rolesOptional.isEmpty() && rolesOptional.contains(String.valueOf(ERole.ROLE_SUPER_ADMIN))) {
      return vendorRequestRepository.findAll();
    } else {
      Optional<String> branchCode = userService.getTokenUserBranchCode();
      if (branchCode.isPresent()) {
        return vendorRequestRepository.findAllByBranchCode(branchCode.get());
      }
    }
    return new ArrayList<>();
  }

  @Override
  public Optional<VendorRequest> getOneVendorRequest(int id) {
    return vendorRequestRepository.findById(id);
  }

  @Override
  public Optional<VendorRequest> getOneVendorRequestByBookingCode(String bookingCode) {
    return vendorRequestRepository.findFirstByBookingCode(bookingCode);
  }

  @Override
  public List<VendorRequestCountDto> getCountGroupByVendor(String branchCode) {
    List<VendorRequestCountDto> bookingCounts = new ArrayList<>();
    List<Object[]> counts;

    if (null == branchCode || StringUtils.isEmpty(branchCode)) {
      counts = vendorRequestRepository.countAndGroupByVendor();
    } else {
      counts = vendorRequestRepository.countAndGroupByVendor(branchCode);
    }

    counts.forEach(objects -> {
      VendorRequestCountDto bookingCount = new VendorRequestCountDto();
      bookingCount.setRefVendorId((Integer) objects[0]);
      bookingCount.setCount((Long) objects[1]);
      bookingCounts.add(bookingCount);
    });
    return bookingCounts;
  }

  @Override
  public List<VendorRequest> getVendorRequestsByIdIn(List<Integer> ids) {
    return vendorRequestRepository.findAllByIdIn(ids);
  }

  @Override
  public void updateDispatchDate(int id, String dispatchDate) {
    Optional<VendorRequest> vendorRequestOptional = this.getOneVendorRequest(id);
    if (vendorRequestOptional.isPresent()) {
      VendorRequest vendorRequest = vendorRequestOptional.get();
      vendorRequest.setDispatchDate(dispatchDate);

      vendorRequestRepository.save(vendorRequest);
    }
  }

  @Override
  public Optional<VendorRequest> getLastUpdatedVendorRequestForCurrentUser() {
    Optional<User> userOptional = userService.getCurrentUserFromToken();
    if (userOptional.isPresent()) {
      return vendorRequestRepository.findFirstByCreatedByOrderByLastModifiedDateDesc(userOptional.get().getUsername());
    }
    return Optional.empty();
  }

  @Override
  public Optional<VendorRequest> getByTransition(Transition transition) {
    return vendorRequestRepository.findFirstByTransitionsIn(Collections.singletonList(transition));
  }

  @Override
  public List<VendorRequest> getByAssignedDriver(int refDriverId) {
    return vendorRequestRepository.findByRefDriverIdOrderByCreatedDateDesc(refDriverId);
  }

  @Override
  public List<VendorRequest> getCurrentUserDriverBookings() {
    Optional<User> userOptional = userService.getCurrentUserFromToken();
    if (userOptional.isPresent()) {
      User currentUser = userOptional.get();
      if (null != currentUser.getDriver()) {
        return this.getByAssignedDriver(currentUser.getDriver().getId());
      }
    }
    return new ArrayList<>();
  }

  @Override
  public List<VendorRequest> findAllByTransitionsIn(List<Transition> transitions) {
    return vendorRequestRepository.findAllByTransitionsIn(transitions);
  }

  @Override
  public Optional<VendorRequest> getByPodId(int podId) {
    Optional<CashReceipt> podOptional = podService.getOne(podId);
    if(podOptional.isPresent()){
      int bookingId = podOptional.get().getRefVendorRequestId();
      return vendorRequestRepository.findById(bookingId);
    }
    return Optional.empty();
  }
}
