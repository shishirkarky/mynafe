/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendorrequest.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.auth.service.UserService;
import com.myna.app.pod.CashReceipt;
import com.myna.app.pod.PodService;
import com.myna.app.rates.RatesComponent;
import com.myna.app.utils.ValidationUtil;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.dto.VendorRequestDto;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.enums.BookingType;
import com.myna.app.vendorrequest.service.VendorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/impl/api/v1")
public class VendorRequestController {
    private VendorRequestService vendorRequestService;
    private VendorRequestDtoAssembler vendorRequestDtoAssembler;
    private UserService userService;
    private RatesComponent ratesComponent;
    private ValidationUtil validationUtil;

    @Autowired
    public void setVendorRequestService(VendorRequestService vendorRequestService) {
        this.vendorRequestService = vendorRequestService;
    }

    @Autowired
    public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
        this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRatesComponent(RatesComponent ratesComponent) {
        this.ratesComponent = ratesComponent;
    }

    @Autowired
  public void setValidationUtil(ValidationUtil validationUtil) {
    this.validationUtil = validationUtil;
  }

  @PostMapping("/vendors/requests")
    public ResponseEntity<?> saveVendorRequest(@RequestBody VendorRequestDto vendorRequestDto) {
        Optional<String> branchCode = userService.getTokenUserBranchCode();
        branchCode.ifPresent(vendorRequestDto::setBranchCode);

        if(BookingType.FULL_BOOKING.equals(vendorRequestDto.getBookingType())) {
            double rate = ratesComponent.getFullBookingRate(
                    vendorRequestDto.getRefVendorId(),
                    vendorRequestDto.getRefFromLocationId(),
                    vendorRequestDto.getRefToLocationId());
            vendorRequestDto.setRate(rate);
        }

        Optional<VendorRequest> vendorRequestOptional = vendorRequestService.saveVendorRequest(vendorRequestDtoAssembler.toDomain(vendorRequestDto));
        if (vendorRequestOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse("Booking created",
                    vendorRequestDtoAssembler.toModel(vendorRequestOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/vendors/requests/{id}")
    public ResponseEntity<?> updateVendorRequest(@PathVariable int id, @RequestBody VendorRequestDto vendorRequestDto) {
      if(vendorRequestDto.isWithTransition()){
        VendorRequestDto newBookingRequest = new VendorRequestDto();
        newBookingRequest.setTransitions(vendorRequestDto.getTransitions());
        vendorRequestDto = newBookingRequest;
      }else {
        if (!validationUtil.validateBookingUpdate(id)) {
          return new ApiResponse().getFailureResponse("Unauthorized!");
        }
      }

      if(BookingType.FULL_BOOKING.equals(vendorRequestDto.getBookingType())) {
            double rate = ratesComponent.getFullBookingRate(
                    vendorRequestDto.getRefVendorId(),
                    vendorRequestDto.getRefFromLocationId(),
                    vendorRequestDto.getRefToLocationId());
            vendorRequestDto.setRate(rate);
        }

        Optional<VendorRequest> vendorRequestOptional = vendorRequestService.updateVendorRequest(id,
                vendorRequestDtoAssembler.toDomain(vendorRequestDto));
        if (vendorRequestOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse("Booking updated",
                    vendorRequestDtoAssembler.toModel(vendorRequestOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/vendors/requests")
    public ResponseEntity<?> getAllVendorRequests() {
        return new ApiResponse().getSuccessResponse(null,
                vendorRequestDtoAssembler.toModels(vendorRequestService.getAllVendorRequest()));
    }

    @GetMapping("/vendors/requests/{id}")
    public ResponseEntity<?> getOneVendorRequest(@PathVariable int id) {
        Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getOneVendorRequest(id);
        if (vendorRequestOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null,
                    vendorRequestDtoAssembler.toModel(vendorRequestOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/vendors/requests/drivers")
    public ResponseEntity<?> getCurrentUserDriverBookings(){
        return new ApiResponse().getSuccessResponse(null,vendorRequestDtoAssembler.toDriverTripDtos(vendorRequestService.getCurrentUserDriverBookings()));
    }
}
