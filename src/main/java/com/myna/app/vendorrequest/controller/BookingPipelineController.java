/**
 * Description: These APIs processes actions on last booking/vendor-request created by the session user
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.vendorrequest.controller;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.pod.PodService;
import com.myna.app.vendorrequest.assembler.VendorRequestDtoAssembler;
import com.myna.app.vendorrequest.entity.VendorRequest;
import com.myna.app.vendorrequest.service.VendorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("impl/api/v1")
public class BookingPipelineController {
    private VendorRequestService vendorRequestService;
    private VendorRequestDtoAssembler vendorRequestDtoAssembler;
    private PodService podService;

    @Autowired
    public void setVendorRequestService(VendorRequestService vendorRequestService) {
        this.vendorRequestService = vendorRequestService;
    }

    @Autowired
    public void setVendorRequestDtoAssembler(VendorRequestDtoAssembler vendorRequestDtoAssembler) {
        this.vendorRequestDtoAssembler = vendorRequestDtoAssembler;
    }

    @Autowired
    public void setPodService(PodService podService) {
        this.podService = podService;
    }

    /**
     * <p>
     * This API updates existing booking ({@link VendorRequest} if id > 0 in request body.
     * Otherwise, creates new booking
     * </p>
     *
     * @return
     */
    @GetMapping("bookings/recents")
    public ResponseEntity<?> getLastCreatedBookingForUser() {
        Optional<VendorRequest> vendorRequestOptional = vendorRequestService.getLastUpdatedVendorRequestForCurrentUser();
        if (vendorRequestOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse("Booking Updated", vendorRequestDtoAssembler.toModel(vendorRequestOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("bookings/{bookingId}/pods/counts")
    public ResponseEntity<?> getPodCountsByBookingId(@PathVariable int bookingId) {
        int count = podService.countAllByBookingId(bookingId);
        return new ApiResponse().getSuccessResponse(null, count);
    }
}
