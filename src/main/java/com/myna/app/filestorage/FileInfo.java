/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.filestorage;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileInfo {
    private String name;
    private String url;
    private String type;
    private String subType;
    private int refId;

    public FileInfo(String name, String url) {
        this.name = name;
        this.url = url;
    }
}
