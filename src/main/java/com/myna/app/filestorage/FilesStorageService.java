/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * Facet Technology Pvt. Limited.
 */
package com.myna.app.filestorage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public interface FilesStorageService {
  void init();

  void save(MultipartFile file, String fileName);

  Resource load(String filename);

  void deleteAll();

  Stream<Path> loadAll();

  Optional<Documents> saveDocuments(Documents documents);

  Optional<Documents> createDocuments(MultipartFile file, FileInfo fileInfo);

  List<Documents> findDocuments(String type, int refId);

  List<Documents> findDocuments(String type, List<Integer> refIds);

  List<Documents> findDocuments(String type);

  Optional<Documents> findDocuments(String type, String subType, int refId);

  List<Documents> findByCreatedBy(String createdBy);
}
