/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.filestorage;

import com.myna.app.apiresponse.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class FileUploadExceptionAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<?> handleMaxSizeException(MaxUploadSizeExceededException exc) {
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ApiResponse().getFailureResponse("File too large!"));
    }
}
