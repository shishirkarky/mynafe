/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.filestorage.documents;

import com.myna.app.apiresponse.ApiResponse;
import com.myna.app.auth.entity.User;
import com.myna.app.auth.service.UserService;
import com.myna.app.filestorage.FilesStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class DocumentsController {
    private FilesStorageService filesStorageService;
    private UserService userService;

    @Autowired
    public void setFilesStorageService(FilesStorageService filesStorageService) {
        this.filesStorageService = filesStorageService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("documents")
    public ResponseEntity<?> getAllDocuments(){
        Optional<User> userOptional = userService.getCurrentUserFromToken();
        if(userOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, filesStorageService.findByCreatedBy(userOptional.get().getUsername()));
        }else{
            return new ApiResponse().getFailureResponse(null);
        }
    }
}
