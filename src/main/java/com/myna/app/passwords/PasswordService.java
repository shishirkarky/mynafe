/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.passwords;

public interface PasswordService {
    boolean matchPasswords(String password, String confirmPassword);

    boolean verifyCurrentUserOldPassword(String oldPassword);

    boolean updateCurrentUserPassword(String password);
}
