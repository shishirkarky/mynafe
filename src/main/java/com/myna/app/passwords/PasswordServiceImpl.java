/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.passwords;

import com.myna.app.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordService {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean matchPasswords(String newPassword, String newPasswordRepeat) {
        return newPassword.equals(newPasswordRepeat);
    }

    @Override
    public boolean verifyCurrentUserOldPassword(String oldPassword) {
        return userService.validateCurrentUserPassword(oldPassword);
    }

    @Override
    public boolean updateCurrentUserPassword(String newPassword) {
        return userService.updateCurrentUserPassword(newPassword);
    }
}
