/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* Facet Technology Pvt. Limited.
*
*/
package com.myna.app.passwords;

import com.myna.app.apiresponse.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("impl/api/v1")
public class PasswordController {
    private PasswordService passwordService;

    @Autowired
    public void setPasswordService(PasswordService passwordService) {
        this.passwordService = passwordService;
    }

    @PutMapping("passwords")
    public ResponseEntity<?> changeCurrentUserPassword(@ModelAttribute ChangePasswordDto changePasswordDto) {
        if (!passwordService.matchPasswords(changePasswordDto.getNewPassword(), changePasswordDto.getNewPasswordRepeat())) {
            return new ApiResponse().getFailureResponse("Password mismatch.");
        } else if (!passwordService.verifyCurrentUserOldPassword(changePasswordDto.getOldPassword())) {
            return new ApiResponse().getFailureResponse("Invalid old password.");
        }
        boolean updateStatus = passwordService.updateCurrentUserPassword(changePasswordDto.getNewPassword());
        if (updateStatus) {
            return new ApiResponse().getSuccessResponse("Password updated.", null);
        } else {
            return new ApiResponse().getFailureResponse("Password update failure.");
        }
    }
}
