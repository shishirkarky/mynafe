import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";
import {DateUtils} from "./c-date-utils.js";
/*
* EVENT FOR SAVE BUTTON
 */
document.addEventListener('DOMContentLoaded', () => {
    loadSubVendorInPodSelect().then(r => console.log("Sub-Vendors loaded successfully."));
    loadLocationsInSelect().then(r => console.log("Locations loaded successfully."));
    loadConfigPackagesInSelect().then(r => console.log("Config Packages loaded successfully."));

    let id = $("#id").val();
    setTimeout(function () {
        loadPodData(id).then(r => console.log("POD data loaded successfully."))
    }, 200);
});
/*
* VENDOR IN SELECT
 */
let loadSubVendorInPodSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_SUB_VENDORS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#consignorName").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/*
* FILL POD DETAILS
 */
let loadPodData = async (id) => {
    // setTimeout(function () {}, 5000);
    let url = Constants.API_CASH_RECEIPT_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        loadPodOnly(result.data);
        loadPodDetails(result.data);
    } else {
        new CToastNotification().getFailureToastNotification("POD data not found.");
    }
}

let loadPodOnly = (data) => {
    const {
        dn,
        consignorName,
        vendor,
        consigneeName,
        customerBillNumber,
        refFromLocationId,
        refToLocationId,
        consignorVat,
        consigneeVat,
        refVendorRequestId,
        consigneeContact,
        customerBillDate
    } = data;

    $("[name='refVendorRequestId']").val(refVendorRequestId).attr("selected", "selected").change();
    $("[name='consignorVat']").val(consignorVat);
    $("[name='customerBillNumber']").val(customerBillNumber);

    if (null != vendor) {
        $("#consignorName").select2("destroy");
        $("#consignorName").val(vendor.id);
        $("#consignorName").select2();
    }

    $("[name='consigneeName']").val(consigneeName);
    $("[name='consigneeVat']").val(consigneeVat);
    $("[name='refFromLocationId']").val(refFromLocationId).attr("selected", "selected").change();
    $("[name='refToLocationId']").val(refToLocationId).attr("selected", "selected").change();
    $("#consigneeContact").val(consigneeContact);
    $("#customerBillDate").val(customerBillDate);
    $("#dn").html(dn);
}

let loadPodDetails = (data) => {
    if (null != data.cashReceiptDetails) {
        data.cashReceiptDetails.forEach(function (value, index) {
            $(".cashReceiptDetailsDiv:first .cashReceiptDetailsDescription").select2("destroy");
            if (index === 0) {
                $("#cashReceiptDetailsDescription").val(value.description).attr("selected", "selected").change();
                $("#quantity").val(value.quantity);
                $("#unitOfMeasurement").val(value.unitOfMeasurement).attr("selected", "selected").change();
                $("#noOfPackage").val(value.noOfPackage);
                $("#taxableAmount").val(value.taxableAmount);
                $("#remarks").val(value.remarks);
            } else {
                let cloneDiv = $(".cashReceiptDetailsDiv").eq(0).clone();

                cloneDiv.find("#cashReceiptDetailsDescription").val(value.description).attr("selected", "selected").change().show();
                cloneDiv.find("#cashReceiptDetailsDescription").select2();

                cloneDiv.find("#quantity").val(value.quantity).show();
                cloneDiv.find("#unitOfMeasurement").val(value.unitOfMeasurement).show();
                cloneDiv.find("#noOfPackage").val(value.noOfPackage).show();
                cloneDiv.find("#taxableAmount").val(value.taxableAmount).show();
                cloneDiv.find("#remarks").val(value.remarks).show();
                $(cloneDiv).insertAfter(".cashReceiptDetailsDiv:last");
            }
            $(".cashReceiptDetailsDiv:first .cashReceiptDetailsDescription").select2();
        });
    } else {
        $("#cashReceiptDetailsDescription").val("").attr("selected", "selected").change();
        $("#quantity").val("");
        $("#unitOfMeasurement").val("").attr("selected", "selected").change();
        $("#noOfPackage").val("");
        $("#taxableAmount").val("");
        $("#remarks").val("");
    }
}
/*
* FILLING SELECT OPTIONS
 */

/*
LOCATION IN SELECT
 */
let loadLocationsInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let data = result.data;
        loadLocationInSelect(data).then(r => console.log("Location loaded successfully."));
    }
}
let loadLocationInSelect = async (data) => {
    let len = data.length;
    for (let i = 0; i < len; i++) {
        if (null != data[i].district) {
            let id = data[i].id;
            let district = data[i].district;
            let city = data[i].city;

            $("#refFromLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
            $("#refToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
        }
    }
}

/*
PACKAGES IN DESCRIPTION SELECT
 */
let loadConfigPackagesInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].brand + " - " + result.data[i].name

                $("#cashReceiptDetailsDescription").append("<option value='" + id + "'>" + name + "</option>");
            }
        }

        $(".cashReceiptDetailsDiv:last .cashReceiptDetailsDescription").select2();
    }
}

/*
UPDATE FUNCTION
 */
let sendCashReceiptUpdateReq = () => {
    let crudUtils = new CrudUtils();
    let id = $("#id").val();
    crudUtils.sendPutRequest(Constants.API_CASH_RECEIPT_UPDATE.replace("{id}", id), getCashReceiptFormData());
};

let getCashReceiptFormData = () => {
    let cashReceipt = {
        "consignorVat": $("[name='consignorVat']").val(),
        "customerBillNumber": $("[name='customerBillNumber']").val(),
        "refFromLocationId": $("[name='refFromLocationId']").val(),
        "consigneeVat": $("[name='consigneeVat']").val(),
        "consigneeName": $("[name='consigneeName']").val(),
        "refToLocationId": $("[name='refToLocationId']").val(),
        "refVendorRequestId": $("#refVendorRequestId").val(),
        "consignorName": $("#consignorName option:selected").text(),
        "consigneeContact": $("#consigneeContact").val(),
        "customerBillDate": $("#customerBillDate").val()
    };
    if (null !== $("#consignorName option:selected").val() && '' !== $("#consignorName option:selected").val()) {
        cashReceipt['vendor'] = {
            id: $("#consignorName option:selected").val()
        };
    }
    let cashReceiptDetails = [];

    $('.cashReceiptDetailsDiv').each(function () {
        let description = $(this).find($("[name='cashReceiptDetailsDescription']")).val();
        let quantity = $(this).find($("[name='quantity']")).val();
        let unitOfMeasurement = $(this).find($("[name='unitOfMeasurement']")).val();
        let noOfPackage = $(this).find($("[name='noOfPackage']")).val();
        let taxableAmount = $(this).find($("[name='taxableAmount']")).val();
        let remarks = $(this).find($("[name='remarks']")).val();

        let cashReceiptDetailsJson = {
            description,
            quantity,
            unitOfMeasurement,
            noOfPackage,
            taxableAmount,
            remarks
        };

        cashReceiptDetails.push(cashReceiptDetailsJson);
    });

    cashReceipt['cashReceiptDetails'] = cashReceiptDetails;

    return JSON.stringify(cashReceipt);
}

/*
CLONE CASH RECEIPT DETAILS
 */
let cloneCashReceiptDetailReq = () => {
    $(".cashReceiptDetailsDiv:first .cashReceiptDetailsDescription").select2("destroy");

    $(".cashReceiptDetailsDiv").eq(0)
        .clone()
        .find("input").val("").end()
        .show()
        .insertAfter(".cashReceiptDetailsDiv:last");

    $(".cashReceiptDetailsDiv:last .cashReceiptDetailsDescription").select2("destroy");
    $(".cashReceiptDetailsDiv:last .cashReceiptDetailsDescription").select2();
    $(".cashReceiptDetailsDiv:first .cashReceiptDetailsDescription").select2();
}

let removeCashReceiptDetailReq = () => {
    $(".cashReceiptDetailsDiv:last").remove();
}

/*
DATE CONVERTER
 */
let nepaliToEnglish = (inputId, successId) => {
    new DateUtils().englishToNepali(inputId, successId);
};

let englishToNepali = (inputId, successId) => {
    new DateUtils().englishToNepali(inputId, successId);
};

let consignorNameOnChangeAction = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ONE
        .replace("{vendorId}", $("#consignorName").val()));
    if (result.status === 200) {
        $("#consignorVat").val(result.data.panVat);
    }

}

window.englishToNepali = englishToNepali;
window.nepaliToEnglish = nepaliToEnglish;
window.removeCashReceiptDetailReq = removeCashReceiptDetailReq;
window.cloneCashReceiptDetailReq = cloneCashReceiptDetailReq;
window.sendCashReceiptUpdateReq = sendCashReceiptUpdateReq;
window.consignorNameOnChangeAction = consignorNameOnChangeAction;