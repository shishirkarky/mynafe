import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadCustomers().then(r => console.log("Data loaded successfully."));

    $("#editCustomerModal").on("hidden.bs.modal", function () {
        loadCustomers().then(r => console.log("Data loaded successfully."));
    });
});

// LOAD DATA IN DATATABLE
let loadCustomers = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_CUSTOMER_GET_ALL);
    let customerTable = $('#customerDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (customerTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = customerTable.rows({selected: true}).data()[0];
                        loadCustomerDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "firstName"},
            {data: "middleName"},
            {data: "lastName"},
            {data: "address"},
            {data: "phone"},
            {data: "panNumber"},
            {data: "receiverName"},
            {data: "signature"},
            {data: "email"}
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadCustomerDetailModal = async (id) => {
    let url = Constants.API_CUSTOMER_GET_ONE.replace("{customerId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, firstName, middleName, lastName, licenseNumber, address, phone, panNumber, receiverName, signature, email} = result.data;
        $("[name='firstName']").val(firstName);
        $("[name='middleName']").val(middleName);
        $("[name='lastName']").val(lastName);
        $("[name='licenseNumber']").val(licenseNumber);
        $("[name='address']").val(address);
        $("[name='phone']").val(phone);
        $("[name='panNumber']").val(panNumber);
        $("[name='receiverName']").val(receiverName);
        $("[name='signature']").val(signature);
        $("[name='email']").val(email);
        $("#updateCustomerBtn").attr("onClick", "updateCustomer(" + id + ")");
        // $("#deleteCustomerBtn").attr("onClick", "deleteCustomer(" + id + ")");
        $("#editCustomerModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getCustomerJsonFromModal = () => {
    return JSON.stringify({
        "firstName": $("[name='firstName']").val(),
        "middleName": $("[name='middleName']").val(),
        "lastName": $("[name='lastName']").val(),
        "licenseNumber": $("[name='licenseNumber']").val(),
        "address": $("[name='address']").val(),
        "phone": $("[name='phone']").val(),
        "panNumber": $("[name='panNumber']").val(),
        "receiverName": $("[name='receiverName']").val(),
        "signature": $("[name='signature']").val(),
        "email": $("[name='email']").val()
    });
}

let updateCustomer = (id) => new CrudUtils().sendPutRequest(Constants.API_CUSTOMER_UPDATE.replace("{customerId}", id), getCustomerJsonFromModal());

// DELETE ONE DATA
// let deleteCustomer = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_CUSTOMER_DELETE.replace("{customerId}",id));
//     $("#editCustomerModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadCustomerDetailModal = loadCustomerDetailModal;
window.updateCustomer = updateCustomer;
// window.deleteCustomer = deleteCustomer;



