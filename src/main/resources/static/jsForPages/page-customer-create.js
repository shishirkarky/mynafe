import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveCustomerBtn').addEventListener('click', sendCustomerSaveReq);
    loadCustomers().then(r => console.log("customers loaded"));
});

let sendCustomerSaveReq = (ev) => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_CUSTOMER_SAVE, getCustomerFormData());

    setTimeout(function () {
        loadCustomers().then(r => console.log("customers loaded"));
    },150);
};

let getCustomerFormData = () => {
    return JSON.stringify({
        "licenseNumber": $("[name='licenseNumber']").val(),
        "address": $("[name='address']").val(),
        "phone": $("[name='phone']").val(),
        "panNumber": $("[name='panNumber']").val(),
        "receiverName": $("[name='receiverName']").val(),
        "signature": $("[name='signature']").val(),
        "email": $("[name='email']").val()
    });
}

let loadCustomers = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_CUSTOMER_GET_ALL);
    let customerTable = $('#customerDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: "excel",
                title: "staffs"
            },
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (customerTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = customerTable.rows({selected: true}).data()[0];
                        loadCustomerDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "receiverName"},
            {data: "address"},
            {data: "phone"},
            {data: "panNumber"},
            {data: "signature"},
            {data: "email"}
        ],
        "destroy": true
    });
}

let loadCustomerDetailModal = async (id) => {
    let url = Constants.API_CUSTOMER_GET_ONE.replace("{customerId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, licenseNumber, address, phone, panNumber, receiverName, signature, email} = result.data;
        $("[name='elicenseNumber']").val(licenseNumber);
        $("[name='eaddress']").val(address);
        $("[name='ephone']").val(phone);
        $("[name='epanNumber']").val(panNumber);
        $("[name='ereceiverName']").val(receiverName);
        $("[name='esignature']").val(signature);
        $("[name='eemail']").val(email);
        $("#updateCustomerBtn").attr("onClick", "updateCustomer(" + id + ")");
        $("#editCustomerModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

let getCustomerJsonFromModal = () => {
    return JSON.stringify({
        "licenseNumber": $("[name='elicenseNumber']").val(),
        "address": $("[name='eaddress']").val(),
        "phone": $("[name='ephone']").val(),
        "panNumber": $("[name='epanNumber']").val(),
        "receiverName": $("[name='ereceiverName']").val(),
        "signature": $("[name='esignature']").val(),
        "email": $("[name='eemail']").val()
    });
}

let updateCustomer = (id) => {
    new CrudUtils().sendPutRequest(Constants.API_CUSTOMER_UPDATE.replace("{customerId}", id), getCustomerJsonFromModal());
    setTimeout(function () {
        loadCustomers().then(r => console.log("customers loaded"));
    },150)
};

window.updateCustomer = updateCustomer;