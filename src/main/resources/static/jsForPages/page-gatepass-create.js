import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {DateUtils} from "./c-date-utils.js";

$(document).ready(function () {
    loadDriverInSelect().then(r => console.log("Drivers loaded successfully."));
    loadVehicleInSelect().then(r => console.log("Vehicles loaded successfully."));
    loadVendorInSelect().then(r => console.log("Vehicles loaded successfully."));
});

/*
* FILLING SELECT OPTIONS
 */
let loadVendorInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
let loadDriverInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].firstName) {
                let id = result.data[i].id;
                let name = result.data[i].firstName + " " + result.data[i].lastName;

                $("#refDriverId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let loadVehicleInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].registrationNumber) {
                let id = result.data[i].id;
                let name = result.data[i].registrationNumber;

                $("#refVehicleId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}


/*
* EVENT FOR SAVE BUTTON
 */
document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveGatepassBtn').addEventListener('click', sendGatePassSaveReq);
    document.getElementById('addGatePassDetailsBtn').addEventListener('click', cloneGatePassDetailReq);
    document.getElementById('removeGatePassDetailsBtn').addEventListener('click', removeGatePassDetailReq);
});

let sendGatePassSaveReq = (ev) => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_GATE_PASS_SAVE, getGatePassFormData());
};

let getGatePassFormData = () => {
    let gatePass = {
        "refVendorId": $("[name='refVendorId']").val(),
        "refDriverId": $("[name='refDriverId']").val(),
        "refVehicleId": $("[name='refVehicleId']").val(),
        "date": $("[name='date']").val(),
        "dateEn": $("[name='dateEn']").val(),
        "preparedBy": $("[name='preparedBy']").val(),
        "receivedBy": $("[name='receivedBy']").val(),
        "loadedBy": $("[name='loadedBy']").val(),
        "approvedBy": $("[name='approvedBy']").val()
    };

    let gatePassDetails = [];

    $('.gatePassDetailsDiv').each(function () {
        let invoiceNumbers = $(this).find($("[name='invoiceNumbers']")).val();
        let deliveryDestination = $(this).find($("[name='deliveryDestination']")).val();
        let noOfPackages = $(this).find($("[name='noOfPackages']")).val();
        let remarks = $(this).find($("[name='remarks']")).val();

        let gatePassDetailsJson = {
            invoiceNumbers,
            deliveryDestination,
            noOfPackages,
            remarks
        };

        gatePassDetails.push(gatePassDetailsJson);
    });

    gatePass['gatePassDetails'] = gatePassDetails;
    console.log(gatePass);

    return JSON.stringify(gatePass);
}

// GATE PASS DETAILS ADD AND REMOVE
let cloneGatePassDetailReq = () => {
    $(".gatePassDetailsDiv").eq(0)
        .clone()
        .find("input").val("").end() // ***
        .show()
        .insertAfter(".gatePassDetailsDiv:last");
}

let removeGatePassDetailReq = () => {
    $(".gatePassDetailsDiv:last").remove();
}

/*
DATE CONVERTER
 */
let nepaliToEnglish = (inputId, successId) =>{
    new DateUtils().nepaliToEnglish(inputId, successId);
};

let englishToNepali = (inputId, successId) =>{
    new DateUtils().englishToNepali(inputId, successId);
};

window.englishToNepali = englishToNepali;
window.nepaliToEnglish = nepaliToEnglish;