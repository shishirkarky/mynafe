import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";
import {DateUtils} from "./c-date-utils.js";


$(document).ready(function () {
    loadVendorRequests().then(r => console.log("Data loaded successfully."));

    // loadDriverInSelect().then(r => console.log("Drivers loaded successfully."));
    // loadVendorInSelect().then(r => console.log("Vehicles loaded successfully."));
    // loadVehicleInSelect().then(r => console.log("Vehicles loaded successfully."));
    // loadLocationsInSelect().then(r => console.log("Locations loaded successfully."));

    // $("#editVendorRequestModal").on("hidden.bs.modal", function () {
    //     loadVendorRequests().then(r => console.log("Vendors loaded successfully."));
    // });
});

/*
* FILLING SELECT OPTIONS
 */
let loadDriverInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].firstName) {
                let id = result.data[i].id;
                let name = result.data[i].firstName + " " + result.data[i].lastName;

                $("#refDriverId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/*
VEHICLE IN SELECT
 */
let loadVehicleInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].registrationNumber) {
                let id = result.data[i].id;
                let name = result.data[i].registrationNumber;

                $("#refVehicleId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/*
* VENDOR IN SELECT
 */
let loadVendorInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/*
* LOCATION IN FROM AND TO SELECT
 */
let loadLocationsInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let data = result.data;
        loadLocationInSelect(data).then(r => console.log("Location loaded successfully."));
    }
}
let loadLocationInSelect = async (data) => {
    let len = data.length;
    for (let i = 0; i < len; i++) {
        if (null != data[i].district) {
            let id = data[i].id;
            let district = data[i].district;
            let city = data[i].city;

            $("#refFromLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
            $("#refToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
        }
    }
}
// LOAD DATA IN DATATABLE
let loadVendorRequests = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ALL);
    let dataTable = $('#vendorRequestDataTable').DataTable({
        select: {
            style: 'os'
        },
        searchBuilder: {
            columns: [0, 1, 2, 3, 4,5, 6, 9]
        },
        dom: 'Brtip',
        buttons: [{
            extend: "excel",
            title: "booking"
        }],
        scrollX: true,
        "data": resource.data,
        "columns": [
            {data: "bookingType", defaultContent: "", searchBuilderType: 'string'},
            {data: "bookingCode", defaultContent: "", searchBuilderType: 'string'},
            {data: "vendor.name", defaultContent: ""},
            {
                "data": "DriverName",
                "render": function (data, type, row, meta) {
                    if (null != row.driver) {
                        return row.driver.firstName + " " + row.driver.middleName + " " + row.driver.lastName;
                    } else {
                        return "";
                    }
                },
                defaultContent: ""
            },
            {data: "vehicle.registrationNumber", defaultContent: ""},
            {data: "vehicle.type", defaultContent: ""},
            {data: "dateOfRequest", defaultContent: "", searchBuilderType: 'string'},
            {data: "branchCode", defaultContent: "", searchBuilderType: 'string'},
            {data: "dispatchDate", defaultContent: "", searchBuilderType: 'string'},
            {
                "data": "dispatchLocation",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    if (null != row.fromLocation) {
                        return row.fromLocation.city + ", " + row.fromLocation.district
                    } else {
                        return "";
                    }
                }
            },
            {data: "totalAmount", defaultContent: ""},
            {data: "advancePaymentAmount", defaultContent: ""},
            {data: "dueAmount", defaultContent: ""},
            {data: "dhalaAndPaltiExpenses", defaultContent: ""},
            {data: "labourExpenses", defaultContent: ""},
            {data: "fuelAmount", defaultContent: ""},
            {data: "maintainanceAmount", defaultContent: ""},
            {data: "otherExpenses", defaultContent: ""},
            {data: "currentKm", defaultContent: ""}
        ],
        "destroy": true
    });

    dataTable.searchBuilder.container().prependTo(dataTable.table().container());
}

// LOAD ONE DATA IN MODAL
let loadVendorRequestDetailModal = async (id) => {
    let url = Constants.API_VENDOR_REQUEST_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, refVendorId, refDriverId, refVehicleId, refFromLocationId, refToLocationId, totalAmount, dateOfRequest, dispatchDate, description, remarks, advancePaymentAmount, fuelAmount, dhalaAndPaltiExpenses, labourExpenses, maintainanceAmount, otherExpenses} = result.data;
        $("[name='refVendorId']").val(refVendorId).attr("selected", "selected").change();
        $("[name='refDriverId']").val(refDriverId).attr("selected", "selected").change();
        $("[name='refVehicleId']").val(refVehicleId).attr("selected", "selected").change();
        $("[name='refFromLocationId']").val(refFromLocationId).attr("selected", "selected").change();
        $("[name='refToLocationId']").val(refToLocationId).attr("selected", "selected").change();
        $("[name='dateOfRequest']").val(dateOfRequest);
        $("[name='dateOfRequestEn']").val(dateOfRequestEn);
        $("[name='dispatchDate']").val(dispatchDate);
        $("[name='dispatchDateEn']").val(dispatchDateEn);
        $("[name='vendorRequestDescription']").val(description);
        $("[name='remarks']").val(remarks);
        $("[name='totalAmount']").val(totalAmount);
        $("[name='advancePaymentAmount']").val(advancePaymentAmount);
        $("[name='fuelAmount']").val(fuelAmount);
        $("[name='dhalaAndPaltiExpenses']").val(dhalaAndPaltiExpenses);
        $("[name='labourExpenses']").val(labourExpenses);
        $("[name='maintainanceAmount']").val(maintainanceAmount);
        $("[name='otherExpenses']").val(otherExpenses);
        $("#updateVendorRequestBtn").attr("onClick", "updateVendorRequest(" + id + ")");
        // $("#deleteVendorRequestBtn").attr("onClick", "deleteVendorRequest(" + id + ")");
        $("#editVendorRequestModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getVendorRequestJsonFromModal = () => {
    let VendorRequest = {
        "refVendorId": $("[name='refVendorId']").val(),
        "refDriverId": $("[name='refDriverId']").val(),
        "refVehicleId": $("[name='refVehicleId']").val(),
        "refFromLocationId": $("[name='refFromLocationId']").val(),
        "refToLocationId": $("[name='refToLocationId']").val(),
        "dateOfRequest": $("[name='dateOfRequest']").val(),
        "dispatchDate": $("[name='dispatchDate']").val(),
        "description": $("[name='vendorRequestDescription']").val(),
        "remarks": $("[name='remarks']").val(),
        "totalAmount": $("[name='totalAmount']").val(),
        "fuelAmount": $("[name='fuelAmount']").val(),
        "advancePaymentAmount": $("[name='advancePaymentAmount']").val(),
        "dhalaAndPaltiExpenses": $("[name='dhalaAndPaltiExpenses']").val(),
        "labourExpenses": $("[name='labourExpenses']").val(),
        "maintainanceAmount": $("[name='maintainanceAmount']").val(),
        "otherExpenses": $("[name='otherExpenses']").val()
    };

    return JSON.stringify(VendorRequest);
}
/*
DATE CONVERTER
 */
let nepaliToEnglish = (inputId, successId) => {
    new DateUtils().nepaliToEnglish(inputId, successId);
};

let englishToNepali = (inputId, successId) => {
    new DateUtils().englishToNepali(inputId, successId);
};


let updateVendorRequest = (id) => new CrudUtils().sendPutRequest(Constants.API_VENDOR_REQUEST_UPDATE.replace("{id}", id), getVendorRequestJsonFromModal());

// // DELETE ONE DATA
// let deleteVendorRequest = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_VENDOR_REQUEST_DELETE.replace("{id}", id));
//     $("#editVendorRequestModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadVendorRequestDetailModal = loadVendorRequestDetailModal;
window.updateVendorRequest = updateVendorRequest;
window.englishToNepali = englishToNepali;
window.nepaliToEnglish = nepaliToEnglish;
// window.deleteVendorRequest = deleteVendorRequest;



