import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    loadDrivers().then(r => console.log("drivers loaded..."));
    loadDriverInSelect().then(r => console.log("Drivers loaded in select"));

    $('#driverDocumentsDataTable').DataTable();
});

/**
 * Driver tab
 */
/**
 * This method loads drivers in select option
 * @returns {Promise<void>}
 */
let loadDriverInSelect = async () => {
    $('#refDriverId').find('option').not(':first').remove();

    let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].firstName) {
                let id = result.data[i].id;
                let name = result.data[i].firstName + " " + result.data[i].lastName;

                $("#refDriverId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

/**
 * This method saves driver
 */
let saveDriver = () => {
    let id = $("#refDriverId").val();
    if (null !== id && id !== '') {
        new CrudUtils().sendPutRequest(Constants.API_DRIVER_UPDATE
            .replace("{driverId}", id), getDriverFormData());
    } else {
        new CrudUtils().sendPostRequest(Constants.API_DRIVER_SAVE, getDriverFormData());
    }

    setTimeout(function () {
        loadDriverInSelect().then(r => console.log("Drivers reloaded in select."));
    }, 500);

    setTimeout(function () {
        loadDrivers().then(r => console.log("Drivers reloaded..."));
    }, 500);
};
/**
 * This method gets json data from form
 * @returns {string}
 */
let getDriverFormData = () => {
    return JSON.stringify({
        "firstName": $("[name='firstName']").val(),
        "middleName": $("[name='middleName']").val(),
        "lastName": $("[name='lastName']").val(),
        "licenseNumber": $("[name='licenseNumber']").val(),
        "address": $("[name='address']").val(),
        "phone": $("[name='phone']").val(),
        "serviceType": $("[name='serviceType']").val()
    });
}

/**
 * This method loads driver details into form by driver id
 * @returns {Promise<void>}
 */
let loadDriverById = async () => {
    let id = $("#refDriverId").val();
    let url = Constants.API_DRIVER_GET_ONE.replace("{driverId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {firstName, middleName, lastName, licenseNumber, address, phone, serviceType} = result.data;
        $("[name='firstName']").val(firstName);
        $("[name='middleName']").val(middleName);
        $("[name='lastName']").val(lastName);
        $("[name='licenseNumber']").val(licenseNumber);
        $("[name='address']").val(address);
        $("[name='phone']").val(phone);
        $("[name='serviceType']").val(serviceType);
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}
/**
 * Documents Tab
 */
/**
 * This method upload driver documents
 * @param id
 */
let uploadDriverDocument = () => {
    let id = $("#refDriverId").val();
    if (null !== id && id !== '') {
        let data = new FormData();
        let json = {
            refId: id,
            type: 'DRIVER',
            subType: $("[name='driverDocumentSubType']").val()
        };
        let jsonData = JSON.stringify(json);
        data.append("file", $("input[name=driverFileBytes]")[0].files[0]);
        data.append("jsondata", jsonData);

        new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);
        setTimeout(() => {
            loadDriverDocuments().then(r => console.log("Driver documents reloaded"));
        }, 1000);
    } else {
        new CToastNotification().getFailureToastNotification("Please select DRIVER.");
    }
}

/**
 * This method loads driver documents
 * @param id
 * @returns {Promise<void>}
 */
let loadDriverDocuments = async () => {
    let id = $("#refDriverId").val();
    if (null !== id && id !== '') {
        let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
            .replace("{type}", "DRIVER")
            .replace("{refId}", id);
        let resource = await CrudUtils.fetchResource(url);
        console.log(resource);
        let driverTable = $('#driverDocumentsDataTable').DataTable({
            scrollY: '15vh',
            "bFilter": false,
            "paging": false,
            "ordering": false,
            "info": false,
            "data": resource.data,
            "columns": [
                {data: "subType"},
                {
                    data: "DocumentUrl",
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                        let a = '<a href="' + row.url + '" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
                        return a;
                    }
                }
            ],
            "destroy": true
        });
    } else {
        $('#driverDocumentsDataTable').DataTable().clear().draw();
        new CToastNotification().getFailureToastNotification("Please select DRIVER");
    }
}

let documentTabClickAction = () => {
    loadDriverDocuments().then(r => console.log("Driver documents loaded"));
}

let loadDrivers = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    let driverTable = $('#driverDataTable').DataTable({
        scrollX: true,
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [{
            extend: "excel",
            title: "drivers"
        }],
        "data": resource.data,
        "columns": [
            {data: "firstName", defaultContent:""},
            {data: "middleName", defaultContent:""},
            {data: "lastName", defaultContent:""},
            {data: "licenseNumber", defaultContent:""},
            {data: "address", defaultContent:""},
            {data: "phone", defaultContent:""},
            {data: "serviceType", defaultContent:""}
        ],
        "destroy": true
    });
}
window.uploadDriverDocument = uploadDriverDocument;
window.saveDriver = saveDriver;
window.documentTabClickAction = documentTabClickAction;
window.loadDriverById = loadDriverById;