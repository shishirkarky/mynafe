import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    loadPackages().then(r => console.log("packages loaded..."));
    loadVendorInSelect().then(r => console.log("Vendors loaded"));
});

/**
 * Filter Tab
 */
/**
 * loads main vendors in vendor select
 * @returns {Promise<void>}
 */
let loadVendorInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
                $("#erefVendorId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let savePackage = () => {
    new CrudUtils().sendPostRequest(Constants.API_CONFIG_PACKAGES_SAVE, getPackageFormData());
    setTimeout(function () {
        loadPackages().then(r => console.log("packages loaded..."));
    }, 500);
};

let getPackageFormData = () => {
    return JSON.stringify({
        "code": $("[name='code']").val(),
        "brand": $("[name='brand']").val(),
        "type": $("[name='type']").val(),
        "name": $("[name='name']").val(),
        "description": $("[name='packageDescription']").val(),
        "refVendorId": $("[name='refVendorId']").val()
    });
}

// -------------------------------------------------------VIEW AND EDIT--------------------------------------------
let loadPackages = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL);
    let packagesTable = $('#packagesDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: "excel",
                title: "packages"
            },
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (packagesTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = packagesTable.rows({selected: true}).data()[0];
                        loadPackagesDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {
                "data": "Vendor",
                "render": function (data, type, row, meta) {
                    if (null != row.vendor) {
                        return row.vendor.name;
                    } else {
                        return "";
                    }
                },
                defaultContent: ""
            },
            {data: "code", defaultContent: ""},
            {data: "brand", defaultContent: ""},
            {data: "type", defaultContent: ""},
            {data: "name", defaultContent: ""},
            {data: "description", defaultContent: ""}
        ],
        "destroy": true
    });
}

let loadPackagesDetailModal = async (id) => {
    let url = Constants.API_CONFIG_PACKAGES_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, code, brand, type, name, description, refVendorId} = result.data;
        $("[name='ecode']").val(code);
        $("[name='erefVendorId']").val(refVendorId).attr("selected", "selected").change();
        $("[name='ebrand']").val(brand);
        $("[name='etype']").val(type);
        $("[name='ename']").val(name);
        $("[name='edescription']").val(description);
        $("#updateConfigPackagesBtn").attr("onClick", "updatePackages(" + id + ")");
        $("#editConfigPackagesModal").modal('show');

    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

let getPackagesJsonFromModal = () => {
    const code = $("[name='ecode']").val();
    const brand = $("[name='ebrand']").val();
    const type = $("[name='etype']").val();
    const name = $("[name='ename']").val();
    const description = $("[name='edescription']").val();
    const refVendorId = $("[name='erefVendorId']").val();
    return JSON.stringify({
        code,
        brand,
        type,
        name,
        description,
        refVendorId
    });
}

let updatePackages = (id) => {
    new CrudUtils().sendPutRequest(Constants.API_CONFIG_PACKAGES_UPDATE.replace("{id}", id), getPackagesJsonFromModal());
    setTimeout(function () {
        loadPackages().then(r => console.log("packages loaded..."));
    }, 500);
}

window.savePackage = savePackage;
window.loadPackagesDetailModal = loadPackagesDetailModal;
window.updatePackages = updatePackages;