import {CrudUtils} from "./c-crud-utils.js";
import * as Constants from "./c-constants.js";
import {CToastNotification} from "./c-toastNotification.js";

let searchParcel = async () => {
  CrudUtils.showLoader();
  clearOldSearch();
  let $_dn = $("#dn").val();
  let $_customerBillNumber = $("#customerBillNumber").val();

  let result = await CrudUtils.fetchResource(Constants.PUBLIC_API_TRACK_PARCEL
    .replace("{dn}", $_dn)
    .replace("{customerBillNumber}", $_customerBillNumber));

  if (result.status === 200) {
    const {podStatus, receiverName, receiverContact, dn, consignorName, vendorRequest, customerBillNumber, consigneeName, fromLocation, toLocation, cashReceiptDetails, consigneeContact} = result.data;

    $("#res-consigneeContact").html(consigneeContact);
    $("#res-bookingCode").html(vendorRequest.bookingCode);
    $("#res-dispatchedDate").html(vendorRequest.dispatchDate);
    $("#res-dispatchedBranch").html(vendorRequest.branchCode);
    $("#res-dn").html(dn);
    $("#res-consignerName").html(consignorName);
    $("#res-customerBillNumber").html(customerBillNumber);
    $("#res-consigneeName").html(consigneeName);
    if (null != vendorRequest.driver) {
      $("#res-driverName").html(vendorRequest.driver.firstName + " " + vendorRequest.driver.middleName + " " + vendorRequest.driver.lastName);
      $("#res-driverContactNumber").html(vendorRequest.driver.phone);
    }
    if (null != vendorRequest.vehicle) {
      $("#res-vehicleNumber").html(vendorRequest.vehicle.registrationNumber);
    }
    if (null != fromLocation) {
      $("#res-fromLocation").html(fromLocation.district + ", " + fromLocation.city);
    }
    if (null != toLocation) {
      $("#res-toLocation").html(toLocation.district + ", " + toLocation.city);
    }
    $("#res-status").html(podStatus);
    if (podStatus === 'DELIVERED') {
      $("#res-receiverContact").html(receiverContact);
      $("#res-receiverName").html(receiverName);
    }

    let table = $('#dataTable');
    if (null != cashReceiptDetails) {
      table.DataTable({
        "bFilter": false,
        "data": cashReceiptDetails,
        "columns": [
          {data: "packageDetails.name", defaultContent: ""},
          {data: "quantity", defaultContent: ""},
          {data: "noOfPackage", defaultContent: ""}
        ],
        "destroy": true
      });
    } else {
      table.DataTable();
    }

    let transitions = await CrudUtils.fetchResource(Constants.PUBLIC_API_TRACK_PARCEL_TRANSITIONS
      .replace("{dn}", dn));
    if (transitions.status === 200) {
      let transitionTable = $('#transitionsDataTable');
      transitionTable.DataTable({
        "bFilter": false,
        "paging": false,
        "data": transitions.data,
        "columns": [
          {data: "transition.branchCode", defaultContent: ""},
          {data: "transition.createdDate", defaultContent: ""},
          {data: "booking.dateOfRequest", defaultContent: ""},
          {data: "booking.bookingCode", defaultContent: ""},
          {data: "booking.driver.firstName", defaultContent: ""},
          {data: "booking.vehicle.registrationNumber", defaultContent: ""},
          {data: "booking.driver.phone", defaultContent: ""}
        ],
        "destroy": true
      });
    }
  } else {
    new CToastNotification().getFailureToastNotification("Transitions not found.");
  }
  CrudUtils.hideLoader();
}

function clearOldSearch() {
  $("#res-consigneeContact").html("");
  $("#res-dispatchedBranch").html("");
  $("#res-vehicleNumber").html("");
  $("#res-bookingCode").html("");
  $("#res-dispatchedDate").html("");
  $("#res-dn").html("");
  $("#res-consignerName").html("");
  $("#res-customerBillNumber").html("");
  $("#res-consigneeName").html("");
  $("#res-driverName").html("");
  $("#res-driverContactNumber").html("");
  $("#res-fromLocation").html("");
  $("#res-toLocation").html("");
  $("#res-status").html("");
  $("#res-receiverContact").html("");
  $("#res-receiverName").html("");
  $('#dataTable').DataTable().clear().destroy();
}

document.searchParcel = searchParcel;
document.clearOldSearch = clearOldSearch;
