import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

$(document).ready(function () {
    loadOfficesInSelect().then(r => console.log("Offices/branches loaded in select"));

    loadTransitionsInTable().then(r => console.log("Transitions loaded in table"))

    $('#podDataTable').DataTable();
});

let loadOfficesInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_OFFICE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].officeCode) {
                let id = result.data[i].officeCode;
                let name;
                if (null != result.data[i].location) {
                    name = result.data[i].officeCode + " - " + result.data[i].location.city + ", " + result.data[i].location.district;
                }else {
                    name = result.data[i].officeCode;
                }

                $("#branchCode").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let createTransitAction = () => {
    let url = Constants.API_TRANSITIONS_CREATE;
    let data = JSON.stringify({
        "pod": {
            "dn": $("#dn").val()
        }
    });
    new CrudUtils().sendPostRequest(url, data);
    setTimeout(function () {
        loadTransitionsInTable().then(r => console.log("Transitions loaded in table"));
    }, 200);
}

let loadTransitionsInTable = async (fromBranchCode) => {
    let url;
    if (null == fromBranchCode || fromBranchCode === '') {
        url = Constants.API_TRANSITIONS_GET_ALL_FOR_CURRENT_BRANCH;
    } else {
        url = Constants.API_TRANSITIONS_GET_ALL_FROM_BRANCH.replace("{fromBranchCode}", fromBranchCode);
    }

    let resource = await CrudUtils.fetchResource(url);
    let transitionTable = $('#transitionDataTable').DataTable({
        searchBuilder: {
            columns: [0, 1, 2, 4, 6, 8]
        },
        dom: 'Brtip',
        buttons: [
            'selectAll',
            'selectNone',
            {
                extend: "excel",
                title: "booking-transitions"
            },
            {
                text: '<i class="fa fa-warning"></i>&nbsp;&nbsp;<i class="fa fa-trash-o"></i>',
                className: 'btn btn-danger',
                action: function () {
                    if (transitionTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = transitionTable.rows({selected: true}).data();

                        let ids = [];
                        $.each(selectedRowData, function (index, value) {
                            ids.push(value.id);
                        });
                        deleteTransitions(ids);
                    }
                }
            }
        ],
        select: {
            style: 'multi'
        },
        "data": resource.data,
        "columns": [
            {
                data: "createdDate",
                defaultContent: ""
            },
            {
                data: "branchCode",
                defaultContent: ""
            },
            {
                data: "fromBranchCode",
                defaultContent: ""
            },
            {
                data: "transactionId",
                defaultContent: ""
            },
            {
                "data": "bookingType",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    if (null != row.pod && null != row.pod.vendorRequest) {
                        return row.pod.vendorRequest.bookingType;
                    } else {
                        return "";
                    }
                }
            },
            {
                "data": "bookingCode",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    if (null != row.pod && null != row.pod.vendorRequest) {
                        return row.pod.vendorRequest.bookingCode;
                    } else {
                        return "";
                    }
                }
            },
            {
                "data": "vendor",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    if (null != row.pod && null != row.pod.vendorRequest && null != row.pod.vendorRequest.vendor) {
                        return row.pod.vendorRequest.vendor.name;
                    } else {
                        return "";
                    }
                }
            },
            {
                data: "pod.dn",
                defaultContent: ""
            },
            {
                "data": "toLocation",
                "defaultContent": "",
                "render": function (data, type, row, meta) {
                    if (null != row.pod && null != row.pod.toLocation) {
                        return row.pod.toLocation.city + ", " + row.pod.toLocation.district
                    } else {
                        return "";
                    }
                }
            },
            {
                data: "pod.podStatus",
                defaultContent: ""
            }
        ],
        "destroy": true
    });
    transitionTable.searchBuilder.container().prependTo(transitionTable.table().container());
}

// let branchCodeOnChangeAction = () => {
//     let fromBranchCode = $("#branchCode").val();
//     loadTransitionsInTable(fromBranchCode).then(r => console.log("Pod loaded successfully."));
// }

let deleteTransitions = (transitionIds) => {
    let payload = JSON.stringify({
        "transitionIds": transitionIds
    });
    new CrudUtils().sendDeleteRequestWithPayload(Constants.API_TRANSITIONS_DELETE_MULTIPLE, payload);
    setTimeout(function () {
        loadTransitionsInTable().then(r => console.log("Transitions loaded in table"));
    }, 200);
}

let verifyTransitionAction = async () => {
  $("#v-dn").html('');
  $("#v-consigneeName").html('');
  $("#v-consigneeContact").html('');
  $("#v-consignorName").html('');
  $("#v-consignorVat").html('');
  $("#v-customerBillDate").html('');
  $("#v-customerBillNumber").html('');

  let $_dn= $("#dn").val();
  if(''!==$_dn){
    let resource = await CrudUtils.fetchResource(Constants.API_CASH_RECEIPT_BY_DN_GET_ONE.replace("{dn}",$_dn));
    if (resource.status === 200) {
      const {consigneeName,consigneeContact,consignorName,consignorVat,customerBillDate,customerBillNumber}= resource.data;
      $("#v-dn").html($_dn);
      $("#v-consigneeName").html(consigneeName);
      $("#v-consigneeContact").html(consigneeContact);
      $("#v-consignorName").html(consignorName);
      $("#v-consignorVat").html(consignorVat);
      $("#v-customerBillDate").html(customerBillDate);
      $("#v-customerBillNumber").html(customerBillNumber);
      loadTransitionVerificationTable(resource.data.cashReceiptDetails);
      $("#verifyTransitionModal").modal('show');
    }else{
      new CToastNotification().getFailureToastNotification(
        "POD data not found."
      )
    }
  }
}

let loadTransitionVerificationTable = (data) => {
  let transitionVerificationTable = $('#transitionVerificationDataTable').DataTable({
    dom: 'ti',
    paging:false,
    "data": data,
    "columns": [
      {
        data: "packageDetails.name",
        defaultContent: ""
      },
      {
        data: "quantity",
        defaultContent: ""
      },
      {
        data: "noOfPackage",
        defaultContent: ""
      }
    ],
    "destroy": true
  });
}
// window.branchCodeOnChangeAction = branchCodeOnChangeAction;
window.createTransitAction = createTransitAction;
window.deleteTransitions = deleteTransitions;
window.verifyTransitionAction = verifyTransitionAction;
