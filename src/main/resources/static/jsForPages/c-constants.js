export const TOKEN_KEY = 'MYNATLSSECRETKEY';
export const BASE_URL = "http://143.244.133.99:8088";
//Resource APIs
export const API_OAUTH_GET = `${BASE_URL}/api/v1/oauth/token`;
export const API_LOGOUT = `${BASE_URL}/api/v1/logout`;

export let API_USER_PROFILE_GET = `${BASE_URL}/api/v1/profiles`;


/*
* CONFIG DATA APIs
 */
/*
CONFIG LOCATIONS
 */
export let API_CONFIG_LOCATIONS_SAVE = `${BASE_URL}/impl/api/v1/configs/locations`;
export const API_CONFIG_LOCATIONS_UPDATE = `${BASE_URL}/impl/api/v1/configs/locations/{id}`;
export const API_CONFIG_LOCATIONS_GET_ALL = `${BASE_URL}/impl/api/v1/configs/locations`;
export const API_CONFIG_LOCATIONS_GET_ONE = `${BASE_URL}/impl/api/v1/configs/locations/{id}`;

/*
CONFIG PACKAGES
 */
export let API_CONFIG_PACKAGES_SAVE = `${BASE_URL}/impl/api/v1/configs/packages`;
export const API_CONFIG_PACKAGES_UPDATE = `${BASE_URL}/impl/api/v1/configs/packages/{id}`;
export const API_CONFIG_PACKAGES_GET_ALL = `${BASE_URL}/impl/api/v1/configs/packages`;
export const API_CONFIG_PACKAGES_GET_ALL_BY_VENDOR = `${BASE_URL}/impl/api/v1/packages/vendors/{vendorId}`;
export const API_CONFIG_PACKAGES_GET_ONE = `${BASE_URL}/impl/api/v1/configs/packages/{id}`;

/*
CONFIG RATES
 */
export const API_CONFIG_RATES_SAVE = `${BASE_URL}/impl/api/v1/configs/rates`;
export const API_CONFIG_RATES_UPDATE = `${BASE_URL}/impl/api/v1/configs/rates/{id}`;
export const API_CONFIG_RATES_GET_ALL = `${BASE_URL}/impl/api/v1/configs/rates`;
export const API_CONFIG_RATES_GET_ONE = `${BASE_URL}/impl/api/v1/configs/rates/{id}`;
export const API_CONFIG_RATES_GET_ALL_BY_FILTER = `${BASE_URL}/impl/api/v1/configs/rates/vendors/{vendorId}/locations/{fromLocationId}?bookingType={bookingType}`;
export const API_CONFIG_RATES_IMPORT_VALIDATE = `${BASE_URL}/impl/api/v1/imports/rates/configs/validate`;
export const API_CONFIG_RATES_IMPORT = `${BASE_URL}/impl/api/v1/imports/rates/configs`;
export const API_CONFIG_RATES_DELETE_ALL = `${BASE_URL}/impl/api/v1/configs/rates`;

export const API_DRIVER_SAVE = `${BASE_URL}/impl/api/v1/drivers`;
export const API_DRIVER_UPDATE = `${BASE_URL}/impl/api/v1/drivers/{driverId}`;
export const API_DRIVER_GET_ALL = `${BASE_URL}/impl/api/v1/drivers`;
export const API_DRIVER_GET_ONE = `${BASE_URL}/impl/api/v1/drivers/{driverId}`;

export const API_VEHICLE_SAVE = `${BASE_URL}/impl/api/v1/vehicles`;
export const API_VEHICLE_UPDATE = `${BASE_URL}/impl/api/v1/vehicles/{vehicleId}`;
export const API_VEHICLE_GET_ALL = `${BASE_URL}/impl/api/v1/vehicles`;
export const API_VEHICLE_GET_ALL_BY_TYPE = `${BASE_URL}/impl/api/v1/vehicles/vehicletypes/{vehicleType}`;
export const API_VEHICLE_GET_ONE = `${BASE_URL}/impl/api/v1/vehicles/{vehicleId}`;

export const API_VEHICLE_LOG_BOOK_SAVE = `${BASE_URL}/impl/api/v1/vehicles/logbooks`;
export const API_VEHICLE_LOG_BOOK_GET_ALL = `${BASE_URL}/impl/api/v1/vehicles/logbooks`;
export const API_VEHICLE_LOG_BOOK_GET_ONE = `${BASE_URL}/impl/api/v1/vehicles/logbooks/{id}`;
export const API_VEHICLE_LOG_BOOK_UPDATE = `${BASE_URL}/impl/api/v1/vehicles/logbooks/{id}`;


export const API_CUSTOMER_SAVE = `${BASE_URL}/impl/api/v1/customers`;
export const API_CUSTOMER_UPDATE = `${BASE_URL}/impl/api/v1/customers/{customerId}`;
export const API_CUSTOMER_GET_ALL = `${BASE_URL}/impl/api/v1/customers`;
export const API_CUSTOMER_GET_ONE = `${BASE_URL}/impl/api/v1/customers/{customerId}`;

export const API_VENDOR_SAVE = `${BASE_URL}/impl/api/v1/vendors`;
export const API_VENDOR_UPDATE = `${BASE_URL}/impl/api/v1/vendors/{vendorId}`;
export const API_VENDOR_GET_ALL = `${BASE_URL}/impl/api/v1/vendors`;
export const API_VENDOR_MAIN_VENDORS_GET_ALL = `${BASE_URL}/impl/api/v1/vendors/mains`;
export const API_VENDOR_SUB_VENDORS_GET_ALL = `${BASE_URL}/impl/api/v1/vendors/subs`;
export const API_VENDOR_GET_ONE = `${BASE_URL}/impl/api/v1/vendors/{vendorId}`;

export const API_INVENTORY_SAVE = `${BASE_URL}/impl/api/v1/inventories`;
export const API_INVENTORY_UPDATE = `${BASE_URL}/impl/api/v1/inventories/{inventoryId}`;
export const API_INVENTORY_GET_ALL = `${BASE_URL}/impl/api/v1/inventories`;
export const API_INVENTORY_GET_ONE = `${BASE_URL}/impl/api/v1/inventories/{inventoryId}`;
export const API_INVENTORY_DELETE = `${BASE_URL}/impl/api/v1/inventories/{inventoryId}`;

export const API_OFFICE_SAVE = `${BASE_URL}/impl/api/v1/offices`;
export const API_OFFICE_UPDATE = `${BASE_URL}/impl/api/v1/offices/{officecode}`;
export const API_OFFICE_GET_ALL = `${BASE_URL}/impl/api/v1/offices`;
export const API_OFFICE_GET_ONE = `${BASE_URL}/impl/api/v1/offices/{officecode}`;

export const API_VENDOR_REQUEST_GET_LAST_CREATED = `${BASE_URL}/impl/api/v1/bookings/recents`;
export const API_VENDOR_REQUEST_SAVE = `${BASE_URL}/impl/api/v1/vendors/requests`;
export const API_VENDOR_REQUEST_UPDATE = `${BASE_URL}/impl/api/v1/vendors/requests/{id}`;
export const API_VENDOR_REQUEST_GET_ALL = `${BASE_URL}/impl/api/v1/vendors/requests`;
export const API_VENDOR_REQUEST_GET_ONE = `${BASE_URL}/impl/api/v1/vendors/requests/{id}`;
export const API_VENDOR_REQUEST_COUNT_BY_BOOKING_ID = `${BASE_URL}/impl/api/v1/bookings/{bookingId}/pods/counts`;

export const API_GATE_PASS_SAVE = `${BASE_URL}/impl/api/v1/gatepasses`;


export const API_CASH_RECEIPT_STATUS_UPDATE = `${BASE_URL}/impl/api/v1/cashreceipts/{id}/status?type={type}`;
export const API_CASH_RECEIPT_SAVE = `${BASE_URL}/impl/api/v1/cashreceipts`;
export const API_CASH_RECEIPT_UPDATE = `${BASE_URL}/impl/api/v1/cashreceipts/{id}`;
export const API_CASH_RECEIPT_GET_ALL = `${BASE_URL}/impl/api/v1/cashreceipts`;
export const API_CASH_RECEIPT_UPLOADS_GET_ALL = `${BASE_URL}/impl/api/v1/cashreceipts/uploads`;
export const API_CASH_RECEIPT_UPLOADS_PDF_POST = `${BASE_URL}/impl/api/v1/cashreceipts/images/download`;
export const API_CASH_RECEIPT_GET_ALL_BY_BOOKING_ID = `${BASE_URL}/impl/api/v1/cashreceipts/bookings/{bookingId}?status={status}`;
export const API_CASH_RECEIPT_GET_ONE = `${BASE_URL}/impl/api/v1/cashreceipts/{id}`;
export const API_CASH_RECEIPT_DELETE_ONE = `${BASE_URL}/impl/api/v1/cashreceipts/{id}`;
export const API_CASH_RECEIPT_BY_DN_GET_ONE = `${BASE_URL}/impl/api/v1/cashreceipts/dn/{dn}`;
export const API_CASH_RECEIPT_GET_BY_BILL_NUMBER = `${BASE_URL}/impl/api/v1/cashreceipts/bills/{customerBillNumber}`;


export const API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL = `${BASE_URL}/impl/api/v1/uploads/documents/{type}/{refId}`;
export const API_DOCUMENTS_SAVE = `${BASE_URL}/impl/api/v1/uploads`;

export let API_DOCUMENTS_GET_ALL_BY_CURRENT_USER = `${BASE_URL}/impl/api/v1/documents`;

export let API_TRANSITIONS_CREATE = `${BASE_URL}/impl/api/v1/transitions`;
export let API_TRANSITIONS_GET_ALL_FOR_CURRENT_BRANCH = `${BASE_URL}/impl/api/v1/transitions`;
export let API_TRANSITIONS_GET_ALL_FROM_BRANCH = `${BASE_URL}/impl/api/v1/transitions/branches/{fromBranchCode}`;
export let API_TRANSITIONS_DELETE_MULTIPLE = `${BASE_URL}/impl/api/v1/transitions`;

export let API_HIRING_VEHICLE_RATE_GET_ALL = `${BASE_URL}/impl/api/v1/configs/rates/hiring/vehicles`;


/*
* DATE CONVERSION
 */
export let API_DATE_CONVERT_N_TO_E = `${BASE_URL}/impl/api/v1/conversions/ntoe/{nepaliDate}`;
export let API_DATE_CONVERT_E_TO_N = `${BASE_URL}/impl/api/v1/conversions/eton/{englishDate}`;
export let API_DATE_TODAY_NEPALI = `${BASE_URL}/impl/api/v1/dates/todays/nepali`;
/**
 * Reports
 * @type {string}
 */
export let API_POD_REPORT = `${BASE_URL}/impl/api/v1/reports/pdf/pods/{id}`;
export let API_MULTIPLE_POD_REPORT_BY_BOOKING_ID = `${BASE_URL}/impl/api/v1/reports/bookings/{bookingId}/pdf/pods`;
export let API_VCTS_REPORT_BY_BOOKING_ID = `${BASE_URL}/impl/api/v1/reports/bookings/{bookingId}/vcts`;
export let API_GATE_PASS_REPORT_BY_BOOKING_ID = `${BASE_URL}/impl/api/v1/reports/pdfs/bookings/{bookingId}/gatepasses`;
export let API_EXCEL_REPORT = `${BASE_URL}/impl/api/v1/reports/excel/{reportType}?reportPeriod={reportPeriod}&fromDate={fromDate}&toDate={toDate}&branchCode={office}&driverId={driver}&vendorId={vendor}&vehicleType={vehicleType}`;


export let API_DASHBOARD = `${BASE_URL}/impl/api/v1/dashboard`;
export let API_DASHBOARD_BOOKINGS = `${BASE_URL}/impl/api/v1/dashboard/bookings?branchCode={branchCode}`;


//USER
export const API_USER_SAVE = `${BASE_URL}/api/v1/users`;
export const API_USER_UPDATE = `${BASE_URL}/api/v1/users/{username}`;
export const API_USER_GET = `${BASE_URL}/api/v1/users`;
export const API_USER_GET_ONE = `${BASE_URL}/api/v1/users/`;
export const API_USER_ACTIVATE = `${BASE_URL}/api/v1/users/{username}/activate`;
export const API_USER_DEACTIVATE = `${BASE_URL}/api/v1/users/{username}/deactivate`;
export const API_USER_CHANGE_PASSWORD = `${BASE_URL}/impl/api/v1/passwords`;
//ROLES
export const API_ROLES_GET = `${BASE_URL}/api/v1/users/roles`;
//STAFFS
export const API_STAFFS_GET_ALL = `${BASE_URL}/impl/api/v1/staffs`;
export const API_STAFFS_SAVE = `${BASE_URL}/impl/api/v1/staffs`;
export const API_STAFFS_GET_ONE = `${BASE_URL}/impl/api/v1/staffs/{id}`;
export const API_STAFFS_UPDATE = `${BASE_URL}/impl/api/v1/staffs/{id}`;

export const PUBLIC_API_TRACK_PARCEL = `${BASE_URL}/public/api/v1/parcels?dn={dn}&customerBillNumber={customerBillNumber}`;
export const PUBLIC_API_TRACK_PARCEL_TRANSITIONS = `${BASE_URL}/public/api/v1/transitions/pods/{dn}`;

// --------------------------------------JSP PAGES---------------------------------------------------------------
export const UI_LOGIN_PAGE = `${BASE_URL}/v1/login-page`;

export const UI_USER_PROFILE_VIEW_PAGE = `${BASE_URL}/v1/user-profile-page`;

export const UI_DASHBOARD_PAGE = `${BASE_URL}/v1/dashboard-page`;

export const UI_USER_CREATE_PAGE = `${BASE_URL}/v1/user-create-page`;

export const UI_CONFIG_LOCATIONS_CREATE_PAGE = `${BASE_URL}/v1/config-locations-create-page`;

export const UI_CONFIG_PACKAGES_CREATE_PAGE = `${BASE_URL}/v1/config-packages-create-page`;

export const UI_CONFIG_RATES_CREATE_PAGE = `${BASE_URL}/v1/config-rates-create-page`;


export const UI_DRIVER_CREATE_PAGE = `${BASE_URL}/v1/driver-create-page`;

export const UI_VEHICLE_CREATE_PAGE = `${BASE_URL}/v1/vehicle-create-page`;

export const UI_TRANSITIONS_CREATE_PAGE = `${BASE_URL}/v1/transitions-create-page`;
export const UI_TRANSITIONS_VIEW_PAGE = `${BASE_URL}/v1/transitions-view-page`;

export const UI_VENDOR_CREATE_PAGE = `${BASE_URL}/v1/vendor-create-page`;

export const UI_BOOKING_PIPELINE_PAGE = `${BASE_URL}/v1/booking-pipeline-page`;

export const UI_OFFICE_CREATE_PAGE = `${BASE_URL}/v1/office-create-page`;

export const UI_VENDOR_REQUEST_VIEW_PAGE = `${BASE_URL}/v1/vendor-request-view-page`;

export let UI_CASH_RECEIPT_VIEW_PAGE = `${BASE_URL}/v1/cash-receipt-view-page`;
export let UI_CASH_RECEIPT_VIEW_DETAIL_PAGE = `${BASE_URL}/v1/cash-receipt-detail-view-page?id={id}`;
export let UI_CASH_RECEIPT_EDIT_PAGE = `${BASE_URL}/v1/cash-receipt-edit-page?id={id}`;
export let UI_CASH_RECEIPT_UPLOADS_VIEW_PAGE = `${BASE_URL}/v1/pod-uploads-view-page`;

export const UI_VEHICLE_LOG_BOOK_CREATE_PAGE = `${BASE_URL}/v1/vehiclelogbook-create-page`;
export const UI_VEHICLE_LOG_BOOK_VIEW_PAGE = `${BASE_URL}/v1/vehiclelogbook-view-page`;

export const UI_UPLOAD_LOGO = `${BASE_URL}/v1/upload-logo`;

export const UI_STAFF_CREATE_PAGE = `${BASE_URL}/v1/staff-create-page`;

export const UI_REPORTS_PAGE = `${BASE_URL}/v1/reports-search-page`;
export const UI_CUSTOMER_SETTINGS_PAGE = `${BASE_URL}/v1/customer-create-page`;
export const UI_HIRING_VEHICLE_RATE_SETTINGS_PAGE = `${BASE_URL}/v1/hiring-vehicle-rate-create-page`;
export const UI_TRACK_PARCEL_PAGE = `${BASE_URL}/public/v1/parcel-tracking`;

