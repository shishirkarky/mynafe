import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

$(document).ready(function () {
    let id = $("#id").val(1);
    loadLogo().then(r => console.log("Data loaded successfully."));
});


let uploadLogo = () => {
    let data = new FormData();
    let json = {
        type: 'LOGO',
        subType: 'LOGO'
    };
    let jsonData = JSON.stringify(json);
    data.append("file", $("input[name=logoFileBytes]")[0].files[0]);
    data.append("jsondata", jsonData);

    new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);
    setTimeout(() => {
    }, 500);
    loadLogo().then(r => console.log("Data loaded successfully."));
}

let loadLogo = async () => {
    let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
        .replace("{type}", "LOGO")
        .replace("{refId}", 0);
    let resource = await CrudUtils.fetchResource(url);
    if(null!=resource.data){
        let logoData = resource.data[0];
        $("#mynaLogo").attr("src",logoData.url);
    }
}
window.uploadLogo = uploadLogo;