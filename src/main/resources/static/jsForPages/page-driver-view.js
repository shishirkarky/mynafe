import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadDrivers().then(r => console.log("Data loaded successfully."));

    $("#editDriverModal").on("hidden.bs.modal", function () {
        loadDrivers().then(r => console.log("Accounts loaded successfully."));
    });
});

// LOAD DATA IN DATATABLE
let loadDrivers = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    let driverTable = $('#driverDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (driverTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = driverTable.rows({selected: true}).data()[0];
                        loadDriverDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "firstName"},
            {data: "middleName"},
            {data: "lastName"},
            {data: "licenseNumber"},
            {data: "address"},
            {data: "phone"},
            {data: "serviceType"}
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadDriverDetailModal = async (id) => {
    let url = Constants.API_DRIVER_GET_ONE.replace("{driverId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, firstName, middleName, lastName, licenseNumber, address, phone,  serviceType} = result.data;
        $("[name='firstName']").val(firstName);
        $("[name='middleName']").val(middleName);
        $("[name='lastName']").val(lastName);
        $("[name='licenseNumber']").val(licenseNumber);
        $("[name='address']").val(address);
        $("[name='phone']").val(phone);
        $("[name='serviceType']").val(serviceType);
        $("#updateDriverBtn").attr("onClick", "updateDriver(" + id + ")");
        // $("#deleteDriverBtn").attr("onClick", "deleteDriver(" + id + ")");
        $("#uploadDriverDocumentBtn").attr("onClick", "uploadDriverDocument(" + id + ")");
        await loadDriverDocuments(id);
        $("#editDriverModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getDriverJsonFromModal = () => {
    const firstName = $("[name='firstName']").val();
    const middleName = $("[name='middleName']").val();
    const lastName = $("[name='lastName']").val();
    const licenseNumber = $("[name='licenseNumber']").val();
    const address = $("[name='address']").val();
    const phone = $("[name='phone']").val();
    const serviceType = $("[name='serviceType']").val();
    return JSON.stringify({
        firstName,
        middleName,
        lastName,
        licenseNumber,
        address,
        phone,
        serviceType
    });
}

//GET AVAILABLE DOCUMENTS
let loadDriverDocuments = async (id) => {
    let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
        .replace("{type}", "DRIVER")
        .replace("{refId}", id);
    let resource = await CrudUtils.fetchResource(url);
    console.log(resource);
    let driverTable = $('#driverDocumentsDataTable').DataTable({
        scrollY: '15vh',
        "bFilter": false,
        "paging": false,
        "ordering": false,
        "info": false,
        "data": resource.data,
        "columns": [
            {data: "subType"},
            {
                data: "DocumentUrl",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                    let a = '<a href="' + row.url + '" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
                    return a;
                }
            }
        ],
        "destroy": true
    });
}

let updateDriver = (id) => new CrudUtils().sendPutRequest(Constants.API_DRIVER_UPDATE.replace("{driverId}", id), getDriverJsonFromModal());

let uploadDriverDocument = (id) => {
    let data = new FormData();
    let json = {
        refId: id,
        type: 'DRIVER',
        subType: $("[name='driverDocumentSubType']").val()
    };
    let jsonData = JSON.stringify(json);
    data.append("file", $("input[name=driverFileBytes]")[0].files[0]);
    data.append("jsondata", jsonData);

    new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);
    setTimeout(() => {
        loadDriverDocuments(id).then(r => console.log("Driver documents reloaded"));
    }, 500);
}

// DELETE ONE DATA
// let deleteDriver = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_DRIVER_DELETE.replace("{driverId}", id));
//     $("#editDriverModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadDriverDetailModal = loadDriverDetailModal;
window.updateDriver = updateDriver;
// window.deleteDriver = deleteDriver;
window.uploadDriverDocument = uploadDriverDocument;



