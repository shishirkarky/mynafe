import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadLocations().then(r => console.log("Data loaded successfully."));

    $("#editConfigLocationsModal").on("hidden.bs.modal", function () {
        loadLocations().then(r => console.log("Data loaded successfully."));
    });
});

// LOAD DATA IN DATATABLE
let loadLocations = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    let locationsTable = $('#locationsDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (locationsTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = locationsTable.rows({selected: true}).data()[0];
                        loadLocationsDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },

            {data: "district"},
            {data: "city"}
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadLocationsDetailModal = async (id) => {
    let url = Constants.API_CONFIG_LOCATIONS_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, district, city} = result.data;
        $("[name='district']").val(district);
        $("[name='city']").val(city);
        $("#updateConfigLocationsBtn").attr("onClick", "updateLocations(" + id + ")");
        // $("#deleteConfigLocationsBtn").attr("onClick", "deleteLocations(" + id + ")");
        $("#editConfigLocationsModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getLocationsJsonFromModal = () => {
    return JSON.stringify({
        "district": $("[name='district']").val(),
        "city": $("[name='city']").val()
    });
}

let updateLocations = (id) => new CrudUtils().sendPutRequest(Constants.API_CONFIG_LOCATIONS_UPDATE.replace("{id}", id), getLocationsJsonFromModal());

// DELETE ONE DATA
// let deleteLocations = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_CONFIG_LOCATIONS_DELETE.replace("{id}", id));
//     $("#editLocationsModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadLocationsDetailModal = loadLocationsDetailModal;
window.updateLocations = updateLocations;
// window.deleteLocations = deleteLocations;



