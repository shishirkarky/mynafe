import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

/**
 * This js will get id from hidden input id-name as "id"
 * and call get one POD API and fill in
 * input boxes and data table
 */
$(document).ready(function () {
    let id = $("#id").val();
    loadPodDocuments(id).then(r => console.log("POD documents reloaded"));
});


let uploadPodDocument = (id) => {
    let data = new FormData();
    let json = {
        refId: id,
        type: 'POD',
        subType: 'POD'
    };
    let jsonData = JSON.stringify(json);
    data.append("file", $("input[name=podFileBytes]")[0].files[0]);
    data.append("jsondata", jsonData);

    new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);
    setTimeout(() => {
        loadPodDocuments(id).then(r => console.log("POD documents reloaded"));
    }, 500);
}

//GET AVAILABLE DOCUMENTS
let loadPodDocuments = async (id) => {
    let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
        .replace("{type}", "POD")
        .replace("{refId}", id);
    let resource = await CrudUtils.fetchResource(url);
    console.log(resource);
    let podTable = $('#podDocumentsDataTable').DataTable({
        scrollY: '20vh',
        "bFilter": false,
        "paging": false,
        "ordering": false,
        "info": false,
        "data": resource.data,
        "columns": [
            {data: "subType"},
            {
                data: "DocumentUrl",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                    return '<a href="' + row.url + '" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
                }
            }
        ],
        "destroy": true
    });
}
let editPodPage = (id) => {
    let url = Constants.UI_CASH_RECEIPT_EDIT_PAGE.replace("{id}", id);
    window.location = url;
}

let printReport = (id) => {
    let url = Constants.API_POD_REPORT.replace("{id}", id);
    let fileNameWithExtension = 'pod-' + id + '.pdf';
    CrudUtils.downloadPdfFile(url, fileNameWithExtension);
}

window.editPodPage = editPodPage;
window.printReport = printReport
window.uploadPodDocument = uploadPodDocument;