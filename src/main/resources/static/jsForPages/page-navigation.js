import * as Constants from './c-constants.js';
import * as Util from './c-utils.js';
import {CAuthentication} from "./c-authentication.js";
import {CrudUtils} from "./c-crud-utils.js";

$(document).ready(function () {
  checkCurrentAuthToken().then(r => console.log("token verified."));
});

//todo validate token if not then refresh
let checkCurrentAuthToken = async () => {
  let token = new CAuthentication().getCurrentToken();
  if (null == token || '' === token) {
    loadLoginPage();
  } else {
    document.getElementsByTagName("html")[0].style.visibility = "visible";

    setTimeout(function () {
      loadProfileDetailsInTopBar();
    }, 50);
  }
}

let loadProfileDetailsInTopBar = async () => {
  let resource = await CrudUtils.fetchResource(Constants.API_USER_PROFILE_GET);
  console.log(resource.data);
  if (resource.status === 200) {
    if (null != resource.data.staffs) {
      let {firstName, middleName, lastName, phoneNumber, email, branchCode} = resource.data.staffs;

      let fullName;
      if (null != firstName && null != middleName && null != lastName) {
        fullName = firstName.concat(" ").concat(middleName).concat(" ").concat(lastName);
      } else if (null != firstName && null != lastName) {
        fullName = firstName.concat(" ").concat(lastName);
      } else {
        fullName = firstName;
      }
      $("#profileName").html(fullName);
    }

    const {username, roles, navigationJson} = resource.data;
    $("#profileUsername").html(username);

    setDynamicNavBar(
      JSON.parse(navigationJson)
    );
  }

  function setDynamicNavBar(navJson) {
    navJson.menu.unshift({
      id: 'dashboard-page',
      value: 'Dashboard'
    });

    let getIconClass = (id) => {
      let $icon;
      if (id.includes('dashboard')) {
        $icon = 'fa fa-home';
      } else if (id.includes('booking')) {
        $icon = 'fa fa-book';
      } else if (id.includes('transition')) {
        $icon = 'fa fa-arrows-h';
      } else if (id.includes('log')) {
        $icon = 'fa fa-file-text-o';
      } else if (id.includes('dashboard')) {
        $icon = 'fa fa-tachomete';
      } else if (id.includes('pod')) {
        $icon = 'fa fa-envelope';
      } else if (id.includes('report')) {
        $icon = 'fa fa-files-o';
      } else if (id.includes('user')) {
        $icon = 'fa fa-users';
      } else if (id.includes('office')) {
        $icon = 'fa fa-briefcase';
      } else if (id.includes('staff')) {
        $icon = 'fa fa-users';
      } else if (id.includes('vehicle')) {
        $icon = 'fa fa-automobile';
      } else if (id.includes('driver')) {
        $icon = 'fa fa-users';
      } else if (id.includes('vendor')) {
        $icon = 'fa fa-building';
      } else if (id.includes('location')) {
        $icon = 'fa fa-location-arrow';
      } else if (id.includes('rate')) {
        $icon = 'fa fa-money';
      } else if (id.includes('logo')) {
        $icon = 'fa fa-photo';
      } else if (id.includes('package')) {
        $icon = 'fa fa-shopping-cart';
      } else if (id.includes('customer')) {
        $icon = 'fa fa-users';
      }
      return $icon;
    }

    $.each(navJson.menu, function (i, item) {
      if (null == navJson.menu.subMenu) {
        let $id = item.id;
        let $value = item.value;

        let $icon = getIconClass($id);

        let $onClickId = "'" + $id + "'";
        let $myList = '<li><a id="' + $id + '" onclick="clickAction(' + $onClickId + ')"> <i class="' + $icon + '" aria-hidden="true"></i> <span>' + $value + '</span> </a></li>';
        $('.nav-ul').append($myList);
      }
    });
  }
}

let clickAction = function (id) {
  console.log("clickAction: ", id);
  switch (id) {
    case "dashboard-page":
      loadDashboardPage();
      break;
    case "booking-pipeline":
      loadBookingPipelinePage();
      break;
    case "booking-view":
      loadVendorRequestViewPage();
      break;
    case "transition-create":
      loadTransitionsCreatePage();
      break;
    case "transition-view":
      loadTransitionsViewPage();
      break;
    case "vehicle-log-book-create":
      loadVehicleLogBookCreatePage();
      break;
    case "vehicle-log-book-view":
      loadVehicleLogBookViewPage();
      break;
    case "pod-view":
      loadCashReceiptViewPage();
      break;
    case "pod-uploads-view":
      loadCashReceiptUploadsViewPage();
      break;
    case "reports":
      loadReportsPage();
      break;
    case "user-settings-page":
      loadUserCreatePage();
      break;
    case "office-settings-page":
      loadOfficeCreatePage();
      break;
    case "driver-settings-page":
      loadDriverCreatePage();
      break;
    case "staff-settings-page":
      loadStaffsCreatePage();
      break;
    case "vehicle-settings-page":
      loadVehicleCreatePage();
      break;
    case "vendor-settings-page":
      loadVendorCreatePage();
      break;
    case "location-settings-page":
      loadConfigLocationCreatePage();
      break;
    case "rates-settings-page":
      loadConfigRateCreatePage();
      break;
    case "logo-settings-page":
      loadLogoUploadPage();
      break;
    case "packages-settings-page":
      loadConfigPackagesCreatePage();
      break;
    case "customer-settings-page":
      loadCustomerSettingsPage();
      break;
    case "hiring-vehicle-rate-settings-page":
      loadHiringVehicleRateSettingsPage();
      break;
    case "package-tracking-page":
      loadPackageTrackingPage();
      break;
    default:
  }
}

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('userProfileViewPageBtn').addEventListener('click', loadUserProfilePage);
});
// -------------------HEADER---------------------------------------
let loadLoginPage = () => Util.loadPage(Constants.UI_LOGIN_PAGE);
let loadUserProfilePage = () => Util.loadPage(Constants.UI_USER_PROFILE_VIEW_PAGE);

// --------------------------ADMIN NAV--------------------------
let loadDashboardPage = () => Util.loadPage(Constants.UI_DASHBOARD_PAGE);

let loadUserCreatePage = () => Util.loadPage(Constants.UI_USER_CREATE_PAGE);

let loadConfigLocationCreatePage = () => Util.loadPage(Constants.UI_CONFIG_LOCATIONS_CREATE_PAGE);

let loadConfigPackagesCreatePage = () => Util.loadPage(Constants.UI_CONFIG_PACKAGES_CREATE_PAGE);

let loadConfigRateCreatePage = () => Util.loadPage(Constants.UI_CONFIG_RATES_CREATE_PAGE);

let loadDriverCreatePage = () => Util.loadPage(Constants.UI_DRIVER_CREATE_PAGE);

let loadVehicleCreatePage = () => Util.loadPage(Constants.UI_VEHICLE_CREATE_PAGE);

let loadVendorCreatePage = () => Util.loadPage(Constants.UI_VENDOR_CREATE_PAGE);

let loadOfficeCreatePage = () => Util.loadPage(Constants.UI_OFFICE_CREATE_PAGE);

let loadLogoUploadPage = () => Util.loadPage(Constants.UI_UPLOAD_LOGO);

let loadStaffsCreatePage = () => Util.loadPage(Constants.UI_STAFF_CREATE_PAGE);

// --------------------------------CUSTOM NAV------------------------------------------------
let loadTransitionsCreatePage = () => Util.loadPage(Constants.UI_TRANSITIONS_CREATE_PAGE);
let loadTransitionsViewPage = () => Util.loadPage(Constants.UI_TRANSITIONS_VIEW_PAGE);

let loadBookingPipelinePage = () => Util.loadPage(Constants.UI_BOOKING_PIPELINE_PAGE);
let loadVendorRequestViewPage = () => Util.loadPage(Constants.UI_VENDOR_REQUEST_VIEW_PAGE);

let loadCashReceiptViewPage = () => Util.loadPage(Constants.UI_CASH_RECEIPT_VIEW_PAGE);
let loadCashReceiptUploadsViewPage = () => Util.loadPage(Constants.UI_CASH_RECEIPT_UPLOADS_VIEW_PAGE);

let loadVehicleLogBookCreatePage = () => Util.loadPage(Constants.UI_VEHICLE_LOG_BOOK_CREATE_PAGE);
let loadVehicleLogBookViewPage = () => Util.loadPage(Constants.UI_VEHICLE_LOG_BOOK_VIEW_PAGE);

let loadReportsPage = () => Util.loadPage(Constants.UI_REPORTS_PAGE);
let loadCustomerSettingsPage = () => Util.loadPage(Constants.UI_CUSTOMER_SETTINGS_PAGE);
let loadHiringVehicleRateSettingsPage = () => Util.loadPage(Constants.UI_HIRING_VEHICLE_RATE_SETTINGS_PAGE);
let loadPackageTrackingPage = () => Util.loadPage(Constants.UI_TRACK_PARCEL_PAGE);

window.loadReportsPage = loadReportsPage;
window.clickAction = clickAction;
