import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";
import {CAuthentication} from "./c-authentication.js";

$(document).ready(function () {
  loadPodInTable().then(r => console.log("PODs loaded successfully."));
});

let loadPodInTable = async () => {
  CrudUtils.showLoader();
  let resource = await CrudUtils.fetchResource(Constants.API_CASH_RECEIPT_UPLOADS_GET_ALL);
  loadTable(resource.data);
  CrudUtils.hideLoader();
}

let loadTable = (data) => {
  let dataTable = $('#podDataTable').DataTable({
    select: {
      style: 'multi'
    },
    searchBuilder: {
      columns: [1, 2, 3, 4, 5, 6, 7, 8]
    },
    dom: 'Brtip',
    buttons: [{
      extend: "excel",
      title: "POD"
    },
      {
        text: 'Download Photos',
        className: 'btn btn-sm btn-success',
        action: function () {
          if (dataTable.rows({selected: true}).count() === 0) {
            new CToastNotification().getFailureToastNotification("No records selected!");
          } else {
            let selectedRowData = dataTable.rows({selected: true}).data();
            downloadPhotosPdf(selectedRowData).then(value => console.log("image to pdf requested"));
          }
        }
      }],
    scrollX: true,
    pageLength: 20,
    processing: true,
    serverSide: false,
    data: data,
    "columns": [
      {
        data: null,
        defaultContent: '',
        className: 'select-checkbox',
        orderable: false,
        searchable: false
      },
      {
        data: "vendorRequest.dispatchDate", defaultContent: "",
        searchBuilderType: 'string'
      },
      {
        data: "branchCode", defaultContent: ""
      },
      {
        data: "vendorRequest.bookingCode", defaultContent: "",
        searchBuilderType: 'string'
      },
      {data: "dn", defaultContent: "", searchBuilderType: 'string'},
      {data: "consignorName", defaultContent: ""},
      {data: "consigneeName", defaultContent: ""},
      {data: "customerBillNumber", defaultContent: ""},
      {
        "data": "Driver",
        defaultContent: "",
        "render": function (data, type, row, meta) {
          if (row.vendorRequest != null && row.vendorRequest.driver != null) {
            return row.vendorRequest.driver.firstName + " " + row.vendorRequest.driver.middleName + " " + row.vendorRequest.driver.lastName
          }
        }

      },
      {
        "data": "Photo",
        defaultContent: "",
        "render": function (data, type, row, meta) {
          let anchorTag = "";
          $(row.photos).each(function (index) {
            anchorTag = anchorTag + "<a class='btn btn-sm btn-info' href='" + row.photos[index].photoUrl + "' target='_blank'>" + row.photos[index].title + "</a> ";
          });

          return anchorTag;
        }
      },
      {data: "podStatusRemarks", defaultContent: ""},
    ],
    "destroy": true
  });

  dataTable.searchBuilder.container().prependTo(dataTable.table().container());
}

let downloadPhotosPdf = async (pods) => {
  CrudUtils.showLoader();
  let podList = [];
  $.each(pods, function (index, value) {
    if (!value.transit) {
      podList.push(value.id);
    }
  });

  let request = {
    pods: podList
  }
  let url = Constants.API_CASH_RECEIPT_UPLOADS_PDF_POST;
  $.ajax({
    url: url,
    type: 'post',
    data: JSON.stringify(request),
    headers: new CAuthentication().getHeadersWithToken(),
    dataType: 'json',
    contentType: 'application/json',
    success: function (data) {
      if(data.status===200){
        new CToastNotification()
          .getSuccessToastNotification(data.message);
        downloadPDF(data.data);
      }else{
        new CToastNotification().getFailureToastNotification(data.message);
      }
    }
  });

  CrudUtils.hideLoader();
}

function downloadPDF(pdf) {
  console.log(pdf);
  const linkSource = `data:application/pdf;base64,${pdf}`;
  const downloadLink = document.createElement("a");
  const fileName = "pod.pdf";
  downloadLink.href = linkSource;
  downloadLink.download = fileName;
  downloadLink.click();
}

document.downloadPhotosPdf = downloadPhotosPdf;
