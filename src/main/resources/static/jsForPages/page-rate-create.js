import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";


document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveConfigRateBtn').addEventListener('click', sendRateSaveReq);
});

$(document).ready(function () {
    loadPackagesInSelect().then(r => console.log("Data loaded successfully."));
    loadLocationsToAndFromInSelect().then(r => console.log("Data loaded successfully."));
});

let sendRateSaveReq = (ev) => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_CONFIG_RATES_SAVE, getRateFormData());
};

// FILLING SELECT OPTIONS
let loadPackagesInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refConfPackageId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}


let loadLocationsToAndFromInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].district && null != result.data[i].city) {
                let id = result.data[i].id;
                let name = result.data[i].city + ", " + result.data[i].district ;

                $("#refConfToLocationId").append("<option value='" + id + "'>" + name + "</option>");
                $("#refConfFromLocationId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
let getRateFormData = () => {
    return JSON.stringify({
        "refConfPackageId": $("[name='refConfPackageId']").val(),
        "refConfFromLocationId": $("[name='refConfFromLocationId']").val(),
        "refConfToLocationId": $("[name='refConfToLocationId']").val(),
        "rate": $("[name='rate']").val()
    });
}