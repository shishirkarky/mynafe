import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CAuthentication} from "./c-authentication.js";

$(document).ready(function () {
    loadProfileDetails().then(r => console.log("Data loaded successfully."));
    loadLogo().then(r => console.log("Logo loaded successfully."));
});

let loadProfileDetails = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_USER_PROFILE_GET);
    console.log(resource.data);
    if (resource.status === 200) {
        const {firstName, middleName, lastName, phoneNumber, email, branchCode} = resource.data.staffs;
        const {username, roles} = resource.data;
        $("#username").html(username);

        let rolesList = [];
        $.each(roles, function (index, value) {
            rolesList.push(value.name);
        });
        $("#roles").html(rolesList.join(", "))

        if (null != firstName && null != middleName && null != lastName) {
            $("#firstName").html(firstName.concat(" ").concat(middleName).concat(" ").concat(lastName));
        } else if (null != firstName && null != lastName) {
            $("#firstName").html(firstName.concat(" ").concat(lastName));
        } else {
            $("#firstName").html(firstName);
        }

        $("#middleName").html(middleName);
        $("#lastName").html(lastName);
        $("#phoneNumber").html(phoneNumber);
        $("#email").html(email);
        $("#branchCode").html(branchCode);
    }
}

$("#changePasswordForm").submit(function (event) {
    event.preventDefault();
    let myform = document.getElementById("changePasswordForm");
    let fd = new FormData(myform);

    new CAuthentication().changePasswordRequest(
        Constants.API_USER_CHANGE_PASSWORD,
        fd
    );
});

let loadLogo = async () => {
    let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
        .replace("{type}", "LOGO")
        .replace("{refId}", 0);
    let resource = await CrudUtils.fetchResource(url);
    if (null != resource.data) {
        let logoData = resource.data[0];
        $("#logoImage").attr("src", logoData.url);
    }
}
