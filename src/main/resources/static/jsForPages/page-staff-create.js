import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    loadStaffs().then(r=>console.log("staffs loaded..."));
    loadOfficesInSelect().then(r => console.log("Offices/branches loaded in select"));
});

/*
* FILLING SELECT OPTIONS
 */
let loadOfficesInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_OFFICE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].officeCode) {
                let id = result.data[i].officeCode;
                let name;
                if (null != result.data[i].location) {
                    name = result.data[i].officeCode + " - " + result.data[i].location.city + ", " + result.data[i].location.district;
                }else {
                    name = result.data[i].officeCode;
                }

                $("#branchCode").append("<option value='" + id + "'>" + name + "</option>");
                $("#ebranchCode").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let sendStaffSaveReq = () => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_STAFFS_SAVE, getStaffFormData());
    setTimeout(function () {
        loadStaffs().then(r=>console.log("staffs loaded..."));
    },500);
};

let getStaffFormData = () => {
    return JSON.stringify({
        "firstName": $("[name='firstName']").val(),
        "middleName": $("[name='middleName']").val(),
        "lastName": $("[name='lastName']").val(),
        "phoneNumber": $("[name='phoneNumber']").val(),
        "email": $("[name='email']").val(),
        "branchCode": $("[name='branchCode']").val()
    });
}

// --------------------------------------VIEW AND EDIT--------------------------------------------
let loadStaffs = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_STAFFS_GET_ALL);
    let staffTable = $('#staffDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: "excel",
                title: "staffs"
            },
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (staffTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = staffTable.rows({selected: true}).data()[0];
                        loadStaffDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "firstName"},
            {data: "middleName"},
            {data: "lastName"},
            {data: "phoneNumber"},
            {data: "email"},
            {data: "branchCode"}
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadStaffDetailModal = async (id) => {
    let url = Constants.API_STAFFS_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, firstName, middleName, lastName, phoneNumber, email, branchCode} = result.data;
        $("[name='efirstName']").val(firstName);
        $("[name='emiddleName']").val(middleName);
        $("[name='elastName']").val(lastName);
        $("[name='ephoneNumber']").val(phoneNumber);
        $("[name='eemail']").val(email);
        $("[name='ebranchCode']").val(branchCode).attr("selected","selected").change();
        $("#updateStaffBtn").attr("onClick", "updateStaff(" + id + ")");
        $("#uploadStaffDocumentBtn").attr("onClick", "uploadStaffDocument(" + id + ")");
        await loadStaffDocuments(id);
        $("#editStaffModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA

let updateStaff = (id) => new CrudUtils().sendPutRequest(Constants.API_STAFFS_UPDATE.replace("{id}", id), getStaffJsonFromModal());
let getStaffJsonFromModal = () => {
    const firstName = $("[name='efirstName']").val();
    const middleName = $("[name='emiddleName']").val();
    const lastName = $("[name='elastName']").val();
    const phoneNumber = $("[name='ephoneNumber']").val();
    const email = $("[name='eemail']").val();
    const branchCode = $("[name='ebranchCode']").val();
    return JSON.stringify({
        firstName,
        middleName,
        lastName,
        phoneNumber,
        email,
        branchCode
    });
}
// ------------------------------------------------------DOCUMENT VIEW AND UPLOAD--------------------------------------
let loadStaffDocuments = async (id) => {
    let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
        .replace("{type}", "STAFFS")
        .replace("{refId}", id);
    let resource = await CrudUtils.fetchResource(url);
    console.log(resource);
    let staffTable = $('#staffDocumentsDataTable').DataTable({
        scrollY: '15vh',
        "bFilter": false,
        "paging": false,
        "ordering": false,
        "info": false,
        "data": resource.data,
        "columns": [
            {data: "subType"},
            {
                data: "DocumentUrl",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                    let a = '<a href="' + row.url + '" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
                    return a;
                }
            }
        ],
        "destroy": true
    });
}
let uploadStaffDocument = (id) => {
    let data = new FormData();
    let json = {
        refId: id,
        type: 'STAFFS',
        subType: $("[name='staffDocumentSubType']").val()
    };
    let jsonData = JSON.stringify(json);
    data.append("file", $("input[name=staffFileBytes]")[0].files[0]);
    data.append("jsondata", jsonData);

    new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);
    setTimeout(() => {
        loadStaffDocuments(id).then(r => console.log("Staff documents reloaded"));
    }, 500);
}

window.sendStaffSaveReq = sendStaffSaveReq;
window.loadStaffDetailModal = loadStaffDetailModal;
window.updateStaff = updateStaff;
window.uploadStaffDocument = uploadStaffDocument;