import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import * as Util from "./c-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

$(document).ready(function () {
  loadPodInTable().then(r => console.log("PODs loaded successfully."));
  loadDriverInSelect().then(r => console.log("Drivers loaded successfully."));
  $('#podDataTable').DataTable();
});

let loadDriverInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      if (null != result.data[i].firstName) {
        let id = result.data[i].id;
        let name = result.data[i].firstName + " " + result.data[i].lastName;

        $("#refDriverId").append("<option value='" + id + "'>" + name + "</option>");
      }
    }
  }
}

let loadPodInTable = async () => {
  CrudUtils.showLoader();
  let resource = await CrudUtils.fetchResource(Constants.API_CASH_RECEIPT_GET_ALL);
  loadTable(resource.data);
  CrudUtils.hideLoader();
}

let loadTable = (data) => {
  let dataTable = $('#podDataTable').DataTable({
    select: {
      style: 'multi'
    },
    searchBuilder: {
      columns: [1, 2, 3, 4, 5, 7, 8, 9, 11, 12]
    },
    dom: 'Brtip',
    buttons: [{
      extend: "excel",
      title: "POD"
    },
      {
        text: '<i class="fa fa-signal"></i>',
        className: 'btn btn-info',
        action: function () {
          if (dataTable.rows({selected: true}).count() === 0) {
            new CToastNotification().getFailureToastNotification("No records selected!");
          } else {
            let selectedRowData = dataTable.rows({selected: true}).data();
            openPodListStatusChangeModal(selectedRowData);
          }
        }
      },
      {
        text: '<i class="fa fa-trash-o"></i>',
        className: 'btn btn-danger',
        action: function () {
          if (dataTable.rows({selected: true}).count() === 0) {
            new CToastNotification().getFailureToastNotification("No records selected!");
          } else if (dataTable.rows({selected: true}).count() > 1) {
            new CToastNotification().getFailureToastNotification("Select 1 row !");
          } else {
            let selectedRowData = dataTable.rows({selected: true}).data();
            deletePod(selectedRowData);
          }
        }
      }],
    scrollX: true,
    pageLength: 20,
    processing: true,
    serverSide: false,
    data: data,
    "columns": [
      {
        data: null,
        defaultContent: '',
        className: 'select-checkbox',
        orderable: false,
        searchable: false
      },
      {
        data: "vendorRequest.dispatchDate", defaultContent: "",
        searchBuilderType: 'string'
      },
      {
        "data": "DN",
        defaultContent: "",
        "render": function (data, type, row, meta) {
          if (row.transit) {
            return row.dn;
          } else {
            let a = '<a onclick="loadPodDetailPage(' + row.id + ')">' + row.dn + '</a>';
            return a;
          }
        },
        searchBuilderType: 'string'
      },
      {
        "data": "bookingType",
        "defaultContent": "",
        "render": function (data, type, row, meta) {
          if (null != row.vendorRequest) {
            return row.vendorRequest.bookingType;
          } else {
            return "";
          }
        }
      },
      {
        data: "vendorRequest.bookingCode", defaultContent: "",
        searchBuilderType: 'string'
      },
      {data: "consignorName", defaultContent: ""},
      {
        "data": "Driver",
        defaultContent: "",
        "render": function (data, type, row, meta) {
          if (row.vendorRequest != null && row.vendorRequest.driver != null) {
            return row.vendorRequest.driver.firstName + " " + row.vendorRequest.driver.middleName + " " + row.vendorRequest.driver.lastName
          }
        }

      },
      {data: "vendorRequest.vehicle.registrationNumber", defaultContent: ""},
      {data: "consigneeName", defaultContent: ""},
      {data: "customerBillNumber", defaultContent: ""},
      {data: "consigneeContact", defaultContent: ""},
      {
        "data": "fromLocation",
        "render": function (data, type, row, meta) {
          if (null != row.fromLocation) {
            return row.fromLocation.city + ", " + row.fromLocation.district;
          }
        },
        defaultContent: ""
      },
      {
        "data": "toLocation",
        "render": function (data, type, row, meta) {
          if (null != row.toLocation) {
            return row.toLocation.city + ", " + row.toLocation.district;
          }
        },
        defaultContent: ""
      },
      {
        "data": "noOfPackages",
        "render": function (data, type, row, meta) {
          if (null != row.cashReceiptDetails) {
            let noOfPackages = 0;
            $.each(row.cashReceiptDetails, function () {
              if (null != this.noOfPackage && this.noOfPackage !== '') {
                noOfPackages = noOfPackages + parseInt(this.noOfPackage);
              }
            });

            return noOfPackages;
          }
        },
        defaultContent: ""
      },
      {
        "data": "packages",
        "render": function (data, type, row, meta) {
          if (null != row.cashReceiptDetails) {
            let packagesName = "";
            $.each(row.cashReceiptDetails, function () {
              if (this.packageDetails != null) {
                packagesName = packagesName.concat(",").concat(this.packageDetails.name);
              }
            });
            if (packagesName.startsWith(",")) {
              packagesName = packagesName.substr(1);
            }
            return packagesName;
          }
        },
        defaultContent: ""
      },
      {data: "podStatus", defaultContent: ""},
      {data: "podStatusUpdateDate", defaultContent: ""},
      {data: "transit", defaultContent: ""},
      {
        "data": "NewBookingCode",
        "render": function (data, type, row, meta) {
          if (null != row.transitBooking) {
            return row.transitBooking.bookingCode;
          }
        },
        defaultContent: ""
      },
      {
        "data": "NewDispatchDate",
        "render": function (data, type, row, meta) {
          if (null != row.transitBooking) {
            return row.transitBooking.dispatchDate;
          }
        },
        defaultContent: ""
      },
      {
        "data": "NewVehicleNumber",
        "render": function (data, type, row, meta) {
          if (null != row.transitBooking && null != row.transitBooking.vehicle) {
            return row.transitBooking.vehicle.registrationNumber;
          }
        },
        defaultContent: ""
      },
      {
        "data": "NewDriverName",
        "render": function (data, type, row, meta) {
          if (null != row.transitBooking && null != row.transitBooking.driver) {
            return row.transitBooking.driver.firstName + " " + row.transitBooking.driver.middleName + " " + row.transitBooking.driver.lastName
          }
        },
        defaultContent: ""
      },
      {
        "data": "TransitLocation",
        "render": function (data, type, row, meta) {
          if (null != row.transitBooking && null != row.transitBooking.fromLocation) {
            return row.transitBooking.fromLocation.city + ", " + row.transitBooking.fromLocation.district;
          }
        },
        defaultContent: ""
      },
      {data: "createdBy", defaultContent: ""},
      {data: "lastModifiedBy", defaultContent: ""},
      {
        "data": "Action",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          let a = '<a class="btn btn-sm btn-primary" onclick="printReport(' + row.id + ')"><i class="fa fa-print"></i> </a>' +
            '&nbsp;&nbsp;&nbsp;<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Change status" onclick="openPodStatusChangeModal(' + row.id + ')"><i class="fa fa-signal"></i> </a>';
          return a;
        }

      }
    ],
    "destroy": true
  });

  dataTable.searchBuilder.container().prependTo(dataTable.table().container());
}

let printReport = (id) => {
  let url = Constants.API_POD_REPORT.replace("{id}", id);

  let fileNameWithExtension = 'pod-' + id + '.pdf';
  CrudUtils.downloadPdfFile(url, fileNameWithExtension);
}


let loadPodDetailPage = (id) => {
  Util.loadPage(Constants.UI_CASH_RECEIPT_VIEW_DETAIL_PAGE.replace("{id}", id));
}

/**
 * Open modal to change status of selected PODs
 * @param pods
 */
function openPodListStatusChangeModal(pods) {
  let podList = [];
  $.each(pods, function (index, value) {
    if (!value.transit) {
      podList.push(value.id);
    }
  });
  $("#mupdatePodStatusBtn").attr("onClick", "updateMultiplePodStatus(" + JSON.stringify(podList) + ")");
  $("#multiplePodStatusChangeModal").modal('show');
}

/**
 * Open modal to change status of POD
 * @param id
 */
let openPodStatusChangeModal = async (id) => {
  if (null != id) {
    let result = await CrudUtils.fetchResource(Constants.API_CASH_RECEIPT_GET_ONE
      .replace("{id}", id));
    if (result.status === 200) {
      let data = result.data;
      if (null != data.podStatus) {
        $("#podStatus").val(data.podStatus).attr("selected", "selected").change();
      }
      if (null != data.podStatusRemarks) {
        $("#podStatusRemarks").val(data.podStatusRemarks);
      }

      $("#receiverName").val(data.receiverName);
      $("#receiverContact").val(data.receiverContact);

      $("#podStatusModalDN").html(data.dn);

      $("#updatePodStatusBtn").attr("onClick", "updatePodStatus(" + id + ")");
      $("#uploadPodDocumentBtn").attr("onClick", "uploadPodDocument(" + id + ")");

      $("#podStatusChangeModal").modal('show');

      await loadPodDocuments(id);
    } else {
      new CToastNotification().getFailureToastNotification("Data not found.")
    }
  } else {
    new CToastNotification().getFailureToastNotification("Id not found.")
  }
}
/**
 * Change Multiple POD Status
 * @param pods
 */
let updateMultiplePodStatus = (pods) => {
  let jsonData = JSON.stringify({
    podStatus: $("#mpodStatus").val(),
    podStatusRemarks: $("#mpodStatusRemarks").val(),
    receiverName: $("#mreceiverName").val(),
    receiverContact: $("#mreceiverContact").val(),
    podStatusUpdateDate: $("#mpodStatusUpdateDate").val(),
    ids: pods
  });

  let url = Constants.API_CASH_RECEIPT_STATUS_UPDATE.replace("{id}", 0)
    .replace("{type}", 'pod');
  new CrudUtils().sendPatchRequest(url, jsonData);
  setTimeout(function () {
    loadPodInTable().then(r => console.log("Pod table updated"));
  }, 200);
}
/**
 * Change POD Status
 * @param id
 */
let updatePodStatus = (id) => {
  let jsonData = JSON.stringify({
    podStatus: $("#podStatus").val(),
    podStatusRemarks: $("#podStatusRemarks").val(),
    receiverName: $("#receiverName").val(),
    receiverContact: $("#receiverContact").val(),
    podStatusUpdateDate: $("#podStatusUpdateDate").val()
  });

  let url = Constants.API_CASH_RECEIPT_STATUS_UPDATE.replace("{id}", id)
    .replace("{type}", 'pod');

  new CrudUtils().sendPatchRequest(url, jsonData);
  setTimeout(function () {
    loadPodInTable().then(r => console.log("Pod table updated"));
  }, 5000);
}

let loadPodDocuments = async (id) => {
  let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
    .replace("{type}", "POD")
    .replace("{refId}", id);
  let resource = await CrudUtils.fetchResource(url);
  console.log(resource);
  let podTable = $('#podDocumentsDataTable').DataTable({
    scrollY: '20vh',
    "bFilter": false,
    "paging": false,
    "ordering": false,
    "info": false,
    "data": resource.data,
    "columns": [
      {data: "subType"},
      {
        data: "DocumentUrl",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
          return '<a href="' + row.url + '" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
        }
      }
    ],
    "destroy": true
  });
}

let uploadPodDocument = (id) => {
  let data = new FormData();
  let json = {
    refId: id,
    type: 'POD',
    subType: 'POD'
  };
  let jsonData = JSON.stringify(json);
  data.append("file", $("input[name=podFileBytes]")[0].files[0]);
  data.append("jsondata", jsonData);

  new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);
  setTimeout(() => {
    loadPodDocuments(id).then(r => console.log("POD documents reloaded"));
  }, 500);
}

let deletePod = (data) => {
  let resource = new CrudUtils().sendDeleteRequest(Constants.API_CASH_RECEIPT_DELETE_ONE.replace("{id}", data[0].id));
  if (resource.status === 200) {
    new CToastNotification().getSuccessToastNotification(resource.message);
  } else {
    new CToastNotification().getFailureToastNotification(resource.message);
  }

  location.reload();

}

window.printReport = printReport;
window.loadPodDetailPage = loadPodDetailPage;
window.openPodStatusChangeModal = openPodStatusChangeModal;
window.updateMultiplePodStatus = updateMultiplePodStatus;
window.updatePodStatus = updatePodStatus;
window.uploadPodDocument = uploadPodDocument;
window.deletePod = deletePod;
