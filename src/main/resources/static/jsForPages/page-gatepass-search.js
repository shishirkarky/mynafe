import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {DateUtils} from "./c-date-utils.js";

$(document).ready(function () {
    loadBookingCodeInSelect().then(r => console.log("Drivers loaded successfully."));
    $('#gatePassDataTable').DataTable();
});

/*
* FILLING SELECT OPTIONS
 */
/*
Load Booking code in select
 */
let loadBookingCodeInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].bookingCode) {
                let id = result.data[i].id;
                let name = result.data[i].bookingCode;

                $("#refVendorRequestId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let loadGatePassDataTableForBooking = async () => {
    let bookingId = $("#refVendorRequestId").val();
    if (bookingId !== '') {
        let url = Constants.API_CASH_RECEIPT_GET_ALL_BY_BOOKING_ID
            .replace("{bookingId}", bookingId)
            .replace("{status}", "ACTIVE");

        let resource = await CrudUtils.fetchResource(url);
        $('#gatePassDataTable').DataTable({
            "data": resource.data,
            "columns": [
                {data: "dn"},
                {data: "vendorRequest.bookingCode"},
                {data: "vendorRequest.driver.firstName"},
                {data: "vendorRequest.vehicle.registrationNumber"},
                {data: "vendorRequest.vendor.name"},
                {
                    "data": "Action",
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        let a = '<a class="btn btn-sm btn-primary" onclick="printGatepassReport(' + row.id + ')"><i class="fa fa-print"></i> </a>';
                        return a;
                    }

                }
            ],
            "destroy": true
        });
    }
}

let bookingCodeOnChangeAction = () => {
    loadGatePassDataTableForBooking().then(r => console.log("Gate-pass loaded successfully."));
}
let printAllGatePass = () => {

}
let printGatepassReport = () => {

}
window.bookingCodeOnChangeAction = bookingCodeOnChangeAction;
window.printAllGatePass = printAllGatePass;
window.printGatepassReport = printGatepassReport;

