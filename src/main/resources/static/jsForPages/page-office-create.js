import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    loadLocationInSelect().then(r => console.log("locations loaded..."));
    loadOffices().then(r => console.log("offices loaded..."));
});

let saveOffice = () => {
    new CrudUtils().sendPostRequest(Constants.API_OFFICE_SAVE, getOfficeFormData());
    setTimeout(function () {
        loadOffices().then(r => console.log("office loaded..."));
    }, 300)
};

let getOfficeFormData = () => {
    return JSON.stringify({
        "officeCode": $("[name='officeCode']").val(),
        "officeRepresentativeName": $("[name='officeRepresentativeName']").val(),
        "category": $("[name='category']").val(),
        "refLocationId": $("[name='refLocationId']").val(),
        "facility": $("[name='facility']").val(),
        "phoneNumber": $("[name='phoneNumber']").val(),
        "email": $("[name='email']").val(),
        "type": $("[name='type']").val()
    });
}
let loadLocationInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let data = result.data;
        let len = data.length;
        for (let i = 0; i < len; i++) {
            if (null != data[i].district) {
                let id = data[i].id;
                let district = data[i].district;
                let city = data[i].city;

                $("#refLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
                $("#erefLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
            }
        }
    }
}

//-------------------------VIEW AND UPDATE-----------------------------------------------------------

/**
 * Load office details in datatable
 * @returns {Promise<void>}
 */
let loadOffices = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_OFFICE_GET_ALL);
    let officeTable = $('#officeDataTable').DataTable({
        scrollX: true,
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (officeTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = officeTable.rows({selected: true}).data()[0];
                        console.log(selectedRowData);
                        loadOfficeDetailModal(selectedRowData.officeCode);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {
                data: "officeCode",
                defaultContent: ""
            },
            {
                data: "type",
                defaultContent: ""
            },
            {
                data: "officeRepresentativeName",
                defaultContent: ""
            },
            {
                "data": "Location",
                "render": function (data, type, row, meta) {
                    if (null != row.location) {
                        return row.location.city + ", " + row.location.district;
                    } else {
                        return "";
                    }
                }
            },
            {
                data: "phoneNumber",
                defaultContent: ""
            },
            {
                data: "email",
                defaultContent: ""
            },
            {
                data: "category",
                defaultContent: ""
            },
            {
                data: "facility",
                defaultContent: ""
            }
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadOfficeDetailModal = async (officeCode) => {
    let url = Constants.API_OFFICE_GET_ONE.replace("{officecode}", officeCode);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {officeCode, officeRepresentativeName, category, refLocationId, facility, phoneNumber, email, type} = result.data;
        $("[name='eofficeCode']").val(officeCode);
        $("[name='eofficeRepresentativeName']").val(officeRepresentativeName);
        $("[name='ecategory']").val(category);
        $("[name='erefLocationId']").val(refLocationId).attr("selected", "selected").change();
        $("[name='efacility']").val(facility);
        $("[name='ephoneNumber']").val(phoneNumber);
        $("[name='eemail']").val(email);
        $("[name='etype']").val(type).attr("selected", "selected").change();
        $("#updateOfficeBtn").attr("onClick", "updateOffice(" + officeCode + ")");
        $("#editOfficeModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getOfficeJsonFromModal = () => {
    const officeCode = $("[name='eofficeCode']").val();
    const officeRepresentativeName = $("[name='eofficeRepresentativeName']").val();
    const category = $("[name='ecategory']").val();
    const refLocationId = $("[name='erefLocationId']").val();
    const facility = $("[name='efacility']").val();
    const phoneNumber = $("[name='ephoneNumber']").val();
    const email = $("[name='eemail']").val();
    const type = $("[name='etype']").val();

    return JSON.stringify({
        officeCode,
        officeRepresentativeName,
        category,
        refLocationId,
        facility,
        phoneNumber,
        email,
        type
    });
}

let updateOffice = (officeCode) => new CrudUtils().sendPutRequest(Constants.API_OFFICE_UPDATE.replace("{officecode}", officeCode), getOfficeJsonFromModal());

window.loadOfficeDetailModal = loadOfficeDetailModal;
window.updateOffice = updateOffice;
window.saveOffice = saveOffice;