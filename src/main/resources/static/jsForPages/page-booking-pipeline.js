import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {DateUtils} from "./c-date-utils.js";
import * as Util from "./c-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

$(document).ready(function () {
  /**
   * Booking Section
   */
  loadBookingCodeInSelect().then(r => console.log("Booking code loaded successfully."));
  loadTodaysNepaliDate().then(r => console.log("today nepali date loaded."));
  loadTransitItemsInSelect().then(r => console.log("Transit items loaded."));

  /**
   * Expenses and assignment section
   */
  loadDriverInSelect().then(r => console.log("Drivers loaded successfully."));
  loadMainVendorInBookingSelect().then(r => console.log("Main Vendors loaded successfully."));
  loadSubVendorInPodSelect().then(r => console.log("Sub-Vendors loaded successfully."));
  loadLocationsInSelect().then(r => console.log("Locations loaded successfully."));
  loadConsigneeNameInSelect().then(r => console.log("Customers loaded successfully."));
  // loadConfigPackagesInSelect().then(r => console.log("Config Packages loaded successfully."));
});
/**
 * Booking section
 */
let loadTransitItemsInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_TRANSITIONS_GET_ALL_FOR_CURRENT_BRANCH);
  let len = result.data.length;
  for (let i = 0; i < len; i++) {
    if (null != result.data[i].id) {
      let id = result.data[i].id;
      let name = result.data[i].pod.dn;

      $("#refTransitionIds").append("<option value='" + id + "'>" + name + "</option>");
    }
  }
}
/*
* Load today's nepali date in input
 */
let loadTodaysNepaliDate = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_DATE_TODAY_NEPALI);
  if (result.status === 200) {
    $("#dateOfRequest").val(result.data);
  }
}
/*
Load Boooking code in select
 */
let loadBookingCodeInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      if (null != result.data[i].bookingCode) {
        let id = result.data[i].id;
        let name = result.data[i].bookingCode;

        $("#refVendorRequestId").append("<option value='" + id + "'>" + name + "</option>");
      }
    }
  }
}
/*
* FILLING SELECT OPTIONS
 */
let loadDriverInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      if (null != result.data[i].firstName) {
        let id = result.data[i].id;
        let name = result.data[i].firstName + " " + result.data[i].lastName;

        $("#refDriverId").append("<option value='" + id + "'>" + name + "</option>");
      }
    }
  }
}
/*
* CONSIGNEE NAME IN SELECT
 */
let loadConsigneeNameInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_CUSTOMER_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      let name = result.data[i].receiverName;
      let id = result.data[i].id;
      if (null != name && name !== '')
        $("#consigneeId").append("<option value='" + id + "'>" + name + "</option>");
      $("#econsigneeId").append("<option value='" + id + "'>" + name + "</option>");
    }
  }
}
/*
* VENDOR IN SELECT
 */
let loadMainVendorInBookingSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      if (null != result.data[i].name) {
        let id = result.data[i].id;
        let name = result.data[i].name;

        $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
      }
    }
  }
}

let loadSubVendorInPodSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_VENDOR_SUB_VENDORS_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      if (null != result.data[i].name) {
        let id = result.data[i].id;
        let name = result.data[i].name;

        $("#consignorName").append("<option value='" + id + "'>" + name + "</option>");
        $("#econsignorName").append("<option value='" + id + "'>" + name + "</option>");
      }
    }
  }
}
/*
* LOCATION IN FROM AND TO SELECT
 */
let loadLocationsInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
  if (result.status === 200) {
    let data = result.data;
    loadLocationInSelect(data).then(r => console.log("Location loaded successfully."));
  }
}
let loadLocationInSelect = async (data) => {
  let len = data.length;
  for (let i = 0; i < len; i++) {
    if (null != data[i].district) {
      let id = data[i].id;
      let district = data[i].district;
      let city = data[i].city;

      $("#dispatchFrom").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
      $("#finalDestination").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
      $("#refFromLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
      $("#erefFromLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
      $("#refToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
      $("#erefToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
    }
  }
}

/**
 * This method will save if booking id is not selected and
 * update if booking id is selected
 * then set the values in summary box as per last updated booking
 */
let saveBookingAction = () => {
  let id = $("[name='refVendorRequestId']").val();
  if (id === '') {
    new CrudUtils().sendPostRequest(Constants.API_VENDOR_REQUEST_SAVE, getVendorRequestFormData());
  } else {
    new CrudUtils().sendPutRequest(Constants.API_VENDOR_REQUEST_UPDATE.replace("{id}", id), getVendorRequestFormData());
  }
  $("#bookingLi").removeClass("active");
  $("#podInfoTab").click();

  /**
   * set last updated booking id
   */
  setTimeout(async () => {
    let lastBooking;
    if (id === '') {
      lastBooking = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_LAST_CREATED);
      /**
       * add new created booking into select option
       */
      if (lastBooking.status === 200) {
        $("#refVendorRequestId").append("<option value='" + lastBooking.data.id + "'>" + lastBooking.data.bookingCode + "</option>");
      }
    } else {
      lastBooking = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ONE.replace("{id}", id));
    }
    /**
     * Set values to booking summary section
     */
    if (lastBooking.status === 200) {
      $("#refVendorRequestId").val(lastBooking.data.id).attr("selected", "selected").change();
      updateSummarySection(lastBooking.data);
    }
  }, 800);
}

let updateSummarySection = (bookingData) => {
  $("#summaryBookingCode").html(bookingData.bookingCode);
  $("#summaryBookingType").html(bookingData.bookingType);
  if (null != bookingData.vendor) {
    $("#summaryVendor").html(bookingData.vendor.name);
  }
  $("#summaryDate").html(bookingData.dateOfRequest);
  setTimeout(async () => {
    await setSummaryPodsCount();
  }, 500);
}
let setSummaryPodsCount = async () => {
  let bookingId = $("#refVendorRequestId").val();
  let podCount = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_COUNT_BY_BOOKING_ID.replace("{bookingId}", bookingId));
  if (podCount.status === 200) {
    $("#summaryPodCount").html(podCount.data);
  }
}

let getVendorRequestFormData = () => {
  let transitionIds = $("#refTransitionIds").val();
  let transitionArray = [];
  $(transitionIds).each(function (index, element) {
    let data = {"id": element};
    transitionArray.push(data);
  });

  let VendorRequest = {
    "dateOfRequest": $("[name='dateOfRequest']").val(),
    "bookingType": $("[name='bookingType']").val(),
    "refVendorId": $("[name='refVendorId']").val(),
    "refFromLocationId": $("[name='dispatchFrom']").val(),
    "refToLocationId": $("[name='finalDestination']").val(),
    ['transitions']: transitionArray,
    "withTransition":true
  };

  return JSON.stringify(VendorRequest);
}
/*
DATE CONVERTER
 */
let englishToNepali = (inputId, successId) => {
  new DateUtils().englishToNepali(inputId, successId);
};

let nepaliToEnglish = (inputId, successId) => {
  new DateUtils().englishToNepali(inputId, successId);
};


/*
SAVE FUNCTION
 */
let savePodAction = () => {
  if ($("#refVendorRequestId").val() === '') {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
  } else {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_CASH_RECEIPT_SAVE, getCashReceiptFormData());
    setTimeout(async () => {
      await setSummaryPodsCount();
      await resetPodSection();
    }, 500);
  }
};

let resetPodSection = async () => {
  $("#podForm").trigger("reset");
  $("#podForm select").attr("selected", "selected").change();
  changeDispatchLocation();
  $(".cashReceiptDetailsDiv").not(':first').remove();
}

let getCashReceiptFormData = () => {
  let cashReceipt = {
    "consignorVat": $("[name='consignorVat']").val(),
    "customerBillNumber": $("[name='customerBillNumber']").val(),
    "refFromLocationId": $("[name='refFromLocationId']").val(),
    "refToLocationId": $("[name='refToLocationId']").val(),
    "refVendorRequestId": $("#refVendorRequestId").val(),
    "consignorName": $("#consignorName option:selected").text(),
    "date": $("#dateOfRequest").val(),

    "consigneeName": $("#consigneeName").val(),
    "consigneeVat": $("#consigneeVat").val(),
    "consigneeContact": $("#consigneeContact").val(),
    "consigneeNumber": $("#consigneeContact").val(),

    "customerBillDate": $("#customerBillDate").val()
  };

  if (null !== $("#consignorName").val() && $("#consignorName").val() > 0) {
    cashReceipt['vendor'] = {
      id: $("#consignorName").val()
    }
  }

  // if ($("#consigneeId").val() >= 0) {
  //     let consigneeName = $("#consigneeName").val();
  //     let consigneeId = parseInt($("#consigneeId").val());
  //     cashReceipt['consigneeName'] = consigneeName;
  //     cashReceipt['consignee'] = {
  //         id: consigneeId
  //     };
  // }

  let cashReceiptDetails = [];

  $('.cashReceiptDetailsDiv').each(function () {
    let description = $(this).find($("[name='cashReceiptDetailsDescription']")).val();
    let quantity = $(this).find($("[name='quantity']")).val();
    let unitOfMeasurement = $(this).find($("[name='unitOfMeasurement']")).val();
    let noOfPackage = $(this).find($("[name='noOfPackage']")).val();
    let taxableAmount = $(this).find($("[name='taxableAmount']")).val();
    let remarks = $(this).find($("[name='remarks']")).val();

    let cashReceiptDetailsJson = {
      description,
      quantity,
      unitOfMeasurement,
      noOfPackage,
      taxableAmount,
      remarks
    };

    cashReceiptDetails.push(cashReceiptDetailsJson);
  });

  cashReceipt['cashReceiptDetails'] = cashReceiptDetails;

  return JSON.stringify(cashReceipt);
}

/*
CLONE CASH RECEIPT DETAILS
 */
let cloneCashReceiptDetailReq = () => {
  $(".cashReceiptDetailsDiv:first .cashReceiptDetailsDescription").select2("destroy");

  $(".cashReceiptDetailsDiv").eq(0)
    .clone()
    .find("input").val("").end()
    .show()
    .insertAfter(".cashReceiptDetailsDiv:last");

  $(".cashReceiptDetailsDiv:last .cashReceiptDetailsDescription").select2("destroy");
  $(".cashReceiptDetailsDiv:last .cashReceiptDetailsDescription").select2();
  $(".cashReceiptDetailsDiv:first .cashReceiptDetailsDescription").select2();
}

let removeCashReceiptDetailReq = () => {
  $(".cashReceiptDetailsDiv:last").remove();
}

let assignBtnAction = () => {
  $("#assignmentInfoTab").click();
}

let saveExpensesAndAssignmentAction = () => {
  if ($("#refVendorRequestId").val() === '') {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
  } else {
    let bookingId = $("#refVendorRequestId").val();

    let VendorRequest = {
      "refDriverId": $("[name='refDriverId']").val(),
      "vehicleType": $("[name='vehicleType']").val(),
      "refVehicleId": $("[name='refVehicleId']").val(),
      "vcts": $("[name='vcts']").val(),
      "dhalaAndPaltiExpenses": $("[name='dhalaAndPaltiExpenses']").val(),
      "labourExpenses": $("[name='labourExpenses']").val(),
      "fuelAmount": $("[name='fuelAmount']").val(),
      "totalAmount": $("[name='totalAmount']").val(),
      "advancePaymentAmount": $("[name='advancePaymentAmount']").val(),
      "dueAmount": $("[name='dueAmount']").val(),
      "maintainanceAmount": $("[name='maintainanceAmount']").val(),
      "otherExpenses": $("[name='otherExpenses']").val(),
      "currentKm": $("[name='currentKm']").val()
    };

    let request = JSON.stringify(VendorRequest);
    new CrudUtils().sendPutRequest(Constants.API_VENDOR_REQUEST_UPDATE.replace("{id}", bookingId), request);
    $("#availablePodsInfoTab").click();
  }
}

let loadPodDetailPage = (id) => {
  Util.loadPage(Constants.UI_CASH_RECEIPT_VIEW_DETAIL_PAGE.replace("{id}", id));
}

let printReport = (id) => {
  if ($("#refVendorRequestId").val() === '') {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
  } else {
    let url = Constants.API_POD_REPORT.replace("{id}", id);

    let fileNameWithExtension = 'pod-' + id + '.pdf';
    CrudUtils.downloadPdfFile(url, fileNameWithExtension);
  }
}


let bookingCodeChangeAction = async () => {
  console.log("Booking Code changed...")
  let bookingCode = $("#refVendorRequestId").val();
  if (bookingCode === '') {
    await loadTodaysNepaliDate();
  }
  let resource = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ONE.replace("{id}", bookingCode));
  if (resource.status === 200) {
    let data = resource.data;

    /**
     * Booking section
     */
    $("#bookingType").val(data.bookingType).attr("selected", "selected").change();
    $("#refVendorId").val(data.refVendorId).attr("selected", "selected").change();
    $("#dispatchFrom").val(data.refFromLocationId).attr("selected", "selected").change();
    $("#finalDestination").val(data.refToLocationId).attr("selected", "selected").change();
    $("#dateOfRequest").val(data.dateOfRequest);

    /**
     * Expenses & Assignment section
     */
    $("#refDriverId").val(data.refDriverId).attr("selected", "selected").change();
    $("#vehicleType").val(data.vehicleType).attr("selected", "selected").change();
    /**
     * setTimeout for refVehicleId select
     * 1. vehicleType is changed
     * 2. triggers vehicleTypeOnChangeAction
     * 3. once vehicles is appended in select option, vehicle is selected from data
     */
    setTimeout(function () {
      $("#refVehicleId").val(data.refVehicleId).attr("selected", "selected").change();
    }, 800);

    $("#vcts").val(data.vcts).attr("selected", "selected").change();
    $("#dhalaAndPaltiExpenses").val(data.dhalaAndPaltiExpenses);
    $("#labourExpenses").val(data.labourExpenses);
    $("#fuelAmount").val(data.fuelAmount);
    $("#totalAmount").val(data.totalAmount);
    $("#advancePaymentAmount").val(data.advancePaymentAmount);
    $("#maintainanceAmount").val(data.maintainanceAmount);
    $("#otherExpenses").val(data.otherExpenses);
    $("#currentKm").val(data.currentKm);
    $("#dueAmount").val(data.dueAmount);

    let transitions = data.transitions;
    if (null !== transitions) {
      let transitionArray = [];
      $(transitions).each(function (index, element) {
        transitionArray.push(element.id);
      });
      $("#refTransitionIds").val(transitionArray).attr("selected", "selected").change();
    }

    /**
     * Summary section
     */
    updateSummarySection(data);
  }
}
let printVctsReport = () => {
  if ($("#refVendorRequestId").val() === '') {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
  } else {
    let bookingId = $("#refVendorRequestId").val();
    let url = Constants.API_VCTS_REPORT_BY_BOOKING_ID.replace("{bookingId}", bookingId);

    let fileNameWithExtension = 'vcts-booking-' + bookingId + '.xls';
    CrudUtils.downloadExcelFile(url, fileNameWithExtension);
  }
}
let printAllPodReports = () => {
  if ($("#refVendorRequestId").val() === '') {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
  } else {
    let bookingId = $("#refVendorRequestId").val();
    let url = Constants.API_MULTIPLE_POD_REPORT_BY_BOOKING_ID.replace("{bookingId}", bookingId);

    let fileNameWithExtension = 'pod-booking-' + bookingId + '.pdf';
    CrudUtils.downloadPdfFile(url, fileNameWithExtension);
  }
}
let printGatepassReport = () => {
  if ($("#refVendorRequestId").val() === '') {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
  } else {
    let bookingId = $("#refVendorRequestId").val();
    let url = Constants.API_GATE_PASS_REPORT_BY_BOOKING_ID.replace("{bookingId}", bookingId);

    let fileNameWithExtension = 'gate-pass-booking-' + bookingId + '.pdf';
    CrudUtils.downloadPdfFile(url, fileNameWithExtension);
  }
}

/**
 * Change POD Status
 * @param type
 * @param typeId
 */
let updatePodStatus = (type, typeId) => {
  if ($("#refVendorRequestId").val() === '') {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
  } else {
    let jsonData = JSON.stringify({
      podStatus: $("#podStatus").val(),
      podStatusRemarks: $("#podStatusRemarks").val()
    });

    let id = 0;
    if ('booking' === type) {
      id = $("#refVendorRequestId").val();
    } else if ('pod' === type) {
      id = typeId;
    }

    let url = Constants.API_CASH_RECEIPT_STATUS_UPDATE.replace("{id}", id)
      .replace("{type}", type);

    new CrudUtils().sendPatchRequest(url, jsonData);
    setTimeout(function () {
      loadPodsDataTableForBooking().then(r => console.log("Pod table updated"));
    }, 200);
  }
}

let consignorNameOnChangeAction = async () => {
  if ($("#consignorName").val() > 0) {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ONE
      .replace("{vendorId}", $("#consignorName").val()));
    if (result.status === 200) {
      $("#consignorVat").val(result.data.panVat);
    }
  }
}

/**
 * Expenses & Assignment section methods
 */
let vehicleTypeOnChangeAction = async () => {
  if (null !== $("#vehicleType").val() && $("#vehicleType").val() !== '') {
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL_BY_TYPE
      .replace("{vehicleType}", $("#vehicleType").val()));
    if (result.status === 200) {
      let data = result.data;
      let len = data.length;
      if (len === 0) {
        $("#refVehicleId").empty();
        $("#refVehicleId").append("<option value=''>Select Vehicle</option>");
      } else {
        for (let i = 0; i < len; i++) {
          if (null != data[i].registrationNumber) {
            let id = data[i].id;
            let name = data[i].registrationNumber;

            $("#refVehicleId").append("<option value='" + id + "'>" + name + "</option>");
          }
        }
      }
    }
  }
}

let updateDueAmount = () => {
  let totalAmount = $("#totalAmount").val();
  let advancePaymentAmount = $("#advancePaymentAmount").val();
  $("#dueAmount").val(totalAmount - advancePaymentAmount);
}

let triggerPodCreationTabClickAction = async () => {
  let vendorId = $("#refVendorId").val();
  await loadConfigPackagesInSelect(vendorId);
}
/*
PACKAGES IN DESCRIPTION SELECT
 */
let loadConfigPackagesInSelect = async (vendorId) => {

  $('#cashReceiptDetailsDescription')
    .find('option')
    .remove()
    .end()
    .append('<option value="0" selected>Select Item</option>');

  if (null != vendorId && parseInt(vendorId) > 0) {
    $(".cashReceiptDetailsDiv:first #cashReceiptDetailsDescription").select2("destroy");
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL_BY_VENDOR.replace("{vendorId}", vendorId));
    if (null != result.body && null != result.body.data) {
      let data = result.body.data;
      let len = data.length;
      for (let i = 0; i < len; i++) {
        if (null != data[i].name) {
          let id = data[i].id;
          let name = data[i].brand + " - " + data[i].name

          $("#cashReceiptDetailsDescription").append("<option value='" + id + "'>" + name + "</option>");
        }
      }
      $(".cashReceiptDetailsDiv:first #cashReceiptDetailsDescription").select2();
    }
  } else {
    new CToastNotification().getFailureToastNotification("Items - Please select vendor")
  }
}

/**
 * On click of Available PODs section tab
 * @returns {Promise<void>}
 */
let clickAvailablePodsSection = async () => {
  await loadPodsDataTableForBooking();
  await loadConfigPackagesInAvailablePodSection();
}

/**
 * Available PODs
 * Datatable - load PODs for booking
 */
let loadPodsDataTableForBooking = async () => {
  let bookingId = $("#refVendorRequestId").val();
  if (bookingId !== '') {
    let url = Constants.API_CASH_RECEIPT_GET_ALL_BY_BOOKING_ID.replace("{bookingId}", bookingId)
      .replace("{status}", "");

    let resource = await CrudUtils.fetchResource(url);
    let podDataTable = $('#podDataTable').DataTable({
      scrollX: true,
      dom: 'Brtip',
      buttons: [{
        extend: "excel",
        title: "booking-pod"
      }],
      "data": resource.data,
      "columns": [
        {
          "data": "Action",
          "render": function (data, type, row, meta) {
            if (row.transit) {
              return row.dn;
            } else {
              let a = '<a onclick="loadPodDetailPage(' + row.id + ')">' + row.dn + '</a>';
              return a;
            }
          }
        },
        {
          "data": "vendorRequest.bookingCode",
          "defaultContent": ""
        },
        {
          "data": "consignorName",
          "defaultContent": ""
        },
        {
          "data": "fromLocation",
          "defaultContent": "",
          "render": function (data, type, row, meta) {
            if (null != row.fromLocation) {
              return row.fromLocation.city + ", " + row.fromLocation.district
            } else {
              return "";
            }
          }
        },
        {
          "data": "toLocation",
          "defaultContent": "",
          "render": function (data, type, row, meta) {
            if (null != row.toLocation) {
              return row.toLocation.city + ", " + row.toLocation.district
            } else {
              return "";
            }
          }
        },
        {
          "data": "podStatus",
          "defaultContent": ""
        },
        {
          "data": "transit",
          "defaultContent": ""
        },
        {
          "data": "Action",
          "orderable": false,
          "searchable": false,
          "render": function (data, type, row, meta) {
            if (row.transit) {
              return '<a class="btn btn-sm btn-primary" onclick="printReport(' + row.id + ')"><i class="fa fa-print"></i> </a>';
            } else {
              return '<a class="btn btn-sm btn-primary" onclick="printReport(' + row.id + ')"><i class="fa fa-print"></i> </a>' +
                '&nbsp;&nbsp;&nbsp;<a class="btn btn-sm btn-primary" onclick="showPodEditModal(' + row.id + ')"><i class="fa fa-edit"></i> </a>';
            }
          }

        }
      ],
      "destroy": true
    });
  } else {
    new CToastNotification().getFailureToastNotification('Booking Code not found.');
    $('#podDataTable').DataTable();
  }
}

/**
 * Available PODs
 * POD Edit Modal - Load packages in select option
 * @returns {Promise<void>}
 */
let loadConfigPackagesInAvailablePodSection = async () => {
  let vendorId = $("#refVendorId").val();

  $('#ecashReceiptDetailsDescription')
    .find('option')
    .remove()
    .end()
    .append('<option value="0" selected>Select Item</option>');

  if (null != vendorId && parseInt(vendorId) > 0) {
    $(".ecashReceiptDetailsDiv:first #ecashReceiptDetailsDescription").select2("destroy");
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL_BY_VENDOR.replace("{vendorId}", vendorId));
    if (null != result.body && null != result.body.data) {
      let data = result.body.data;
      let len = data.length;
      for (let i = 0; i < len; i++) {
        if (null != data[i].name) {
          let id = data[i].id;
          let name = data[i].brand + " - " + data[i].name

          $("#ecashReceiptDetailsDescription").append("<option value='" + id + "'>" + name + "</option>");
        }
      }
      $(".ecashReceiptDetailsDiv:first #ecashReceiptDetailsDescription").select2();
    }
  } else {
    new CToastNotification().getFailureToastNotification("Items - Please select vendor")
  }
}

/**
 * Available PODs
 * POD Edit Modal - show on edit button click
 * @param id
 */
let showPodEditModal = async (id) => {
  let url = Constants.API_CASH_RECEIPT_GET_ONE.replace("{id}", id);
  let result = await CrudUtils.fetchResource(url);
  if (result.status === 200) {
    loadPodOnly(result.data);
    loadPodDetails(result.data);
    $("#updatePodBtn").attr("onClick", "sendCashReceiptUpdateReq(" + id + ")");
    $('#editPodModal').modal({backdrop: 'static', keyboard: false});
    $('#editPodModal').modal('show');
  } else {
    new CToastNotification().getFailureToastNotification("POD data not found.");
  }
}

let loadPodOnly = (data) => {
    const {
        dn,
        vendor,
        consignee,
        customerBillNumber,
        refFromLocationId,
        refToLocationId,
        consignorVat,
        refVendorRequestId,
        customerBillDate,
        consigneeName,
        consigneeVat,
        consigneeContact
    } = data;

  $("[name='erefVendorRequestId']").val(refVendorRequestId).attr("selected", "selected").change();
  $("[name='econsignorVat']").val(consignorVat);
  $("[name='ecustomerBillNumber']").val(customerBillNumber);

    if (null != vendor) {
        $("#econsignorName").select2("destroy");
        $("#econsignorName").val(vendor.id).attr("selected", "selected").change();
        $("#econsignorName").select2();
    }


  $("[name='erefFromLocationId']").val(refFromLocationId).attr("selected", "selected").change();
  $("[name='erefToLocationId']").val(refToLocationId).attr("selected", "selected").change();

  // if(consignee!=null) {
  // $("#econsigneeId").select2("destroy");
  // $("#econsigneeId").val(consignee.id).attr("selected", "selected").change();
  // $("#econsigneeId").select2();

  $("#econsigneeName").val(consigneeName);
  $("#econsigneeContact").val(consigneeContact);
  $("[name='econsigneeVat']").val(consigneeVat);
  // }
  $("#ecustomerBillDate").val(customerBillDate);
  $("#edn").html(dn);
}

let loadPodDetails = (data) => {
  $(".ecashReceiptDetailsDiv:not(:first)").remove();
  if (null != data.cashReceiptDetails) {
    data.cashReceiptDetails.forEach(function (value, index) {
      $(".ecashReceiptDetailsDiv:first .ecashReceiptDetailsDescription").select2("destroy");
      if (index === 0) {
        $("#ecashReceiptDetailsDescription").val(value.description).attr("selected", "selected").change();
        $("#equantity").val(value.quantity);
        $("#eunitOfMeasurement").val(value.unitOfMeasurement).attr("selected", "selected").change();
        $("#enoOfPackage").val(value.noOfPackage);
        $("#etaxableAmount").val(value.taxableAmount);
        $("#eremarks").val(value.remarks);
      } else {

        let cloneDiv = $(".ecashReceiptDetailsDiv").eq(0).clone();

        cloneDiv.find("#ecashReceiptDetailsDescription").val(value.description).attr("selected", "selected").change().show();
        cloneDiv.find("#ecashReceiptDetailsDescription").select2();

        cloneDiv.find("#equantity").val(value.quantity).show();
        cloneDiv.find("#eunitOfMeasurement").val(value.unitOfMeasurement).show();
        cloneDiv.find("#enoOfPackage").val(value.noOfPackage).show();
        cloneDiv.find("#etaxableAmount").val(value.taxableAmount).show();
        cloneDiv.find("#eremarks").val(value.remarks).show();
        $(cloneDiv).insertAfter(".ecashReceiptDetailsDiv:last");
      }

      $(".ecashReceiptDetailsDiv:first .ecashReceiptDetailsDescription").select2();
    });
  } else {
    $("#ecashReceiptDetailsDescription").val("").attr("selected", "selected").change();
    $("#equantity").val("");
    $("#eunitOfMeasurement").val("").attr("selected", "selected").change();
    $("#enoOfPackage").val("");
    $("#etaxableAmount").val("");
    $("#eremarks").val("");
  }
}

/**
 * Available PODs
 * POD Edit Modal - get consignor vat on change of consignor name select option
 * @returns {Promise<void>}
 */
let econsignorNameOnChangeAction = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ONE
    .replace("{vendorId}", $("#econsignorName").val()));
  if (result.status === 200) {
    $("#econsignorVat").val(result.data.panVat);
  }

}

/**
 * Available PODs
 * POD Edit Modal - clone item details row
 */
let ecloneCashReceiptDetailReq = () => {
  $(".ecashReceiptDetailsDiv:first .ecashReceiptDetailsDescription").select2("destroy");

  $(".ecashReceiptDetailsDiv").eq(0)
    .clone()
    .find("input").val("").end()
    .show()
    .insertAfter(".ecashReceiptDetailsDiv:last");

  $(".ecashReceiptDetailsDiv:last .ecashReceiptDetailsDescription").select2("destroy");
  $(".ecashReceiptDetailsDiv:last .ecashReceiptDetailsDescription").select2();
  $(".ecashReceiptDetailsDiv:first .ecashReceiptDetailsDescription").select2();
}

/**
 * Available PODs
 * POD Edit Modal - remove item details last row
 */
let eremoveCashReceiptDetailReq = () => {
  $(".ecashReceiptDetailsDiv:last").remove();
}
let sendCashReceiptUpdateReq = (id) => {
  let crudUtils = new CrudUtils();
  crudUtils.sendPutRequest(Constants.API_CASH_RECEIPT_UPDATE.replace("{id}", id), getPodEditFormData());
}

let getPodEditFormData = () => {
  let cashReceipt = {
    "consignorVat": $("[name='econsignorVat']").val(),
    "customerBillNumber": $("[name='ecustomerBillNumber']").val(),
    "refFromLocationId": $("[name='erefFromLocationId']").val(),
    "refToLocationId": $("[name='erefToLocationId']").val(),
    "refVendorRequestId": $("#refVendorRequestId").val(),
    "consignorName": $("#econsignorName option:selected").text(),
    "customerBillDate": $("#ecustomerBillDate").val(),

    "consigneeName": $("#econsigneeName").val(),
    "consigneeVat": $("#econsigneeVat").val(),
    "consigneeContact": $("#econsigneeContact").val()
  };

  if (null !== $("#econsignorName option:selected").val() && '' !== $("#econsignorName option:selected").val()) {
    cashReceipt['vendor'] = {
      id: $("#econsignorName option:selected").val()
    };
  }
  // if(null!=$("#econsigneeId option:selected").val() && 0!=$("#econsigneeId option:selected").val()){
  //     cashReceipt['consignee'] = {
  //         id: $("#econsigneeId option:selected").val()
  //     };
  // }
  let cashReceiptDetails = [];

  $('.ecashReceiptDetailsDiv').each(function () {
    let description = $(this).find($("[name='ecashReceiptDetailsDescription']")).val();
    let quantity = $(this).find($("[name='equantity']")).val();
    let unitOfMeasurement = $(this).find($("[name='eunitOfMeasurement']")).val();
    let noOfPackage = $(this).find($("[name='enoOfPackage']")).val();
    let taxableAmount = $(this).find($("[name='etaxableAmount']")).val();
    let remarks = $(this).find($("[name='eremarks']")).val();

    let cashReceiptDetailsJson = {
      description,
      quantity,
      unitOfMeasurement,
      noOfPackage,
      taxableAmount,
      remarks
    };

    cashReceiptDetails.push(cashReceiptDetailsJson);
  });

  cashReceipt['cashReceiptDetails'] = cashReceiptDetails;
  console.log(cashReceipt);
  return JSON.stringify(cashReceipt);
}

function changeDispatchLocation() {
  $("#refFromLocationId").val($("#dispatchFrom").val()).attr("selected", "selected").change();
}

let consigneeNameOnChangeAction = async () => {
  let consigneeId = $("#consigneeId").val();
  if (consigneeId > 0) {
    let result = await CrudUtils.fetchResource(Constants.API_CUSTOMER_GET_ONE
      .replace("{customerId}", consigneeId));

    if (result.status === 200) {
      const {panNumber, phone, firstName, middleName, lastName} = result.data;
      $("#consigneeContact").val(phone);
      $("#consigneeVat").val(panNumber);
      $("#consigneeName").val(firstName + " " + middleName + " " + lastName);
    }
  }
}

let econsigneeNameOnChangeAction = async () => {
  let consigneeId = $("#econsigneeId").val();
  if (consigneeId > 0) {
    let result = await CrudUtils.fetchResource(Constants.API_CUSTOMER_GET_ONE
      .replace("{customerId}", consigneeId));

    if (result.status === 200) {
      const {panNumber, phone, firstName, middleName, lastName} = result.data;
      $("#econsigneeContact").val(phone);
      $("#econsigneeVat").val(panNumber);
      $("#econsigneeName").val(firstName + " " + middleName + " " + lastName);
    }
  }
}

function hidePodEditModal() {
  $('#editPodModal').modal('hide');
}

window.bookingCodeChangeAction = bookingCodeChangeAction;
window.savePodAction = savePodAction;
window.cloneCashReceiptDetailReq = cloneCashReceiptDetailReq;
window.removeCashReceiptDetailReq = removeCashReceiptDetailReq;
window.assignBtnAction = assignBtnAction;
window.saveExpensesAndAssignmentAction = saveExpensesAndAssignmentAction;
window.printReport = printReport;
window.printAllPodReports = printAllPodReports;
window.printVctsReport = printVctsReport;
window.printGatepassReport = printGatepassReport;
window.loadPodDetailPage = loadPodDetailPage;
window.clickAvailablePodsSection = clickAvailablePodsSection;
window.englishToNepali = englishToNepali;
window.nepaliToEnglish = nepaliToEnglish;
window.saveBookingAction = saveBookingAction;
window.updatePodStatus = updatePodStatus;
window.consignorNameOnChangeAction = consignorNameOnChangeAction;
window.vehicleTypeOnChangeAction = vehicleTypeOnChangeAction;
window.updateDueAmount = updateDueAmount;
window.triggerPodCreationTabClickAction = triggerPodCreationTabClickAction;
window.showPodEditModal = showPodEditModal;
window.sendCashReceiptUpdateReq = sendCashReceiptUpdateReq;
window.econsignorNameOnChangeAction = econsignorNameOnChangeAction;
window.ecloneCashReceiptDetailReq = ecloneCashReceiptDetailReq;
window.eremoveCashReceiptDetailReq = eremoveCashReceiptDetailReq;
window.changeDispatchLocation = changeDispatchLocation;
window.consigneeNameOnChangeAction = consigneeNameOnChangeAction;
window.econsigneeNameOnChangeAction = econsigneeNameOnChangeAction;
window.hidePodEditModal = hidePodEditModal;
