import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";

$(document).ready(function () {
    fillDashboard().then(r => console.log("Dashboard loaded successfully."));
    fillBranchCodeInSelect().then(r => console.log("Branch Code in select loaded successfully."));
    fillCurrentDate();
})

let redirectToDashboard = (token) => {

}
let fillCurrentDate = () => {
    let date = new Date().getDate();
    let year = new Date().getFullYear();
    let month = new Date().getMonth();
    $("#currentDate").html(date + " / " + month + " / " + year);
}
let fillBranchCodeInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_OFFICE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].officeCode) {
                let name = result.data[i].officeCode;

                $("#bookingBranchCode").append("<option value='" + name + "'>" + name + "</option>");
            }
        }
    }
}
let fillDashboard = async () => {
    await fillDataSummaryBlock();
    await fillVendorBookingBarGraphBlock("");
}

let fillDataSummaryBlock = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_DASHBOARD);
    if (result.status === 200) {
        let data = result.data.barGraph;

        $("#vendorsCount").html(data.vendors);
        $("#vehiclesCount").html(data.vehicles);
        $("#driversCount").html(data.drivers);
        $("#activeVehiclesCount").html(data.activeVehicles);
        $("#activeDriversCount").html(data.activeDrivers);
        $("#completedTasksCount").html(data.completedTasks);
        $("#pendingTasksCount").html(data.pendingTasks);
    }
}

let fillVendorBookingBarGraphBlock = async (data) => {
    console.log(data);
    let result = await CrudUtils.fetchResource(Constants.API_DASHBOARD_BOOKINGS.replace("{branchCode}", data));
    if (result.status === 200) {
        let data = result.data;
        let barGraphData = [];

        for (let key in data) {
            let list = [];
            list.push(key, data[key]);
            barGraphData.push(list);
        }

        let plot = $.plot('#vendorBookingBarGraph', [barGraphData], {
            colors: ['#8CC9E8'],
            series: {
                bars: {
                    show: true,
                    barWidth: 0.8,
                    align: 'center'
                }
            },
            xaxis: {
                mode: 'categories',
                tickLength: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                borderColor: 'rgba(0,0,0,0.1)',
                borderWidth: 1,
                labelMargin: 15,
                backgroundColor: 'transparent'
            },
            tooltip: true,
            tooltipOpts: {
                content: '%y',
                shifts: {
                    x: -10,
                    y: 20
                },
                defaultTheme: false
            }
        });
    }
}

let exportExcelReport = () => {
    let reportType = $("#reportType").val();
    let url = Constants.API_EXCEL_REPORT
        .replace("{reportType}", reportType)
        .replace("{reportPeriod}", "ALL")
        .replace("{fromDate}", null)
        .replace("{toDate}", null);
    CrudUtils.downloadExcelFile(url, reportType+'.xls');
}

//todo fix branch code is not passing in API call for bar graph
let reloadVendorBookingSummary = () => {
    let branchCode = $("#bookingBranchCode option:selected").val();
    fillVendorBookingBarGraphBlock(branchCode).then(r => "Vendor Booking Summary reloaded for branch code: " + branchCode);
}

window.exportExcelReport = exportExcelReport;
window.reloadVendorBookingSummary = reloadVendorBookingSummary;