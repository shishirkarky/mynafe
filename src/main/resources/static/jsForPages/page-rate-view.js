import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {

    loadPackagesInSelect().then(r => console.log("Data loaded successfully."));
    loadLocationsToAndFromInSelect().then(r => console.log("Data loaded successfully."));
    loadRates().then(r => console.log("Data loaded successfully."));

    $("#editConfigRatesModal").on("hidden.bs.modal", function () {
        loadRates().then(r => console.log("Data loaded successfully."));
    });
});

// LOAD DATA IN DATATABLE
let loadRates = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_CONFIG_RATES_GET_ALL);
    let packagesTable = $('#ratesDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (packagesTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = packagesTable.rows({selected: true}).data()[0];
                        loadRatesDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },

            {data: "item.code"},
            {data: "fromLocation.city"},
            {data: "toLocation.city"},
            {data: "rate"}
        ],
        "destroy": true
    });
}
// FILLING SELECT OPTIONS
let loadPackagesInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refConfPackageId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}


let loadLocationsToAndFromInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].district && null != result.data[i].city) {
                let id = result.data[i].id;
                let name = result.data[i].city + ", " + result.data[i].district ;

                $("#refConfToLocationId").append("<option value='" + id + "'>" + name + "</option>");
                $("#refConfFromLocationId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
// LOAD ONE DATA IN MODAL
let loadRatesDetailModal = async (id) => {
    let url = Constants.API_CONFIG_RATES_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {rate, refConfPackageId, refConfFromLocationId, refConfToLocationId} = result.data;
        $("[name='rate']").val(rate);
        $("[name='refConfPackageId']").val(refConfPackageId).attr("selected", "selected").change();
        $("[name='refConfFromLocationId']").val(refConfFromLocationId).attr("selected", "selected").change();
        $("[name='refConfToLocationId']").val(refConfToLocationId).attr("selected", "selected").change();
        $("#updateConfigRatesBtn").attr("onClick", "updateRates(" + id + ")");
        // $("#deleteConfigRatesBtn").attr("onClick", "deleteRates(" + id + ")");
        $("#editConfigRatesModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getRatesJsonFromModal = () => {
    const refConfPackageId = $("[name='refConfPackageId']").val();
    const refConfFromLocationId = $("[name='refConfFromLocationId']").val();
    const refConfToLocationId = $("[name='refConfToLocationId']").val();
    const rate = $("[name='rate']").val();
    return JSON.stringify({
        refConfPackageId,
        refConfFromLocationId,
        refConfToLocationId,
        rate
    });
}

let updateRates = (id) => new CrudUtils().sendPutRequest(Constants.API_CONFIG_RATES_UPDATE.replace("{id}", id), getRatesJsonFromModal());

// DELETE ONE DATA
// let deleteRates = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_CONFIG_RATES_DELETE.replace("{id}",id));
//     $("#editConfigRatesModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadRatesDetailModal = loadRatesDetailModal;
window.updateRates = updateRates;
// window.deleteRates = deleteRates;



