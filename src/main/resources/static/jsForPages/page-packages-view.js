import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadPackages().then(r => console.log("Data loaded successfully."));
    loadVendorInSelect().then(r => console.log("Vendors loaded."))
    $("#editConfigPackagesModal").on("hidden.bs.modal", function () {
        loadPackages().then(r => console.log("Data loaded successfully."));
    });
});

/**
 * loads main vendors in vendor select
 * @returns {Promise<void>}
 */
let loadVendorInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

// LOAD DATA IN DATATABLE
let loadPackages = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL);
    let packagesTable = $('#packagesDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (packagesTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = packagesTable.rows({selected: true}).data()[0];
                        loadPackagesDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {
                "data": "Vendor",
                "render": function (data, type, row, meta) {
                    if (null != row.vendor) {
                        return row.vendor.name;
                    } else {
                        return "";
                    }
                },
                defaultContent: ""
            },
            {data: "code", defaultContent: ""},
            {data: "brand", defaultContent: ""},
            {data: "type", defaultContent: ""},
            {data: "name", defaultContent: ""},
            {data: "description", defaultContent: ""}
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadPackagesDetailModal = async (id) => {
    let url = Constants.API_CONFIG_PACKAGES_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, code, brand, type, name, description, refVendorId} = result.data;
        $("[name='code']").val(code);
        $("[name='refVendorId']").val(refVendorId).attr("selected", "selected").change();
        $("[name='brand']").val(brand);
        $("[name='type']").val(type);
        $("[name='name']").val(name);
        $("[name='description']").val(description);
        $("#updateConfigPackagesBtn").attr("onClick", "updatePackages(" + id + ")");
        // $("#deleteConfigPackagesBtn").attr("onClick", "deletePackages(" + id + ")");
        $("#editConfigPackagesModal").modal('show');

    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getPackagesJsonFromModal = () => {
    const code = $("[name='code']").val();
    const brand = $("[name='brand']").val();
    const type = $("[name='type']").val();
    const name = $("[name='name']").val();
    const description = $("[name='packageDescription']").val();
    const refVendorId = $("[name='refVendorId']").val();
    return JSON.stringify({
        code,
        brand,
        type,
        name,
        description,
        refVendorId
    });
}

let updatePackages = (id) => new CrudUtils().sendPutRequest(Constants.API_CONFIG_PACKAGES_UPDATE.replace("{id}", id), getPackagesJsonFromModal());

// DELETE ONE DATA
// let deletePackages = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_CONFIG_PACKAGES_DELETE.replace("{id}",id));
//     $("#editConfigPackagesModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadPackagesDetailModal = loadPackagesDetailModal;
window.updatePackages = updatePackages;
// window.deletePackages = deletePackages;



