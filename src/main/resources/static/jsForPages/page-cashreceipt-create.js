import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {DateUtils} from "./c-date-utils.js";
/*
* EVENT FOR SAVE BUTTON
 */
document.addEventListener('DOMContentLoaded', () => {
    loadLocationsInSelect().then(r => console.log("Locations loaded successfully."));
    loadConfigPackagesInSelect().then(r => console.log("Config Packages loaded successfully."));
    loadBookingCodeInSelect().then(r => console.log("Booking code loaded successfully."));

    document.getElementById('saveCashReceiptBtn').addEventListener('click', sendCashReceiptSaveReq);
    // document.getElementById('saveAndPrintCashReceiptBtn').addEventListener('click', sendAndPrintCashReceiptSaveReq);
    document.getElementById('addCashReceiptDetailsBtn').addEventListener('click', cloneCashReceiptDetailReq);
    document.getElementById('removeCashReceiptDetailsBtn').addEventListener('click', removeCashReceiptDetailReq);
});

/*
* FILLING SELECT OPTIONS
 */
/*

/*
LOCATION IN SELECT
 */
let loadLocationsInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let data = result.data;
        loadLocationInSelect(data).then(r => console.log("Location loaded successfully."));
    }
}
let loadLocationInSelect = async (data) =>{
    let len = data.length;
    for (let i = 0; i < len; i++) {
        if (null != data[i].district) {
            let id = data[i].id;
            let district = data[i].district;
            let city = data[i].city;

            $("#refFromLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
            $("#refToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
        }
    }
}

/*
PACKAGES IN DESCRIPTION SELECT
 */
let loadConfigPackagesInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].brand + " - " +result.data[i].name

                $("#cashReceiptDetailsDescription").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

/*
SAVE FUNCTION
 */
let sendCashReceiptSaveReq = () => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_CASH_RECEIPT_SAVE, getCashReceiptFormData());
};

// let sendAndPrintCashReceiptSaveReq = () => {
//     let savedData = sendCashReceiptSaveReq();
//     console.log(savedData);
//     let id = savedData.id;
//     setTimeout(() => {
//         printReport(id)
//     }, 200);
// }

let printReport = (id) => {
    let url = Constants.API_POD_REPORT.replace("{id}", id);
    window.open(url, "_blank");
}
/*
Load Boooking code in select
 */
let loadBookingCodeInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].bookingCode) {
                let id = result.data[i].id;
                let name = result.data[i].bookingCode ;

                $("#refVendorRequestId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let getCashReceiptFormData = () => {
    let cashReceipt = {
        "vendorInvoiceNumber": $("[name='vendorInvoiceNumber']").val(),
        "consignorName": $("[name='consignorName']").val(),
        "consigneeName": $("[name='consigneeName']").val(),
        "customerBillNumber": $("[name='customerBillNumber']").val(),
        "date": $("[name='date']").val(),
        "dateEn": $("[name='dateEn']").val(),
        "refFromLocationId": $("[name='refFromLocationId']").val(),
        "refToLocationId": $("[name='refToLocationId']").val(),
        "receiverName": $("[name='receiverName']").val(),
        "receivedDate": $("[name='receivedDate']").val(),
        "receivedDateEn": $("[name='receivedDateEn']").val(),
        "refVendorRequestId": $("[name='refVendorRequestId']").val(),
    };

    let cashReceiptDetails = [];

    $('.cashReceiptDetailsDiv').each(function () {
        let description = $(this).find($("[name='cashReceiptDetailsDescription']")).val();
        let quantity = $(this).find($("[name='quantity']")).val();
        let taxableAmount = $(this).find($("[name='taxableAmount']")).val();
        let value = $(this).find($("[name='value']")).val();

        let cashReceiptDetailsJson = {
            description,
            quantity,
            taxableAmount,
            value
        };

        cashReceiptDetails.push(cashReceiptDetailsJson);
    });

    cashReceipt['cashReceiptDetails'] = cashReceiptDetails;

    return JSON.stringify(cashReceipt);
}

/*
CLONE CASH RECEIPT DETAILS
 */
let cloneCashReceiptDetailReq = () => {
    $(".cashReceiptDetailsDiv").eq(0)
        .clone()
        .find("input").val("").end() // ***
        .show()
        .insertAfter(".cashReceiptDetailsDiv:last");
}

let removeCashReceiptDetailReq = () => {
    $(".cashReceiptDetailsDiv:last").remove();
}
/*
DATE CONVERTER
 */
let nepaliToEnglish = (inputId, successId) =>{
    new DateUtils().nepaliToEnglish(inputId, successId);
};

let englishToNepali = (inputId, successId) =>{
    new DateUtils().englishToNepali(inputId, successId);
};

window.englishToNepali = englishToNepali;
window.nepaliToEnglish = nepaliToEnglish;