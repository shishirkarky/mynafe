import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {DateUtils} from "./c-date-utils.js";


document.addEventListener('DOMContentLoaded', () => {
    loadBookingCodeInSelect().then(r => console.log("Bookingcode loaded successfully."));

    document.getElementById('saveVehicleLogBookBtn').addEventListener('click', sendVehicleLogBookSaveReq);
});


/*
Load Boooking code in select
 */
let loadBookingCodeInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].bookingCode) {
                let id = result.data[i].id;
                let name = result.data[i].bookingCode;

                $("#refVendorRequestId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let sendVehicleLogBookSaveReq = (ev) => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_VEHICLE_LOG_BOOK_SAVE, getVehicleLogBookFormData());
};

let getVehicleLogBookFormData = () => {
    return JSON.stringify({
        "booking":{
            "id":$("[name='refVendorRequestId']").val()
        },
        "checkInDate": $("[name='checkInDate']").val(),
        "checkInTime": $("[name='checkInTime']").val(),
        "checkOutDate": $("[name='checkOutDate']").val(),
        "checkOutTime": $("[name='checkOutTime']").val(),
        "inKm": $("[name='inKm']").val(),
        "outKm": $("[name='outKm']").val(),
        "laborName": $("[name='laborName']").val(),
        "numberOfTrip": $("[name='numberOfTrip']").val()
    });
}

let bookingCodeOnChangeAction = async () => {
    let id = $("#refVendorRequestId").val();
    if (null !== id && id !== '') {
        let url = Constants.API_VENDOR_REQUEST_GET_ONE.replace("{id}", id);
        let result = await CrudUtils.fetchResource(url);
        if (result.status === 200) {
            let data = result.data;
            if (null != data.driver) {
                $("#assignedDriver").val(data.driver.firstName + " " + data.driver.middleName + " " + data.driver.lastName);
            }
            if(null!=data.vehicle) {
                $("#assignedVehicle").val(data.vehicle.registrationNumber);
                $("#ownerFullName").val(data.vehicle.ownerFirstName + " " + data.vehicle.ownerMiddleName + " " + data.vehicle.ownerLastName );
            }
        } else {
            $("#assignedDriver").val('');
            $("#assignedVehicle").val('');
            $("#ownerFullName").val('');
        }
    }else{
        $("#assignedDriver").val('');
        $("#assignedVehicle").val('');
        $("#ownerFullName").val('');
    }
}

window.bookingCodeOnChangeAction = bookingCodeOnChangeAction;