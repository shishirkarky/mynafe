import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadOffices().then(r => console.log("Data loaded successfully."));
    loadLocationInSelect().then(r => console.log("locations loaded..."));

    $("#editOfficeModal").on("hidden.bs.modal", function () {
        loadOffices().then(r => console.log("Offices loaded successfully."));
    });
});

let loadLocationInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let data = result.data;
        let len = data.length;
        for (let i = 0; i < len; i++) {
            if (null != data[i].district) {
                let id = data[i].id;
                let district = data[i].district;
                let city = data[i].city;

                $("#refLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
            }
        }
    }
}

// LOAD DATA IN DATATABLE
let loadOffices = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_OFFICE_GET_ALL);
    let officeTable = $('#officeDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (officeTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = officeTable.rows({selected: true}).data()[0];
                        console.log(selectedRowData);
                        loadOfficeDetailModal(selectedRowData.officeCode);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {
                data: "officeCode",
                defaultContent: ""
            },
            {
                data: "type",
                defaultContent: ""
            },
            {
                data: "officeRepresentativeName",
                defaultContent: ""
            },
            {
                "data": "Location",
                "render": function (data, type, row, meta) {
                   if(null!=row.location){
                       return row.location.city + ", " + row.location.district;
                   }else{
                       return "";
                   }
                }
            },
            {
                data: "phoneNumber",
                defaultContent: ""
            },
            {
                data: "email",
                defaultContent: ""
            }
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadOfficeDetailModal = async (officeCode) => {
    let url = Constants.API_OFFICE_GET_ONE.replace("{officecode}", officeCode);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {officeCode, officeRepresentativeName, category, refLocationId, facility, phoneNumber, email, type} = result.data;
        $("[name='officeCode']").val(officeCode);
        $("[name='officeRepresentativeName']").val(officeRepresentativeName);
        $("[name='category']").val(category);
        $("[name='refLocationId']").val(refLocationId).attr("selected", "selected").change();
        $("[name='facility']").val(facility);
        $("[name='phoneNumber']").val(phoneNumber);
        $("[name='email']").val(email);
        $("[name='type']").val(type).attr("selected", "selected").change();
        $("#updateOfficeBtn").attr("onClick", "updateOffice(" + officeCode + ")");
        // $("#deleteOfficeBtn").attr("onClick", "deleteOffice(" + officeCode + ")");
        $("#editOfficeModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getOfficeJsonFromModal = () => {
    const officeCode = $("[name='officeCode']").val();
    const officeRepresentativeName = $("[name='officeRepresentativeName']").val();
    const category = $("[name='category']").val();
    const refLocationId = $("[name='refLocationId']").val();
    const facility = $("[name='facility']").val();
    const phoneNumber = $("[name='phoneNumber']").val();
    const email = $("[name='email']").val();
    const type = $("[name='type']").val();

    return JSON.stringify({
        officeCode,
        officeRepresentativeName,
        category,
        refLocationId,
        facility,
        phoneNumber,
        email,
        type
    });
}

let updateOffice = (officeCode) => new CrudUtils().sendPutRequest(Constants.API_OFFICE_UPDATE.replace("{officecode}", officeCode), getOfficeJsonFromModal());

window.loadOfficeDetailModal = loadOfficeDetailModal;
window.updateOffice = updateOffice;



