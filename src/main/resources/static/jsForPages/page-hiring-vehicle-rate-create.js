import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";

$(document).ready(function () {
  loadVendorInSelect().then(r => console.log("Vendors loaded successfully."));

  loadDataTable().then(r => console.log("Table loaded successfully."));
});
let loadDataTable = async () => {
  let resource = await CrudUtils.fetchResource(Constants.API_HIRING_VEHICLE_RATE_GET_ALL);
  let dataTable = $('#dataTable').DataTable({
    select: {
      style: 'os'
    },
    dom: 'frtip',
    "data": resource.data,
    "columns": [
      {data: "vendor.name"},
      {data: "vehicle.ownerFirstName"},
      {data: "vehicleType"},
      {data: "totalWorkingHours"},
      {data: "totalAmount"},
      {data: "mileage"},
      {data: "fuelRate"},
      {data: "otRate"}

    ],
    "destroy": true
  });
}
/*
 * loads main vendors in vendor select
 * @returns {Promise<void>}
 */
let loadVendorInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      if (null != result.data[i].name) {
        let id = result.data[i].id;
        let name = result.data[i].name;

        $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
      }
    }
  }
}

/**
 * Expenses & Assignment section methods
 */
let vehicleTypeOnChangeAction = async () => {
  if (null !== $("#vehicleType").val() && $("#vehicleType").val() !== '') {
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL_BY_TYPE
      .replace("{vehicleType}", $("#vehicleType").val()));
    if (result.status === 200) {
      let data = result.data;
      let len = data.length;
      if (len === 0) {
        $("#refVehicleId").empty();
        $("#refVehicleId").append("<option value=''>Select Vehicle</option>");
      } else {
        for (let i = 0; i < len; i++) {
          if (null != data[i].registrationNumber) {
            let id = data[i].id;
            let name = data[i].registrationNumber;

            $("#refVehicleId").append("<option value='" + id + "'>" + name + "</option>");
          }
        }
      }
    }
  }
}

let vehicleOnChangeAction = async () => {
  if (null !== $("#refVehicleId").val() && $("#refVehicleId").val() !== 0) {
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ONE
      .replace("{vehicleId}", $("#refVehicleId").val()));
    if (result.status === 200) {
      let data = result.data;
      let ownerName;
      if (null != data.ownerFirstName) {
        ownerName = data.ownerFirstName;
        if (null != data.ownerMiddleName) {
          ownerName = ownerName + " " + data.ownerMiddleName;
        }
        if (null != data.ownerLastName) {
          ownerName = ownerName + " " + data.ownerLastName;
        }
        $("#refVehicleOwnerId").val(ownerName);
      }

    }
  }
}

window.vehicleTypeOnChangeAction = vehicleTypeOnChangeAction;
window.vehicleOnChangeAction = vehicleOnChangeAction;
