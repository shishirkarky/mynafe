import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {DateUtils} from "./c-date-utils.js";

$(document).ready(function () {
    loadDriverInSelect().then(r => console.log("Drivers loaded successfully."));
    loadVendorInSelect().then(r => console.log("Vehicles loaded successfully."));
    loadVehicleInSelect().then(r => console.log("Vehicles loaded successfully."));
    loadLocationsInSelect().then(r => console.log("Locations loaded successfully."));
});

/*
* FILLING SELECT OPTIONS
 */
let loadDriverInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].firstName) {
                let id = result.data[i].id;
                let name = result.data[i].firstName + " " + result.data[i].lastName;

                $("#refDriverId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/*
VEHICLE IN SELECT
 */
let loadVehicleInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].registrationNumber) {
                let id = result.data[i].id;
                let name = result.data[i].registrationNumber;

                $("#refVehicleId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/*
* VENDOR IN SELECT
 */
let loadVendorInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/*
* LOCATION IN FROM AND TO SELECT
 */
let loadLocationsInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let data = result.data;
        loadLocationInSelect(data).then(r => console.log("Location loaded successfully."));
    }
}
let loadLocationInSelect = async (data) =>{
    let len = data.length;
    for (let i = 0; i < len; i++) {
        if (null != data[i].district) {
            let id = data[i].id;
            let district = data[i].district;
            let city = data[i].city;

            $("#refFromLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
            $("#refToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
        }
    }
}
/*
* EVENT FOR SAVE BUTTON
 */
document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveVendorRequestBtn').addEventListener('click', sendVendorRequestSaveReq);
});

let sendVendorRequestSaveReq = (ev) => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_VENDOR_REQUEST_SAVE, getVendorRequestFormData());
};

let getVendorRequestFormData = () => {
    let VendorRequest = {
        "refVendorId": $("[name='refVendorId']").val(),
        "refDriverId": $("[name='refDriverId']").val(),
        "refVehicleId": $("[name='refVehicleId']").val(),
        "refFromLocationId": $("[name='refFromLocationId']").val(),
        "refToLocationId": $("[name='refToLocationId']").val(),
        "dateOfRequest": $("[name='dateOfRequest']").val(),
        "dateOfRequestEn": $("[name='dateOfRequestEn']").val(),
        "dispatchDate": $("[name='dispatchDate']").val(),
        "dispatchDateEn": $("[name='dispatchDateEn']").val(),
        "description": $("[name='vendorRequestDescription']").val(),
        "remarks": $("[name='remarks']").val(),
        "totalAmount": $("[name='totalAmount']").val(),
        "advancePaymentAmount": $("[name='advancePaymentAmount']").val(),
        "fuelAmount": $("[name='fuelAmount']").val(),
        "dhalaAndPaltiExpenses": $("[name='dhalaAndPaltiExpenses']").val(),
        "labourExpenses": $("[name='labourExpenses']").val(),
        "maintainanceAmount": $("[name='maintainanceAmount']").val(),
        "otherExpenses": $("[name='otherExpenses']").val()
    };

    return JSON.stringify(VendorRequest);
}
/*
DATE CONVERTER
 */
let nepaliToEnglish = (inputId, successId) =>{
    new DateUtils().nepaliToEnglish(inputId, successId);
};

let englishToNepali = (inputId, successId) =>{
    new DateUtils().nepaliToEnglish(inputId, successId);
};

window.englishToNepali = englishToNepali;
window.nepaliToEnglish = nepaliToEnglish;