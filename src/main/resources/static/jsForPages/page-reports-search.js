import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";

$(document).ready(function () {
    loadDriverInSelect().then(r => console.log("Drivers loaded successfully."));
    loadVendorInSelect().then(r => console.log("Vendors loaded successfully."));
    fillBranchCodeInSelect().then(r => console.log("Branch Code in select loaded successfully."));
});
let downloadDailyReport = () => {
    let reportType = $("#dailyReportType").val();
    let vendor = $("#vendor").val();
    let driver = $("#driver").val();
    let office = $("#office").val();
    let vehicleType = $("#vehicleType").val();
    callDownloadFileForReport("CUSTOM", reportType, null, null, vendor, driver, office, vehicleType);
}

let downloadCustomReport = () => {
    let reportType = $("#cReportType").val();
    let fromDate = $("#fromDate").val();
    let toDate = $("#toDate").val();
    let vendor = $("#cVendor").val();
    let driver = $("#cDriver").val();
    let office = $("#cOffice").val();
    let vehicleType = $("#cVehicleType").val();
    callDownloadFileForReport("CUSTOM", reportType, fromDate, toDate, vendor, driver, office, vehicleType);
}

let callDownloadFileForReport = (reportPeriod, reportType, fromDate, toDate, vendor, driver, office, vehicleType) => {
    let url = Constants.API_EXCEL_REPORT
        .replace("{reportType}", reportType)
        .replace("{reportPeriod}", reportPeriod)
        .replace("{fromDate}", fromDate)
        .replace("{toDate}", toDate)
        .replace("{vendor}", vendor)
        .replace("{driver}", driver)
        .replace("{office}", office)
        .replace("{vehicleType}", vehicleType);
    CrudUtils.downloadExcelFile(url, reportType+'.xls');
}

let loadDriverInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].firstName) {
                let id = result.data[i].id;
                let name = result.data[i].firstName + " " + result.data[i].lastName;

                $("#daily #driver").append("<option value='" + id + "'>" + name + "</option>");
                $("#custom #cDriver").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let loadVendorInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#daily #vendor").append("<option value='" + id + "'>" + name + "</option>");
                $("#custom #cVendor").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
let fillBranchCodeInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_OFFICE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].officeCode) {
                let id = result.data[i].officeCode;
                let name = result.data[i].officeCode;
                if(result.data[i].location!=null){
                    name = name + " (" + result.data[i].location.city + " , "+result.data[i].location.district+")";
                }

                $("#daily #office").append("<option value='" + id + "'>" + name + "</option>");
                $("#custom #cOffice").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
window.downloadDailyReport = downloadDailyReport;
window.downloadCustomReport = downloadCustomReport;