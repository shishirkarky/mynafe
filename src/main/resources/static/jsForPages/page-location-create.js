import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    loadLocations().then(r => console.log("Data loaded successfully."));
});

let saveLocation = () => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_CONFIG_LOCATIONS_SAVE, getLocationsFormData());
    setTimeout(function () {
        loadLocations().then(r => console.log("Data loaded successfully."));
    }, 500);
};

let getLocationsFormData = () => {
    return JSON.stringify({
        "district": $("[name='district']").val(),
        "city": $("[name='city']").val()
    });
}

// ----------------------------------------------VIEW AND EDIT------------------------------------------------------
let loadLocations = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    let locationsTable = $('#locationsDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: "excel",
                title: "locations"
            },
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (locationsTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = locationsTable.rows({selected: true}).data()[0];
                        loadLocationsDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },

            {data: "district"},
            {data: "city"}
        ],
        "destroy": true
    });
}

let updateLocations = (id) => {
    new CrudUtils().sendPutRequest(Constants.API_CONFIG_LOCATIONS_UPDATE.replace("{id}", id), getLocationsJsonFromModal());
    setTimeout(function () {
        loadLocations().then(r => console.log("Data loaded successfully."));
    }, 500);
}

    let loadLocationsDetailModal = async (id) => {
        let url = Constants.API_CONFIG_LOCATIONS_GET_ONE.replace("{id}", id);
        let result = await CrudUtils.fetchResource(url);
        if (result.status === 200) {
            const {id, district, city} = result.data;
            $("[name='edistrict']").val(district);
            $("[name='ecity']").val(city);
            $("#updateConfigLocationsBtn").attr("onClick", "updateLocations(" + id + ")");
            $("#editConfigLocationsModal").modal('show');
        } else {
            new CToastNotification().getFailureToastNotification("Data not found.");
        }
    }

    let getLocationsJsonFromModal = () => {
        return JSON.stringify({
            "district": $("[name='edistrict']").val(),
            "city": $("[name='ecity']").val()
        });
    }


    window.saveLocation = saveLocation;
    window.loadLocationsDetailModal = loadLocationsDetailModal;
    window.updateLocations = updateLocations;