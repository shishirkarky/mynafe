import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

$(document).ready(function () {
    loadFileUploads().then(r => console.log("Data loaded successfully."));
});

//Load data in datatables
let loadFileUploads = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_DOCUMENTS_GET_ALL_BY_CURRENT_USER);
    let fileUploadTable = $('#fileUploadDataTable').DataTable({
        "data": resource.data,
        "columns": [
            {data: "type"},
            {data: "subType"},
            {data: "refId"},
            {
                data: "DocumentUrl",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                    let a = '<a href="' + row.url + '" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
                    return a;
                }
            }
        ],
        "destroy": true
    });
}