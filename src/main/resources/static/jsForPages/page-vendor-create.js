import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    loadMainVendorsInSelect().then(r => console.log("Vendors loaded successfully."));
    loadVendors().then(r => console.log("Data loaded successfully."));
});
// ---------------------------------------SAVE VENDOR--------------------------------------------------------
let saveVendor = () => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_VENDOR_SAVE, getVendorFormData());
    setTimeout(function () {
        loadVendors().then(r => console.log("Data loaded successfully."));
    }, 500);
};

let loadMainVendorsInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refMainVendorId").append("<option value='" + id + "'>" + name + "</option>");
                $("#erefMainVendorId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let getVendorFormData = () => {
    return JSON.stringify({
        "name": $("[name='name']").val(),
        "email": $("[name='email']").val(),
        "phone": $("[name='phone']").val(),
        "description": $("[name='vendorDescription']").val(),
        "dispatchLocation": $("[name='dispatchLocation']").val(),
        "deliveryMode": $("[name='deliveryMode']").val(),
        "businessType": $("[name='businessType']").val(),
        "panVat": $("[name='panVat']").val(),
        "refMainVendorId": $("[name='refMainVendorId']").val(),
        "rateCalculationType": $("[name='rateCalculationType']").val()
    });
}

// ------------------------------------------------VIEW AND EDIT-------------------------------------------------
let loadVendors = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ALL);
    let vendorTable = $('#vendorDataTable').DataTable({
        scrollX: true,
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: "excel",
                title: "vendors"
            },
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (vendorTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = vendorTable.rows({selected: true}).data()[0];
                        loadVendorDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "name", defaultContent: ''},
            {data: "email", defaultContent: ''},
            {data: "phone", defaultContent: ''},
            {data: "description", defaultContent: ''},
            {data: "dispatchLocation", defaultContent: ''},
            {data: "deliveryMode", defaultContent: ''},
            {data: "businessType", defaultContent: ''},
            {data: "panVat", defaultContent: ''},
            {data: "rateCalculationType", defaultContent: ''}
        ],
        "destroy": true
    });
}

let updateVendor = (id) => {
    new CrudUtils().sendPutRequest(Constants.API_VENDOR_UPDATE.replace("{vendorId}", id), getVendorJsonFromModal());
    setTimeout(function () {
        loadVendors().then(r => console.log("Vendors loaded successfully."));
    },500);
};

let loadVendorDetailModal = async (id) => {
    let url = Constants.API_VENDOR_GET_ONE.replace("{vendorId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, name, email, phone, description, dispatchLocation, deliveryMode, businessType, panVat, refMainVendorId, rateCalculationType} = result.data;
        $("[name='ename']").val(name);
        $("[name='eemail']").val(email);
        $("[name='ephone']").val(phone);
        $("[name='edescription']").val(description);
        $("[name='edispatchLocation']").val(dispatchLocation);
        $("[name='edeliveryMode']").val(deliveryMode);
        $("[name='ebusinessType']").val(businessType);
        $("[name='epanVat']").val(panVat);
        $("[name='erefMainVendorId']").val(refMainVendorId).attr("selected", "selected").change();
        $("[name='erateCalculationType']").val(rateCalculationType).attr("selected", "selected").change();
        $("#updateVendorBtn").attr("onClick", "updateVendor(" + id + ")");
        $("#editVendorModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

let getVendorJsonFromModal = () => {
    const name = $("[name='ename']").val();
    const email = $("[name='eemail']").val();
    const phone = $("[name='ephone']").val();
    const description = $("[name='edescription']").val();
    const dispatchLocation = $("[name='edispatchLocation']").val();
    const deliveryMode = $("[name='edeliveryMode']").val();
    const businessType = $("[name='ebusinessType']").val();
    const panVat = $("[name='epanVat']").val();
    const refMainVendorId = $("[name='erefMainVendorId']").val();
    const rateCalculationType = $("[name='erateCalculationType']").val();
    return JSON.stringify({
        name,
        email,
        phone,
        description,
        dispatchLocation,
        deliveryMode,
        businessType,
        panVat,
        refMainVendorId,
        rateCalculationType
    });
}


window.loadVendorDetailModal = loadVendorDetailModal;
window.updateVendor = updateVendor;
window.saveVendor = saveVendor;