import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadMainVendorsInSelect().then(r => console.log("Vendors loaded successfully."));

    loadVendors().then(r => console.log("Data loaded successfully."));

    $("#editVendorModal").on("hidden.bs.modal", function () {
        loadVendors().then(r => console.log("Vendors loaded successfully."));
    });
});

let loadMainVendorsInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refMainVendorId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

// LOAD DATA IN DATATABLE
let loadVendors = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_VENDOR_GET_ALL);
    let vendorTable = $('#vendorDataTable').DataTable({
        scrollX: true,
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (vendorTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = vendorTable.rows({selected: true}).data()[0];
                        loadVendorDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "name", defaultContent: ''},
            {data: "email", defaultContent: ''},
            {data: "phone", defaultContent: ''},
            {data: "description", defaultContent: ''},
            {data: "dispatchLocation", defaultContent: ''},
            {data: "deliveryMode", defaultContent: ''},
            {data: "businessType", defaultContent: ''},
            {data: "panVat", defaultContent: ''},
            {data: "rateCalculationType", defaultContent: ''}
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadVendorDetailModal = async (id) => {
    let url = Constants.API_VENDOR_GET_ONE.replace("{vendorId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {id, name, email, phone, description, dispatchLocation, deliveryMode, businessType, panVat, refMainVendorId} = result.data;
        $("[name='name']").val(name);
        $("[name='email']").val(email);
        $("[name='phone']").val(phone);
        $("[name='description']").val(description);
        $("[name='dispatchLocation']").val(dispatchLocation);
        $("[name='deliveryMode']").val(deliveryMode);
        $("[name='businessType']").val(businessType);
        $("[name='panVat']").val(panVat);
        $("[name='refMainVendorId']").val(refMainVendorId).attr("selected", "selected").change();
        $("[name='rateCalculationType']").val(rateCalculationType).attr("selected", "selected").change();
        $("#updateVendorBtn").attr("onClick", "updateVendor(" + id + ")");
        // $("#deleteVendorBtn").attr("onClick", "deleteVendor(" + id + ")");
        $("#editVendorModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getVendorJsonFromModal = () => {
    const name = $("[name='name']").val();
    const email = $("[name='email']").val();
    const phone = $("[name='phone']").val();
    const description = $("[name='description']").val();
    const dispatchLocation = $("[name='dispatchLocation']").val();
    const deliveryMode = $("[name='deliveryMode']").val();
    const businessType = $("[name='businessType']").val();
    const panVat = $("[name='panVat']").val();
    const refMainVendorId = $("[name='refMainVendorId']").val();
    const rateCalculationType = $("[name=rateCalculationType]").val();
    return JSON.stringify({
        name,
        email,
        phone,
        description,
        dispatchLocation,
        deliveryMode,
        businessType,
        panVat,
        refMainVendorId,
        rateCalculationType
    });
}

let updateVendor = (id) => new CrudUtils().sendPutRequest(Constants.API_VENDOR_UPDATE.replace("{vendorId}", id), getVendorJsonFromModal());

// DELETE ONE DATA
// let deleteVendor = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_VENDOR_DELETE.replace("{vendorId}",id));
//     $("#editVendorModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadVendorDetailModal = loadVendorDetailModal;
window.updateVendor = updateVendor;
// window.deleteVendor = deleteVendor;



