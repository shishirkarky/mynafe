import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";

$(document).ready(function () {
    loadDriverInSelect().then(r => console.log("Drivers loaded successfully."));
    loadVehicleInSelect().then(r => console.log("Vehicles loaded successfully."));
    loadCashReceiptInSelect().then(r => console.log("Cash Receipts loaded successfully."));
    document.getElementById('searchCashReceiptBtn').addEventListener('click', searchCashReceipt);

});

/*
* FILLING SELECT OPTIONS
 */
let loadDriverInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_DRIVER_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].firstName) {
                let id = result.data[i].id;
                let name = result.data[i].firstName + " " + result.data[i].lastName;

                $("#refDriverId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let loadVehicleInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].registrationNumber) {
                let id = result.data[i].id;
                let name = result.data[i].registrationNumber;

                $("#refVehicleId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

let loadCashReceiptInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CASH_RECEIPT_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].customerBillNumber) {
                let id = result.data[i].id;
                let name = result.data[i].customerBillNumber;

                $("#refCashReceiptId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}

/*
* EVENT FOR SAVE BUTTON
 */
document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveDriverPackagesBtn').addEventListener('click', sendDriverPackagesSaveReq);
});

let sendDriverPackagesSaveReq = (ev) => {
    let crudUtils = new CrudUtils();
    crudUtils.sendPostRequest(Constants.API_DRIVER_PACKAGES_SAVE, getDriverPackagesFormData());
};

let getDriverPackagesFormData = () => {
    let cashReceiptIds = $("[name='refCashReceiptId']").val();
    let finalCashReceiptIds = cashReceiptIds.join(",");

    return JSON.stringify({
        "refDriverId": $("[name='refDriverId']").val(),
        "refCashReceiptId": finalCashReceiptIds,
        "refVehicleId": $("[name='refVehicleId']").val()
    });
}

/*
* Search Cash Receipt
 */
let searchCashReceipt = async () => {
    let customerBillNumber = $("[name='queryCustomerBillNumber']").val();
    let result = await CrudUtils.fetchResource(Constants.API_CASH_RECEIPT_GET_BY_BILL_NUMBER.replace("{customerBillNumber}", customerBillNumber));
}
