import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadDriverPackages().then(r => console.log("Data loaded successfully."));
});

// LOAD DATA IN DATATABLE
let loadDriverPackages = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_DRIVER_PACKAGES_GET_ALL);
    let driverPackagesTable = $('#driverPackagesDataTable').DataTable({
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "driver.firstName"},
            {data: "driver.middleName"},
            {data: "driver.lastName"},
            {data: "status"},
            {data: "dispatchedDate"},
            {data: "customerBillNumber"},
            {data: "fromLocation"},
            {data: "toLocation"}
        ],
        "destroy": true
    });
}