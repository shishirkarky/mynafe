import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";
import {CAuthentication} from "./c-authentication.js";

$(document).ready(function () {
    loadVendorInSelect().then(r => console.log("Vendors loaded successfully."));
    loadLocationsInSelect().then(r => console.log("Locations loaded successfully."));
    loadConfigPackagesInSelect().then(r => console.log("Config Packages loaded successfully."));
});

/*
* LOCATION IN FROM AND TO SELECT
 */
let loadLocationsInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_LOCATIONS_GET_ALL);
    if (result.status === 200) {
        let data = result.data;

        let len = data.length;
        for (let i = 0; i < len; i++) {
            if (null != data[i].district) {
                let id = data[i].id;
                let district = data[i].district;
                let city = data[i].city;

                $("#refConfFromLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
                $("#refConfToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
                $("#editRefConfToLocationId").append("<option value='" + id + "'>" + district + ", " + city + "</option>");
            }
        }
    }
}


/**
 * Filter Tab
 */
/**
 * loads main vendors in vendor select
 * @returns {Promise<void>}
 */
let loadVendorInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_VENDOR_MAIN_VENDORS_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].name;

                $("#refVendorId").append("<option value='" + id + "'>" + name + "</option>");
                $("#consignorName").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
let filterNextBtnAction = () => {
    $("#addRateCardTab").trigger("click");
}

/**
 * Add Rate Configuration Tab
 */
let loadConfigPackagesInSelect = async () => {
    let result = await CrudUtils.fetchResource(Constants.API_CONFIG_PACKAGES_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].name) {
                let id = result.data[i].id;
                let name = result.data[i].brand + " - " + result.data[i].name

                $("#refConfPackageId").append("<option value='" + id + "'>" + name + "</option>");
                $("#editRefConfPackageId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
let saveRateConfigAction = () => {
    let bookingType = $("[name='bookingType']").val();
    let vendorId = $("[name='refVendorId']").val();
    let fromLocationId = $("[name='refConfFromLocationId']").val();
    if (null === vendorId || vendorId === '') {
        new CToastNotification().getFailureToastNotification("Vendor not found.");
    } else if (null === fromLocationId || fromLocationId === '') {
        new CToastNotification().getFailureToastNotification("Dispatch Location not found.");
    } else if (null === bookingType || bookingType === '') {
        new CToastNotification().getFailureToastNotification("Booking Type not found.");
    } else {
        let payload = JSON.stringify({
            "bookingType": $("[name='bookingType']").val(),
            "refVendorId": $("[name='refVendorId']").val(),
            "refConfPackageId": $("[name='refConfPackageId']").val(),
            "refConfFromLocationId": $("[name='refConfFromLocationId']").val(),
            "refConfToLocationId": $("[name='refConfToLocationId']").val(),
            "rate": $("[name='rate']").val()
        });

        new CrudUtils().sendPostRequest(Constants.API_CONFIG_RATES_SAVE, payload);
    }
}
let validateImportRateAction = async () => {
    let vendorId = parseInt($("[name='refVendorId']").val());
    if (vendorId === 0) {
        new CToastNotification().getFailureToastNotification("Please select vendor.");
    } else {
        let data = new FormData();
        data.append("file", $("input[name=file]")[0].files[0]);
        let payload = JSON.stringify({
            "refVendorId": vendorId
        });
        data.append("payload", payload);

        let resource = await new CrudUtils().sendMultiPartPostRequest(Constants.API_CONFIG_RATES_IMPORT_VALIDATE, data);
        if(null!=resource.data && null!=resource.data.errors && resource.data.errors.length>0){

            $('#errorResponse #errorsCount').html(resource.data.errors.length);
            $('#errorResponse #statusCode').html('400');
            $('#errorResponse #msg').html('Some data are invalid. Please review.');
            $('#errorResponse #errors').empty()
            $(resource.data.errors).each(
                function (index, value) {
                    $("#errorResponse #errors").append(
                        $('<span>' + resource.data.errors[index]
                            + '</span><br>'));
                });
            $('#errorResponse').modal('show');
        }
        if (null != resource.data && null!=resource.data.rates) {
            let table = $('#importRateConfigDataTable');
            table.DataTable({
                "data": resource.data.rates,
                "columns": [
                    {data: "item.name", defaultContent: ""},
                    {
                        "data": "toLocation",
                        "render": function (data, type, row, meta) {
                            if (null != row.toLocation) {
                                return row.toLocation.city + ", " + row.toLocation.district;
                            }
                        },
                        defaultContent: ""
                    },
                    {data: "rate", defaultContent: ""}
                ],
                "destroy": true
            });
        }
    }
}
let importRateBtnAction = () => {
    let data = new FormData();
    data.append("file", $("input[name=file]")[0].files[0]);

    let payload = JSON.stringify({
        "bookingType": $("[name='bookingType']").val(),
        "refVendorId": $("[name='refVendorId']").val(),
        "refConfFromLocationId": $("[name='refConfFromLocationId']").val()
    });
    data.append("payload", payload);

    new CrudUtils().sendMultiPartPostRequest(Constants.API_CONFIG_RATES_IMPORT, data);
}
/**
 * Available Rate Config Tab
 */
let loadRates = async () => {
    $('#ratesDataTable').DataTable();

    let bookingType = $("[name='bookingType']").val();
    let vendorId = $("[name='refVendorId']").val();
    let fromLocationId = $("[name='refConfFromLocationId']").val();

    if (null === vendorId || vendorId === '') {
        new CToastNotification().getFailureToastNotification("Vendor not found. Unable to apply filter.");
    } else if (null === fromLocationId || fromLocationId === '') {
        new CToastNotification().getFailureToastNotification("Dispatch Location not found. Unable to apply filter.");
    } else if (null === bookingType || bookingType === '') {
        new CToastNotification().getFailureToastNotification("Booking Type not found. Unable to apply filter.");
    } else {
        CrudUtils.showLoader();
        let resource = await CrudUtils.fetchResource(Constants.API_CONFIG_RATES_GET_ALL_BY_FILTER
            .replace("{bookingType}", bookingType)
            .replace("{vendorId}", vendorId)
            .replace("{fromLocationId}", fromLocationId));
        let packagesTable = $('#ratesDataTable').DataTable({
            select: {
                style: 'os'
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: "excel",
                    title: "Rate Configuration"
                },
                {
                    text: '<i class="fa fa-edit"></i> Edit',
                    className: 'btn btn-info',
                    action: function () {
                        if (packagesTable.rows({selected: true}).count() === 0) {
                            new CToastNotification().getFailureToastNotification("No records selected!");
                        } else {
                            let selectedRowData = packagesTable.rows({selected: true}).data()[0];
                            loadRatesDetailModal(selectedRowData.id);
                        }
                    }
                }
            ],
            "data": resource.data,
            "columns": [
                {
                    data: null,
                    defaultContent: '',
                    className: 'select-checkbox',
                    orderable: false
                },
                {
                    "data": "Item",
                    defaultContent: "",
                    "render": function (data, type, row, meta) {
                        if (null != row.item) {
                            return row.item.code + " - " + row.item.name;
                        }
                    }
                },
                {
                    "data": "toLocation",
                    "render": function (data, type, row, meta) {
                        if (null != row.toLocation) {
                            return row.toLocation.city + ", " + row.toLocation.district;
                        }
                    },
                    defaultContent: ""
                },
                {data: "rate", defaultContent: ""}
            ],
            "destroy": true
        });
        CrudUtils.hideLoader();
    }
}

let loadRatesDetailModal = async (id) => {
    let url = Constants.API_CONFIG_RATES_GET_ONE.replace("{id}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {rate, refConfPackageId, refConfToLocationId} = result.data;
        $("[name='rate']").val(rate);
        $("[name='refConfPackageId']").val(refConfPackageId).attr("selected", "selected").change();
        $("[name='refConfToLocationId']").val(refConfToLocationId).attr("selected", "selected").change();
        $("#updateConfigRatesBtn").attr("onClick", "updateRates(" + id + ")");
        $("#editConfigRatesModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

let updateRates = (id) => {
    let payload = JSON.stringify({
        "bookingType": $("[name='bookingType']").val(),
        "refVendorId": $("[name='refVendorId']").val(),
        "refConfPackageId": $('#editRefConfPackageId').val(),
        "refConfFromLocationId": $("[name='refConfFromLocationId']").val(),
        "refConfToLocationId": $('#editRefConfToLocationId').val(),
        "rate": $('#editRate').val()
    });

    new CrudUtils().sendPutRequest(Constants.API_CONFIG_RATES_UPDATE.replace("{id}", id), payload);

    setTimeout(function () {
        loadRates().then(r => console.log("Rates loaded."));
    }, 200)
}

function clearRateConfigurationBySuperAdmin(){
  if(confirm("Are you sure? This will delete all rate configuration!")){
    new CrudUtils().sendDeleteRequest(Constants.API_CONFIG_RATES_DELETE_ALL);
  }
}

window.saveRateConfigAction = saveRateConfigAction;
window.filterNextBtnAction = filterNextBtnAction;
window.loadRates = loadRates;
window.loadRatesDetailModal = loadRatesDetailModal;
window.updateRates = updateRates;
window.validateImportRateAction = validateImportRateAction;
window.importRateBtnAction = importRateBtnAction;
document.clearRateConfigurationBySuperAdmin = clearRateConfigurationBySuperAdmin;
