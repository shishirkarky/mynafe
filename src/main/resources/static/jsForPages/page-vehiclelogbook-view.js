import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

let datatableId = $("#vehicleLogBookDatatable");
let getOneAPI = (id) => Constants.API_VEHICLE_LOG_BOOK_GET_ONE.replace("{id}", id);
let getAllAPI = Constants.API_VEHICLE_LOG_BOOK_GET_ALL;
let updateOneAPI = (id) => Constants.API_VEHICLE_LOG_BOOK_UPDATE.replace("{id}", id);

$(document).ready(function () {
  loadDataTable().then(r => console.log("Data loaded successfully."));
  loadBookingCodeInSelect().then(r => console.log("Booking-code loaded successfully."))
  $("#editVehicleLogBookModal").on("hidden.bs.modal", function () {
    loadDataTable().then(r => console.log("Data loaded successfully."));
  });
});

/*
Load Boooking code in select
 */
let loadBookingCodeInSelect = async () => {
  let result = await CrudUtils.fetchResource(Constants.API_VENDOR_REQUEST_GET_ALL);
  if (result.status === 200) {
    let len = result.data.length;
    for (let i = 0; i < len; i++) {
      if (null != result.data[i].bookingCode) {
        let id = result.data[i].id;
        let name = result.data[i].bookingCode;

        $("#refVendorRequestId").append("<option value='" + id + "'>" + name + "</option>");
      }
    }
  }
}

// LOAD DATA IN DATATABLE
let loadDataTable = async () => {
  let resource = await CrudUtils.fetchResource(getAllAPI);
  let dataTable = datatableId.DataTable({
    select: {
      style: 'os'
    },
    searchBuilder: {
      columns: [0, 1, 2, 3, 4, 5, 7]
    },
    dom: 'Brtip',
    buttons: [{
      extend: "excel",
      title: "vehicle-log-book"
    }],
    scrollX: true,
    "data": resource.data,
    "columns": [
      {
        data: "booking.dispatchDate", defaultContent: '',
        searchBuilderType: 'string'
      },
      {
        data: "booking.bookingType", defaultContent: '',
        searchBuilderType: 'string'
      },
      {
        data: "booking.bookingCode", defaultContent: '',
        searchBuilderType: 'string'
      },
      {data: "booking.vendor.name", defaultContent: ''},
      {
        "data": "dispatchLocation",
        "defaultContent": "",
        "render": function (data, type, row, meta) {
          if (null != row.booking && null != row.booking.fromLocation) {
            return row.booking.fromLocation.city + ", " + row.booking.fromLocation.district
          } else {
            return "";
          }
        }
      },
      {
        "data": "Driver",
        defaultContent: "",
        "render": function (data, type, row, meta) {
          if (null !== row.booking && null !== row.booking.driver) {
            return row.booking.driver.firstName + " " + row.booking.driver.middleName + " " + row.booking.driver.lastName;
          }
        }

      },
      {data: "booking.vehicle.registrationNumber", defaultContent: ''},
      {
        "data": "Owner",
        defaultContent: "",
        "render": function (data, type, row, meta) {
          if (null !== row.booking && null !== row.booking.vehicle) {
            return row.booking.vehicle.ownerFirstName + " " + row.booking.vehicle.ownerMiddleName + " " + row.booking.vehicle.ownerLastName;
          }
        }

      },
      {data: "checkInTime", defaultContent: ''},
      {data: "checkOutTime", defaultContent: ''},
      {data: "inKm", defaultContent: ''},
      {data: "outKm", defaultContent: ''},
      {data: "outKm", defaultContent: ''},
      {
        "data": "Action",
        "render": function (data, type, row, meta) {
          let a = '<a onclick="loadVehicleLogBookDetailModal(' + row.id + ')">Edit</a>';
          return a;
        }

      },
    ],
    "destroy": true
  });

  dataTable.searchBuilder.container().prependTo(dataTable.table().container());
}

// LOAD ONE DATA IN MODAL
let loadVehicleLogBookDetailModal = async (id) => {
  let result = await CrudUtils.fetchResource(getOneAPI(id));
  if (result.status === 200) {
    const {
      booking, checkInDate, checkOutDate, inKm
      , outKm, laborName, numberOfTrip, checkInTime, checkOutTime
    } = result.data;
    let bookingId = booking.id;
    $("[name='refVendorRequestId']").val(bookingId).attr("selected", "selected").change();
    $("[name='checkInDate']").val(checkInDate);
    $("[name='checkInTime']").val(checkInTime);
    $("[name='checkOutDate']").val(checkOutDate);
    $("[name='checkOutTime']").val(checkOutTime);
    $("[name='inKm']").val(inKm);
    $("[name='outKm']").val(outKm);
    $("[name='laborName']").val(laborName);
    $("[name='numberOfTrip']").val(numberOfTrip);
    $("#updateVehicleLogBookBtn").attr("onClick", "updateVehicleLogBook(" + id + ")");
    $("#deleteVehicleLogBookBtn").attr("onClick", "deleteVehicleLogBook(" + id + ")");
    $("#editVehicleLogBookModal").modal('show');
  } else {
    new CToastNotification().getFailureToastNotification("Data not found.");
  }
}

// UPDATE ONE DATA
let getVehicleLogBookJsonFromModal = () => {
  return JSON.stringify({
    "booking": {
      "id": $("[name='refVendorRequestId']").val()
    },
    "checkInDate": $("[name='checkInDate']").val(),
    "checkInDateEn": $("[name='checkInDateEn']").val(),
    "checkInTime": $("[name='checkInTime']").val(),
    "checkOutDate": $("[name='checkOutDate']").val(),
    "checkOutDateEn": $("[name='checkOutDateEn']").val(),
    "checkOutTime": $("[name='checkOutTime']").val(),
    "inKm": $("[name='inKm']").val(),
    "outKm": $("[name='outKm']").val(),
    "laborName": $("[name='laborName']").val(),
    "numberOfTrip": $("[name='numberOfTrip']").val()
  });

}
let updateVehicleLogBook = (id) => new CrudUtils().sendPutRequest(updateOneAPI(id), getVehicleLogBookJsonFromModal());

window.loadVehicleLogBookDetailModal = loadVehicleLogBookDetailModal;
window.updateVehicleLogBook = updateVehicleLogBook;
