import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


document.addEventListener('DOMContentLoaded', () => {
    loadVehicles().then(r => console.log("vehicles loaded..."));
    loadVehicleInSelect().then(r => console.log("vehicles loaded in select."));
});
/**
 * Vehicle Tab
 */
/**
 * This method loads vehicle in select
 * @returns {Promise<void>}
 */
let loadVehicleInSelect = async () => {
    $('#refVehicleId').find('option').not(':first').remove();
    let result = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL);
    if (result.status === 200) {
        let len = result.data.length;
        for (let i = 0; i < len; i++) {
            if (null != result.data[i].registrationNumber) {
                let id = result.data[i].id;
                let name = result.data[i].registrationNumber;
                $("#refVehicleId").append("<option value='" + id + "'>" + name + "</option>");
            }
        }
    }
}
/**
 * This method will save / update vehicle
 */
let saveVehicle = () => {
    let id = $("#refVehicleId").val();

    if (null !== id && id !== '') {
        new CrudUtils().sendPutRequest(Constants.API_VEHICLE_UPDATE
            .replace("{vehicleId}", id), getVehicleFormData());
    } else {
        new CrudUtils().sendPostRequest(Constants.API_VEHICLE_SAVE, getVehicleFormData());
    }

    setTimeout(function () {
        loadVehicleInSelect().then(r => console.log("vehicles loaded in select."));
        loadVehicles().then(r => console.log("vehicles loaded..."));
    }, 500);
};

/**
 * This method gets JSON from vehicle form
 * @returns {string}
 */
let getVehicleFormData = () => {
    return JSON.stringify({
        "description": $("[name='vehicleDescription']").val(),
        "type": $("[name='type']").val(),
        "registrationNumber": $("[name='registrationNumber']").val(),
        "ownerFirstName": $("[name='ownerFirstName']").val(),
        "ownerMiddleName": $("[name='ownerMiddleName']").val(),
        "ownerLastName": $("[name='ownerLastName']").val(),
        "chasisNumber": $("[name='chasisNumber']").val(),
        "modelNumber": $("[name='modelNumber']").val(),
        "buildYear": $("[name='buildYear']").val(),
        "color": $("[name='color']").val(),
        "currentMileage": $("[name='currentMileage']").val()
    });
}

let loadVehicleById = async () => {
    let id = $("#refVehicleId").val();

    let url = Constants.API_VEHICLE_GET_ONE.replace("{vehicleId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {
            description, type, registrationNumber, ownerFirstName, ownerMiddleName
            , ownerLastName, chasisNumber, modelNumber, buildYear, color, currentMileage
        } = result.data;
        $("[name='vehicleDescription']").val(description);
        $("[name='type']").val(type);
        $("[name='registrationNumber']").val(registrationNumber);
        $("[name='ownerFirstName']").val(ownerFirstName);
        $("[name='ownerMiddleName']").val(ownerMiddleName);
        $("[name='ownerLastName']").val(ownerLastName);
        $("[name='chasisNumber']").val(chasisNumber);
        $("[name='modelNumber']").val(modelNumber);
        $("[name='buildYear']").val(buildYear);
        $("[name='color']").val(color);
        $("[name='currentMileage']").val(currentMileage);
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

/**
 * Documents Tab
 */
/**
 * This method upload vehicle document
 * @param id
 */
let uploadVehicleDocument = () => {
    let id = $("#refVehicleId").val();
    if (null !== id && id !== '') {
        let data = new FormData();
        let json = {
            refId: id,
            type: 'VEHICLE',
            subType: $("[name='vehicleDocumentSubType']").val()
        };
        let jsonData = JSON.stringify(json);
        data.append("file", $("input[name=vehicleFileBytes]")[0].files[0]);
        data.append("jsondata", jsonData);

        new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);

        setTimeout(() => {
            loadVehicleDocuments().then(r => console.log("Driver documents reloaded"));
        }, 1000);
    }else{
        new CToastNotification().getFailureToastNotification("Please select VEHICLE");
    }

}
/**
 * This method load vehicle documents
 * @returns {Promise<void>}
 */
let loadVehicleDocuments = async () => {
    let id = $("#refVehicleId").val();
    if (null !== id && id !== '') {
        let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
            .replace("{type}", "VEHICLE")
            .replace("{refId}", id);
        let resource = await CrudUtils.fetchResource(url);
        $('#vehicleDocumentsDataTable').DataTable({
            scrollY: '15vh',
            "bFilter": false,
            "paging": false,
            "ordering": false,
            "info": false,
            "data": resource.data,
            "columns": [
                {data: "subType"},
                {
                    data: "DocumentUrl",
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                        var a = '<a href="' + row.url + '" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
                        return a;
                    }
                }
            ],
            "destroy": true
        });
    } else {
        $('#vehicleDocumentsDataTable').DataTable().clear().draw();
        new CToastNotification().getFailureToastNotification("Please select VEHICLE");
    }
}
/**
 * This method loads vehicle documents
 */
let documentTabClickAction = () => {
    loadVehicleDocuments().then(r => console.log("Vehicle documents loaded"));
}

let loadVehicles = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL);
    let vehicleTable = $('#vehicleDataTable').DataTable({
        scrollX: true,
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [{
            extend: "excel",
            title: "vehicles"
        }],
        "data": resource.data,
        "columns": [
            {data: "type", defaultContent:""},
            {data: "registrationNumber", defaultContent:""},
            {data: "ownerFirstName", defaultContent:""},
            {data: "ownerMiddleName", defaultContent:""},
            {data: "ownerLastName", defaultContent:""},
            {data: "chasisNumber", defaultContent:""},
            {data: "modelNumber", defaultContent:""},
            {data: "currentMileage", defaultContent:""},
            {data: "buildYear", defaultContent:""},
            {data: "color", defaultContent:""},
            {data: "description", defaultContent:""}
        ],
        "destroy": true
    });
}
window.saveVehicle = saveVehicle;
window.documentTabClickAction = documentTabClickAction;
window.loadVehicleById = loadVehicleById;
window.uploadVehicleDocument = uploadVehicleDocument;