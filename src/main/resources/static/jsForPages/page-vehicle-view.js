import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";


$(document).ready(function () {
    loadVehicles().then(r => console.log("Data loaded successfully."));

    $("#editVehicleModal").on("hidden.bs.modal", function () {
        loadVehicles().then(r => console.log("Data loaded successfully."));
    });
});

// LOAD DATA IN DATATABLE
let loadVehicles = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_VEHICLE_GET_ALL);
    let vehicleTable = $('#vehicleDataTable').DataTable({
        select: {
            style: 'os'
        },
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (vehicleTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else {
                        let selectedRowData = vehicleTable.rows({selected: true}).data()[0];
                        loadVehicleDetailModal(selectedRowData.id);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            {data: "type"},
            {data: "registrationNumber"},
            {data: "ownerFirstName"},
            {data: "ownerLastName"},
            {data: "chasisNumber"},
            {data: "modelNumber"}
        ],
        "destroy": true
    });
}

// LOAD ONE DATA IN MODAL
let loadVehicleDetailModal = async (id) => {
    let url = Constants.API_VEHICLE_GET_ONE.replace("{vehicleId}", id);
    let result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {description, type, registrationNumber, ownerFirstName, ownerMiddleName
            , ownerLastName, chasisNumber, modelNumber, buildYear, color, currentMileage} = result.data;
        $("[name='vehicleDescription']").val(description);
        $("[name='type']").val(type);
        $("[name='registrationNumber']").val(registrationNumber);
        $("[name='ownerFirstName']").val(ownerFirstName);
        $("[name='ownerMiddleName']").val(ownerMiddleName);
        $("[name='ownerLastName']").val(ownerLastName);
        $("[name='chasisNumber']").val(chasisNumber);
        $("[name='modelNumber']").val(modelNumber);
        $("[name='buildYear']").val(buildYear);
        $("[name='color']").val(color);
        $("[name='currentMileage']").val(currentMileage);
        $("#updateVehicleBtn").attr("onClick", "updateVehicle(" + id + ")");
        // $("#deleteVehicleBtn").attr("onClick", "deleteVehicle(" + id + ")");
        $("#uploadVehicleDocumentBtn").attr("onClick", "uploadVehicleDocument(" + id + ")");
        await loadVehicleDocuments(id);
        $("#editVehicleModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("Data not found.");
    }
}

// UPDATE ONE DATA
let getVehicleJsonFromModal = () => {
    return JSON.stringify({
        "description": $("[name='description']").val(),
        "type": $("[name='type']").val(),
        "registrationNumber": $("[name='registrationNumber']").val(),
        "ownerFirstName": $("[name='ownerFirstName']").val(),
        "ownerMiddleName": $("[name='ownerMiddleName']").val(),
        "ownerLastName": $("[name='ownerLastName']").val(),
        "chasisNumber": $("[name='chasisNumber']").val(),
        "modelNumber": $("[name='modelNumber']").val(),
        "buildYear": $("[name='buildYear']").val(),
        "color": $("[name='color']").val(),
        "currentMileage": $("[name='currentMileage']").val()
    });
}


//GET AVAILABLE DOCUMENTS
let loadVehicleDocuments = async (id) => {
    let url = Constants.API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL
        .replace("{type}","VEHICLE")
        .replace("{refId}", id);
    let resource = await CrudUtils.fetchResource(url);
    let vehicleTable = $('#vehicleDocumentsDataTable').DataTable({
        scrollY: '15vh',
        "bFilter" : false,
        "paging":   false,
        "ordering": false,
        "info":     false,
        "data": resource.data,
        "columns": [
            {data: "subType"},
            { data: "DocumentUrl",
                "orderable": false,
                "searchable": false,
                "render": function(data,type,row,meta) { // render event defines the markup of the cell text
                    var a = '<a href="'+row.url+'" target="_blank"><i class="fa fa-download"></i></a>'; // row object contains the row data
                    return a;
                }
            }
        ],
        "destroy": true
    });
}

let updateVehicle = (id) => new CrudUtils().sendPutRequest(Constants.API_VEHICLE_UPDATE.replace("{vehicleId}", id), getVehicleJsonFromModal());

let uploadVehicleDocument = (id) => {
    let data = new FormData();
    let json = {
        refId : id,
        type : 'VEHICLE',
        subType : $("[name='vehicleDocumentSubType']").val()
    };
    let jsonData = JSON.stringify(json);
    data.append("file", $("input[name=vehicleFileBytes]")[0].files[0]);
    data.append("jsondata", jsonData);

    new CrudUtils().sendMultiPartPostRequest(Constants.API_DOCUMENTS_SAVE, data);
    setTimeout(() => {
        loadVehicleDocuments(id).then(r => console.log("Driver documents reloaded"));
    }, 500);

}

// DELETE ONE DATA
// let deleteVehicle = (id) => {
//     new CrudUtils().sendDeleteRequest(Constants.API_VEHICLE_DELETE.replace("{vehicleId}",id));
//     $("#editVehicleModal").modal('hide');
// };

// THIS IS REQUIRED TO MAKE JSP KNOW ABOUT THE AVAILABLE METHODS
window.loadVehicleDetailModal = loadVehicleDetailModal;
window.updateVehicle = updateVehicle;
// window.deleteVehicle = deleteVehicle;
window.uploadVehicleDocument = uploadVehicleDocument;



