import * as Constants from "./c-constants.js";
import {CAuthentication} from "./c-authentication.js";
import * as Util from "./c-utils.js";

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('trackParcel').addEventListener('click', getTrackParcelUI);
});

$( "#loginForm" ).submit(function( event ) {
    event.preventDefault();
    let myform = document.getElementById("loginForm");
    let fd = new FormData(myform );

    new CAuthentication().sendAuthenticationRequest(
        Constants.API_OAUTH_GET,
        fd
    )
});

let getTrackParcelUI = () =>{
  Util.loadPage(Constants.UI_TRACK_PARCEL_PAGE);
}

document.getTrackParcelUI = getTrackParcelUI;
